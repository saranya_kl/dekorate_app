/**
 * Created by vikramaditya on 9/2/15.
 */
import React from 'react';
import Helper from '../../components/helper.js';
import styles from './Modal.scss';
var {withStyles} = Helper;
@withStyles(styles) class Modal extends React.Component {
  constructor() {
    super();
    this.state = {
      isOpen: false
    }
  }
  propTypes:{
    isOpen: React.PropTypes.bool.isRequired,
    onClose: React.PropTypes.func.isRequired,
    modalStyle: React.PropTypes.object,
    overlayStyle: React.PropTypes.object
    };

  componentWillMount() {
    if(this.props.isOpen){
      document.body.style.overflow = 'hidden';
    }
    this.setState({isOpen: this.props.isOpen})
  }
  componentDidMount(){
    this.isVisible = true;
    window.addEventListener('keydown',this.handleKeyEvent.bind(this));
  }
  componentWillUnmount(){
    this.isVisible = false;
    document.body.style.overflow = 'scroll';
    window.removeEventListener('keydown',this.handleKeyEvent.bind(this));
  }
  handleKeyEvent(e){
    let ESC_KEY = 27;
    if(e.keyCode == ESC_KEY){
      document.body.style.overflow = 'scroll';
      this.isVisible && this.props.onClose();
    }
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.isOpen) {
      document.body.style.overflow = 'hidden';
    }
    else {
      document.body.style.overflow = 'scroll';
    }
    this.setState({isOpen: nextProps.isOpen})
  }

  closeModal() {
    document.body.style.overflow = 'scroll';
    this.props.onClose();
    this.setState({isOpen: false});
  }

  render() {
    let open = this.state.isOpen;
    let id = this.props.id ? this.props.id : 'dialog';
    return (
      <div className='Modal' style={this.props.modalStyle}>
        { open && <div id={id} style={this.props.style} className="window">
          {this.props.children}
        </div>
        }
        {open && <div style={this.props.overlayStyle} onClick={this.closeModal.bind(this)} className='mask'></div>}
      </div>
    )
  }
}
export default Modal;
