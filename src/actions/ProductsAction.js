/**
 * Created by vikramaditya on 9/22/15.
 */
import Reflux from 'reflux';
import Constants from '../constants/AppConstants.js';
import ProductActionTypes from '../constants/ProductActionTypes.js';
import ProductsStore from '../stores/ProductsStore.js';
import ProductServices from '../services/ProductServices.js';
import Services from '../services/Services.js';

let ProductsAction = Reflux.createActions([
  'fetchRecentProducts',
  'fetchProducts',
  'onProductChange',
  'onProductUpdate',

  'addProduct',
  'updateProduct',
  'deleteProduct',
  'scrapeProductURL',

  'setProduct',
  'setBulkProduct',
  'setUrlProduct',
  'setOffset',
  'resetStore',
  'onError'
]);

ProductsAction.fetchLockList = [];

ProductsAction.fetchRecentProducts.preEmit = (offset = 0, flag=false) => {
  Services.getAPI(Constants.SERVER_URL + 'getproducts',`?offset=${offset}&isdirty=${flag}`)
    .then((response) => {
      let arr = response.body.data.map((product)=> product.productid);
      ProductsAction.setOffset(response.body.lastid, flag);
      ProductServices.getBulkProducts(arr, flag, []);
    })
    .catch(response => {ProductsAction.onError(response)});
};
ProductsAction.fetchProducts.preEmit = (allIds) => {
  let notInStoreIds = ProductsStore.removeExistingFromList(allIds);
  if (notInStoreIds.length > 0) {
    ProductServices.getBulkProducts(notInStoreIds, false, allIds);
  }else{
    ProductsAction.setBulkProduct([], false, allIds);
  }
};
ProductsAction.onProductChange.preEmit = (id) => {
  if (ProductsStore.checkProduct(id)) {
    ProductsStore._removeProduct(id);
    ProductsAction.fetchProducts([id]);
  }//else{
    //ProductsAction.fetchProducts([id]);
  //}
};
ProductsAction.onProductUpdate.preEmit = (data) => {
  if (ProductsStore.checkProduct(data.productid)) {
    ProductsAction.setProduct(data);
  }
};
ProductsAction.addProduct.preEmit = (product) => {
  delete product.productid;
  Services.postAPI(Constants.SERVER_URL + 'product/new', {data: product})
    .then((response) => {ProductsAction.fetchProducts([response.body.productid])})
    .catch(response => {ProductsAction.onError(response)});
};
ProductsAction.updateProduct.preEmit = (product) => {
  Services.postAPI(Constants.SERVER_URL + 'product/update', {data: product})
    .then(() => {ProductsAction.setProduct(product)})
    .catch(response => {ProductsAction.onError(response)});
};
ProductsAction.deleteProduct.preEmit = (pid) => {
  Services.postAPI(Constants.SERVER_URL + 'product/delete', {data:{id: pid}})
    .then(() => {ProductsStore._removeProduct(id)})
    .catch(response => {ProductsAction.onError(response)});
};
ProductsAction.scrapeProductURL.preEmit = (url) => {
  Services.postAPI(Constants.SERVER_URL + 'scrap', {data:{url: url}})
    .then(response => {ProductsAction.setUrlProduct(response, url)})
    .catch(response => {ProductsAction.onError(response)});
};
export default ProductsAction;
