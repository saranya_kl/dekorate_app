/**
 * Created by vikramaditya on 11/3/15.
 */
import Reflux from 'reflux';
import Services from '../services/Services.js';
import ProjectAssignedUserStore from '../stores/ProjectAssignedUserStore.js';
import Constants from '../constants/AppConstants.js';
const ProjectAssignedUserActions = Reflux.createActions([
  'fetchAssignedUsers',
  'fetchAllBackendUsers',
  'assignedUserChange',
  'setAssignedUsers',
  'setAllBackendUsers',
  'checkAssignedUsers',

  'onError'
]);
ProjectAssignedUserActions.fetchAssignedUsers.preEmit = (pid)=> {
  if(!ProjectAssignedUserStore.checkAssignedUsers(pid)) {
    Services.postAPI(Constants.SERVER_URL + 'assign/getusers', {data: {projectid: pid}})
      .then((response)=> {
        ProjectAssignedUserActions.setAssignedUsers(pid, response)
      });
  }
};
ProjectAssignedUserActions.fetchAllBackendUsers.preEmit = ()=> {
  if(!ProjectAssignedUserStore.hasBackendUsers()) {
    Services.getAPI(Constants.SERVER_URL + 'getallbackendusers')
      .then((response)=> {
        ProjectAssignedUserActions.setAllBackendUsers(response)
      });
  }
};
ProjectAssignedUserActions.assignedUserChange.preEmit = (pid)=>{
  if(ProjectAssignedUserStore.checkAssignedUsers(pid)) {
    ProjectAssignedUserStore._removeAssignedUsers(pid);
    ProjectAssignedUserActions.fetchAssignedUsers(pid);
  }
};
export default ProjectAssignedUserActions;
