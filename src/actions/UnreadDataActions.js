/**
 * Created by vikramaditya on 11/26/15.
 */
import Reflux from 'reflux';
import _ from 'lodash';
import Services from '../services/Services.js';
import Constants from '../constants/AppConstants.js';
const UnreadDataActions = Reflux.createActions([
  'setStatus',
  'setAllDataStatus',
  'markRead',
  'readData',
  'onError'
]);
UnreadDataActions.readData.preEmit = (id) => {
  Services.getAPI(Constants.SERVER_URL + 'read/' + id, '?type=backend')
    .then(()=> {UnreadDataActions.markRead(id)})
    .catch((response)=>UnreadDataActions.onError(response));
};
export default UnreadDataActions;
