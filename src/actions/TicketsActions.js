/**
 * Created by vikramaditya on 10/27/15.
 */
import Reflux from 'reflux';
import TicketsStore from '../stores/TicketsStore.js';
import Services from '../services/Services.js';
import Constants from '../constants/AppConstants.js';
import UnreadDataActions from '../actions/UnreadDataActions';
const TicketsActions = Reflux.createActions([
  'fetchMyProjects',
  'fetchMyClosedProjects',
  'fetchAllProjects',
  'fetchAllClosedProjects',

  'deleteProject',

  'setProject',

  'setAllProjects',
  'setMyProjects',
  'setMyClosedProjects',
  'setAllClosedProjects',

  'onProjectChange',
  'onError'
]);
TicketsActions.fetchAllProjects.preEmit = () => {
  Services.getAPI(Constants.SERVER_URL + 'projects','?limit=true')
    .then(response => {
      UnreadDataActions.setAllDataStatus(response.body.ids);
      TicketsActions.setAllProjects(response);
    })
    .catch((response)=>TicketsActions.onError(response));
};
TicketsActions.fetchMyProjects.preEmit = (userid, role ='') => {
  let post = {data: {backenduserid: userid, role: role}};
  if(role == ''){delete post.data.role;}
  Services.postAPI(Constants.SERVER_URL + 'getbackenduserproject',post)
    .then(response => {TicketsActions.setMyProjects(response)})
};
TicketsActions.fetchMyClosedProjects.preEmit = (userid, role ='') => {
  let post = {
    data : {type: 'me', backenduserid: userid, role: role}
  };
  if(role == ''){
    delete post.data.role;
  }
  Services.postAPI(Constants.SERVER_URL + 'project/closedprojects',post)
    .then(response => {TicketsActions.setMyClosedProjects(response)});
};
TicketsActions.fetchAllClosedProjects.preEmit = (userid, role ='') => {
  let post = {
    data : {type: 'global', backenduserid: userid, role: role}
  };
  if(role == ''){
    delete post.data.role;
  }
  Services.postAPI(Constants.SERVER_URL + 'project/closedprojects',post)
    .then(response => {TicketsActions.setAllClosedProjects(response)});
};
TicketsActions.onProjectChange.preEmit = (pid) => {
  if(TicketsStore.checkProject(pid)){
    //TicketsStore._removeProject(pid);
    Services.getAPI(Constants.SERVER_URL + `project/${pid}`,'?limit=true')
      .then(response => {TicketsActions.setProject(response)});
  }
};

export default TicketsActions;
