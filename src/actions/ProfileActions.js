/**
 * Created by vikramaditya on 10/27/15.
 */
import Reflux from 'reflux';
import Services from '../services/Services.js';
import Constants from '../constants/AppConstants.js';
const ProfileActions = Reflux.createActions([
  'fetchProfile',
  'updateProfile',
  'insertProfile',
  'setProfile',
  'onError'
]);
ProfileActions.fetchProfile.preEmit = (userid) => {
  Services.getAPI(Constants.SERVER_URL + 'designer/' + userid)
    .then(response => {ProfileActions.setProfile(response)})
    .catch(response => ProfileActions.onError(response));
};
ProfileActions.updateProfile.preEmit = (profile) => {
  Services.postAPI(Constants.SERVER_URL + 'designer/update', {data: {data: profile}})
    .then(response => {ProfileActions.setProfile({body: {data: profile}})})
    .catch(response => ProfileActions.onError(response));
};
ProfileActions.insertProfile.preEmit = (profile) => {
  Services.postAPI(Constants.SERVER_URL + 'designer/insert', {data: profile})
    .then(response => {ProfileActions.setProfile({body: {data: profile}})})
    .catch(response => ProfileActions.onError(response));
};
export default ProfileActions;
