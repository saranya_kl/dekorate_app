/**
 * Created by vikramaditya on 10/26/15.
 */
import Reflux from 'reflux';
import TimelineStore from '../stores/TimelineStore.js';
import Services from '../services/Services.js';
import Constants from '../constants/AppConstants.js';
const TimelineActions = Reflux.createActions([
  'fetchTimeLine',
  'getTimeLine',
  'setTimeLine',
  'onError'
]);
TimelineActions.fetchTimeLine.preEmit = (userid,projectid) => {
  let data = {
    data: {
      userid: userid,
      projectid: projectid
    }
  };
  if(!TimelineStore.checkTimeLine(projectid)){
    Services.postAPI(Constants.SERVER_URL + 'timeline',data)
      .then(response => {TimelineActions.setTimeLine(response);})
      .catch(response => {TimelineActions.onError(response)});
  }
};
export default TimelineActions;
