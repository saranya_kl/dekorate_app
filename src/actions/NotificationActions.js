/**
 * Created by vikramaditya on 8/14/15.
 */
import Reflux from 'reflux';
import Constants from '../constants/AppConstants.js';
import Services from '../services/Services.js';
import NotificationTypes from '../constants/NotificationTypes.js';
import ProductsAction from './ProductsAction.js';
import TicketsActions from './TicketsActions.js';
import ProjectsActions from './ProjectsActions.js';
import ProjectAssignedUserActions from './ProjectAssignedUserActions.js';
import DRPActions from './DRPActions.js';
import WorkspaceActions from './WorkspaceActions.js';
import LoginStore from '../stores/LoginStore.js';
import ResourcePermissionConstants from '../constants/ResourcePermissionConstants';
import ResourceConstants from '../constants/ResourceConstants';
let NotificationActions = Reflux.createActions([
  NotificationTypes.FETCH_ALL_NOTIFICATIONS,
  NotificationTypes.READ_NOTIFICATION,
  NotificationTypes.RESET_BADGE_COUNT,
  'setAllNotifications',
  'setBadgeCount',
  'onReadNotification',
  //'onNewCommentPost',
  //'onNewProjectCreate',
  //'onProjectDelete',
  //'onProjectActionSubmit',
  //'onStyleQuizSubmit',
  //'onFeedback',
  'onNewNotification',
  'notify'
]);

NotificationActions.FETCH_ALL_NOTIFICATIONS.preEmit = (userid)=> {
  Services.postAPI(Constants.SERVER_URL+'getnotifications',{data:{userid: userid}})
    .then((response) => {NotificationActions.setAllNotifications(response)});
};
NotificationActions.READ_NOTIFICATION.preEmit = (id)=> {
  Services.postAPI(Constants.SERVER_URL+'notifications/read',{data: {id: id}});
    //.then((response) => {NotificationActions.onReadNotification(response)}); //TODO update store
};
NotificationActions.RESET_BADGE_COUNT.preEmit = (userid)=> {
  Services.postAPI(Constants.SERVER_URL+'notifications/removebadge',{data:{userid: userid}})
    .then((response) => {NotificationActions.setBadgeCount(0)});
};
NotificationActions.onNewNotification.preEmit = (message) => {
  let ROLE = LoginStore.getCurrentDashboard();
  let userID = LoginStore.getUserID();
  if(message.type == NotificationTypes.PRODUCT_UPDATED){
    ProductsAction.onProductChange(message.data.productid);
  }
  if (message.type == NotificationTypes.PROJECT_ASSIGNED) {
    TicketsActions.fetchMyProjects(LoginStore.getUserID(), ROLE);
    ProjectsActions.onProjectChange(message.data.projectid);
    ProjectAssignedUserActions.assignedUserChange(message.data.projectid);
    if(LoginStore.checkPermission(ResourceConstants.Projects_ShowAllProjects)){
      TicketsActions.onProjectChange(message.data.projectid);
    }
  }
  if (message.type == NotificationTypes.PROJECT_NEW && LoginStore.checkPermission(ResourceConstants.Projects_ShowAllProjects)) {
    TicketsActions.fetchAllProjects();
  }
  if(message.type == NotificationTypes.FORCE_RELOAD){
    setTimeout(() => window.location.reload(), 2000);
  }
  if(message.type == NotificationTypes.WORKSPACE_COMMENT || message.type == NotificationTypes.WORKSPACE_SHARED_ASSETS_DELETE || message.type == NotificationTypes.WORKSPACE_SHARED_ASSETS ){
    WorkspaceActions.onWorkSpaceChange(message.data.projectid);
  }
  if (message.type == NotificationTypes.PROJECT_CLOSE) {
    if(LoginStore.checkPermission(ResourceConstants.Projects_ShowAllProjects)){
      TicketsActions.fetchAllProjects();TicketsActions.fetchMyClosedProjects(LoginStore.getUserID(), ROLE); TicketsActions.fetchAllClosedProjects(LoginStore.getUserID(), ROLE);
    }
    else{
      TicketsActions.fetchMyClosedProjects(LoginStore.getUserID(), ROLE);
    }
  }
  if(message.type == NotificationTypes.LOCK_PROJECT){
    ProjectsActions.onProjectChange(message.data.projectid);
    TicketsActions.onProjectChange(message.data.projectid);
  }
  if(message.type != NotificationTypes.PRODUCT_UPDATED){
    NotificationActions.FETCH_ALL_NOTIFICATIONS(userID);
  }
  if(message.type == NotificationTypes.COMMENT || message.type == NotificationTypes.USERFEEDBACK_SUBMITTED){
    DRPActions.onDesignResponseChange(message.data.drid);
  }
  if(message.type == NotificationTypes.COLLABORATOR_ADDED || message.type == NotificationTypes.PROJECT_ACTION || message.type == NotificationTypes.PROJECT_ACTION_SUBMITTED){
    ProjectsActions.onProjectChange(message.data.projectid);
  }
  if(message.type == NotificationTypes.NEW_DESIGN_RESPONSE){
    DRPActions.fetchDesignResponse([message.data.drid]);//Not in Store so fresh fetching.
  }
  //if(message.type == NotificationTypes.PRODUCT_UPDATED){
  //  ProjectActions.GET_STATS();
  //}
};
export default NotificationActions;
