/**
 * Created by vikramaditya on 8/3/15.
 */
import Reflux from 'reflux';
import LoginActionTypes from '../constants/LoginActionTypes.js';
import Constants from '../constants/AppConstants.js';
import Services from '../services/Services.js';

let LoginActions = Reflux.createActions([
  LoginActionTypes.LOGIN_USER,
  LoginActionTypes.REGISTER_USER,
  'setUser',
  'onRegister',
  'setRole',
  'onForceLogin',
  'onLoginError'
]);
LoginActions.LOGIN_USER.preEmit = (type, token, userid='not-required') => {
  Services.loginAPI(Constants.SERVER_URL + 'login',{data:{auth_token:token, userid: userid, type: type}})
    .then(response => {LoginActions.setUser(response);})
    .catch((response) => {console.log(response); LoginActions.onLoginError(response)});
};
LoginActions.REGISTER_USER.preEmit = (token) => {
  Services.postAPI(Constants.SERVER_URL + 'user/register',{data:{auth_token:token}})
    .then(response => {LoginActions.onRegister(response); });
};
export default LoginActions;
