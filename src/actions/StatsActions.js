/**
 * Created by vikramaditya on 11/4/15.
 */
import Reflux from 'reflux';
import Services from '../services/Services';
import Constants from '../constants/AppConstants';
const StatsActions = Reflux.createActions([
  'fetchDesignerStats',
  'fetchProductStats',
  'fetchAllUsersStats',

  'setDesignerStats',
  'setAllUsersStats',
  'setProductStats',

  'onError'

]);
StatsActions.fetchProductStats.preEmit = () => {
  Services.getAPI(Constants.SERVER_URL + 'getmetric')
    .then((response)=> {
      StatsActions.setProductStats(response);
    })
  .catch(error => {StatsActions.onError(error)});
};
StatsActions.fetchDesignerStats.preEmit = () => {
  Services.getAPI(Constants.SERVER_URL + 'getdesignerstat')
    .then((response)=> {
      StatsActions.setDesignerStats(response);
    })
    .catch(error => {StatsActions.onError(error)});
};
StatsActions.fetchAllUsersStats.preEmit = () => {
  Services.getAPI(Constants.SERVER_URL + 'getallusers')
    .then((response)=> {
      StatsActions.setAllUsersStats(response);
    })
    .catch(error => {StatsActions.onError(error)});
};
export default StatsActions;
