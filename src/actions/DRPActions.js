/**
 * Created by vikramaditya on 10/27/15.
 */
import Reflux from 'reflux';
import DRPStore from '../stores/DRPStore.js';
import Services from '../services/Services.js';
import Constants from '../constants/AppConstants.js';
import ProjectsActions from './ProjectsActions.js';
import UnreadDataActions from '../actions/UnreadDataActions';
const DRPActions = Reflux.createActions([
  'fetchDesignResponse',
  'fetchDraftDesignResponse',

  'setDesignResponse',
  'setDraftDesignResponse',

  'sendDesignResponse',
  'saveDesignResponse',

  'updateDesignResponse',

  'deleteDesignResponse',
  'deleteDraftDesignResponse',

  'postComment',
  'deleteComment',

  'onDesignResponseChange',
  'onDesignResponseDraftChange',

  'makeFeedGlobal',
  'onError'
]);
DRPActions.fetchDesignResponse.preEmit = (ids) => {
  let notInStoreIds = DRPStore.removeExistingDRPFromList(ids);
  if(notInStoreIds.length > 0){
    Services.postAPI(Constants.SERVER_URL + 'project/drs', {ids: notInStoreIds})
      .then(response => {
        UnreadDataActions.setAllDataStatus(response.body.ids);
        DRPActions.setDesignResponse(response);
      })
      .catch((response) => DRPActions.onError(response));
  }
};
DRPActions.fetchDraftDesignResponse.preEmit = (ids) => {
  let notInStoreIds = DRPStore.removeExistingDraftFromList(ids);
  if(notInStoreIds.length > 0){
    Services.postAPI(Constants.SERVER_URL + 'project/draftdesignresponse', {data: {ids: notInStoreIds}})
      .then(response => {
        DRPActions.setDraftDesignResponse(response)
      })
      .catch((response) => DRPActions.onError(response));
  }
};
DRPActions.deleteDesignResponse.preEmit = (pid, drid)=> {
  Services.postAPI(Constants.SERVER_URL + 'drs/remove', {data: {drid: drid, projectid: pid}})
    .then(response => {
      DRPStore._removeDesignResponse(drid);
      ProjectsActions.onProjectChange(pid);
    })
    .catch((response) => DRPActions.onError(response));
};
DRPActions.deleteDraftDesignResponse.preEmit = (pid, drid)=> {
  Services.postAPI(Constants.SERVER_URL + 'draftdesignresponse/remove', {data: {projectid: pid, id: drid}})
    .then(response => {
      DRPStore._removeDraftDesignResponse(drid);
      ProjectsActions.onProjectChange(pid);
    })
    .catch((response) => DRPActions.onError(response));
};
DRPActions.updateDesignResponse.preEmit = (data)=> {
  Services.postAPI(Constants.SERVER_URL + 'drs/update', {data: data})
    .then((response)=> {
      DRPActions.onDesignResponseChange(response)
    })
    .catch((response) => DRPActions.onError(response));
};
DRPActions.sendDesignResponse.preEmit = (designResponse, userid, pid, drid = '') => {

  let newDesignResponse = _.cloneDeep(Constants.DESIGN_RESPONSE_OBJECT);
  if (drid) {
    newDesignResponse.data.id = drid;
  }
  newDesignResponse.data.projectid = pid;
  newDesignResponse.data.userid = userid;
  newDesignResponse.data.thumburl = '';
  newDesignResponse.data.attributes = _.cloneDeep(designResponse);

  Services.postAPI(Constants.SERVER_URL + 'drs/new', newDesignResponse)
    .then(response => {
      drid && DRPStore._removeDraftDesignResponse(drid);
      ProjectsActions.onProjectChange(pid);
    })
    .catch((response) => DRPActions.onError(response));
};
DRPActions.saveDesignResponse.preEmit = (designResponse, userid, pid, drid = '') => {
  let newDesignResponse = _.cloneDeep(Constants.DESIGN_RESPONSE_OBJECT);
  if (drid) {
    newDesignResponse.data.id = drid
  }

  newDesignResponse.data.projectid = pid;
  newDesignResponse.data.userid = userid;
  newDesignResponse.data.thumburl = '';
  newDesignResponse.data.attributes = _.cloneDeep(designResponse);
  Services.postAPI(Constants.SERVER_URL + 'drs/save', newDesignResponse)
    .then(response => {
      //fetch Project
      drid ? DRPActions.onDesignResponseDraftChange(drid) : ProjectsActions.onProjectChange(pid);
    })
    .catch((response) => DRPActions.onError(response));

};
DRPActions.postComment.preEmit = (drid, text, userid, imgurl, projectid) => {
  let comment = {
    data: {drid: drid, userid: userid, username: 'Dekorate', text: text, imageUrl: imgurl, projectid: projectid}
  };
  Services.postAPI(Constants.SERVER_URL + 'drs/comment', comment)
    .then(response => {
      DRPActions.onDesignResponseChange(drid);
    })
    .catch((response) => DRPActions.onError(response));

};
DRPActions.deleteComment.preEmit = (projectid, drid, cid) => {
  let comment = {
    data: {
      drid: drid, id: cid, projectid: projectid
    }
  };
  Services.deleteAPI(Constants.SERVER_URL + 'drs/comment', comment)
    .then(response => {
      DRPActions.onDesignResponseChange(drid)
    })
    .catch((response) => DRPActions.onError(response));
};
DRPActions.makeFeedGlobal.preEmit = (drid, location='local') => {
  Services.getAPI(Constants.SERVER_URL + `insertDesignResponsePost/${drid}?location=${location}`)
    .then(() => {alert(`Success \nFeed is now ${location}`)})
    .catch((response) => DRPActions.onError(response));
};
DRPActions.onDesignResponseChange.preEmit = (drid) => {
  if (DRPStore.checkDesignResponse(drid)) {
    DRPStore._removeDesignResponse(drid);
    DRPActions.fetchDesignResponse([drid]);
  }
};
DRPActions.onDesignResponseDraftChange.preEmit = (drid) => {
  if (DRPStore.checkDraftDesignResponse(drid)) {
    DRPStore._removeDraftDesignResponse(drid);
    DRPActions.fetchDraftDesignResponse([drid]);
  }
};
export default DRPActions;
