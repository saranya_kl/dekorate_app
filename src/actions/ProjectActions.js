/**
 * Created by vikramaditya on 7/20/15.
 */

import Reflux from 'reflux';
import Services from '../services/Services.js';
import ProjectActionTypes from '../constants/ProjectActionTypes.js';
import Constants from '../constants/AppConstants.js';
import _ from 'lodash';

let ProjectActions = Reflux.createActions([
  ProjectActionTypes.GET_PROJECT,
  ProjectActionTypes.GET_DESIGN_RESPONSE,
  ProjectActionTypes.GET_DRAFT_DESIGN_RESPONSE,
  ProjectActionTypes.REFRESH_DESIGN_RESPONSE,
  ProjectActionTypes.REFRESH_USER_FEEDBACK,
  ProjectActionTypes.UPDATE_PROJECT,
  ProjectActionTypes.READ_DOCUMENT,
  ProjectActionTypes.UPDATE_DESIGN_RESPONSE,
  ProjectActionTypes.POST_COMMENT,
  ProjectActionTypes.DELETE_COMMENT,
  ProjectActionTypes.POST_DESIGN_RESPONSE,
  ProjectActionTypes.SAVE_DESIGN_RESPONSE,
  ProjectActionTypes.REQUEST_STYLE_QUIZ,
  ProjectActionTypes.CANCEL_USER_ACTION,
  ProjectActionTypes.CANCEL_PROJECT_ACTION,
  ProjectActionTypes.SET_USER_ACTION,
  ProjectActionTypes.SEND_PROJECT_ACTION,
  ProjectActionTypes.UPLOAD_AUDIO,
  ProjectActionTypes.GET_USER_FEEDBACK,
  ProjectActionTypes.UPLOAD_IMAGES,
  ProjectActionTypes.DELETE_DB_PROJECT,
  ProjectActionTypes.DELETE_DESIGN_RESPONSE,
  ProjectActionTypes.DELETE_DESIGN_RESPONSE_DRAFT,
  ProjectActionTypes.LOCK_PROJECT,
  ProjectActionTypes.CLOSE_PROJECT,
  ProjectActionTypes.GET_BACKEND_USERS,
  ProjectActionTypes.GET_ALL_DESIGNERS,
  ProjectActionTypes.GET_PROJECT_BACKEND_USERS,
  ProjectActionTypes.ASSIGN_PROJECT,
  ProjectActionTypes.UN_ASSIGN_PROJECT,
  ProjectActionTypes.UPLOAD_QUIZ,
  ProjectActionTypes.GET_ALL_QUIZ,
  ProjectActionTypes.GET_QUIZ,
  ProjectActionTypes.GET_WORKSPACE_OBJECT,
  ProjectActionTypes.UPLOAD_WORKSPACE_OBJECT,
  ProjectActionTypes.SEND_WORKSPACE_CHAT,
  ProjectActionTypes.DELETE_WORKSPACE_ASSETS,
  ProjectActionTypes.START_PROJECT,
  ProjectActionTypes.GET_STATS,
  ProjectActionTypes.GET_DESIGNER_STATS,
  ProjectActionTypes.DELETE_QUIZ,
  ProjectActionTypes.UPDATE_QUIZ,

  'setProject',
  'setDesignResponse',
  'setComments',
  'onDesignResponsePost',
  'onDesignResponseDraftPost',
  'onAudioUpload',
  'onLockProject',
  'onCloseProject',
  'onDesignResponseDelete',
  'onDesignResponseDraftDelete',
  'onProjectDelete',
  'onStyleQuizSent',
  'onSetUserAction',
  'onProjectActionSend',
  'onProjectActionCancel',
  'setUserFeedBack',
  'setCurrentDesignResponse',
  'setDraftDesignResponse',
  'setDesignResponseImages',
  'resetNewDesignResponse',
  'removeDesignResponseImage',
  'showCurrentFeedBack',
  'setProjectLocally',
  'setFeedBackLocally',
  'onProjectUpdate',
  'onDesignResponseUpdate',
  'downloadJSON',
  'onCommentRead',
  'onDeleteComment',
  'setBackendUsers',
  'setProjectBackendUsers',
  'setAllDesigners',
  'onAssignProject',
  'setAllQuiz',
  'setQuiz',
  'setCurrentWorkSpaceObject',
  'onWorkSpaceUpload',
  'onWorkSpaceChatSend',
  'onWorkSpaceDelete',
  'onProjectStart',
  'refreshDesignResponse',
  'refreshFeedBack',
  'setStatsData',
  'setDesignerStat',
  'deleteQuiz',
  'uploadedQuiz',
  'quizupdated',
  'onError'
]);

ProjectActions.GET_PROJECT.preEmit = (pid) => {
  Services.getAPI(Constants.SERVER_URL + 'project/' + pid)
    .then(response => {ProjectActions.setProject(response)})
    .catch((response) => {ProjectActions.onError(response)});
};
ProjectActions.GET_DESIGN_RESPONSE.preEmit = (ids) => {
  Services.postAPI(Constants.SERVER_URL + 'project/drs', {ids: ids})
    .then(response => {
      ProjectActions.setDesignResponse(response)
    });
};
ProjectActions.GET_DRAFT_DESIGN_RESPONSE.preEmit = (ids) => {
  Services.postAPI(Constants.SERVER_URL + 'project/draftdesignresponse', {data: {ids: ids}})
    .then(response => {
      ProjectActions.setDraftDesignResponse(response)
    });
};
ProjectActions.REFRESH_DESIGN_RESPONSE.preEmit = (ids) => {
  Services.postAPI(Constants.SERVER_URL + 'project/drs', {ids: ids})
    .then(response => {
      ProjectActions.refreshDesignResponse(response)
    });
};

ProjectActions.POST_COMMENT.preEmit = (drid, text, userid, imgurl, projectid) => {
  let comment = {
    data: {
      drid: drid,
      userid: userid,
      username: 'Dekorate',
      text: text,
      imageUrl: imgurl,
      projectid: projectid
    }
  };
  Services.postAPI(Constants.SERVER_URL + 'drs/comment', comment)
    .then(response => {
      ProjectActions.setComments(response, comment.data)
    });
};
ProjectActions.DELETE_COMMENT.preEmit = (projectid, drid, cid, index) => {
  let comment = {
    data: {
      drid: drid,
      id: cid,
      projectid: projectid
    }
  };
  Services.deleteAPI(Constants.SERVER_URL + 'drs/comment', comment)
    .then(response => {
      ProjectActions.onDeleteComment(response, index)
    });
};
ProjectActions.POST_DESIGN_RESPONSE.preEmit = (designResponse, userid, pid, drid = '') => {

  var newDesignResponse = _.cloneDeep(Constants.DESIGN_RESPONSE_OBJECT);
  if (drid != '') {
    newDesignResponse.data.id = drid;
  }
  newDesignResponse.data.projectid = pid;
  newDesignResponse.data.userid = userid;
  newDesignResponse.data.thumburl = '';
  newDesignResponse.data.attributes = _.cloneDeep(designResponse);

  //console.log(newDesignResponse);
  Services.postAPI(Constants.SERVER_URL + 'drs/new', newDesignResponse)
    .then(response => {
      ProjectActions.onDesignResponsePost(response, drid)
    });
};
ProjectActions.SAVE_DESIGN_RESPONSE.preEmit = (designResponse, userid, pid, drid = '') => {
  var newDesignResponse = _.cloneDeep(Constants.DESIGN_RESPONSE_OBJECT);
  if (drid != '') {
    newDesignResponse.data.id = drid;
  }
  newDesignResponse.data.projectid = pid;
  newDesignResponse.data.userid = userid;
  newDesignResponse.data.thumburl = '';
  newDesignResponse.data.attributes = _.cloneDeep(designResponse);
  Services.postAPI(Constants.SERVER_URL + 'drs/save', newDesignResponse)
    .then(response => {
      ProjectActions.onDesignResponseDraftPost(response)
    });
};
ProjectActions.REQUEST_STYLE_QUIZ.preEmit = (userid, pid) => {

  var styleQuiz = _.cloneDeep(Constants.USER_ACTION);
  styleQuiz.data.projectid = pid;
  styleQuiz.data.userid = userid;

  Services.postAPI(Constants.SERVER_URL + 'user/action', styleQuiz)
    .then(response => {
      ProjectActions.onStyleQuizSent(response)
    });
};
ProjectActions.SEND_PROJECT_ACTION.preEmit = (action) => {
  Services.postAPI(Constants.SERVER_URL + 'project/action', action)
    .then(response => {
      ProjectActions.onProjectActionSend(response);
    });
};
ProjectActions.CANCEL_USER_ACTION.preEmit = (actionid, userid) => {
  Services.deleteAPI(Constants.SERVER_URL + 'user/action', {data: {actionid: actionid, userid: userid}})
    .then(response => {
      ProjectActions.onStyleQuizSent(response);
    });
};
ProjectActions.CANCEL_PROJECT_ACTION.preEmit = (actionid, pid) => {
  Services.deleteAPI(Constants.SERVER_URL + 'project/action', {data: {actionid: actionid, projectid: pid}})
    .then(response => {
      ProjectActions.onProjectActionCancel(response);
    });
};
ProjectActions.UPLOAD_AUDIO.preEmit = (file, pid) => {
  Services.uploadAPI(Constants.SERVER_URL + 'upload', file, `?type=DesignResponse&projectid=${pid}`)
    .then(response => {
      ProjectActions.onAudioUpload(response)
    });
};
ProjectActions.GET_USER_FEEDBACK.preEmit = (drid) => {
  Services.getAPI(Constants.SERVER_URL + 'drs/userfeedback/' + drid)
    .then(response => {
      ProjectActions.setUserFeedBack(response, drid)
    });
};
ProjectActions.REFRESH_USER_FEEDBACK.preEmit = (drid) => {
  Services.getAPI(Constants.SERVER_URL + 'drs/userfeedback/' + drid)
    .then(response => {
      ProjectActions.refreshFeedBack(response, drid)
    });
};
ProjectActions.UPLOAD_IMAGES.preEmit = (files, tag, query = '') => {
  files.forEach(file => {
    Services.uploadAPI(Constants.SERVER_URL + 'upload', file, query)
      .then(response => {
        ProjectActions.setDesignResponseImages(response, tag, file.name)
      });
  });
};
ProjectActions.DELETE_DB_PROJECT.preEmit = (pid) => {
  Services.deleteAPI(Constants.SERVER_URL + 'project/' + pid, {data: {projectid: [pid]}})
    .then(response => ProjectActions.onProjectDelete(response));
};
ProjectActions.DELETE_DESIGN_RESPONSE.preEmit = (pid, drid)=> {
  Services.postAPI(Constants.SERVER_URL + 'drs/remove', {data: {drid: drid, projectid: pid}})
    .then(response => ProjectActions.onDesignResponseDelete(drid));
};
ProjectActions.DELETE_DESIGN_RESPONSE_DRAFT.preEmit = (pid, drid)=> {
  Services.postAPI(Constants.SERVER_URL + 'draftdesignresponse/remove', {data: {id: drid, projectid: pid}})
    .then(response => ProjectActions.onDesignResponseDraftDelete(drid));
};
ProjectActions.LOCK_PROJECT.preEmit = (pid, state)=> {
  Services.postAPI(Constants.SERVER_URL + 'project/togglelock', {data: {projectid: pid, toggle: state}})
    .then(()=> {
      ProjectActions.onLockProject(state)
    });
};
ProjectActions.CLOSE_PROJECT.preEmit = (pid, state)=> {
  Services.postAPI(Constants.SERVER_URL + 'project/close', {data: {projectid: pid, toggle: state}})
    .then(()=> {
      ProjectActions.onCloseProject(state)
    });
};
ProjectActions.UPDATE_PROJECT.preEmit = (data) => {
  Services.postAPI(Constants.SERVER_URL + 'project/update', {data: data})
    .then((response)=> {
      ProjectActions.onProjectUpdate(response)
    });
};
ProjectActions.UPDATE_DESIGN_RESPONSE.preEmit = (data) => {
  Services.postAPI(Constants.SERVER_URL + 'drs/update', {data: data})
    .then((response)=> {
      ProjectActions.onDesignResponseUpdate(response)
    });
};
ProjectActions.READ_DOCUMENT.preEmit = (pid, tag, drid) => {
  (tag == 'COMMENT') && ProjectActions.onCommentRead(drid, tag);
  Services.getAPI(Constants.SERVER_URL + 'read/' + pid, '?type=backend');
};
ProjectActions.GET_BACKEND_USERS.preEmit = () => {
  Services.getAPI(Constants.SERVER_URL + 'getallbackendusers')
    .then((response)=> {
      ProjectActions.setBackendUsers(response)
    });
};
ProjectActions.GET_PROJECT_BACKEND_USERS.preEmit = (pid) => {
  Services.postAPI(Constants.SERVER_URL + 'assign/getusers', {data: {projectid: pid}})
    .then((response)=> {
      ProjectActions.setProjectBackendUsers(response)
    });
};
ProjectActions.GET_ALL_DESIGNERS.preEmit = () => {
  Services.getAPI(Constants.SERVER_URL + 'getdesigners')
    .then((response)=> {
      ProjectActions.setAllDesigners(response)
    });
};
ProjectActions.ASSIGN_PROJECT.preEmit = (userid, pid, role) => {
  Services.postAPI(Constants.SERVER_URL + 'assign/project', {data: {backenduserid: userid, projectid: pid, role: role}})
    .then((response)=> {
      ProjectActions.onAssignProject(response, userid)
    });
};
ProjectActions.UN_ASSIGN_PROJECT.preEmit = (userid, pid) => {
  Services.postAPI(Constants.SERVER_URL + 'assign/delete', {data: {backenduserid: userid, projectid: pid}})
    .then((response)=> {
      ProjectActions.onAssignProject(response, userid)
    });
};
ProjectActions.UPLOAD_QUIZ.preEmit = (data) => {
  Services.postAPI(Constants.SERVER_URL + 'quiz/insert', {data: data})
    .then((response)=> {
      console.log(response);
    });
};
ProjectActions.GET_QUIZ.preEmit = (id) => {
  Services.getAPI(Constants.SERVER_URL + 'quiz/' + id)
    .then((response)=> {
      ProjectActions.setQuiz(response);
    });
};
ProjectActions.GET_ALL_QUIZ.preEmit = (userid) => {
  Services.postAPI(Constants.SERVER_URL + 'quiz/ids', {data: {backenduserid: userid}})
    .then((response)=> {
      ProjectActions.setAllQuiz(response);
    });
};
ProjectActions.GET_WORKSPACE_OBJECT.preEmit = (pid) => {
  Services.postAPI(Constants.SERVER_URL + 'getworkspace', {data: {projectid: pid}})
    .then((response)=> {
      ProjectActions.setCurrentWorkSpaceObject(response);
    });
};
ProjectActions.UPLOAD_WORKSPACE_OBJECT.preEmit = (data, query) => {
  Services.postAPI(Constants.SERVER_URL + 'workspace/add', {data: data}, query)
    .then((response)=> {
      ProjectActions.onWorkSpaceUpload(response);
    });
};
ProjectActions.SEND_WORKSPACE_CHAT.preEmit = (data) => {
  Services.postAPI(Constants.SERVER_URL + 'workspace/chat', {data: data})
    .then((response)=> {
      ProjectActions.onWorkSpaceChatSend(response);
    });
};
ProjectActions.DELETE_WORKSPACE_ASSETS.preEmit = (data, query) => {
  Services.postAPI(Constants.SERVER_URL + 'workspace/delete', {data : data}, query)
    .then((response)=> {
      ProjectActions.onWorkSpaceDelete(response);
    });
};

ProjectActions.START_PROJECT.preEmit = (pid, state) => {
  Services.postAPI(Constants.SERVER_URL + 'project/toggleworking', {data: {projectid: pid, toggle: state}})
    .then((response)=> {
      ProjectActions.onProjectStart(response);
    });
};
ProjectActions.GET_STATS.preEmit = () => {
  Services.getAPI(Constants.SERVER_URL + 'getmetric')
    .then((response)=> {
      ProjectActions.setStatsData(response);
    });
};
ProjectActions.GET_DESIGNER_STATS.preEmit = () => {
  Services.getAPI(Constants.SERVER_URL + 'getdesignerstat')
    .then((response)=> {
      ProjectActions.setDesignerStat(response);
    });
};
ProjectActions.DELETE_QUIZ.preEmit = (id) => {
  Services.deleteAPI(Constants.SERVER_URL + 'quiz/'+id)
    .then((response)=> {
      ProjectActions.deleteQuiz(response);
    });
};
ProjectActions.UPLOAD_QUIZ.preEmit = (data,query='') => {
  Services.postAPI(Constants.SERVER_URL + 'quiz/insert'+query, {data: data})
    .then((response)=> {
      ProjectActions.uploadedQuiz(response);
    });
};
ProjectActions.UPDATE_QUIZ.preEmit = (data,query='') => {
  Services.postAPI(Constants.SERVER_URL + 'quiz/update'+query,  {data:data})
    .then((response)=> {
      ProjectActions.quizupdated(response);
    });
};
export default ProjectActions;
