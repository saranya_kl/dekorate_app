/**
 * Created by vikramaditya on 9/23/15.
 */
import Reflux from 'reflux';
import Constants from '../constants/AppConstants.js';
import WorkspaceStore from '../stores/WorkspaceStore.js';
import Services from '../services/Services.js';
import ProductsAction from './ProductsAction.js';

let WorkspaceActions = Reflux.createActions([
  'getWorkSpace',
  'setWorkSpace',
  'fetchWorkSpace',
  'onWorkSpaceChange',

  'addWorkSpaceAsset',
  'onAddWorkSpaceAssetSuccess',

  'deleteWorkspaceAsset',
  'onDeleteWorkspaceAssetSuccess',

  'addChat',
  'onChatAdd',

  'setWorkspaceProductCart',
  'addWorkspaceProductCart',

  'deleteWorkspaceCart',
  'onDeleteWorkspaceCartSuccess',

  'addWorkspaceFiles',
  'onAddWorkspaceFilesSuccess',
  'onError'
]);
WorkspaceActions.fetchWorkSpace.preEmit = (id) => {
  if (!WorkspaceStore.checkItem(id)) {
    Services.postAPI(Constants.SERVER_URL + 'getworkspace', {data: {projectid: id}})
      .then((response) => {
        let data =  response.body.data;
        let productIds = [];
        data['assets']['productcarts'] && data['assets']['productcarts'].map((cart) => {
          cart.list.map((product)=> {
            productIds[product.productid] = '1';
          });
          cart.cancelledlist && cart.cancelledlist.map((product)=> {
            productIds[product.productid] = '1';
          })
        });
        ProductsAction.fetchProducts(Object.keys(productIds));
        WorkspaceActions.setWorkSpace(data);
      })
      .catch((response)=> {WorkspaceActions.onError(response)});
  }
};
WorkspaceActions.onWorkSpaceChange.preEmit = (id) => {
  if (WorkspaceStore.checkItem(id)) {
    WorkspaceStore._removeItem(id);
    WorkspaceActions.fetchWorkSpace(id);
  }
};
WorkspaceActions.deleteWorkspaceAsset.preEmit = (data, query, tag, index) => {
  //For Store update
  WorkspaceActions.onDeleteWorkspaceAssetSuccess(data.projectid, tag, index);
  Services.postAPI(Constants.SERVER_URL + 'workspace/delete', {data: data}, query)
    .then((response)=> {WorkspaceActions.onWorkSpaceChange(data.projectid)})
    .catch((response)=> {WorkspaceActions.onError(response)});
};
WorkspaceActions.addWorkSpaceAsset.preEmit = (data, query, tag) => {
  WorkspaceActions.onAddWorkSpaceAssetSuccess(data.data, data.projectid, tag);
  Services.postAPI(Constants.SERVER_URL + 'workspace/add', {data: data}, query)
    .then((response)=> {WorkspaceActions.onWorkSpaceChange(data.projectid)})
    .catch((response)=> {WorkspaceActions.onError(response)});
};
WorkspaceActions.addChat.preEmit = (data, chat_object) => {
  //For Store update
  WorkspaceActions.onChatAdd(data.projectid, chat_object);
  Services.postAPI(Constants.SERVER_URL + 'workspace/chat', {data: data})
    .then((response)=> {WorkspaceActions.onWorkSpaceChange(data.projectid)})
    .catch((response)=> {WorkspaceActions.onError(response)});
};
WorkspaceActions.addWorkspaceProductCart.preEmit = (pid, index, cart) => {
  //For Store update
  WorkspaceActions.setWorkspaceProductCart(pid, index, cart);
  Services.postAPI(Constants.SERVER_URL + 'workspace/productcarts/product/add', {data: {projectid: pid, name: cart.name, cancelledlist: cart.cancelledlist, requests: cart.requests, productcartid: cart.id, data: cart.list }})
    .then((response)=> {WorkspaceActions.onWorkSpaceChange(pid)})
    .catch((response)=> {WorkspaceActions.onError(response)});
};
WorkspaceActions.deleteWorkspaceCart.preEmit = (pid, index, cartId) => {
  //For Store update
  WorkspaceActions.onDeleteWorkspaceCartSuccess(pid, index);
  Services.postAPI(Constants.SERVER_URL + 'workspace/productcarts/remove', {data: {projectid: pid, productcartid: cartId}})
    .then((response)=> {WorkspaceActions.onWorkSpaceChange(pid)})
    .catch((response)=> {WorkspaceActions.onError(response)});
};
WorkspaceActions.addWorkspaceFiles.preEmit = (pid, urls) => {
  //For Store update
  WorkspaceActions.onAddWorkspaceFilesSuccess(pid, urls);
  Services.postAPI(Constants.SERVER_URL + 'workspace/finalasset', {data: { finalasseturl: urls, projectid: pid}})
    .then((response)=> {WorkspaceActions.onWorkSpaceChange(pid)})
    .catch((response)=> {WorkspaceActions.onError(response)});
};
export default WorkspaceActions;
