/**
 * Created by vikramaditya on 10/27/15.
 */
import Reflux from 'reflux';
import _ from 'lodash';
import ProjectsStore from '../stores/ProjectsStore.js';
import Services from '../services/Services.js';
import TicketsActions from './TicketsActions.js';
import LoginStore from '../stores/LoginStore.js';
import ProjectAssignedUserActions from './ProjectAssignedUserActions';
import Constants from '../constants/AppConstants.js';

const ProjectsActions = Reflux.createActions([
  'fetchProject',
  'getProject',
  'setProject',

  'deleteProject',
  'closeProject',

  'assignProject',
  'unAssignProject',
  'startProject',

  'sendProjectAction',
  'cancelProjectAction',

  'sendUserAction',
  'cancelUserAction',

  'onProjectChange',

  'onError'
]);
ProjectsActions.fetchProject.preEmit = (pid) => {
  if(!ProjectsStore.checkProject(pid)){
    Services.getAPI(Constants.SERVER_URL + 'project/' + pid)
      .then(response => {ProjectsActions.setProject(response)})
      .catch((response) => {ProjectsActions.onError(response)});
  }
};
ProjectsActions.deleteProject.preEmit = (pid) => {
  Services.deleteAPI(Constants.SERVER_URL + 'project/' + pid, {data: {projectid: [pid]}})
    .then(() => {
      TicketsActions.deleteProject(pid);
    })
    .catch((response) => {ProjectsActions.onError(response)});
};
ProjectsActions.closeProject.preEmit = (pid, state)=> {
  Services.postAPI(Constants.SERVER_URL + 'project/close', {data: {projectid: pid, toggle: state}})
    .then(()=> {
      const uid = LoginStore.getUserID();
      const role = LoginStore.getCurrentDashboard();
      ProjectsActions.onProjectChange(pid);
      TicketsActions.deleteProject(pid);
      TicketsActions.fetchMyProjects(uid, role);
      TicketsActions.fetchMyClosedProjects(uid, role);
      TicketsActions.fetchAllClosedProjects(uid, role);
    })
    .catch((response) => {ProjectsActions.onError(response)});
};
ProjectsActions.assignProject.preEmit = (userid, pid, role) => {
  Services.postAPI(Constants.SERVER_URL + 'assign/project', {data: {backenduserid: userid, projectid: pid, role: role}})
    .then(()=> {ProjectsActions.onProjectChange(pid);TicketsActions.onProjectChange(pid); ProjectAssignedUserActions.assignedUserChange(pid)})
    .catch((response) => {ProjectsActions.onError(response)});
};
ProjectsActions.unAssignProject.preEmit = (userid, pid) => {
  Services.postAPI(Constants.SERVER_URL + 'assign/delete', {data: {backenduserid: userid, projectid: pid}})
    .then(()=> {ProjectsActions.onProjectChange(pid);TicketsActions.onProjectChange(pid); ProjectAssignedUserActions.assignedUserChange(pid)})
    .catch((response) => {ProjectsActions.onError(response)});
};
ProjectsActions.sendProjectAction.preEmit = (action) => {
  let pid = action.data.projectid;
  Services.postAPI(Constants.SERVER_URL + 'project/action', action)
    .then(()=> {ProjectsActions.onProjectChange(pid)})
    .catch((response) => {ProjectsActions.onError(response)});
};
ProjectsActions.cancelProjectAction.preEmit = (actionId, pid) => {
  Services.deleteAPI(Constants.SERVER_URL + 'project/action', {data: {actionid: actionId, projectid: pid}})
    .then(() => {ProjectsActions.onProjectChange(pid)})
    .catch((response) => {ProjectsActions.onError(response)});
};
ProjectsActions.sendUserAction.preEmit = (uid, pid) => {
  let styleQuiz = _.cloneDeep(Constants.USER_ACTION);
  styleQuiz.data.projectid = pid;
  styleQuiz.data.userid = uid;
  Services.postAPI(Constants.SERVER_URL + 'user/action', styleQuiz)
    .then(() => {ProjectsActions.onProjectChange(pid)})
    .catch((response) => {ProjectsActions.onError(response)});
};
ProjectsActions.cancelUserAction.preEmit = (actionid, userid) => {
  Services.deleteAPI(Constants.SERVER_URL + 'user/action', {data: {actionid: actionid, userid: userid}})
    .then(() => {ProjectsActions.onProjectChange(pid)})
    .catch((response) => ProjectsActions.onError(response));
};
ProjectsActions.startProject.preEmit = (pid, state) => {
  Services.postAPI(Constants.SERVER_URL + 'project/toggleworking', {data: {projectid: pid, toggle: state}})
    .then(()=> {ProjectsActions.onProjectChange(pid);TicketsActions.onProjectChange(pid)})
    .catch((response) => {ProjectsActions.onError(response)});
};
ProjectsActions.onProjectChange.preEmit = (pid) => {
  if(ProjectsStore.checkProject(pid)){
    ProjectsStore._removeProject(pid);
    ProjectsActions.fetchProject(pid);
  }
};
export default ProjectsActions;
