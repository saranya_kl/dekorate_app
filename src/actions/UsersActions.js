/**
 * Created by vikramaditya on 10/26/15.
 */
import Reflux from 'reflux';
import UsersStore from '../stores/UsersStore.js';
import Services from '../services/Services.js';
import Constants from '../constants/AppConstants.js';
const UsersActions = Reflux.createActions([
  'fetchUserDetails',
  'fetchUsers',
  'fetchDesigners',
  'setUser',
  'setDesigners',
  'onUserChange'
]);
UsersActions.fetchUserDetails.preEmit = (userid) => {
  if(!UsersStore.checkUser(userid)){
    Services.getAPI(Constants.SERVER_URL + 'user/' + userid)
      .then(response => {UsersActions.setUser(response); });
  }
};
UsersActions.fetchUsers.preEmit = (ids) => { //Not in use
  //let filtered = UsersStore.removeExistingFromList(ids);
  Services.postAPI(Constants.SERVER_URL + 'getusers',{data: {ids: ids}})
    .then(response => {UsersActions.setUsers(response)})
};
UsersActions.fetchDesigners.preEmit = () => {
  if(!UsersStore.hasDesigners()) {
    Services.getAPI(Constants.SERVER_URL + 'getdesigners')
      .then(response => {
        UsersActions.setDesigners(response)
      })
  }
};
UsersActions.onUserChange.preEmit = (userid) => {
  if (UsersStore.checkUser(userid)) {
    UsersStore._removeUser(userid);
    UsersActions.fetchUserDetails(userid);
  }
};
export default UsersActions;
