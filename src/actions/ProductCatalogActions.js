/**
 * Created by vikramaditya on 9/22/15.
 */
import Reflux from 'reflux';
import Services from '../services/Services.js';
import Constants from '../constants/AppConstants.js';

const SEARCH_URL = 'productsearch';
const PRODUCT_ID_URL = 'getallproductsids';

const ProductCatalogActions = Reflux.createActions([

  'getSearchResults',
  'getESearchResults',
  'getAllProductIds',
  'setList',
  'setSearchResults',
  'onError'
]);


ProductCatalogActions.getSearchResults.preEmit = (data) => {
  let _query = data.query, modelled = data.modelled || false;
  Services.postAPI(Constants.SERVER_URL + SEARCH_URL, {data: {search: _query, ismodel: modelled}})
    .then(response => ProductCatalogActions.setSearchResults(response.body.data,_query))
    .catch(response => {
      console.log(response);
      ProductCatalogActions.onError();
    })
};

ProductCatalogActions.getESearchResults.preEmit = (term) => {
  Services.postAPI(Constants.SERVER_URL + 'product/search',{data:{search: term}})
    .then(response => {ProductCatalogActions.setSearchResults(response.body.data,'')})
    .catch(response => {ProductCatalogActions.onError(response)});
};

ProductCatalogActions.getAllProductIds.preEmit = () => {
  Services.getAPI(Constants.SERVER_URL + PRODUCT_ID_URL)
    .then(response => ProductCatalogActions.setSearchResults(response.body.data))
    .catch(response => {
      console.log(response);
      ProductCatalogActions.onError();
    })
};


export default ProductCatalogActions;
