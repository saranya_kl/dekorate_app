/**
 * Created by vikramaditya on 10/26/15.
 */
import Reflux from 'reflux';
import TimelineActions from '../actions/TimelineActions.js';
import Constants from '../constants/AppConstants.js';
let ITEM_LOADING = Constants.ITEM_LOADING;

export default Reflux.createStore({
  listenables: [TimelineActions],
  timeLine: new Map(),
  getTimeLine(pid){
    let that = this;
    if (that.timeLine.get(pid)) {
      return that.timeLine.get(pid);
    }
    else {
      return ITEM_LOADING;
    }
  },
  setTimeLine(response){
    let that = this, timeLine = response.body.data;
    that.timeLine.set(timeLine.projectid, timeLine);
    that.trigger({timeline: timeLine});
  },
  checkTimeLine(pid){
    let that = this;
    return that.timeLine.has(pid);
  },
  onError(err){
    let that = this;
    that.trigger({error: err});
  },
  _removeTimeLine(id){
    let that = this;
    that.timeLine.delete(id);
  },
  _removeAllTimeLine(){
    let that = this;
    that.timeLine.clear();
  }
})
