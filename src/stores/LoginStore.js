/**
 * Created by vikramaditya on 7/28/15.
 */
import Reflux from 'reflux';
import Actions from '../actions/LoginActions.js';
import Constants from '../constants/AppConstants.js';
import AuthConstants from '../constants/AuthConstants.js'
import TriggerTypes from '../constants/TriggerTypes.js';
import RouteCategories from '../constants/RouteCategories.js';
import Roles from '../constants/Roles.js';
import jwt from 'jsonwebtoken';
import ResourceConstants from '../constants/ResourceConstants.js'
import ResourcePermissionConstants from '../constants/ResourcePermissionConstants.js'
import _ from 'lodash';
export default Reflux.createStore({
  listenables: [Actions],

  _jwt: null,
  _userId: null,
  _isLoggedIn: false,
  _currentDashboard: null,

  loginState: '',
  user: {
    access_token: '',
    roles: [],
    userid: '',
    email: '',
    current_role: '',
    name: '',
    imageUrl: ''
  },

  _autoLogin(){
    let _jwt = localStorage.getItem('deko_jwt');
    if (_jwt) {
      // TODO Check expiration
      this._jwt = _jwt;
      this._userId = jwt.decode(_jwt).id;
      this._isLoggedIn = true;
      console.log('&*&*&* auto login success');
    }
    else {
      console.log('&*&*&* auto login failed');
    }
  },

  _loginUser(token){
    let that = this;
    // TODO Check expiration
    let _data = jwt.decode(token);

    //Set store state
    that._jwt = token;
    that._userId = _data.id;
    that._isLoggedIn = true;
    that._roles = _data.roles.map(obj => obj.role);
    that._dashboards = new Set();
    that._pathRegx = [];
    that._roles.map((role) => {
      let dashboards = AuthConstants.Role_Dashboard[role];
      dashboards && dashboards.map((d) => {
        that._dashboards.add(d)
      });
    });
    that._dashboards.forEach((d) => {
      that._pathRegx.push(new RegExp(`^\/${d}`, 'gi'));
    });

    //Set into local storage
    localStorage.setItem('deko_jwt', token);
  },
  testPath(path){
    let that = this;
    let dashboard = /^\/dashboard/;
    let found = dashboard.test(path);
    that._pathRegx.map((reg) => {
      if (reg.test(path)) {
        found = true;
      }
    });
    return found;
  },
  setUser(response){
    let that = this;
    let resp = response.body;

    that.loginState = 'signed_in';
    that.user.access_token = resp.access_token;
    that.user.roles = resp.roles;
    that.user.userid = resp.id;
    that.user.imageUrl = resp.imageUrl;
    that.user.name = `${resp.firstname} ${resp.lastname}`;
    if ((resp.authorize != undefined) && !resp.authorize) {
      that.trigger(TriggerTypes.UNAUTHORIZED);
    } else {

      that._loginUser(that.user.access_token);
      that.trigger(TriggerTypes.LOGIN_SUCCESS);
    }
  },
  onLoginError(resp){
    this.loginError = resp;
    this.trigger(TriggerTypes.LOGIN_ERROR);
  },
  getUserName(){
    return this.user.name;
  },
  onForceLogout(){
    let that = this;
    that._isLoggedIn = false;
    localStorage.clear('deko_jwt');
    that._dashboards && that._dashboards.clear();
    that.trigger(TriggerTypes.LOGOUT);// Fire logged out
  },
  setDashboard(dashboard){
    this._currentDashboard = dashboard;
    this.trigger(TriggerTypes.CHANGE_ROUTE);
  },
  setDashboardVal(dashboard){
    // Only to set the dashboard value - not firing a change
    this._currentDashboard = dashboard;
  },
  getUserID(){
    return this._userId;
  },
  getCurrentDashboard(){
    return this._currentDashboard;
  },
  getLoginState(){
    return this.loginState;
  },
  logoutUser(){
    let that = this;
    window.gapi && window.gapi.auth2 && window.gapi.auth2.getAuthInstance().signOut();// google signout
    that.loginState = '';
    that._isLoggedIn = false;
    localStorage.clear('deko_jwt');
    that._dashboards && that._dashboards.clear();
    that.trigger(TriggerTypes.LOGOUT);// Fire logged out

  },
  onRegister(response){
    console.log('user registered', response);
  },
  getUserInfo(){
    let imageUrl = this.user.imageUrl;
    let name = this.user.name;
    return {imageUrl, name};
  },
  getJWT(){
    return this._jwt;
  },
  isLoggedIn(){
    //if(!this._jwt) this._autoLogin();
    return this._isLoggedIn;
  },

  checkPermission(resourceName){
    let that = this;
    let returnValue = false;

    if (_.intersection(ResourcePermissionConstants[resourceName], that._roles).length) {
      returnValue = true;
    }
    //console.log('Checking ', resourceName, ' for ', that._roles, ' against ', ResourcePermissionConstants[resourceName], ' : ', returnValue);
    return returnValue;
  },
  checkRole(role){
    let that = this;
    let returnValue = false;
    if (_.intersection(role, that._roles).length){
      returnValue = true;
    }
    return returnValue;
  },
  getDashboards(){
    let that = this;
    return that._dashboards;
  }
});
