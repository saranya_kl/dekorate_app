/**
 * Created by vikramaditya on 9/9/15.
 */
import Reflux from 'reflux';
import WorkspaceActions from '../actions/WorkspaceActions.js';
import TriggerTypes from '../constants/TriggerTypes.js';
import Constants from '../constants/AppConstants.js';

let ITEM_LOADING = Constants.ITEM_LOADING;
export default Reflux.createStore({
  listenables: [WorkspaceActions],
  items: new Map(),
  getWorkSpace(id){
    let that = this;
    if (that.checkItem(id)) {
      return that.items.get(id);
    }
    else {
      return ITEM_LOADING;
    }
  },
  setWorkSpace(item){
    let that = this;
    that.items.set(item.projectid, item);
    that.trigger();
  },

  onAddWorkSpaceAssetSuccess(new_asset, pid, type){
    let that = this;
    let data = that.items.get(pid);
    data['assets'][type].unshift(new_asset);
    that.items.set(pid, data);
  },
  onDeleteWorkspaceAssetSuccess(pid, type, index){
    let that = this;
    let data = that.items.get(pid);
    data['assets'][type].splice(index, 1);
    that.items.set(pid, data);
  },
  onChatAdd(pid, chat){
    let that = this;
    let data = that.items.get(pid);
    data['chat']['msgs'].unshift(chat);
    that.items.set(pid, data);
  },
  getWorkspaceProductCart(pid, index){
    let that = this;
    let data = that.items.get(pid);
    return (data['assets']['productcarts'][index] || {});
  },
  setWorkspaceProductCart(pid, index, cart){
    let that = this;
    that.items.get(pid)['assets']['productcarts'][index] = cart;
  },
  onDeleteWorkspaceCartSuccess(pid, index){
    let that = this;
    that.items.get(pid)['assets']['productcarts'].splice(index, 1);
  },
  onAddWorkspaceFilesSuccess(pid, urls){
    let that = this;
    that.items.get(pid)['assets']['finalasset'] = urls;
  },
  onError(err){

  },
  getProductListIds(id){
    let that = this;
    let arr = {};
    that.items.get(id)['assets']['productcarts'].map((cart) => {
      cart.list.map((product)=> {
        arr[product.productid] = '1';
      })
    });
    return Object.keys(arr);
  },
  resetStore(){
    let that = this;
    that._removeAllItems();
  },
  checkItem(id){
    let that = this;
    return that.items.has(id);
  },
  _removeItem(id){
    let that = this;
    that.items.delete(id);
  },
  _removeAllItems(){
    let that = this;
    that.items.clear();
  }
  /////////////////////////////////////////////////
});
