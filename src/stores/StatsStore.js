/**
 * Created by vikramaditya on 11/4/15.
 */
import Reflux from 'reflux';
import StatsActions from '../actions/StatsActions';

export default Reflux.createStore({
  listenables: [StatsActions],
  designer_stats: {},
  user_stats: {},
  products_stats: {},
  setAllUsersStats(response){
    let that = this;
    that.user_stats = response.body;
    that.trigger({userStats: that.user_stats});
  },
  setDesignerStats(response){
    let that = this;
    that.designer_stats = response.body;
    that.trigger({designerStats: that.designer_stats});
  },
  setProductStats(response){
    let that = this;
    that.products_stats = response.body;
    that.trigger({productStats: that.products_stats});
  },
  getAllUsersStats(){
    return this.user_stats;
  },
  getDesignerStats(){
    return this.designer_stats;
  },
  getProductStats(){
    return this.products_stats;
  },
  onError(err){
    this.trigger({error: err});
  }
})
