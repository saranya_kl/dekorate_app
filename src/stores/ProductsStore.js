/**
 * Created by vikramaditya on 9/22/15.
 */
import Reflux from 'reflux';
import Services from '../services/ProductServices.js';
import Constants from '../constants/AppConstants.js';
import ProductConstants from '../constants/ProductConstants.js';
import Actions from '../actions/ProductsAction.js';
import _ from 'lodash';
let ITEM_LOADING = Constants.ITEM_LOADING;
export default Reflux.createStore({
  listenables: [Actions],
  items: new Map(),
  dirtyProducts: new Map(),
  nonDirtyProducts: new Map(),
  searchResults: [],
  offsetId: {dirty: '0', non_dirty: '0'},
  urlFetchedProducts: new Map(),
  // Called from Components
  getProduct(id){
    let that = this;
    if (that.items.has(id)) {
      return that.items.get(id);
    }
    else {
      return ITEM_LOADING;
    }
  },

  // Called from Actions
  setProduct(item){
    let that = this;
    that.items.set(item.productid, item);
    if(item.isdirty){
      that.dirtyProducts.set(item.productid, item);
      that.nonDirtyProducts.delete(item.productid);
    }else{
      that.nonDirtyProducts.set(item.productid, item);
      that.dirtyProducts.delete(item.productid);
    }
    that.trigger({update: item, isDirty: item.isdirty});
  },
  setUrlProduct(response, url){
    let that = this;
    let product = _.cloneDeep(ProductConstants.DUMMY_PRODUCT);
    let online_product = response.body;

    product = _.assign(product, {name: online_product.title}, {amount: online_product.amount},
      {url: online_product.url}, {originproductid: online_product.originproductid}, {brand: online_product.brand},
      {marketplace: online_product.marketplace}, {colour: online_product.colour}, {material: online_product.material});
    product.assets.image = online_product.image;
    product.displayImage.url = online_product.image[0];

    //TODO commenting for now to allow re-fetching
    //that.urlFetchedProducts.set(url, product);
    that.trigger({fetched: product});
  },
  setOffset(id, is_dirty){
    let that = this;
    is_dirty ? that.offsetId.dirty = id : that.offsetId.non_dirty = id;
  },
  getUrlFetchedProduct(url){
    let that = this;
    if (that.urlFetchedProducts.has(url)) {
      return that.urlFetchedProducts.get(url);
    }
    else {
      return ITEM_LOADING;
    }
  },
  getOffset(is_dirty){
    let that = this;
    return is_dirty ? that.offsetId.dirty : that.offsetId.non_dirty;
  },
  getDirtyProducts(){
    return this.dirtyProducts;
  },
  getNonDirtyProducts(){
    return this.nonDirtyProducts;
  },
  getSearchResults(){
    return this.searchResults;
  },
  setBulkProduct(_items, is_dirty, allIds){
    let that = this;
    let arr = {};
    for (let item of _items) {
      that.items.set(item.productid, item);
      arr[item.productid] = '1';
    }
    if (!allIds.length) {
      if (is_dirty) {
        for (let item of _items) {
          that.dirtyProducts.set(item.productid, item);
        }
      }
      else {
        for (let item of _items) {
          that.nonDirtyProducts.set(item.productid, item);
        }
      }
    }else{
      that.searchResults = [];
      for (let id of allIds) {
        that.items.get(id) && that.searchResults.push(that.items.get(id));
      }
    }
    that.trigger({products: arr, searchResults: allIds.length});
  },
  onError(err){
    let that = this;
    that.trigger({error: err});
    //that._unlockAll();
  },
  resetStore(){
    let that = this;
    //that._unlockAll();
    that._removeAllItems();
  },


  // Private
  removeExistingFromList(idArray){
    let that = this;
    return idArray.filter((id) => !that.items.get(id));
  },
  _removeAllItems(){
    let that = this;
    that.items.clear();
  },
  checkProduct(id){
    let that = this;
    return that.items.has(id);
  },
  _removeProduct(id){
    let that = this;
    that.items.delete(id);
  }
})
