/**
 * Created by vikramaditya on 10/26/15.
 */
import Reflux from 'reflux';
import DRPActions from '../actions/DRPActions.js';
import Constants from '../constants/AppConstants.js';
let ITEM_LOADING = Constants.ITEM_LOADING;

export default Reflux.createStore({
  listenables: [DRPActions],
  drp: new Map(),
  draft_drp: new Map(),
  getDesignResponse(id){
    let that = this;
    if (that.drp.has(id)) {
      return that.drp.get(id);
    }
    else {
      return ITEM_LOADING;
    }
  },
  setDesignResponse(response){
    let that = this, drp = response.body.data;
    drp.map((d) => {that.drp.set(d.id, d)});
    that.trigger({drp: drp});
  },
  onError(err){
    let that = this;
    console.log('Some error occurred', err);
    that.trigger({error: err});
  },
  checkDesignResponse(drid){
    let that = this;
    return that.drp.has(drid);
  },
  removeExistingDRPFromList(idArray){
    let that = this;
    return idArray.filter((id) => !that.drp.has(id));
  },
  removeExistingDraftFromList(idArray){
    let that = this;
    return idArray.filter((id) => !that.draft_drp.has(id));
  },
  _removeDesignResponse(id){
    let that = this;
    that.drp.delete(id);
  },
  _removeAllDesignResponse(){
    let that = this;
    that.drp.clear();
  },
  //////////DRAFT//////////////////
  getDraftDesignResponse(id){
    let that = this;
    if (that.draft_drp.has(id)) {
      return that.draft_drp.get(id);
    }
    else {
      return ITEM_LOADING;
    }
  },
  setDraftDesignResponse(response){
    let that = this;
    let drp = response.body.data;
    drp.map((d) => {that.draft_drp.set(d.id, d)});
    that.trigger({draft: drp});
  },
  checkDraftDesignResponse(drid){
    let that = this;
    return that.draft_drp.has(drid);
  },
  _removeDraftDesignResponse(id){
    let that = this;
    that.draft_drp.delete(id);
  }
})
