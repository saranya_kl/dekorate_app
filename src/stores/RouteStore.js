/**
 * Created by vikramaditya on 9/8/15.
 */
import Reflux from 'reflux';
import Actions from '../actions/RouterActions.js';
export default Reflux.createStore({
  listenables: [Actions],
  init(){
    this._nextRouterPath= null;
  },
  setTransitionPath(path){
    this._nextRouterPath = path;
  },
  getnextTransitionPath() {
    let nextPath = this._nextRouterPath;
    this._nextRouterPath = null;
    return nextPath;
  }
})
