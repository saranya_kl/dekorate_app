/**
 * Created by vikramaditya on 10/26/15.
 */
import Reflux from 'reflux';
import ProfileActions from '../actions/ProfileActions.js';
import Constants from '../constants/AppConstants.js';
let ITEM_LOADING = Constants.ITEM_LOADING;

export default Reflux.createStore({
  listenables: [ProfileActions],
  profile: new Map(),
  getProfile(id){
    let that = this;
    if (that.profile.has(id)) {
      return that.profile.get(id);
    }
    else {
      return ITEM_LOADING;
    }
  },
  setProfile(response){
    let that = this, profile = response.body.data;
    let userid = profile.id;
    that.profile.set(userid, profile);
    that.trigger({profile: profile});
  },
  onError(err){
    let that = this;
    that.trigger({error: err});
  },
  _removeProfile(id){
    let that = this;
    that.profile.delete(id);
  },
  _removeAllProfile(){
    let that = this;
    that.profile.clear();
  }
})
