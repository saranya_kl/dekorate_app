/**
 * Created by vikramaditya on 10/26/15.
 */
import Reflux from 'reflux';
import ProjectsActions from '../actions/ProjectsActions.js';
import Constants from '../constants/AppConstants.js';
let ITEM_LOADING = Constants.ITEM_LOADING;

export default Reflux.createStore({
  listenables: [ProjectsActions],
  projects: new Map(),
  getProject(id){
    let that = this;
    if (that.projects.get(id)) {
      return that.projects.get(id);
    }
    else {
      return ITEM_LOADING;
    }
  },
  setProject(response){
    let that = this, project = response.body.data;
    that.projects.set(project.id, project);
    that.trigger({project: project});
  },
  onError(err){
    let that = this;
    that.trigger({error: err});
  },
  checkProject(pid){
    let that = this;
    return that.projects.has(pid);
  },
  _removeProject(id){
    let that = this;
    that.projects.delete(id);
  },
  _removeAllProjects(){
    let that = this;
    that.projects.clear();
  }
})
