/**
 * Created by vikramaditya on 10/26/15.
 */
import Reflux from 'reflux';
import _ from 'lodash';
import UsersActions from '../actions/UsersActions';
import Constants from '../constants/AppConstants.js';
let ITEM_LOADING = Constants.ITEM_LOADING;
export default Reflux.createStore({
  listenables: [UsersActions],
  users: new Map(),
  designers: [],
  getUser(id){
    let that = this;
    if (that.users.get(id)) {
      return that.users.get(id);
    }
    else {
      return ITEM_LOADING;
    }
  },
  setUser(response){
    let that = this;
    let user = response.body.data;
    that.users.set(user.id, user);
    that.trigger({users: user});
  },
  setDesigners(response){
    let that = this;
    let users = response.body.data;
    that.designers = users;
    users.map((user) => {
      that.users.set(user.id, user);
    });
    that.trigger({designers: users});
  },
  getDesigners(){
    let that = this;
    return that.designers;
  },
  checkUser(userid){
    let that = this;
    return that.users.has(userid);
  },
  hasDesigners(){
    return this.designers.length;
  },
  onError(err){
    let that = this;
    that.trigger({error: err});
  },
  removeExistingFromList(idArray){
    let that = this;
    return idArray.filter((id) => !that.users.has(id));
  },
  _removeUser(id){
    let that = this;
    that.users.delete(id);
  },
  _removeAllUsers(){
    let that = this;
    that.users.clear();
  }
})
