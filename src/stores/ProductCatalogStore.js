/**
 * Created by vikramaditya on 9/22/15.
 */
import Reflux from 'reflux';
import Constants from '../constants/AppConstants.js';
import Actions from '../actions/ProductCatalogActions.js';

export default Reflux.createStore({

  listenables: [Actions],

  list: [
    "9ca47134528a44f62c5c",
    "0adea8071019712d11ee",
    "9354d32d626adbd9fffa",
    "922c47444999014c09ad",
    "1f57b5e50a9e46b8f39e",
    "461e9279fc540a90f7fe",
    "6ddcc5f97a8a7cd3e6c6",
    "de3ba35b134d95368312",
    "bc6a03d2613fb82f74b5"
  ],
  query: '',

  getList(){
    let that = this;
    return that.list;
  },
  getQuery(){
    let that = this;
    return that.query;
  },
  setSearchResults(data, _query){
    let that = this;
    that.list = data.map(x => x._id);
    that.query = _query;
    that.trigger({search: data});
  },
  onError(err){
    this.trigger({error: err});
  }
})
