/**
 * Created by vikramaditya on 8/14/15.
 */
import Reflux from 'reflux';
import Moment from 'moment';
import Actions from '../actions/NotificationActions.js';
import Constants from '../constants/AppConstants.js';
import TriggerTypes from '../constants/TriggerTypes.js';
import NotificationTypes from '../constants/NotificationTypes.js';

export default Reflux.createStore({
  listenables: [Actions],
  message: '',
  badge_count: 0,
  all_notifications: [],
  //notify(response){
  //  let parsed_message = this.parse(response);
  //  this.currentNotification = parsed_message;
  //  this.message = parsed_message.name + ' ' + parsed_message.message;
  //  this.trigger(TriggerTypes.NEW_PUBNUB_NOTIFICATION);
  //},
  //getNotificationType(){
  //  return this.currentNotification.type ? this.currentNotification.type : '';
  //},
  //parse(message){
  //  let type = {
  //    'comment-created': 'posted a comment',
  //    'project-created': 'created a project',
  //    'userfeedback-submitted': 'submitted a feedback', // drp name
  //    'projectaction-submit': 'submitted project action', //
  //    'project-deleted': 'deleted project', //proj name
  //    'useraction-submitted': 'submitted style quiz' //username
  //  };
  //  var obj = JSON.parse(message);
  //  let data = {
  //    name: obj.data && obj.data.attributes && obj.data.attributes.username || 'Somebody',
  //    type: obj.type,
  //    message: type[obj.type]
  //  };
  //  return data;
  //},
  getNotification(){
    return this.message;
  },
  setAllNotifications(response){
    this.all_notifications = response.body.notifications;
    this.badge_count = response.body.count;
    this.trigger(TriggerTypes.FETCHED_NOTIFICATION);
  },
  setBadgeCount(value){
    this.badge_count = value;
  },
  getAllNotifications(){
    return this.processNotification(this.all_notifications);
  },
  onNewNotification(message){
    let x = JSON.parse(JSON.stringify(message));
    if(message.type != NotificationTypes.PRODUCT_UPDATED){
      this.triggerNotificationType(x);
    }
  },
  triggerNotificationType(message){
    let pid = message.data.projectid;
    let drid = message.data.drid;
    let type = message.type;
    this.trigger({type, pid, drid});
  },
  getBadgeCount(){
    return this.badge_count;
  },
  processNotification(notifications){
    let notifcation = notifications;
    var data = [];
    notifcation.map((object, index) => {
      data[index] = {img: object.img ? object.img : '', id: object.id, read: object.read, text: '', time: Moment(object.created_at).fromNow(), projectid: object.projectid};
      switch(object.type){
      case NotificationTypes.COMMENT:
        //data[index]['text'] = `${object.payload.cmtrname} posted a comment`;
        data[index]['text'] = `${object.payload.cmtrname} commented <br/> in ${object.payload.pname} Project`;
        break;
      case NotificationTypes.PROJECT_ACTION:
        data[index]['text'] = `Project Action is sent for ${object.payload.pname} Project`;
        break;
      case NotificationTypes.PROJECT_ACTION_SUBMITTED:
        data[index]['text'] = `Project Action is submitted for ${object.payload.pname} Project`;
        break;
      case NotificationTypes.NEW_DESIGN_RESPONSE:
        data[index]['text'] = `New Design Response is Sent <br/>${object.payload.pname}`;
        break;
      case NotificationTypes.PROJECT_CLOSE:
        data[index]['text'] = `${object.payload.actorname} closed project ${object.payload.pname}`;
        break;
      case NotificationTypes.LOCK_PROJECT:
        data[index]['text'] = `${object.payload.designername} started working on ${object.payload.pname} Project`;
        break;
      case NotificationTypes.PROJECT_ASSIGNED:
        data[index]['text'] = `You are assigned to ${object.payload.pname} Project`;
        break;
      case NotificationTypes.USERFEEDBACK_SUBMITTED:
        data[index]['text'] = `${object.payload.projectownername} gave Feedback <br/>in ${object.payload.pname} Project`;
        break;
      case NotificationTypes.WORKSPACE_COMMENT:
        data[index]['text'] = `Msg from ${object.payload.cmtrname} in workspace.<br/>${object.payload.pname} Project`;
        break;
      case NotificationTypes.WORKSPACE_SHARED_ASSETS:
        data[index]['text'] = `New Assets shared with you in ${object.payload.pname} Project`;
        break;
      case NotificationTypes.WORKSPACE_SHARED_ASSETS_DELETE:
        data[index]['text'] = `Assets deleted in ${object.payload.pname} Project`;
        break;
      case NotificationTypes.COLLABORATOR_ADDED:
        data[index]['text'] = `${object.payload.projectownername} added a collaborator in ${object.payload.pname} Project`;
        break;
      case NotificationTypes.USER_ACTION_SUBMITTED:
        data[index]['text'] = `${object.payload.username} submitted a user action.`;
        break;
      default :
        data[index]['text'] = 'Unknown Notification type';
        break;
      }
    });
    return data;
  }
})
