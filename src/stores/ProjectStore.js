/**
 * Created by vikramaditya on 7/20/15.
 */
import Reflux from 'reflux';
import ProjectActions from '../actions/ProjectActions.js';
import Constants from '../constants/AppConstants.js';
import Trigger from '../constants/TriggerTypes.js';
import _ from 'lodash';
let UNREAD_STATUS = '01';
let READ_STATUS = '11';
let NONE_STATUS = '00';

export default Reflux.createStore({

  listenables: [ProjectActions],
  Project: {},
  DesignResponses: [],
  currentDesignResponse: {},
  comments: {},
  newDesignResponse: false,
  currentDesignResponseIndex: undefined,
  raw_json: {},
  ids_draft: [],
  ids_non_draft: [],
  unread_data: {},
  unread_comments: {},
  backendusers: [],
  project_backend_users: [],
  all_designers: [],
  quiz_data: [],
  current_quiz: {},
  currentWorkSpaceObject: {},
  currentWorkSpaceID: '',
  all_users: {},
  stats_data: {},
  setProject(resp){
    this.Project = resp.body.data;
    this.raw_json = resp.body;
    this.trigger(Trigger.FETCHED_PROJECT);
  },
  getProjectStatus(project){
    let isOpen = project.attributes.meta.isopen;
    let isAssign = project.attributes.meta.isassign;
    let isWorking = project.attributes.meta.isworking;
    let hasDesign = project.attributes.meta.hasdesign;

    let STATUS = Constants.PROJECT_STATUS;
    if (!isOpen) {
      return STATUS.CLOSED
    }
    if (isWorking) {
      if(hasDesign) {
        return STATUS.DESIGN_SENT
      }
      return STATUS.WORKING;
    }
    if (isAssign) {
      return STATUS.ASSIGNED
    }
    return STATUS.UNASSIGNED;
  },
  getRawJSON(){
    return this.raw_json;
  },
  getAllProject(){
    return this.Project;
  },
  getProject(){
    return this.Project;
  },
  setCurrentDesignResponse(index, newDesignResponse, local = false){
    var self = this;
    self.currentDesignResponseIndex = index;
    self.currentDesignResponse = newDesignResponse ? {} : self.DesignResponses[index];
    self.newDesignResponse = newDesignResponse;
    if (local == true) {
      ProjectActions.setFeedBackLocally(self.currentDesignResponse.attributes.userfeedback);
    }
    self.trigger(Trigger.NEW_DESIGN_RESPONSE);
  },
  refreshDesignResponse(resp){
    this.unread_data = resp.body.ids;
    let r = resp.body.data[0];
    let all_drp = this.DesignResponses;
    all_drp.map((drp, index) => {
      if (drp.id == r.id) {
        this.DesignResponses[index] = r;
      }
    });
    this.setUnreadComments();
    this.trigger(Trigger.REFRESH_DESIGN_RESPONSE);
  },
  refreshFeedBack(resp){
    let fid = resp.id;
    this.unread_data.push({[fid]: UNREAD_STATUS});
  },
  setCommentData(drp){
    this.comments = drp;
    this.trigger(Trigger.CHANGE_COMMENT);
  },
  setDrafts(ids){
    this.ids_draft = ids;
  },
  getDrafts(){
    return this.ids_draft;
  },
  setNonDrafts(ids){
    this.ids_non_draft = ids;
  },
  getNonDrafts(){
    return this.ids_non_draft;
  },
  setComments(resp, data){
    this.comments.attributes.comment.unshift(resp.body.comment);
    this.trigger(Trigger.CHANGE_COMMENT);
  },
  onDeleteComment(resp, index){
    this.comments.attributes.comment.splice(index, 1);
    this.trigger(Trigger.CHANGE_COMMENT);
  },
  getDesignResponseID(){
    return this.comments.attributes ? this.comments.id : undefined;
  },
  getCommentData(){
    if (this.comments && this.comments.attributes) {
      this.notifyCommentRead();
      return this.comments.attributes.comment;
    }
    return [];
  },
  setProjectLocally(file){
    var self = this;
    let json = JSON.parse(file);
    self.DesignResponses = json['design-response'];
    self.setCurrentDesignResponse(0, false, true);
    self.Project = json['project'];
    self.trigger(Trigger.FETCHED_DESIGN_RESPONSES);
  },
  getAllDesignResponse(){
    return this.DesignResponses;
  },
  getAllDraftDesignResponse(){
    return this.draft_designrespnses;
  },
  setDraftDesignResponse(resp){
    this.draft_designrespnses = resp.body.data;
    this.trigger(Trigger.FETCHED_DRAFT_DESIGN_RESPONSES);
  },
  setDesignResponse(resp){
    this.unread_data = resp.body.ids;
    this.DesignResponses = resp.body.data;
    this.setUnreadComments();
    this.raw_json = resp.body;
    this.trigger(Trigger.FETCHED_DESIGN_RESPONSES);
  },
  getUnreadStatus(){
    if (this.unread_data) {
      return this.unread_data;
    }
    return {};
  },
  setUnreadComments(){

    var unread_cid = {};
    this.DesignResponses.map((drp) => {
      var cids = {};
      drp.attributes.comment.map((comment)=> {
        if (this.unread_data[comment.id]) {
          cids[comment.id] = this.unread_data[comment.id];
        }
      });
      Object.keys(cids).length != 0 ? unread_cid[drp.id] = cids : '';
    });
    this.unread_comments = unread_cid;
  },
  notifyCommentRead(){
    this.trigger(Trigger.COMMENT_UNREAD);
  },
  getUnreadComments(){
    return this.unread_comments;
  },
  onCommentRead(drid, tag){
    (tag == 'COMMENT') && this.trigger(Trigger.COMMENT_READ);
  },
  onDesignResponsePost(response, prev_drid = ''){
    var self = this;
    var new_drp = {id: response.body.drid, name: '', thumbUrl: '', created_at: Date.now()};
    this.Project.relationship.designresponses.unshift(new_drp);
    if (this.Project.relationship.draftdesignresponses) {
      let draft = this.Project.relationship.draftdesignresponses;
      for (let i = 0; i < draft.length; i++) {
        if (prev_drid == draft[i].id) {
          this.Project.relationship.draftdesignresponses.splice(i, 1);
          break;
        }
      }
    }
    self.trigger(Trigger.FETCHED_PROJECT);//PostedDesignResponses
  },
  onDesignResponseDraftPost(response){
    var self = this;
    var new_drp = {id: response.body.drid, name: '', thumbUrl: '', created_at: Date.now()};
    let draft = this.Project.relationship.draftdesignresponses;

    /*if (!draft || draft.length == 0) {
      this.Project.relationship.draftdesignresponses = [];
      this.Project.relationship.draftdesignresponses.unshift(new_drp); //on New draft
    } else {
      for (let i = 0; i < draft.length; i++) {
        if (new_drp.id == draft[i].id) { //IF already exists..
          break;
        } else if (i == (draft.length - 1)) {
          this.Project.relationship.draftdesignresponses.unshift(new_drp); //on New draft
        }
      }
    }*/

    if (!draft || draft.length == 0) {
      this.Project.relationship.draftdesignresponses = [];
      this.Project.relationship.draftdesignresponses.unshift(new_drp); //on New draft
    } else {
      var counter = 0;
      draft.map((dr) => {
        if(dr.id != new_drp.id){
          counter++;
        }
      });
      if(counter == draft.length){
        this.Project.relationship.draftdesignresponses.unshift(new_drp); //on New draft
      }
    }
    self.trigger(Trigger.FETCHED_PROJECT);
  },
  onLockProject(state){
    state ? this.trigger(Trigger.PROJECT_LOCKED) : this.trigger(Trigger.PROJECT_UNLOCKED);
  },
  onCloseProject(state){
    state ? this.trigger(Trigger.PROJECT_OPEN) : this.trigger(Trigger.PROJECT_CLOSED);
  },
  onDesignResponseDelete(response){
    let drp = this.Project.relationship.designresponses;
    let drid = response;
    for (var i = 0; i < drp.length; i++) {
      if (drp[i].id == drid) {
        this.Project.relationship.designresponses.splice(i, 1);
        break;
      }
    }
    this.trigger(Trigger.FETCHED_PROJECT);
  },
  onDesignResponseDraftDelete(response){
    let drp = this.Project.relationship.draftdesignresponses;
    let drid = response;
    for (var i = 0; i < drp.length; i++) {
      if (drp[i].id == drid) {
        this.Project.relationship.draftdesignresponses.splice(i, 1);
        break;
      }
    }
    this.trigger(Trigger.FETCHED_PROJECT);
  },
  setBackendUsers(resp){
    this.backendusers = resp.body.data;
    for (let i = 0; i < this.backendusers.length; i++) {
      this.all_users[this.backendusers[i].id] = this.backendusers[i];
    }
    this.trigger(Trigger.FETCHED_ALL_BACKEND_USERS);
  },
  setProjectBackendUsers(resp){
    this.project_backend_users = resp.body.data;
    this.trigger(Trigger.FETCHED_PROJECT_BACKEND_USERS)
  },
  setAllDesigners(resp){
    this.all_designers = resp.body.data;
    this.trigger(Trigger.FETCHED_ALL_DESIGNERS);
  },
  getAllDesigners(){
    return this.all_designers;
  },
  getProjectBackendUsers(){
    return this.project_backend_users;
  },
  getBackendUsers(){
    return this.backendusers;
  },
  getUserInfo(userid){
    let data = this.all_users[userid] ? this.all_users[userid] : {};
    return data;
  },
  getMyInfo(userid){
    var data = {};
    this.backendusers.forEach((user) => {
      if (userid == user.id) {
        data = user;
        return data;
      }
    });
    return data;
  },
  setAllQuiz(response){
    this.quiz_data = response.body.data;
    this.trigger(Trigger.RECEIVED_ALL_QUIZ);
  },
  setQuiz(response){
    this.current_quiz = response.body.data;
    this.trigger(Trigger.RECEIVED_QUIZ);
  },
  getCurrentQuiz(){
    return this.current_quiz;
  },
  getQuizData(){
    return this.quiz_data;
  },
  onProjectDelete(){
    this.trigger(Trigger.PROJECT_DELETED);
  },
  onStyleQuizSent(response){
    this.trigger(Trigger.SENT_STYLE_QUIZ);
  },
  onSetUserAction(response){
    this.trigger(Trigger.USER_ACTION_TOGGLE);
  },
  onProjectActionSend(response){
    this.trigger(Trigger.PROJECT_ACTION_SENT);
  },
  onProjectActionCancel(response){
    this.trigger(Trigger.PROJECT_ACTION_CANCEL);
  },
  onProjectUpdate(response){
    this.trigger(Trigger.UPDATED_PROJECT);
  },
  onDesignResponseUpdate(response){
    this.trigger(Trigger.UPDATED_DESIGN_RESPONSE);
  },
  getAssignedUser(){
    return this.assigned_user;
  },
  onAssignProject(response, userid){
    this.assigned_user = userid;
    this.trigger(Trigger.PROJECT_ASSIGNED);
  },
  setCurrentWorkSpaceObject(resp){
    this.currentWorkSpaceObject = resp.body.data;
    this.trigger(Trigger.FETCHED_WORKSPACE_OBJECT);
  },
  setStatsData(response){
    this.stats_data = response.body;
    this.trigger(Trigger.RECEIVED_STATS);
  },
  setDesignerStat(response){
    this.designer_stats_data = response.body;
    this.trigger(Trigger.RECEIVED_DESIGNER_STATS);
  },
  getDesignerStats(){
    return this.designer_stats_data;
  },
  getStatsData(){
    return this.stats_data;
  },
  onWorkSpaceUpload(){
    this.trigger(Trigger.WORKSPACE_UPLOADED);
  },
  getCurrentWorkSpaceObject(){
    return this.currentWorkSpaceObject;
  },
  onWorkSpaceChatSend(){
    this.trigger(Trigger.WORKSPACE_CHAT_SENT);
  },
  onWorkSpaceDelete(){
    this.trigger(Trigger.WORKSPACE_ASSETS_DELETED);
  },
  onProjectStart(){
    this.trigger(Trigger.PROJECT_STARTED);
  },
  quizupdated(response){
    this.trigger(Trigger.QUIZ_UPDATED);
  },
  uploadedQuiz(response){
    this.trigger(Trigger.UPLOADED_QUIZ);
  },
  deleteQuiz(response){
    this.trigger(Trigger.QUIZ_DELETED);
  },
  onError(){
    this.trigger(Trigger.BACKEND_ERROR);
  }
});
