/**
 * Created by vikramaditya on 11/26/15.
 */
import Reflux from 'reflux';
import UnreadDataActions from '../actions/UnreadDataActions';
export default Reflux.createStore({
  listenables: [UnreadDataActions],
  data: new Map(),
  setStatus(id, status){
    this.data.set(id, status);
  },
  markRead(id){
    let that = this, status = that.getStatus(id);
    that.data.set(id, '1' + status.substring(1, status.length));
  },
  setAllDataStatus(ids){
    let that = this, arr = Object.keys(ids);
    arr.map((id) => {
      that.data.set(id, ids[id]);
    });
  },
  getStatus(id){
    return this.data.get(id);
  },
  onError(err){

  }
})
