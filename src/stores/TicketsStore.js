/**
 * Created by vikramaditya on 10/27/15.
 */
import Reflux from 'reflux';
import TicketsActions from '../actions/TicketsActions.js';
import Constants from '../constants/AppConstants.js';
let ITEM_LOADING = Constants.ITEM_LOADING;

export default Reflux.createStore({
  listenables: [TicketsActions],
  all_projects: new Map(),
  my_projects: new Map(),
  all_closed_projects: new Map(),
  my_closed_projects: new Map(),
  getMyProjects(){
    return this.my_projects
  },
  getMyClosedProjects(){
    return this.my_closed_projects
  },
  getAllProjects(){
    return this.all_projects
  },
  getAllClosedProjects(){
    return this.all_closed_projects
  },

  setMyProjects(response){
    let that = this;
    let projects = response.body.data;
    that.my_projects.clear();
    projects.map((project) => {
      that.my_projects.set(project.id, project);
    });
    that.trigger({my_projects: that.my_projects});
  },
  setMyClosedProjects(response){
    let that = this;
    let projects = response.body.data;
    that.my_closed_projects.clear();
    projects.map((project) => {
      that.my_closed_projects.set(project.id, project);
    });
    that.trigger({my_closed_projects: that.my_closed_projects});
  },
  setAllProjects(response){
    let that = this;
    let projects = response.body.data;
    that.all_projects.clear();
    projects.map((project) => {
      that.all_projects.set(project.id, project);
    });
    that.trigger({all_projects: that.all_projects});
  },
  setAllClosedProjects(response){
    let that = this;
    let projects = response.body.data;
    that.all_closed_projects.clear();
    projects.map((project) => {
      that.all_closed_projects.set(project.id, project);
    });
    that.trigger({all_closed_projects: that.all_closed_projects});
  },
  setProject(response){
    let project = response.body.data, that = this;
    let pid = project.id;
    that.my_projects.has(pid) && that.my_projects.set(pid, project);
    that.my_closed_projects.has(pid) && that.my_closed_projects.set(pid, project);
    that.all_closed_projects.has(pid) && that.all_closed_projects.set(pid, project);
    that.all_projects.has(pid) && that.all_projects.set(pid, project);
    that.trigger({onchange: pid});
  },
  onError(err){
    let that = this;
    console.log('error in ticket store');
    that.trigger({error: err});
  },
  checkProject(pid){
    let that = this;
    return that.all_projects.has(pid) || that.my_closed_projects.has(pid) || that.my_projects.has(pid) || that.all_closed_projects.has(pid);
  },
  _removeProject(id){
    let that = this;
    that.my_projects.has(id) && that.my_projects.delete(id);
    that.my_closed_projects.has(id) && that.my_closed_projects.delete(id);
    that.all_closed_projects.has(id) && that.all_closed_projects.delete(id);
    that.all_projects.has(id) && that.all_projects.delete(id);
  },
  deleteProject(pid){
    let that = this;
    that._removeProject(pid);
  }
})
