/**
 * Created by vikramaditya on 11/3/15.
 */
import Reflux from 'reflux';
import ProjectAssignedUserActions from '../actions/ProjectAssignedUserActions';
import Constants from '../constants/AppConstants.js';
let ITEM_LOADING = Constants.ITEM_LOADING;

export default Reflux.createStore({
  listenables: [ProjectAssignedUserActions],
  assignedUsers: new Map(),
  allBackendUsers: [],
  getAssignedUsers(pid){
    let that = this;
    if (that.assignedUsers.get(pid)) {
      return that.assignedUsers.get(pid);
    }
    else {
      return ITEM_LOADING;
    }
  },
  getAllBackendUsers(){
    return this.allBackendUsers;
  },
  setAssignedUsers(pid, response){
    let users = response.body.data, that = this;
    that.assignedUsers.set(pid, users);
    that.trigger({users: users});
  },
  setAllBackendUsers(response){
    let that = this;
    that.allBackendUsers = response.body.data;
    that.trigger({allBackendUsers: that.allBackendUsers});
  },
  onError(err){
    let that = this;
    that.trigger({error: err});
  },
  checkAssignedUsers(pid){
    let that = this;
    return that.assignedUsers.has(pid);
  },
  hasBackendUsers(){
    return this.allBackendUsers.length;
  },
  _removeAssignedUsers(id){
    let that = this;
    that.assignedUsers.delete(id);
  }
})
