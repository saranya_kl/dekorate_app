/**
 * Created by vikramaditya on 10/20/15.
 */
import Services from './Services.js';
import Helper from '../components/helper.js';
var {AppConstants: Constants} = Helper;
export default {
  downloadProduct: (url, callback=()=>{})=>{
    Services.postAPI(Constants.SERVER_URL+'product/download',{data: {url: url}})
      .then(response => {
        downloadFile(response.body.url);
        callback(true);
      })
      . catch(err => {
        console.log(err);
        callback(false);
      })
  }
}
