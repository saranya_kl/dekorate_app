/**
 * Created by vikramaditya on 6/23/15.
 */
import Request from 'superagent';
import _ from 'lodash';
//import Cookie from 'react-cookie';
import LoginActions from '../actions/LoginActions.js';
import LoginStore from '../stores/LoginStore.js';
let TOKEN_EXPIRED = 'Token Expired';
export default {
  getAPI: function(url, query=''){
    return new Promise(function (resolve, reject) {
      Request.get(url+query).set({'x-access-token': LoginStore.getJWT()}).end(function (err, resp) {
        if (err) {
          if(resp && resp.text == TOKEN_EXPIRED){
            LoginActions.onForceLogout();
          }
          reject('ERROR GETTING ' + url);
          throw err;
        }
        resolve(resp);
      });
    });
  },
  postAPI: function (url, data, query='') {
    return new Promise(function (resolve, reject) {
      Request.post(url+query).set({'x-access-token': LoginStore.getJWT()}).send(data).end(function (err, resp) {
        if (err) {
          if(resp && resp.text == TOKEN_EXPIRED){
            LoginActions.onForceLogout();
          }
          reject('ERROR POSTING ' + url);
          //throw err;
        }
        resolve(resp);
      })
    });
  },
  loginAPI: function(url,data){
    return new Promise(function (resolve, reject) {
      Request.post(url).send(data).end(function (err, resp) {
        if (err) {
          reject('ERROR POSTING ' + url);
          throw err;
        }
        resolve(resp);
      })
    });
  },
  uploadAPI: function(url, file, query=''){
    return new Promise(function (resolve, reject) {
      var req = Request.post(url+query).set({'x-access-token': LoginStore.getJWT()});
      req.attach('file', file, file.name);
      req.end(function (err, resp) {
        if (err) {
          if(resp && resp.text == TOKEN_EXPIRED){
            LoginActions.onForceLogout();
          }
          reject('ERROR UPLOADING ' + url);
          throw err;
        }
        resolve(resp);
      });
    });
  },
  deleteAPI: function(url,data){
    return new Promise(function (resolve, reject) {
      Request.del(url).set({'x-access-token': LoginStore.getJWT()}).send(data).end(function (err, resp) {
        if (err) {
          if(resp && resp.text == TOKEN_EXPIRED){
            LoginActions.onForceLogout();
          }
          reject('ERROR POSTING ' + url);
          throw err;
        }
        resolve(resp);
      })
    });
  },
  zipAPI: function(file,folder,zip,type){
    return new Promise(function (resolve, reject) {
      if(file){
        var xhr = new XMLHttpRequest();
        xhr.open('GET', file+'?x=1', true);
        xhr.responseType = 'arraybuffer';
        xhr.onreadystatechange = function (evt) {
          //console.log(xhr);
          if (xhr.readyState === 4) {
            if (xhr.status === 200) {
              console.log('Downloading',file);
              zip.folder(folder).file(Math.random() + type, xhr.response, {base64: true});
              resolve('SUCCESS');
            }
          }
        };
        xhr.send();
      }else{
        resolve('EMPTY');
      }
    });
  },
  s3UploadAPI: (file, signed_request, index, callback) => {
    return new Promise((resolve, reject) => {
      Request.put(signed_request).set({'x-amz-acl': 'public-read'}).send(file).on('progress', function(e) {
        callback([index, e.percent]);
      }).end(function(err, resp){
        if (err) {
          reject('ERROR IN S3 PUT REQUEST');
          throw err;
        }
        resolve(resp);
      })
    })
  }
}
