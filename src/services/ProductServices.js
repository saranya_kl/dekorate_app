/**
 * Created by vikramaditya on 9/22/15.
 */
import Services from '../services/Services.js';
import Constants from '../constants/AppConstants.js';
import Actions from '../actions/ProductsAction.js';
export default {

  getProduct(id){
    //Not in USE
    Services.postAPI(Constants.SERVER_URL + 'getproductsbyids', {data: {ids: [id]}})
      .then(response => Actions.setProduct(response.body.data[0]))
      .catch(response => {
        console.log(response);
        Actions.onError();
      })
  },
  getBulkProducts(arr, is_dirty=false, allIds = []){
    Services.postAPI(Constants.SERVER_URL + 'getproductsbyids', {data: {ids: arr}})
      .then(response => {
        Actions.setBulkProduct(response.body.data, is_dirty, allIds)
      })
      .catch(response => {
        console.log(response);
        Actions.onError();
      })
  }
}
