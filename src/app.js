/*! React Starter Kit | MIT License | http://www.reactstarterkit.com/ */
import 'babel/polyfill';
import React from 'react';
import Router from 'react-router';
import FastClick from 'fastclick';
//import Cookie from 'react-cookie';

import getRoutes from './routes.js';

import Helper from './components/helper.js';
import Components from './components/component.js';

/* react tap event plugin !important for material ui*/
import injectTapEventPlugin from 'react-tap-event-plugin';

var {LoginStore, RouteCategories, TriggerTypes, LoginActions, NotificationActions} = Helper;
var {DefaultRoute, Route, NotFoundRoute,Redirect } = Router;
var { DashBoards, UnAuthorized} = Components;

injectTapEventPlugin();
//var unsubscribe = LoginStore.listen(onLoginStoreChange);

function run() {

  Router.run(getRoutes(), Router.HistoryLocation, function (Handler, state) {
    React.render(<Handler/>, document.getElementById('app'));
  });

  //route(RouteCategories.LOGIN);
  //
  //
  //if (Cookie.load('logged_in') && Cookie.load('access_token') && Cookie.load('userid') != 'undefined' && Object.keys(Cookie.load('roles'))) {
  //  if (Cookie.load('logged_in') == '1') {
  //    subscribe_pubnub(LoginStore.getUserID()); // TODO further fix
  //    //console.log(Cookie.load('email'), Cookie.load('access_token'), Cookie.load('roles'));
  //    LoginStore.jwtVerify(Cookie.load('email'), Cookie.load('access_token'), Cookie.load('roles'), Cookie.load('userid'), Cookie.load('name'), Cookie.load('imageUrl'));
  //    route(RouteCategories.LOGIN);
  //  }
  //  else {
  //    route(RouteCategories.LOGIN);
  //  }
  //}
  //else {
  //  route(RouteCategories.LOGIN);
  //}
}

//function onLoginStoreChange(props) {
  //if (props == TriggerTypes.LOGIN_SUCCESS) {
  //
  //  if (LoginStore.getLoginState() == 'verified') {
  //    route(key_hash[LoginStore.getCurrentRole()]);
  //  }
  //  else if(LoginStore.getLoginState() == 'signed_inAndVerified'){
  //    React.render(<DashBoards/>, document.getElementById('app'));
  //  }
  //  else {
  //    route(RouteCategories.LOGIN);
  //  }
  //}
  //if (props == TriggerTypes.CHANGE_ROUTE) {
  //  route(key_hash[LoginStore.getCurrentRole()]);
  //}
  //if (props == TriggerTypes.UNAUTHORIZED) {
  //  React.render(<UnAuthorized/>, document.getElementById('app'));
  //}
  //if(props == TriggerTypes.SESSION_EXPIRE){
  //  route(RouteCategories.LOGIN);
  //}
//}

//function subscribe_pubnub(channel) {
//  var pubnub = PUBNUB({
//    subscribe_key: pubnub_subscribe_key
//  });
//  pubnub.subscribe({
//    channel: channel,
//    message: function (message, env, ch, timer, magic_ch) {
//      NotificationActions.onNotification(JSON.stringify(message));
//      NotificationActions.FETCH_ALL_NOTIFICATIONS(LoginStore.getUserID());
//      console.log('Message: ' + JSON.stringify(message));
//    }
//  });
//}
//function unsubscribe(pubnub, channel) {
//  pubnub.unsubscribe({
//    channel: channel
//  });
//}


Promise.all([
  new Promise((resolve) => {
    if (window.addEventListener) {
      window.addEventListener('DOMContentLoaded', resolve);
    } else {
      window.attachEvent('onload', resolve);
    }
  }).then(() => FastClick.attach(document.body))
]).then(run);

//
//let key_hash = {
//  'admin': RouteCategories.ADMIN,
//  'ADMIN': RouteCategories.ADMIN,
//  'designer': RouteCategories.DESIGNER,
//  'DESIGNER': RouteCategories.DESIGNER,
//  'modeller': RouteCategories.MODELLER,
//  'MODELLER': RouteCategories.MODELLER,
//  'visualizer': RouteCategories.VISUALIZER,
//  'VISUALIZER': RouteCategories.VISUALIZER,
//  'login': RouteCategories.LOGIN,
//  'LOGIN': RouteCategories.LOGIN
//};
/*
 var routes = (
 <Route handler={App} path='/'>
 <Route path='crm' name='crm' handler={Designer}>
 <DefaultRoute handler={HomePage}/>
 <Route path='projects/:pid' handler={HomePage}/>
 <Route name='products' path='products' handler={ProductScreen}/>
 </Route>
 <Redirect from='/' to='/crm'/>
 <Redirect from='/login' to='/crm'/>
 <NotFoundRoute handler={NotFoundHandler}/>
 </Route>
 );

 */
