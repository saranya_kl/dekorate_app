/**
 * Created by vikramaditya on 9/23/15.
 */
import React from 'react';
import {SvgIcon} from 'material-ui';
import Helper from '../../../../helper.js';
import styles from './ProductCartHolder.scss';
import ProductCart from './ProductCart/ProductCart.js';
import _ from 'lodash';
var {withStyles, WorkspaceActions, WorkspaceStore} = Helper;
@withStyles(styles) class ProductCartHolder extends React.Component {

  constructor() {
    super();
    this.state = {
      store_cart_data: {},
      cartChange: false
    }
  }

  componentWillMount() {
    this.getStateFromStore(this.props);
  }

  componentWillReceiveProps(nextProps) {
    let that = this;
    if (nextProps.pid != that.props.pid) {
      nextProps.onShow(false);
    }else{
      that.getStateFromStore(nextProps)
    }
  }

  getStateFromStore(props) {
    let that = this;
    let data = props.productCart;//_.cloneDeep(WorkspaceStore.getWorkspaceProductCart(props.pid, props.productCartIndex));
    that.setState({store_cart_data: data});
  }

  goBack() {
    let that = this;
    that.props.onShow(false);
  }

  getTabs() {
    let that = this;
    return (
      [
        <div key={1}>
          <button onClick={that.goBack.bind(that)} className='back-btn'>&larr;</button>
        </div>,
        <div key={2}>
          <button className='cart-btn'>
            <SvgIcon>
              <path
                d='M17,18C15.89,18 15,18.89 15,20A2,2 0 0,0 17,22A2,2 0 0,0 19,20C19,18.89 18.1,18 17,18M1,2V4H3L6.6,11.59L5.24,14.04C5.09,14.32 5,14.65 5,15A2,2 0 0,0 7,17H19V15H7.42A0.25,0.25 0 0,1 7.17,14.75C7.17,14.7 7.18,14.66 7.2,14.63L8.1,13H15.55C16.3,13 16.96,12.58 17.3,11.97L20.88,5.5C20.95,5.34 21,5.17 21,5A1,1 0 0,0 20,4H5.21L4.27,2M7,18C5.89,18 5,18.89 5,20A2,2 0 0,0 7,22A2,2 0 0,0 9,20C9,18.89 8.1,18 7,18Z'/>
            </SvgIcon>
            {that.state.store_cart_data.list &&
            that.state.store_cart_data.list.length > 0 &&
            <span className='cart-counter-label'>{that.state.store_cart_data.list.length}</span>}
          </button>
        </div>
      ]
    )
  }

  render() {
    let that = this;
    let tabs = that.getTabs();
    let cart_data = that.state.store_cart_data;
    return (
      <div className='ProductCartHolder'>
        <div className='ProductCartHolder-Tab'>
          {tabs}
        </div>
        <div className='ProductCartHolder-body'>
          <ProductCart data={cart_data}/>
        </div>
      </div>
    )
  }
}
export default ProductCartHolder;
