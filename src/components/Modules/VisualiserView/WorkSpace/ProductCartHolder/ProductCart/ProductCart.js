/**
 * Created by vikramaditya on 9/24/15.
 */
import React from 'react';
import {SvgIcon} from 'material-ui';
import Helper from '../../../../../helper.js';
import styles from './ProductCart.scss';
import CustomModal from '../../../../../../lib/Modal';
import ProductItemView from '../../../../../Shared/ProductCatalog/ProductItemView.js';
import Moment from 'moment';
var {withStyles, ProductsStore, AppConstants: Constants} = Helper;
@withStyles(styles) class ProductCart extends React.Component {
  constructor() {
    super();
    this.state = {
      data: [],
      loading: true,
      totalCost: '0',
      isModalOpen: false,
      previewProduct: {},
      requests: []
    }
  }

  componentWillMount() {
    let that = this;
    that.unsubscribe_product_store = ProductsStore.listen(that.handleProductStoreChange.bind(that));
    that.setProducts(that.props.data);
  }
  componentWillUnmount(){
    this.unsubscribe_product_store();
  }
  handleProductStoreChange(trigger) {
    let that = this;
    let data = that.state.data, updated_data = [], totalCost = 0;
    if (trigger.products) {
      let arr = trigger.products;
      data.map((product) => {
        if (arr[product.productid]) {
          let p = ProductsStore.getProduct(product.productid);
          p.qty = product.qty;
          p.totalCost = (parseInt(p.price) * parseInt(product.qty));
          totalCost += (parseInt(p.price) * parseInt(p.qty));
          updated_data.push(p);
        } else {
          updated_data.push(product);
        }
      });
      that.setState({data: updated_data, totalCost: totalCost, loading: false});
    } else {
      //if still loading then
      that.setProducts(that.props);
    }
  }
  setProducts(props) {
    let that = this;
    let x = [], totalCost = 0;
    if (props.list) {
      props.list.map(product => {
        let p = ProductsStore.getProduct(product.productid);
        if (typeof(p) === 'object') {
          p.qty = product.qty;
          p.cancelled = false;
          p.totalCost = (parseInt(p.price) * parseInt(p.qty));
          totalCost += (parseInt(p.price) * parseInt(p.qty));
        }
        x.push(p);
      });
      props.cancelledlist.map(product => {
        let p = ProductsStore.getProduct(product.productid);
        if (typeof(p) === 'object') {
          p.qty = product.qty;
          p.cancelled = true;
        }
        x.push(p);
      });
    }
    that.setState({data: x, totalCost: `${totalCost}`, requests: props.requests ? props.requests : []});
  }

  componentWillReceiveProps(nextProps) {
    let that = this;
    that.setProducts(nextProps.data);
  }

  showModal(value, data = {}) {
    this.setState({isModalOpen: value, previewProduct: data});
  }
  getRequestList() {
    let that = this;
    let request_list = that.state.requests;
    return (
      request_list.map((request, index) => {

        return (
          !request.cancelled && <div key={index} className='ProductCart-requests-item'>
            <span>{index+1}.</span>
            <div className='status'>
              {request.done && <SvgIcon className='status-done'>
                <path d="M9 16.2L4.8 12l-1.4 1.4L9 19 21 7l-1.4-1.4L9 16.2z"/>
              </SvgIcon>}
              {request.cancelled && <button disabled="true" className="status-cancel">
                CANCELLED
              </button>}
            </div>
            <div className='request-list'>
              <div className='link'>
                <a href={request.url} target='_blank'>{request.url}</a>
              </div>
              <div className='itemtype'>{request.itemtype}</div>
              <div className='color'>{request.color}</div>
              <div className='note'>{request.note}</div>
              <div className='button-container'>
                <span className='time' style={{maxWidth:'100px'}}>{Moment(request.created_at).format('h:mm a, L')}</span>
              </div>
            </div>
          </div>
        )
      })
    )
  }
  getProductItem(data, index) {
    let that = this;
    let loading = typeof (data) === 'string' ? that.state.loading : false;
    let pImageThumbUrl = Constants.LOADER_GIF;
    let pName = "Loading";
    let pPrice = "0";
    let pFeatures = [];
    let pDetails = false, pCancelled = false;
    if (!loading) {
      pImageThumbUrl = _.result(data,'displayImage.url', Constants.LOADER_GIF);
      pName = data.name;
      pPrice = _.result(data, 'amount',{INR: '0', USD: '0'});
      pCancelled = data.cancelled;
      pDetails = _.result(data,'assets.models.length', false);
      pFeatures = [data.brand, data.material, data.colour];
    }
    return (
      <div key={index} className="ProductListItem" onClick={() => { !loading && that.showModal(true, data)}}>
        <div className="productImgHolder">
          <div className="productImg" style={{backgroundImage:`URL(${pImageThumbUrl})`}}>
          </div>
        </div>

        <div className="productName">
          {pName}
        </div>

        <div className="productPrice">
          <div className="priceContainer">
            <span>{pPrice.INR && `₹${pPrice.INR}`}</span>
            <span>{pPrice.USD && `$${pPrice.USD}`}</span>
          </div>
        </div>

        <div className="productFeatures">
          <div className='list'>
            {pFeatures.map((x, index) => {
              if (x != "") return <li key={index}>{x}</li>; else return null;
            })}
          </div>
          <div className='details'>
            {pDetails && <div title='Modelled' className='modelled'>
              <SvgIcon color={'green'}>
                <path
                  d='M21,16.5C21,16.88 20.79,17.21 20.47,17.38L12.57,21.82C12.41,21.94 12.21,22 12,22C11.79,22 11.59,21.94 11.43,21.82L3.53,17.38C3.21,17.21 3,16.88 3,16.5V7.5C3,7.12 3.21,6.79 3.53,6.62L11.43,2.18C11.59,2.06 11.79,2 12,2C12.21,2 12.41,2.06 12.57,2.18L20.47,6.62C20.79,6.79 21,7.12 21,7.5V16.5M12,4.15L6.04,7.5L12,10.85L17.96,7.5L12,4.15M5,15.91L11,19.29V12.58L5,9.21V15.91M19,15.91V9.21L13,12.58V19.29L19,15.91Z'/>
              </SvgIcon>
            </div>}
            {pCancelled && <div title='Cancelled' className='cancelled'>
              Cancelled
            </div>}
          </div>
        </div>
      </div>
    )
  }

  render() {
    let that = this;
    let data = that.state.data;
    let CartProducts = data.map((product, index) => that.getProductItem(product, index));
    let Requests = that.getRequestList();
    let cartName = that.props.data.name;
    return (
      <div className='ProductCart'>
        <div className='ProductCart-name'>
          <input readOnly ref='cartName' type='text' placeholder='Enter Cart Name' className='ProductCart-name'
                 value={cartName}/>
        </div>
        <div className="ProductCart-products">
          <div className='ProductCart-products-header'>Products
          </div>
          <div className='ProductCart-list'>
            {CartProducts}
          </div>
        </div>
        <div className="ProductCart-requests">
          <div className="ProductCart-requests-header">
            Requests
          </div>
          <div className='ProductCart-requests-body'>
            {Requests}
          </div>
        </div>
        <CustomModal style={{top:'5%',left:'15%',width:'70%',height:'75%'}} isOpen={that.state.isModalOpen}
                     onClose={that.showModal.bind(that, false)}>
          <ProductItemView data={that.state.previewProduct} closeModal={that.showModal.bind(that,false)}/>
        </CustomModal>
      </div>
    )
  }
}
export default ProductCart;
