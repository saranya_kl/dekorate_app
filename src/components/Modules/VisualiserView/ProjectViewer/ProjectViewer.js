/**
 * Created by vikramaditya on 9/9/15.
 */
import React from 'react';
import {SvgIcon, ListItem, Avatar} from 'material-ui';
import _ from 'lodash';
import Helper from '../../../helper.js';
import FilesRepository from '../FilesRepository/FilesRepository.js';
import WorkSpace from '../WorkSpace/WorkSpace.js';
import CanvasPlayerModal from '../../../Roles/Designer/components/CanvasPlayerModal';
import ThumbNails from '../../../Roles/Designer/components/TicketInbox/ProjectViewer/ThumbNails';
import CanvasPlayer from '../../../Roles/Designer/components/TicketInbox/ProjectViewer/CanvasPlayer';

var {ProjectsActions, ProjectsStore, DownloadUtility, ProjectAssignedUserActions, ProjectAssignedUserStore, AppConstants: Constants} = Helper;

class ProjectViewer extends React.Component {
  static contextTypes = {
    setProgressVisibility: React.PropTypes.func
  };
  constructor(props) {
    super();
    let that = this;
    this.isVisible = true;
    this.all_backend_users = [];
    this.menuList = ['Workspace', 'Submission', 'Files'];
    let project = typeof(ProjectsStore.getProject(props.pid)) === 'object' ? ProjectsStore.getProject(props.pid) : {};
    this.state = {
      project: project ,
      current_pid: props.pid,
      currentMenuIndex: 0,
      currentSubmissionIndex: 0,
      submissions: _.result(project,'attributes.submission',[]),
      showModal: false,
      project_backend_users: []
    };
    ProjectsActions.fetchProject(props.pid);
    ProjectAssignedUserActions.fetchAssignedUsers(props.pid);
    ProjectAssignedUserActions.fetchAllBackendUsers();

    that.unsubscribe = ProjectsStore.listen(that.handleProjectStore.bind(that));
    that.unsubscribe_assignedUserStore = ProjectAssignedUserStore.listen(that.handleAssignedUserStore.bind(that));
    window.addEventListener('keydown', that.onKeyPress.bind(that));
  }

  componentWillReceiveProps(nextProps) {
    let that = this;
    if (nextProps.pid != that.props.pid) {
      let project = typeof(ProjectsStore.getProject(nextProps.pid)) === 'object' ? ProjectsStore.getProject(nextProps.pid) : {};
      let submissions = _.result(project,'attributes.submission',[]);
      ProjectsActions.fetchProject(nextProps.pid);
      ProjectAssignedUserActions.fetchAssignedUsers(nextProps.pid);
      that.setBackendUsers(nextProps.pid);
      that.setState({currentMenuIndex: 0, submissions: submissions, current_pid: nextProps.pid, project: project});
    }
  }
  componentWillMount() {
    window.addEventListener('keydown', this.onKeyPress.bind(this));
  }
  componentWillUnmount() {
    let that = this;
    that.isVisible = false;
    window.removeEventListener('keydown', that.onKeyPress.bind(that));
    //that.unsubscribe_projectStore();
    that.unsubscribe_assignedUserStore();
    that.unsubscribe();
  }
  onKeyPress(e) {
    let that = this;
    if (e.keyCode == 37) { //left
      let c_index = (that.state.currentSubmissionIndex - 1) % that.state.submissions.length;
      let index = c_index >= 0 ? c_index : that.state.submissions.length - 1;
      that.showSubmission(index);
    }
    if (e.keyCode == 39) { //right
      let c_index = (that.state.currentSubmissionIndex + 1) % that.state.submissions.length;
      that.showSubmission(c_index);
    }
  }
  handleProjectStore(trigger){
    let that = this;
    let pid = that.props.pid;
    if(trigger.project){
      let project = ProjectsStore.getProject(pid);
      that.setState({submissions: project.attributes.submission, project: project});
      that.context.setProgressVisibility(false);
    }
    if(trigger.error){
      that.context.setProgressVisibility(false);
    }
  }
  handleAssignedUserStore(trigger){
    let that = this;
    let pid = that.props.pid;
    if(trigger.error){
      that.context.setProgressVisibility(false);
    }
    if(trigger.users){
      this.setBackendUsers(pid);
      that.context.setProgressVisibility(false);
    }
    if(trigger.allBackendUsers){
      this.setBackendUsers(pid);
      that.context.setProgressVisibility(false);
    }
  }
  //onStatusChange(props){
  //  if(props == TriggerTypes.FETCHED_PROJECT){
  //    let submissions = ProjectStore.getProject().attributes.submission;
  //    this.setState({submissions: submissions, project: ProjectStore.getProject()});
  //    this.context.setProgressVisibility(false);
  //  }
  //  if (props == TriggerTypes.FETCHED_ALL_BACKEND_USERS) {
  //    this.setBackendUsers();
  //  }
  //  if (props == TriggerTypes.FETCHED_PROJECT_BACKEND_USERS) {
  //    this.setBackendUsers();
  //  }
  //}
  switchMenu(value) {
    this.setState({currentMenuIndex: value});
  }
  downloadZip() {
    let img_url = [];
    let self = this;
    for (let i = 0; i < self.state.submissions.length; i++) {
      img_url[i] = self.state.submissions[i].imageUrl.url;
    }
    self.context.setProgressVisibility(true);
    DownloadUtility.downloadImageZIP(img_url, self.props.pid)
      .then(() => self.context.setProgressVisibility(false))
      .catch(() => self.context.setProgressVisibility(false));
  }
  onShowModal(value) {
    if (value) {
      this.isVisible = false
    } else {
      this.isVisible = true;
    }
    this.setState({showModal: value});
  }
  showSubmission(index){
    if (this.state.currentMenuIndex == 1) {
      this.isVisible && this.setState({currentSubmissionIndex: index});
    }
  }
  setBackendUsers(pid){
    let data = ProjectAssignedUserStore.getAssignedUsers(pid);
    this.all_backend_users = ProjectAssignedUserStore.getAllBackendUsers();
    let backend_user_data = [];
    this.all_backend_users.map((user) => {
      for (let i = 0; i < data.length; i++) {
        if (user.id == data[i].userid) {
          let tmp = _.cloneDeep(user);
          tmp.currentRole = data[i].role;
          backend_user_data.push(tmp);
        }
      }
    });
    this.setState({project_backend_users: backend_user_data});
  }
  getWorkspaceComponent() {
    return (
      <div style={{display:'flex', flexFlow:'row',justifyContent:'space-between'}}>
        <div style={{flex:'2', backgroundColor: '#f2f2f2'}}>
          {/*<NextAction data={this.state.project}/>*/}
          <div style={{height: '95vh', overflowY:'scroll'}}>
            <WorkSpace pid={this.state.current_pid}/>
          </div>
        </div>
      </div>
    )
  }
  getSubmissionComponent() {
    let self = this;
    let submissions = self.state.submissions;
    let currentSubmissionIndex = self.state.currentSubmissionIndex;
    let Modal = <CanvasPlayerModal initialIndex={currentSubmissionIndex} show={self.onShowModal.bind(self)}
                                   data={submissions}/>;
    let showModal = self.state.showModal;
    return (
      <div
        style={{display:'flex', flexFlow:'row', justifyContent:'space-between', height:'100%', backgroundColor:'#e6e6e6'}}>
        <div style={{flex:'2'}}>
          <div style={{display:'flex', flexFlow:'column'}}>
            <div style={{width:'100%', display:'flex', flexFlow:'row', justifyContent:'space-between', borderBottom: '1px solid #bdc3c7'}}>
              <h6 style={{color:'#545454', fontSize:'0.9rem', fontWeight:'400', fontFamily:'Lato', padding:'15px', margin:'0', paddingLeft:'25px'}}>
                {self.state.project.attributes ? self.state.project.attributes.title : ''} </h6>
              <button onClick={self.downloadZip.bind(self)}
                      style={{alignSelf:'center', backgroundColor:'transparent', padding:'3.5px', marginRight:'15px', border:'none'}}>
                <SvgIcon style={{fill:'#545454'}}>
                  <path
                    d="M19.35 10.04C18.67 6.59 15.64 4 12 4 9.11 4 6.6 5.64 5.35 8.04 2.34 8.36 0 10.91 0 14c0 3.31 2.69 6 6 6h13c2.76 0 5-2.24 5-5 0-2.64-2.05-4.78-4.65-4.96zM17 13l-5 5-5-5h3V9h4v4h3z"/>
                </SvgIcon>
              </button>
            </div>
            {submissions.length > 0 && <CanvasPlayer showModal={self.onShowModal.bind(self)}
                                                                imageUrl={submissions[currentSubmissionIndex].imageUrl.url}
                                                                audioUrl={submissions[currentSubmissionIndex].audioUrl}
                                                                annUrl={submissions[currentSubmissionIndex].annUrl}
                                                                fullscreen={true}/> }
            <ThumbNails onClick={self.showSubmission.bind(self)} data={submissions}
                        current={currentSubmissionIndex}/>
          </div>
        </div>
        {showModal && <div style={{position:'absolute'}}>{Modal}</div>}
      </div>
    )
  }
  getCurrentComponent(index) {
    let that = this;
    switch (index) {
    case 0:
      return this.getWorkspaceComponent();
      break;
    case 1:
      return this.getSubmissionComponent();
      break;
    case 2:
      return <FilesRepository pid={that.state.current_pid}/>;
      break;
    default:
      return <div/>;
    }
  }
  getProjectInfoComponent(){
    let self = this;
    let ProjectTitle = <span style={{padding:'10px', borderRight:'1px solid #bdc3c7'}}>{self.state.project.attributes ? self.state.project.attributes.title : ''}</span>;

    let ProjectDescription = <div style={{display:'flex',flexFlow:'row'}}>
      <span style={{margin:'4px 6px', padding:'10px', fontSize:'12px', color:'#008AE6'}}>{self.state.project.relationship ? self.state.project.relationship.owner.name : ''}</span>
    </div>;
    let AssignedUsers = <div style={{flex: '1',display:'flex', borderLeft:'1px solid #bdc3c7'}}>
      {
        self.state.project_backend_users.map((user, index) => {
          return (
            <div key={index} style={{paddingLeft:'20px', width:'130px', display:'flex', paddingTop:'10px'}} title={`${user.firstname} ${user.lastname}`}>
              <img style={{width:'20px', height:'20px', borderRadius:'50%'}} src ={user.imageUrl} />
              <div style={{paddingLeft:'10px'}}>
                <p style={{fontSize:'10px'}}>{`${user.firstname} ${user.lastname}`}</p>
                <p style={{fontSize:'9px'}}>{user.currentRole}</p>
              </div>
            </div>
          )
        })
      }
      {self.state.project_backend_users.length == 0 && <span style={{paddingLeft:'20px'}}>No Assigned Users</span>}
    </div>;
    return (
      <div style={{minHeight: '46px',display: 'flex',flexFlow: 'row', padding: '2px'}}>
        {ProjectTitle}
        {ProjectDescription}
        {AssignedUsers}
      </div>
    )
  }
  getTabList(items, currentIndex) {
    let menuStyle = {
      display: 'flex',
      flexFlow: 'row',
      justifyContent:'space-between',
      border: '1px solid #bdc3c7',
      backgroundColor: '#f2f2f2'
    };
    let flex_row = {
      display: 'flex',
      flexFlow: 'row',
      fontSize: '0.8rem',
      minHeight: '36px'
    };
    let currentStyle = {
      backgroundColor: 'grey',
      color: '#ffffff',
      outline: 'none',
      border: 'none',
      minWidth: '80px',
      borderRight: '1px solid #e6e6e6'
    };
    let buttonStyle = {
      backgroundColor: '#f2f2f2',
      color: '#000000',
      outline: 'none',
      border: 'none',
      minWidth: '80px',
      borderRight: '1px solid #e6e6e6'
    };
    return (
      <div style={menuStyle}>
        <div style={flex_row}>
          {items.map((item, index) => {
            return (
              <button key={index} onClick={this.switchMenu.bind(this,index)}
                      style={index == currentIndex ? currentStyle: buttonStyle}>{item}</button>
            )
          })}
        </div>
        <button onClick={this.props.onClose} style={currentStyle}>Close</button>
      </div>
    )
  }

  render() {
    let CustomTabList = this.getTabList(this.menuList, this.state.currentMenuIndex);
    let ProjectInfo = this.getProjectInfoComponent();
    return (
      <div
        style={{display:'flex',flexFlow:'column', height:'100vh', width:'100%', backgroundColor:'#f2f2f2'}}>
        {ProjectInfo}
        {CustomTabList}
        {this.getCurrentComponent(this.state.currentMenuIndex)}
        {/*self.state.showModal && <div style={{position:'absolute'}}>{Modal}</div>*/}
      </div>
    )
  }
}
export default ProjectViewer;
