/**
 * Created by vikramaditya on 9/9/15.
 */
import React from 'react';
import Moment from 'moment';
import styles from './VisualiserView.scss';
import Helper from '../../helper.js';
//import NewProjectsList from '../../Shared/NewProjectsList';
import ProjectViewer from './ProjectViewer/ProjectViewer.js';

var {withStyles, TicketsActions, TicketsStore, LoginStore, CustomUtility, AppConstants: Constants} = Helper;

@withStyles(styles) class VisualiserView extends React.Component {
  static contextTypes = {
    router: React.PropTypes.func
  };

  constructor() {
    super();
    let that = this;
    that.state = {
      isLoading: true,
      expanded: false,
      my_projects: new Map(),
      current_pid: ''
    };
    that.leftExpandedStyle = {minWidth: '20%'};
    that.leftCollapsedStyle = {minWidth: '100%'};
  }

  componentWillMount() {
    let my_projects = TicketsStore.getMyProjects(), that = this;
    if(my_projects.size > 0){
      that.setState({my_projects: my_projects, isLoading: false});
    }else{
      TicketsActions.fetchMyProjects(LoginStore.getUserID(), LoginStore.getCurrentDashboard());
    }
    that.unsubscribe = TicketsStore.listen(that.handleTicketStore.bind(that));
    if (that.context.router.getCurrentParams().pid) {
      that.expandView(true, that.context.router.getCurrentParams().pid);
    }
  }
  componentWillReceiveProps(nextProps){
    this.setLayout();
  }
  setLayout(){
    let that = this;
    if (that.context.router.getCurrentParams().pid && that.context.router.getCurrentPath() != `/${LoginStore.getCurrentDashboard()}`) {
      let pid = that.context.router.getCurrentParams().pid; //path.replace('/designer/projects/', '');
      that.expandView(true, pid);
    } else {
      that.expandView(false, '');
    }
  }
  componentWillUnmount() {
    this.unsubscribe();
  }
  //onNotification(props) {
  //  if (props.type == NotificationTypes.PROJECT_ASSIGNED) {
  //    TicketsActions.fetchMyProjects(LoginStore.getUserID(), ROLE);
  //  }
  //}
  handleTicketStore(trigger){
    let that = this;
    if(trigger.my_projects){
      let my_projects = TicketsStore.getMyProjects();
      that.setState({my_projects: my_projects, isLoading: false});
    }
  }

  expandView(value, pid) {
    if (value) {
      this.context.router.transitionTo(`/${LoginStore.getCurrentDashboard()}/projects/${pid}`);
      this.setState({expanded: value, current_pid: pid});
    }
    else {
      this.setState({expanded: value});
    }
  }
  onProjectClick(pid, index){
    this.context.router.transitionTo(`/${LoginStore.getCurrentDashboard()}/projects/${pid}`);
    this.setState({expanded: true, current_pid: pid});
  }
  getProjectCard(p, index) {
    let project_status = (`${CustomUtility.getProjectStatus(p)}`);
    let status_color = '#000000', that = this;
    switch (project_status) {
    case Constants.PROJECT_STATUS.UNASSIGNED:
      status_color = '#f35d54';
      break;
    case Constants.PROJECT_STATUS.ASSIGNED:
      status_color = '#feb054';
      break;
    case Constants.PROJECT_STATUS.WORKING:
      status_color = '#6e92cc';
      break;
    case Constants.PROJECT_STATUS.CLOSED:
      status_color = '#82dcd4';
      break;
    case Constants.PROJECT_STATUS.DESIGN_SENT:
      status_color = '#512398';
      break;
    default:
      status_color = '#000000';
    }
    let time = Moment(p.attributes.meta.created_at).format('MMM Do');
    return (
      <div className="ProjectCard" key={index} onClick={that.onProjectClick.bind(that, index)}>
        <div className="ProjectCard-maincard">
          <div className="ProjectCard-header">
            <span className="ProjectCard-header-title">{p.attributes.title}</span>
            <span className="ProjectCard-header-time">{time.substring(0,time.length-2)}</span>
          </div>
          <img src={p.attributes.meta.displayThumb} className="ProjectCard-image" />
          <div title={project_status} className="ProjectCard-user">
            <div className="ProjectCard-user-wrapper">
              <img src={p.relationship.owner.imageUrl} className="ProjectCard-user-image"/>
              <div className="ProjectCard-user-name">{p.relationship.owner.name}</div>
            </div>
            <div className="ProjectCard-user-status" style={{backgroundColor:status_color}}></div>
          </div>
        </div>
      </div>
    )
  }
  getTicketCards(){
    let self = this;
    let MyProjects=[];
    self.state.my_projects.forEach((project, key) => MyProjects.push(self.getProjectCard(project, key)));
    let Loading = self.state.isLoading;
    return (
      <div className="Card">
          <div className='TicketInbox-searchBox'>
            {/*<input type='text' value={self.state.searchTerm} onChange={self.onSearchProject.bind(self, self.state.currentTab)}/>*/}
          </div>
          <div className="Card-list">
            {MyProjects}
          </div>
          {Loading && <h1 style={{textAlign:'center'}}>Loading Projects..</h1>}
        </div>
    );
  }
  getTabList(items, currentIndex) {
    let flex_row = {
      display: 'flex',
      flexFlow: 'row',
      fontSize: '0.8rem',
      minHeight: '48px',
      border: '1px solid #bdc3c7',
      backgroundColor: '#f2f2f2'
    };
    let currentStyle = {
      backgroundColor: 'grey',
      color: '#ffffff',
      outline: 'none',
      border: 'none',
      width: '100%',
      borderRight: '1px solid #e6e6e6'
    };
    let buttonStyle = {
      backgroundColor: '#f2f2f2',
      color: '#000000',
      outline: 'none',
      border: 'none',
      width: '100%',
      borderRight: '1px solid #e6e6e6'
    };
    return (
      <div style={flex_row}>
        {items.map((item, index) => {
          return (
            <button key={index} style={index == currentIndex ? currentStyle: buttonStyle}>{item}</button>
          )
        })}
      </div>
    )
  }

  render() {
    let that = this;
    let leftViewStyle = this.state.expanded ? this.leftExpandedStyle : this.leftCollapsedStyle;
    let LeftMenuTabs = this.getTabList(['My Projects'], 0);
    let errorMessage = <h3
      className='VisualiserView-error'>{this.state.isLoading ? 'Please Wait..' : 'No Projects'}</h3>;
    let LeftView = this.state.my_projects.size != 0 ?
      <div className='VisualiserView-LeftView' style={leftViewStyle}>
        {LeftMenuTabs}
        {that.getTicketCards()}
        {/*<NewProjectsList pid={this.state.current_pid} expanded={this.state.expanded}
                         data={this.state.my_projects}
                         onClick={this.expandView.bind(this, true)}/>*/}
      </div> : errorMessage;

    let RightView = (this.state.my_projects.size != 0) &&
      <ProjectViewer onClose={this.expandView.bind(this,false)} pid={this.state.current_pid}/>;

    return (
      <div className='VisualiserView'>
        {this.state.expanded ? <div className='VisualiserView-RightView'>
          {RightView}
        </div> : LeftView}
      </div>
    )
  }
}
export default VisualiserView;
