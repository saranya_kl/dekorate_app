/**
 * Created by vikramaditya on 10/14/15.
 */
import React from 'react';
import _ from 'lodash';
import {SvgIcon, CircularProgress} from 'material-ui';
import DropZone from 'react-dropzone';
import Services from '../../../../services/Services.js';
import Helper from '../../../helper.js';
import styles from './FilesRepository.scss';
import Svg from '../../../Shared/Svg/Svg';
import FileSize from '../../../../lib/filesize';
var {AppConstants: Constants, WorkspaceActions, WorkspaceStore, withStyles, CustomUtility} = Helper;
let NULL_LINK = 'javascript:void(0)';
import Request from 'superagent';

@withStyles(styles)
class FilesRepository extends React.Component {
  constructor() {
    super();
    //this.urls = [];
    this.state = {
      fetchingWorkSpace: false,
      finalasseturl: [],
      uploading_assets: [],//progress
      showModal: false,
      isUploading: false,
      repoChange: false
    };
  }
  getFileSize(urls) {
    let that = this;
    for(let i=0;i < urls.length;i++){
      let url = urls[i].url;
      CustomUtility.getFileSize(url)
      .then((size) => {
        let _asset = that.state.finalasseturl;
        _asset[i].size = size;
        that.setState({finalasseturl: _asset});
      })
      .catch(() => {
        let _asset = that.state.finalasseturl;
        _asset[i].size = 'n/a';
        that.setState({finalasseturl: _asset});
      })
    }
  }
  componentWillMount() {
    let that = this;
    that.unsubscribe = WorkspaceStore.listen(that.handleChange.bind(that));
    let data = WorkspaceStore.getWorkSpace(that.props.pid);
    let urls = [];
    data.finalasseturl && data.finalasseturl.map((asset, index)=> urls.push({url: asset.url, name: asset.name || `Asset ${index+1}`}));
    that.setState({finalasseturl: urls});
    that.getFileSize(urls);
  }

  handleChange() {
    let that = this;
    let pid = that.props.pid;
    let data = WorkspaceStore.getWorkSpace(pid);
    let urls = [];
    data.finalasseturl.map((asset, index)=> urls.push({url: asset.url, name: asset.name || `Asset ${index+1}`}));
    that.setState({finalasseturl: urls, uploading_assets: [], fetchingWorkSpace: false});
    that.getFileSize(urls);
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  onProgress(p) {
    let that = this;
    let progress = that.state.uploading_assets;
    let [index, percent] = p;
    progress[index].percent = Math.round(percent);
    that.setState({uploading_assets: progress});
  }

  onDrop(files) {
    let that = this, pid = that.props.pid;
    let promise_arr = [], progress = [];
    for (let i = 0; i < files.length; i++) {
      progress[i] = {url: NULL_LINK, percent: 0, name: ''};
      promise_arr.push(that.uploadService(files[i], pid, i));
    }
    Promise.all(promise_arr).then(() => {
      that.setState({isUploading: false});
    })
    .catch((err) => {
      console.log('error S3 upload', err);
    });
    that.setState({uploading_assets: progress, isUploading: true});
  }

  uploadService(file, pid, index) {
    let that = this;
    //console.log(file);
    return new Promise((resolve, reject) => {
      Services.getAPI(Constants.SERVER_URL + 'gets3signedurl', `?projectid=${pid}&filename=${file.name}&filetype=${file.type}`)
        .then(response => {
          let signed_request = response.body.data.signed_request;
          that.state.uploading_assets[index].url = response.body.data.url;
          Services.s3UploadAPI(file, signed_request, index, that.onProgress.bind(that))
            .then(response => {
              resolve('done');
            })
            .catch((err) => {
              reject('failed s3');
            });
        })
        .catch((err2) => {
          reject('failed backend');
        })
    });
  }
  changeAssetName(index, e){
    let that = this, filename = e.target.value;
    let uploading_assets = that.state.uploading_assets;
    uploading_assets[index].name = filename;
    //that.setState({uploading_assets: uploading_assets});
  }
  removeAsset(index){
    let that = this;
    let finalassets = that.state.finalasseturl;
    let r =confirm(`Remove ${finalassets[index].name} ?`);
    if(r){
      finalassets.splice(index, 1);
      that.setState({finalasseturl: finalassets, repoChange: true});
    }
  }
  saveAssetToWorkSpace(){
    let that = this, pid = that.props.pid;
    let urls = _.cloneDeep(that.state.finalasseturl);
    that.state.uploading_assets.map((asset) => {
      urls.push({name: asset.name, url: asset.url});
    });
    WorkspaceActions.addWorkspaceFiles(pid, urls);
    that.setState({repoChange: false, fetchingWorkSpace: true});
  }

  render() {
    let that = this;
    let divStyle = {
      display: 'flex',
      flexFlow: 'column',
      padding: '20px'
    };
    let finalasseturl = that.state.finalasseturl;
    let uploading_assets = that.state.uploading_assets, isUploading = that.state.isUploading, repoChange = that.state.repoChange;
    let fetchingWorkSpace = that.state.fetchingWorkSpace;
    return (
      <div style={divStyle} className="FilesRepository">
        <div className="dropzone-wrapper">
          <DropZone onDrop={that.onDrop.bind(that)} style={{width: '99.8%'}}>
            <SvgIcon style={{width: '30px',display: 'block',fill: '#999'}}>
              <path d="M19 13h-6v6h-2v-6H5v-2h6V5h2v6h6v2z"/>
            </SvgIcon>
            <span style={{fontSize:'0.85em', color:'#999'}}>Upload your new assets here.</span>
          </DropZone>
        </div>
        { uploading_assets.length > 0 && <div className="FilesRepository-label">Uploading assets</div>}
        <div className="FilesRepository-upload-container">
          {
            uploading_assets.map((asset, index) => {
              return (
                <div key={index} className="FilesRepository-upload-wrapper">
                  <input type='text' onChange={that.changeAssetName.bind(that, index)} className='FilesRepository-upload-inputName' placeholder={`Enter Asset Name`}/>
                  <div className="FilesRepository-upload-progress-wrapper">
                    <div className="FilesRepository-upload-progress" style={{width: `${asset.percent}%`}}></div>
                  </div>
                  <span className="FilesRepository-upload-progress-label">Progress: {asset.percent} % </span>
                </div>
              )
            })
          }
        </div>
        { uploading_assets.length > 0 &&
        <button onClick={that.saveAssetToWorkSpace.bind(that)} className={`FilesRepository-saveBtn ${isUploading ? 'custom-secondary-button':'custom-primary-button'}`}>{isUploading ? 'Uploading...': 'Save'} </button> }
        <div className="FilesRepository-label">Final assets</div>

        <div className="FilesRepository-finalasset">
          {
          finalasseturl.map((asset, index) => {
            return (
              <div key={index} className="FilesRepository-finalasset-wrapper">
                <a className="FilesRepository-finalasset-button" href={asset.url} target='_blank'>{asset.name || `Asset ${index+1}`}</a>
                <p className="FilesRepository-finalasset-filesize">{asset.size}</p>
                <div className='remove' onClick={that.removeAsset.bind(that, index)}>
                  <SvgIcon style={{fill:'#f05562', width:'16px', height: '16px'}}>
                    <path d="M19,6.41L17.59,5L12,10.59L6.41,5L5,6.41L10.59,12L5,17.59L6.41,19L12,13.41L17.59,19L19,17.59L13.41,12L19,6.41Z" />
                  </SvgIcon>
                  {/*<Svg iconType={'close_icon'} style={{fill:'#f05562', width:'16px', height: '16px'}}/>*/}
                </div>
              </div>
            )
          })
        }
        </div>
        {repoChange && <button onClick={that.saveAssetToWorkSpace.bind(that)} className='FilesRepository-saveBtn custom-primary-button'>Save Changes</button>}
        {fetchingWorkSpace && <CircularProgress color='green' style={{zIndex:'9999',position:'absolute',left:'50%',top:'30%'}} mode="indeterminate" />}
      </div>
    )
  }
}
export default FilesRepository;
