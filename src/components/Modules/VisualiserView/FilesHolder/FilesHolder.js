import React from 'react';
import _ from 'lodash';
import mui from 'material-ui';
import DropZone from 'react-dropzone';
import Moment from 'moment';
import styles from './FilesHolder.scss'
import Helper from '../../../helper.js';
import Services from '../../../../services/Services.js';
import CustomModal from '../../../Shared/Modal';

var {withStyles, CustomUtility, WorkspaceActions, WorkspaceStore, AppConstants: Constants, LoginStore} = Helper;
var {SvgIcon, RefreshIndicator, Avatar, ListDivider, ListItem} = mui;
let emptyFilesHolderData = {
  assets: {
    designer: [],
    shared: [],
    products: {
      list: []
    }
  },
  chat: {
    msgs: []
  }
};

@withStyles(styles) class FilesHolder extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      pid: props.pid,
      data: emptyFilesHolderData,
      isLoading: false
    }
  }

  setWorkspace(pid) {
    let data = WorkspaceStore.getWorkSpace(pid);
    if (typeof(data) === 'object') {
      this.setState({isLoading: false, data: data})
    } else {
      this.setState({isLoading: true, data: emptyFilesHolderData});
    }
  }

  componentWillMount() {
    //let that = this;
    //that.unsubscribe = WorkspaceStore.listen(that.onStatusChange.bind(this));
    //that.unsubscribe_notification = NotificationStore.listen(this.onNotification.bind(this));
    //WorkspaceActions.fetchWorkSpace(this.props.pid);
  }


  componentDidMount() {
    let that = this;
    let pid = that.props.pid;
    that.setWorkspace(pid);

  }

  componentWillReceiveProps(nextProps) {
    let that = this;
    if (nextProps.data !== that.props.data) {
      that.setWorkspace(nextProps.pid);
    }
  }

  componentWillUnmount() {
    //this.unsubscribe();
  }

  onDelete(item, tag, index) {
    if (this.state.data.assets[tag][index].id != item.id) {
      alert('Please wait refreshing...');
      //console.log('Some error');
    }
    else {
      let id = item.id;
      //let pid = ProjectStore.getProject().id;
      let pid = this.props.pid;

      var r = confirm('Delete Asset ?');
      if (r == true) {
        WorkspaceActions.deleteWorkspaceAsset({id: id, projectid: pid}, `?type=${tag}`, tag, index);

        // Optimistic Update
        let x = this.state.data;
        x.assets[tag].splice(index, 1);
        this.setState({data: x});
      }
    }
  }

  render() {
    let data = this.state.data;
    let isLoading = this.state.isLoading;
    let pid = this.props.pid;
    return (
      <div className='FilesHolder'>
        <LoaderComponent isShown={isLoading}/>

        <div className='FilesHolder-vSection-left'>
          <AssetsComponent pid={pid} data={data.assets} onDelete={this.onDelete.bind(this)}
                           onShowCart={this.props.onShow}/>
        </div>

        <div className='FilesHolder-vSection-right'>
          <ChatComponent pid={pid} data={data.chat}/>
        </div>

      </div>
    );
  }

}

class AssetsComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalOpen: false,
      item: {},
      currentPreviewIndex: 0
    }
  }

  componentDidMount() {
    window.addEventListener('keydown', this.onKeyPress.bind(this));
  }

  componentWillUnmount() {
    window.removeEventListener('keydown', this.onKeyPress.bind(this));
  }

  showImageModal(value, item = {}, index = 0) {
    this.setState({modalOpen: value, item: item, currentPreviewIndex: index});
  }

  onKeyPress(e) {
    let that = this;
    if (that.state.modalOpen && e.keyCode == 37) { //left
      let prev_index = that.state.currentPreviewIndex;
      let length = that.props.data.shared.length;
      let index = (prev_index - 1) >= 0 ? (prev_index - 1) : length - 1;
      that.setState({currentPreviewIndex: index});
    }
    if (that.state.modalOpen && e.keyCode == 39) { //right
      let prev_index = that.state.currentPreviewIndex;
      let length = that.props.data.shared.length;
      let index = (prev_index + 1) % length;
      that.setState({currentPreviewIndex: index});
    }
  }

  render() {
    let data = this.props.data, pid = this.props.pid;
    let currentPreviewIndex = this.state.currentPreviewIndex;
    let currentItem = _.result(data, `shared[${currentPreviewIndex}]`, {});
    let genImageHolderItem = (item, index, tag)=> <ImageHolderComponent  tag={tag} index={index}
                                                                        onDelete={this.props.onDelete} item={item}
                                                                        key={index}
                                                                        showPreview={this.showImageModal.bind(this, true)}/>;
    let ImageViewModal = <CustomModal style={{width:'70%', height:'90%', top:'3%', left:'15%',padding:'0'}}
                                      isOpen={this.state.modalOpen}
                                      onClose={this.showImageModal.bind(this, false)}>
      <div className="ImageViewModal">
        <div className="ImageViewModal-header">
          <span className="ImageViewModal-header-title">{currentItem.name}</span>
          <a href={currentItem.url} download="image" target="_blank" className="ImageViewModal-header-button">
            <SvgIcon style={{fill:'white'}}>
              <path d='M5,20H19V18H5M19,9H15V3H9V9H5L12,16L19,9Z' />
            </SvgIcon>
          </a>
        </div>
        <div className='ImageModal' style={{backgroundImage: `url(${currentItem.url})`}}></div>
        <div className="input-divider"></div>
        <div className='ImageModal-details'>
          <div className="ImageModal-details-wrapper">
            <span className="ImageModal-label"> Title: </span>
            <p className="ImageModal-text">{currentItem.name}</p>
          </div>
          <div className="ImageModal-details-wrapper">
            <span className="ImageModal-label"> Created at: </span>
            <p className="ImageModal-text">{Moment(currentItem.created_at).calendar()}</p>
          </div>
          <div className="ImageModal-details-wrapper">
            <span className="ImageModal-label"> Notes: </span>
            <p className="ImageModal-text">{currentItem.note}</p>
          </div>
        </div>
      </div>
    </CustomModal>;
    return (
      <div>
        {
          //<div className='FilesHolder-section'>
          //  <span className='header'>Personal Assets</span>
          //
          //  <div className='content'>
          //    <ImageUploadComponent type='designer' empty={data.shared.length == 0}/>
          //    {data.designer.map((item, index) => genImageHolderItem(item, index, 'designer'))}
          //  </div>
          //</div>
        }

        <div className='FilesHolder-section'>
          <span className='header'>Shared Assets</span><span className='subHeader'> (with Visualiser)</span>

          <div className='content'>
            <ImageUploadComponent pid = {pid} type='shared' empty={data.shared.length == 0}/>
            {data.shared.map((item, index) => genImageHolderItem(item, index, 'shared'))}
          </div>
        </div>


        <div className='FilesHolder-section'>
          <span className='header'>
            Product Carts</span>
          <span className='subHeader'> (Shared with Visualiser)</span>


          <div className='cart content'>
            <br/>
            {
              data.productcarts && data.productcarts.map((obj, index) => {
                let name = obj.name;
                let items_len = _.result(obj, 'list.length', '0');
                let requests_len = _.result(obj, 'requests.length', '0');
                return (
                  <div key={index} className="cartBox" onClick={this.props.onShowCart.bind(this,true, index)}>
                    <SvgIcon className='cartIcon' viewBox='0 0 520 520'>
                      <path
                        d='M492,0h-80c-7.719,0-14.344,5.531-15.75,13.125L391,64H16c-5.344,0-10.344,2.688-13.313,7.125s-3.516,10.094-1.453,15.031   l80,192C83.719,284.125,89.547,288,96,288h254.281l-11.625,64H128c-8.844,0-16,7.156-16,16s7.156,16,16,16h224   c7.719,0,14.344-5.531,15.75-13.125L429.344,32H496c8.844,0,16-7.156,16-16S504.844,0,496,0z M385.188,96l-11.625,64H288V96   H385.188z M256,96v64h-64V96H256z M160,96v64H66.672L40,96H160z M106.672,256L80,192h80v64H106.672z M192,256v-64h64v64H192z    M288,256v-64h79.75l-11.656,64H288zM176,416c-26.5,0-48,21.469-48,48s21.5,48,48,48s48-21.469,48-48S202.5,416,176,416zM304,416c-26.531,0-48,21.469-48,48s21.469,48,48,48s48-21.469,48-48S330.531,416,304,416z'/>
                    </SvgIcon>
                    <div className="cartName">{name} <br/> {items_len} items | {requests_len} requests</div>
                  </div>
                )
              })
            }

          </div>
        </div>

        {ImageViewModal}
      </div>
    )
  }
}

class ImageUploadComponent extends React.Component {
  constructor() {
    super();
    this.DUMMY_OBJECT = {
      creator_id: '',
      name: '',
      tag: '',
      note: '',
      preview: '',
      isUploading: '',
      url: '',
      thumb: '',
      isRead: false,
      isStarred: false
    };
    this.state = {
      modalOpen: false,
      isDragActive: false,
      modal_data: [],
      data_uploading: false,
      all_marked: false
    };
  }

  saveData() {
    let that = this;
    let pid = that.props.pid;
    let cid = LoginStore.getUserID();
    let data = _.cloneDeep(this.state.modal_data);
    for (let i = 0; i < data.length; i++) {
      delete data[i].preview;
      delete data[i].isUploading;
      data[i].creator_id = cid;
    }
    WorkspaceActions.addWorkSpaceAsset({projectid: pid, data: data}, `?type=${this.props.type}`, this.props.type);
    this.setState({modalOpen: false, modal_data: [],all_marked: false});
  }

  showImageUploadModal(value) {
    this.setState({modalOpen: value});
  }

  onDrop(files) {
    var x = this.state.modal_data;
    let length = this.state.modal_data.length;
    for (let i = 0; i < files.length; i++) {
      let data = _.cloneDeep(this.DUMMY_OBJECT);
      data.preview = files[i].preview;
      data.isUploading = true;
      data.name = files[i].name.substring(0, files[i].name.lastIndexOf('.'));
      this.uploadImage(files[i], length + i);
      x.push(data);
    }
    this.setState({modal_data: x, isDragActive: false});
  }

  uploadImage(file, index) {
    let that = this;
    let pid = that.props.pid;
    Services.uploadAPI(Constants.SERVER_URL + 'upload', file, `?type=workspace&projectid=${pid}&thumb=true`)
      .then((response)=> {
        let url = response.body.url;
        let thumb = response.body.thumb;
        let x = this.state.modal_data;
        if (x[index]) {
          x[index].url = url;
          x[index].thumb = thumb;
          x[index].isUploading = false;
          x[index].preview = thumb;
          this.setState({modal_data: x});
        }
      }
    );
  }

  handleNameChange(index, tag, e) {
    var x = this.state.modal_data;
    tag == 'NAME' ? x[index].name = e.target.value : '';
    tag == 'NOTE' ? x[index].note = e.target.value : '';
    tag == 'TAG' ? x[index].tag = e.target.value : '';//not in use currently
    this.setState({modal_data: x});
  }

  removeItem(index) {
    var x = this.state.modal_data;
    x.splice(index, 1);
    this.setState({modal_data: x});
  }

  onDragOver(value) {
    this.setState({isDragActive: value});
  }
  markFinal(){
    let that = this;
    let x = that.state.modal_data;
    let length = this.state.modal_data.length;
    for(let i=0;i<length;i++){
      x[i].note = that.state.all_marked ? '' : 'Final';
    }
    that.setState({modal_data: x, all_marked: !that.state.all_marked});
  }
  render() {
    let drop_style = {
      textAlign: 'center',
      height: '120px',
      width: '80%',
      margin: '20px auto',
      backgroundColor: '#f2f2f2',
      borderRadius: '3px',
      border: this.state.isDragActive ? '1px solid green' : '1px dashed #e6e6e6'
    };
    let ImageUploadModal = <CustomModal isOpen={this.state.modalOpen} onClose={this.showImageUploadModal.bind(this, false)}
                                        style={{top:'12%', height:'70%',left:'25%', width:'50%', padding:'0'}}>
      <div className="ImageUploadModal">
        <div className="ImageUploadModal-title">
          <span>Upload Image(s)</span>
          {this.state.modal_data.length > 0 && <button className='final-button' onClick={this.markFinal.bind(this)}>
            {this.state.all_marked ? `Unmark All as Final` :`Mark All as Final`}</button>}
        </div>

        <div className='flex-col'>
          <div className="ImageUploadModal-imageWrapper">
            {this.state.modal_data.map((data, index) => {
              return (
                <div key={index} className='flex-col listholder'>
                  <div style={{position:'relative'}}>
                    {data.isUploading ?
                      <img src={data.preview} style={{'WebkitFilter': 'blur(5px)'}} width='150' height='120'/> :
                      <img src={data.preview} width='150' height='120'/>}
                    {data.isUploading && <RefreshIndicator size={40} left={55} top={40} status='loading'/>}
                  </div>

                  <div className='listholder-inputs'>
                    <input type='text' placeholder='Name' className="listholder-input" value={data.name}
                           onChange={this.handleNameChange.bind(this,index, 'NAME')}/>
                    {/*<textarea type='text' placeholder='Note' readOnly className="listholder-input" value={data.note}
                     onChange={this.handleNameChange.bind(this,index, 'NOTE')}/>*/}
                    {data.note.trim() && <label className="listholder-input">{data.note}</label>}
                  </div>
                  <SvgIcon onClick={this.removeItem.bind(this, index)} className="delete-icon">
                    <path
                      d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"/>
                  </SvgIcon>
                </div>
              )
            })}
          </div>
          <DropZone style={drop_style} onDrop={this.onDrop.bind(this)} onDragOver={this.onDragOver.bind(this, true)}
                    onDragLeave={this.onDragOver.bind(this, false)}>
            {!this.state.isDragActive && <div className="DropZone-wrapper">
              <SvgIcon style={{fill:'#808080'}}>
                <path d={Constants.PLUS_ICON}/>
              </SvgIcon>
              <span
                style={{fontSize:'0.7em',color:'#808080'}}> Drop your files here, or click to select files to upload. </span>
            </div>
            }
          </DropZone>
        </div>
        <div className="input-divider"></div>
        <div className="Save-wrapper">
          <button onClick={this.saveData.bind(this)} className="Save-button">Save</button>
        </div>
      </div>
    </CustomModal>;
    let upload_style = {width: this.props.empty ? '96%' : '150px'};
    return (
      <div className='imageHolder' style={upload_style}>
        <div className='uploadBox' onClick={this.showImageUploadModal.bind(this, true)}>
          <SvgIcon>
            <path fill='#808080' d='M9 16h6v-6h4l-7-7-7 7h4zm-4 2h14v2H5z'/>
          </SvgIcon>
        </div>
        {ImageUploadModal}
      </div>
    );
  }
}

class ProductListUploader extends React.Component {
  constructor() {
    super();
    this.state = {
      showUploadModal: false
    }
  }

  onOpenUploadModal() {
    console.log('show modal');
    this.setState({
      showUploadModal: true
    });
  }

  onCloseUploadModal() {
    console.log('hide modal');
    this.setState({
      showUploadModal: false
    });
  }

  render() {
    let self = this;
    let showModal = this.state.showUploadModal;
    let productUploadModal = (showModal) => {
      let x = showModal ? 'block' : 'none';
      return (
        <CustomModal
          isOpen={showModal}
          onClose={self.onCloseUploadModal.bind(self)}
          style={{height: '80%', width: '50%',top: '10%'}}
          >
          <div>
            <input />
          </div>
        </CustomModal>
      )
    };


    return (
      <div className='imageHolder'>
        <div className='uploadBox' onClick={self.onOpenUploadModal.bind(self)}>
          <SvgIcon>
            <path fill='#000000' d='M19,13H13V19H11V13H5V11H11V5H13V11H19V13Z'/>
          </SvgIcon>
        </div>
        {productUploadModal(showModal)}
      </div>
    )
  }
}

class ChatComponent extends React.Component {
  constructor() {
    super();
    this.DUMMY_CHAT = {backenduserid: '', name: '', imageUrl: '', text: '', created_at: ''};
    this.state = {
      value: '',
      data: []
    }
  }

  componentWillMount() {
    var d = _.cloneDeep(this.props.data.msgs);
    this.setState({data: _.cloneDeep(d.reverse())});
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.data.msgs !== this.state.data) {
      var d = _.cloneDeep(nextProps.data.msgs);
      this.setState({data: d});
    }
  }

  KeyPress(e) {
    if (e.keyCode === 13) {
      this.postMessage();
    }
  }

  postMessage() {
    let that = this;
    if (that.state.value.trim() != '') {
      let pid = that.props.pid;
      let userid = LoginStore.getUserID();
      let user = LoginStore.getUserInfo();

      let obj = _.cloneDeep(that.DUMMY_CHAT);
      obj.text = _.cloneDeep(that.state.value);
      obj.name = `${user.name}`;
      obj.backenduserid = userid;
      obj.imageUrl = user.imageUrl;
      obj.created_at = Date.now();
      let x = that.state.data;
      x.unshift(obj);
      WorkspaceActions.addChat({
        projectid: pid,
        text: obj.text,
        backenduserid: LoginStore.getUserID(),
        name: obj.name,
        imageUrl: obj.imageUrl
      }, obj);
      that.setState({value: '', data: x});
    }
  }

  valueChange(e) {
    let val = e.target.value;
    this.setState({value: val});
  }

  render() {
    let self = this;
    let value = self.state.value;
    let genChatMsg = (data, index) => <ChatMsgComponent data={data} key={index}/>;
    return (
      <div className="Chat-top-section">
        <div className="addMessage">
          <input type='text' ref="input" className="addMessage--input" onKeyDown={self.KeyPress.bind(self)}
                 placeholder='Send a message' value={value}
                 onChange={self.valueChange.bind(self)}/>
          <SvgIcon style={{fill:'#808080',zoom:'0.75', cursor:'pointer'}} onClick={self.postMessage.bind(self)}
                   onKeyDown={self.KeyPress.bind(self)}>
            <path d="M2.01 21L23 12 2.01 3 2 10l15 2-15 2z"/>
          </SvgIcon>
        </div>
        <div className="ChatBox">
          {self.state.data.map(genChatMsg)}
        </div>
      </div>
    )
  }
}

class ChatMsgComponent extends React.Component {
  parseMessage(text){
    let regEx = /(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/g;
    text = text.replace(regEx, (x) => `<a target='_blank' href=${x}>${x}</a>`);
    return text;
  }
  render() {
    let data = this.props.data;
    let self_userid = LoginStore.getUserID();
    let text = data.text ? this.parseMessage(data.text) : '';
    return (
      <div>
        { (data.backenduserid == self_userid) ?
          <div className="ChatBox-chatMsg user">
            <div className="ChatBox-chatMsg-Wrapper user">
              <div className="ChatBox-chatMsg-header">
                <span className="ChatBox-chatMsg-header-name">{data.name}</span>
                <span className="ChatBox-chatMsg-header-timestamp">{Moment(data.created_at).calendar()}</span>
              </div>
              <span dangerouslySetInnerHTML={{__html: text}}/>
            </div>
            <Avatar src={data.imageUrl} size={30} style={{marginTop: '7px'}}/>
          </div> :
          <div className="ChatBox-chatMsg frnd">
            <Avatar src={data.imageUrl} size={30} style={{marginTop: '7px'}}/>

            <div className="ChatBox-chatMsg-Wrapper frnd">
              <div className="ChatBox-chatMsg-header">
                <span className="ChatBox-chatMsg-header-name">{data.name}</span>
                <span className="ChatBox-chatMsg-header-timestamp">{Moment(data.created_at).calendar()}</span>
              </div>
              <span dangerouslySetInnerHTML={{__html: text}}/>
            </div>
          </div>
        }
      </div>
    )
  }
}

class LoaderComponent extends React.Component {
  render() {
    let isShown = this.props.isShown;
    return (
      <div>
        {isShown ?
          <div
            style={{zIndex: '1',alignItems:'center',justifyContent:'center',left:'0',top:'0',display:'flex',position:'absolute',background:'rgba(0,0,0,0.42)',width:'100%',height:'100%'}}>
            <img style={{width:'50px',height:'50px'}} src={Constants.LOADER_GIF}/>
          </div>
          : null
        }</div>
    )
  }
}

class ImageHolderComponent extends React.Component {
  constructor() {
    super();
    this.state = {
      hoverMode: false
    }
  }

  onImageHoverEnter(e) {
    this.setState({
      hoverMode: true
    });
    e.preventDefault();
  }

  onImageHoverLeave(e) {
    this.setState({
      hoverMode: false
    });
    e.preventDefault();
  }

  downloadImage(url, name) {
    CustomUtility.saveFile(url, name)
      .then(() => {console.log('done')})
      .catch(() => {console.log('failed')});
  }

  deleteImage(item) {
    this.props.onDelete(item, this.props.tag, this.props.index);
  }

  render() {
    let self = this;
    let item = self.props.item;
    let downloadImage = this.downloadImage.bind(this, item.url, item.name);
    let deleteImage = this.deleteImage.bind(this, item);
    //let showModal = this.props.showPreview.bind(this, item.url, item.name, item.note);
    let showModal = this.props.showPreview.bind(this, item, self.props.index);
    var getHoverComponent = function () {
      return (
        <div className='hoverDiv'>
          <div onClick={showModal} className='viewBtn'>
            <SvgIcon>
              <path fill='#000000'
                    d='M12,9A3,3 0 0,0 9,12A3,3 0 0,0 12,15A3,3 0 0,0 15,12A3,3 0 0,0 12,9M12,17A5,5 0 0,1 7,12A5,5 0 0,1 12,7A5,5 0 0,1 17,12A5,5 0 0,1 12,17M12,4.5C7,4.5 2.73,7.61 1,12C2.73,16.39 7,19.5 12,19.5C17,19.5 21.27,16.39 23,12C21.27,7.61 17,4.5 12,4.5Z'/>
            </SvgIcon>
          </div>
          <a href={item.url} download="image" target="_blank" className="ImageViewModal-header-button">
            <SvgIcon>
              <path fill='#000000' d='M5,20H19V18H5M19,9H15V3H9V9H5L12,16L19,9Z'/>
            </SvgIcon>
          </a>
          <div onClick={deleteImage} className='crossBtn'>
            <SvgIcon>
              <path fill='#000000'
                    d='M19,6.41L17.59,5L12,10.59L6.41,5L5,6.41L10.59,12L5,17.59L6.41,19L12,13.41L17.59,19L19,17.59L13.41,12L19,6.41Z'/>
            </SvgIcon>
          </div>
        </div>
      );
    };

    return (
      <div className='imageHolder' onMouseEnter={self.onImageHoverEnter.bind(self)}
           onMouseLeave={self.onImageHoverLeave.bind(self)}>
        <img src={item.thumb} className='image'/>
        {item.note && <div className='note-overlay'></div> }
        {item.note && <div className='note-text'>{item.note}</div>}
        <div className='label'>{item.name}</div>
        {self.state.hoverMode ? getHoverComponent() : null}
      </div>
    )
  }

}

export default FilesHolder;
/*<button className="addMessage--button" onClick={self.postMessage.bind(self)}> Send</button>*/
