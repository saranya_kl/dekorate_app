/**
 * Created by vikramaditya on 10/21/15.
 */
import React from 'react';
import ProductScreen from '../../Roles/Designer/components/ProductScreen';
class ProductManagerView extends React.Component {
  constructor() {
    super();
  }

  render() {
    return (
      <ProductScreen />
    )
  }
}
export default ProductManagerView;
