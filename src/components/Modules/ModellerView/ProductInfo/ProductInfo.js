import React from 'react';
import Router from 'react-router';
import mui from 'material-ui';

import InfoToolbar from './InfoToolbar';
import InfoContent from './InfoContent';
import Helper from '../../../helper.js';
import styles from './ProductInfo.scss';
var {Colors,Spacing} = mui.Styles;
var ThemeManager = new mui.Styles.ThemeManager();
var {withStyles} = Helper;
@withStyles(styles) class ProductInfo extends React.Component {

  static contextTypes = {
    router: React.PropTypes.func
  };

  constructor() {
    super();
    this.state = {
    };
  }

  render() {
    let self = this;

    return (
      <div className="product-info">
          <InfoToolbar />
          <InfoContent />
      </div>
    );
  }
}
export default ProductInfo;
