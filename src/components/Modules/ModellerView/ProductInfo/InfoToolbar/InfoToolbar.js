import React from 'react';
import Router from 'react-router';
import mui from 'material-ui';
import Helper from '../../../../helper.js';
import styles from './InfoToolbar.scss';
var {Colors,Spacing} = mui.Styles;
var ThemeManager = new mui.Styles.ThemeManager();
var {withStyles} = Helper;
@withStyles(styles) class InfoToolbar extends React.Component {

  static contextTypes = {
    router: React.PropTypes.func
  };

  constructor() {
    super();
    this.state = {
      status: true
    };
  }
 changeStatus() {
   this.setState({status:false});
 }

  render() {
    let self = this;
    let startStyle = { border: '1px solid lightgreen' };
    let workingStyle = { border: '1px solid orange' };
    return (
      <div className="info-toolbar">
        <div className="info-toolbar-title">
          <span>Lamp</span>
          <span> 5 hours ago </span>
        </div>
        <div className="info-toolbar-points">
          <span> 500 </span>
          <span> +300 </span>
        </div>
        <div >
          <button className="info-toolbar-status" style={self.state.status ? startStyle : workingStyle} disabled={!self.state.status} onClick={self.changeStatus.bind(self)}> {self.state.status ? 'Start' : 'Working'} </button>
        </div>
      </div>
    );
  }
}
export default InfoToolbar;

//disabled={self.state.status=='Start'?false:true}
//
//
