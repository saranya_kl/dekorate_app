import React from 'react';
import Router from 'react-router';
import mui from 'material-ui';

import ProductDetails from './ProductDetails';
import Helper from '../../../../helper.js';
import styles from './InfoContent.scss';
import DropZone from 'react-dropzone';
//import ImageModal from '../../../../../../CrmApp/components/TicketInbox/ImageModal/ImageModal.js';

var {Tabs, Tab} = mui;
var {Colors,Spacing} = mui.Styles;
var ThemeManager = new mui.Styles.ThemeManager();
var {withStyles} = Helper;
@withStyles(styles) class InfoContent extends React.Component {

  static contextTypes = {
    router: React.PropTypes.func,
    muiTheme: React.PropTypes.object
  }

  constructor() {
    super();
    this.state = {
      previewURL: '',
      data: [],
      showModal: false
    };
  }
  uploadImage(files) {
    var arr = [];
    for(var i=0;i<files.length;i++) {
      arr.push(files[i].preview);
    }
    console.log(arr);
    this.setState({data:arr, previewURL:files[0].preview});

  }
  setPreview(url){
    this.setState({previewURL:url});
  }
  expandModal(){
    this.setState({showModal:true});
  }
  dismiss() {
    this.setState({showModal:false});
  }
  render() {
    let self = this;
    var row = 3, column = 3;
    var TabContainerStyle = {backgroundColor:'#808080',border: '1px solid silver', width: '100%'};
    var TabItemStyle = {color:'black', backgroundColor:'white',fontSize:'0.75em',fontWeight:'200',overflowY:'auto'};
    var images = [
      "http://1.fimg.in/p/hometown-6758-085791-5-product_432.jpg",
      "http://1.fimg.in/p/hometown-6757-085791-4-product_432.jpg",
      "http://1.fimg.in/p/hometown-6756-085791-3-product_432.jpg",
      "http://1.fimg.in/p/hometown-6756-085791-2-product_432.jpg",
      "http://1.fimg.in/p/hometown-6756-085791-1-product_432.jpg",
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTNfl6OtaKO477wTH9O0YRBYZYcm5qcbd9eehH5yw3vZP4_HsJUbA"
    ];
    return (
      <div className="info-content">
        <Tabs initialSelectedIndex={0} tabItemContainerStyle={TabContainerStyle} style={{width:'100%'}}>
          <Tab label='Details' style={TabItemStyle}>
            <ProductDetails imgarray={images} />
          </Tab>
          <Tab label='Submission' style={TabItemStyle}>
          <div style={{display:'flex',flexFlow: 'row nowrap',backgroundColor:'#f2f2f2',height: '90vh',justifyContent:'space-around',paddingTop:'30px'}}>
            <img src={self.state.previewURL} style ={{width: '200px',height:'200px',border:'1px dashed black'}} onClick={self.expandModal.bind(self)}/>
            {/* self.state.showModal ? <ImageModal url={self.state.previewURL} onDismiss={self.dismiss.bind(self)} /> : ''*/}
            <DropZone style={{paddingTop: '10px',paddingLeft:'55px'}} onDrop={self.uploadImage.bind(self)}>
              <button>Upload Image</button>
            </DropZone>
            <table className="grid-body">
              <tr>
                <td> { self.state.data[0] ? <img style={{width:'95px', height: '95px'}} src={self.state.data[0]} onClick={self.setPreview.bind(self,self.state.data[0])} /> : '' }</td>
                <td> { self.state.data[1] ? <img style={{width:'95px', height: '95px'}} src={self.state.data[1]} onClick={self.setPreview.bind(self,self.state.data[1])}/> : '' }</td>
                <td> { self.state.data[2] ? <img style={{width:'95px', height: '95px'}} src={self.state.data[2]} onClick={self.setPreview.bind(self,self.state.data[2])}/> : '' }</td>
              </tr>
              <tr>
                <td> { self.state.data[3] ? <img style={{width:'95px', height: '95px'}} src={self.state.data[3]} onClick={self.setPreview.bind(self,self.state.data[3])}/> : '' }</td>
                <td> { self.state.data[4] ? <img style={{width:'95px', height: '95px'}} src={self.state.data[4]} onClick={self.setPreview.bind(self,self.state.data[4])}/> : '' }</td>
                <td> { self.state.data[5] ? <img style={{width:'95px', height: '95px'}} src={self.state.data[5]} onClick={self.setPreview.bind(self,self.state.data[5])}/> : '' }</td>
              </tr>
              <tr>
                <td> { self.state.data[6] ? <img style={{width:'95px', height: '95px'}} src={self.state.data[6]} onClick={self.setPreview.bind(self,self.state.data[6])}/> : '' }</td>
                <td> { self.state.data[7] ? <img style={{width:'95px', height: '95px'}} src={self.state.data[7]} onClick={self.setPreview.bind(self,self.state.data[7])}/> : '' }</td>
                <td> { self.state.data[8] ? <img style={{width:'95px', height: '95px'}} src={self.state.data[8]} onClick={self.setPreview.bind(self,self.state.data[8])}/> : '' }</td>
              </tr>
            </table>
            </div>
          </Tab>
        </Tabs>
      </div>
    );
  }
}
export default InfoContent;
//<Grid data={Object.keys(self.state.data).length > 0 ? self.state.data :[]} onClick={self.setPreview.bind(self)} row={self.row} column={self.column} />
//{ self.state.previewURL=='' ? '' : <img src={self.state.previewURL} style={{width:'200px',height:'200px'}}/>}
