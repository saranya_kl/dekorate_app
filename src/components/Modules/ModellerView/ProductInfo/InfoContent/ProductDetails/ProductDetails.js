import React from 'react';
import Router from 'react-router';
import mui from 'material-ui';
import Helper from '../../../../../helper.js';
import styles from './ProductDetails.scss';
//import ImageModal from '../../../../../../../CrmApp/components/TicketInbox/ImageModal/ImageModal.js';

var {SvgIcon} = mui;
var {Colors,Spacing} = mui.Styles;
var ThemeManager = new mui.Styles.ThemeManager();
var {withStyles} = Helper;
@withStyles(styles) class ProductDetails extends React.Component {

  static contextTypes = {
    router: React.PropTypes.func
  };

  constructor() {
    super();
    this.state = {
      showModal: false,
      index: 0,
      url: ''
    };
  }

  componentWillMount(){
    if(this.props.url)
      this.setState({url: this.props.url});
    else
      this.setState({url: this.props.imgarray[0]});
  }
  expandModal(){
    this.setState({showModal:true});
  }

  dismiss() {
    this.setState({showModal:false});
  }
  moveLeft(){
    if(this.state.index!=0)
    {
      var i = this.state.index - 1;
      this.setState({index:i});
    }
  }
  moveRight() {
    if(this.state.index<this.props.imgarray.length-3)
    {
      var i = this.state.index + 1;
      this.setState({index:i});
    }
  }
  setPreview(adder) {
    var ind = this.state.index + adder;
    this.setState({url:this.props.imgarray[ind]});
  }

  render() {
    let self = this;

    return (
      <div className="product-details">
        <div className="product-details-images">
          <img src={self.state.url} style={{width: 'auto', height: '300px',marginTop: '20px'}} onClick={self.expandModal.bind(self)} />
          {/* self.state.showModal ? <ImageModal url={self.state.url} onDismiss={self.dismiss.bind(self)} /> : ''*/}
          <div className="imageplay">
            <SvgIcon className="left-arrow" onClick={self.moveLeft.bind(self)}>
              <path d="M15.41 16.09l-4.58-4.59 4.58-4.59L14 5.5l-6 6 6 6z"/>
            </SvgIcon>
            { self.props.imgarray.length > 0 ?
              <div className="thumbnail-wrapper">
                {self.props.imgarray[self.state.index] ? <img src={self.props.imgarray[self.state.index]} className="thumbnail" onClick={self.setPreview.bind(self,0)} /> : ''}
                {self.props.imgarray[self.state.index+1] ? <img src={self.props.imgarray[self.state.index+1]} className="thumbnail" onClick={self.setPreview.bind(self,1)} /> : ''}
                {self.props.imgarray[self.state.index+2] ? <img src={self.props.imgarray[self.state.index+2]} className="thumbnail" onClick={self.setPreview.bind(self,2)} /> : ''}
              </div>
            : '' }
            <SvgIcon className="right-arrow" onClick={self.moveRight.bind(self)} >
              <path d="M8.59 16.34l4.58-4.59-4.58-4.59L10 5.75l6 6-6 6z"/>
            </SvgIcon>
          </div>
        </div>
        <div className="product-details-download">
          <SvgIcon className="download-icon">
            <path d="M19.35 10.04C18.67 6.59 15.64 4 12 4 9.11 4 6.6 5.64 5.35 8.04 2.34 8.36 0 10.91 0 14c0 3.31 2.69 6 6 6h13c2.76 0 5-2.24 5-5 0-2.64-2.05-4.78-4.65-4.96zM17 13l-5 5-5-5h3V9h4v4h3z"/>
          </SvgIcon>
        </div>
        <div className="product-details-info">
          <table style={{margin: '10px', marginTop: '10%'}}>
            <tr>
              <td> URL: </td>
              <td>
                <a href="http://homesdir.net/wp-content/uploads/2014/12/wildwood-antique-copper-table-lamp-lamp.jpg">
                  http://homesdir.net/wp-content/uploads/2014/12/wildwood-antique-copper-table-lamp-lamp.jpg
                </a>
              </td>
            </tr>
            <tr>
              <td> Dimensions: </td>
              <td> 200x300x400 </td>
            </tr>
            <tr>
              <td> Color: </td>
              <td> Brown </td>
            </tr>
            <tr>
              <td> Material: </td>
              <td> Brass </td>
            </tr>
            <tr>
              <td> Notes: </td>
              <td> Make sure you get the essence of the brown brass correctly </td>
            </tr>
          </table>
        </div>
      </div>
    );
  }
}
export default ProductDetails;
