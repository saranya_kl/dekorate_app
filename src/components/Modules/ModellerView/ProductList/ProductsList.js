import React from 'react';
import Router from 'react-router';
import mui from 'material-ui';

import NewProducts from '../NewProducts';
import Helper from '../../../helper.js';
import styles from './ProductsList.scss';
var {withStyles} = Helper;
var {Tabs,Tab,SvgIcon} = mui;
var {Colors,Spacing} = mui.Styles;
var ThemeManager = new mui.Styles.ThemeManager();

@withStyles(styles) class ProductsList extends React.Component {

  static contextTypes = {
    router: React.PropTypes.func
  };

  constructor() {
    super();
    this.state = {
    };
  }

  render() {
    let UserProject = [
      {
        id: 1,
        title: 'Table Lamp',
        url: 'http://homesdir.net/wp-content/uploads/2014/12/wildwood-antique-copper-table-lamp-lamp.jpg',
        created_at: '2015-06-22T10:45:17.968Z',
        points: '500',
        bonus_points: '200',
        need_attention: false,
        estimated_time: '5 hours',
        status: 'start'

      },
      {
        id: 2,
        title: 'Sofa',
        url: 'http://www.ikea.com/PIAimages/0312397_PE429728_S5.JPG',
        created_at: '2015-05-22T10:45:17.968Z',
        points: '1500',
        bonus_points: '500',
        need_attention: true,
        estimated_time: '10 hours',
        status: 'start'
      },
      {
        id: 3,
        title: 'Table',
        url: 'http://www.ikea.com/PIAimages/20315_PE105482_S5.JPG',
        created_at: '2015-08-02T10:45:17.968Z',
        points: '300',
        bonus_points: '100',
        need_attention: true,
        estimated_time: '3 hours',
        status: 'start'
      }
    ];
    let self = this;
    var TabContainerStyle = {backgroundColor:'silver',border: '1px solid silver', width: '100%'};
    var TabItemStyle = {color:'black', backgroundColor:'white',fontSize:'0.75em',fontWeight:'200',overflowY:'auto'};

    return (
      <div className="products-list">
        <Tabs initialSelectedIndex={0} tabItemContainerStyle={TabContainerStyle} style={{width:'100%'}}>
          <Tab label='New Products' style={TabItemStyle}>
            <NewProducts expanded={false} data={UserProject} />
          </Tab>
          <Tab label='My Products' style={TabItemStyle}>
          </Tab>
        </Tabs>
      </div>
    );
  }
}
export default ProductsList;
