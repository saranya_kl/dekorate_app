import React from 'react';
import Router from 'react-router';
import mui from 'material-ui';
import Helper from '../../../helper.js';
import styles from './NewProducts.scss';
import Moment from 'moment';

var {ListItem,ListDivider,List,Checkbox,Avatar,SvgIcon} = mui;
var {Colors,Spacing} = mui.Styles;
var ThemeManager = new mui.Styles.ThemeManager();
var {withStyles} = Helper;
@withStyles(styles) class NewProducts extends React.Component {

  static contextTypes = {
    router: React.PropTypes.func,
    muiTheme: React.PropTypes.object
  }

  constructor() {
    super();
    this.state = {
      hoverCheckBox: false,
      hoverCheckBoxId: ''
    };
  }

  routeToProject(pid) {
    this.context.router.transitionTo(this.context.router.getCurrentPath() + '/projects/' + pid);
  }

  onHover(arg, pid) {
    this.setState({hoverCheckBox: arg, hoverCheckBoxId: pid});
  }

  shrinkTab(pid) {

  }

  render() {
    let self = this;


    return (
      <div className="new-products">
        <List>
          {
            (self.props.data.length > 0) ? self.props.data.map(function (project, index) {
              let currentStyle = {
                backgroundColor: '#f2f2f2',
                marginBottom: '1px',
                paddingLeft: '5px',
                paddingRight: '5px'
              };
              return ([
                <ListDivider/>,
                <ListItem onClick={self.shrinkTab.bind(self,project.id)} style={currentStyle}>

                  <div className="new-products-item">
                    <div className="new-products-item-wrapper">
                      <Avatar style={{zoom:'0.6'}} src={project.url}/>
                      <div className="new-products-item-title">
                        {project.title}
                        {Moment(project.created_at).fromNow()}
                      </div>
                      {project.need_attention ? <SvgIcon style={{zoom: '0.6',fill: 'red',position: 'absolute', right: '40%',top: '35%'}}>
                            <path d="M11 15h2v2h-2zm0-8h2v6h-2zm.99-5C6.47 2 2 6.48 2 12s4.47 10 9.99 10C17.52 22 22 17.52 22 12S17.52 2 11.99 2zM12 20c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8z"/>
                          </SvgIcon> : '' }
                      <div className="new-products-item-time">
                        {project.points}
                        <span> +{project.bonus_points} </span>
                      </div>
                    </div>
                  </div>
                </ListItem>
              ])
            }) : ''
          }
        </List>
      </div>
    );
  }
}
export default NewProducts;

//{Moment(project.attributes.meta.created_at).calendar()}
