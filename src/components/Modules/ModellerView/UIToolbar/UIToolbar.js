import React from 'react';
import Router from 'react-router';
import mui from 'material-ui';
import Reflux from 'reflux';
import Helper from '../../../helper.js';
import styles from './UIToolbar.scss';
var {withStyles} = Helper;
var {Checkbox,FlatButton,SvgIcon,TextField,Avatar} = mui;
var {Colors,Spacing} = mui.Styles;
var ThemeManager = new mui.Styles.ThemeManager();

@withStyles(styles) class UIToolbar extends React.Component {

  static contextTypes = {
    router: React.PropTypes.func,
    muiTheme: React.PropTypes.object
  };

  constructor() {
    super();
    this.state = {}
  }


  render() {
    var projectcount = "3000";
    var self = this;
    // var theme = this.getTheme();
    return (
      <div className="topToolBar" style={{ backgroundColor: 'white', height: '61px',width: '100%'}}>
        <div className="toolBarButtons">

          <div className="checkBoxWrapper">
            <SvgIcon className="refresh">
              <path
                d="M12 5V1L7 6l5 5V7c3.31 0 6 2.69 6 6s-2.69 6-6 6-6-2.69-6-6H4c0 4.42 3.58 8 8 8s8-3.58 8-8-3.58-8-8-8z"></path>
            </SvgIcon>
          </div>

          <div className="searchbar">
            <div>
              <SvgIcon className="search">
                <path
                  d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"></path>
              </SvgIcon>
              <input type="text" className="searchBox" placeholder="Search"/>
            </div>
          </div>
        </div>
        <div style={{alignSelf:'center'}}>
          <div>
            <span className="project-count"> Total: {projectcount} </span>
            <Avatar>A</Avatar>
          </div>
        </div>
      </div>
    );
  }
}
export default UIToolbar;
