/**
 * Created by vikramaditya on 10/15/15.
 */
import React from 'react';
import UIToolbar from './UIToolbar/UIToolbar.js';
import ProductsList from './ProductList/ProductsList.js';
import ProductInfo from './ProductInfo';
import Helper from '../../helper.js';
import styles from './ModellerView.scss';
var {withStyles} = Helper;
@withStyles(styles)
class ModellerView extends React.Component {
  static contextTypes = {
    router: React.PropTypes.func
  };
  constructor() {
    super();
  }

  componentWillMount() {

  }

  componentWillUnmount() {

  }

  render() {
    return (
      <div className='ModellerView'>
        <div className="home-page-container">
          <div className="home-page-main">
            <ProductsList />
            <ProductInfo />
          </div>
        </div>
      </div>
    )
  }
}
export default ModellerView;
