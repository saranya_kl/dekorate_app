/**
 * Created by vikramaditya on 6/18/15.
 */
import React from 'react';
import {LoginStore} from '../helper.js';

class UnAuthorized extends React.Component {
  static contextTypes = {
    router: React.PropTypes.func
  };
  constructor() {
    super();
  }
  logout() {
    LoginStore.logoutUser();
  }
  render() {
    return (
      <div style={{marginLeft:'20px'}}>
        <h3>
          Access denied
        </h3>
        <p>You are not authorized to view this site.</p>
        <button onClick={this.logout.bind(this)}>Logout</button>
      </div>
    );
  }
}
export default UnAuthorized;
