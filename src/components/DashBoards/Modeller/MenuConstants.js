/**
 * Created by vikramaditya on 9/9/15.
 */
export default ({
  MENU_ICONS: [
    {
      path: 'M10,20V14H14V20H19V12H22L12,3L2,12H5V20H10Z',
      payload: '/',
      regex: '^\/modeller\/projects',
      title: 'Home'
    },
    {
      path: 'M12,4A4,4 0 0,1 16,8A4,4 0 0,1 12,12A4,4 0 0,1 8,8A4,4 0 0,1 12,4M12,14C16.42,14 20,15.79 20,18V20H4V18C4,15.79 7.58,14 12,14Z',
      payload: 'profile',
      regex: '^\/modeller\/profile',
      title: 'Profile'
    }
  ]
})
