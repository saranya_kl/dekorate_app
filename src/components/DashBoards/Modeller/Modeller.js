/**
 * Created by vikramaditya on 8/3/15.
 */
import React from 'react';
import Router from 'react-router';
import Helper from '../../helper.js';
import Menu from '../../Shared/SideMenu/SideMenu.js';
import MenuConstants from './MenuConstants.js';
import {CircularProgress} from 'material-ui';
var RouteHandler = Router.RouteHandler;
//import styles from './Visualiser.scss';
var {LoginStore, withStyles, withAuthentication, AuthConstants} = Helper;
@withAuthentication
//@withStyles(styles)
class Modeller extends React.Component {
  static contextTypes = {
    router: React.PropTypes.func
  };
  static childContextTypes = {
    setProgressVisibility: React.PropTypes.func,
    inProgress: React.PropTypes.func
  };
  constructor() {
    super();
    this.setProgressVisibility = this.setProgressVisibility.bind(this);
    this.state = {
      inProgress: false
    }
  }
  setProgressVisibility(value){
    this.setState({inProgress:value});
  }
  inProgress(){
    return this.state.inProgress;
  }
  getChildContext() {
    return {
      setProgressVisibility: this.setProgressVisibility
    };
  }
  componentWillMount() {
    LoginStore.setDashboardVal(AuthConstants.Dashboards.MODELLER);
  }

  render() {
    return (
      <div style={{display:'flex', flexFlow:'row',background: '#fff',fontWeight:'300'}}>
        <Menu Constants={MenuConstants}/>
        {this.state.inProgress && <CircularProgress color='green' style={{zIndex:'9999',position:'absolute',left:'50%',top:'30%'}} mode="indeterminate" />}
        <RouteHandler />
      </div>
    )
  }
}
export default Modeller;
