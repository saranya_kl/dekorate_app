/**
 * Created by vikramaditya on 8/3/15.
 */
import React from 'react';
import Router from 'react-router';
import mui from 'material-ui';
import Helper from '../../helper.js';
import Menu from '../../Shared/SideMenu/SideMenu.js';
import MenuConstants from './MenuConstants.js';
var {withAuthentication,AuthConstants,LoginStore} = Helper;
var {CircularProgress} = mui;
var ThemeManager = new mui.Styles.ThemeManager();
var RouteHandler = Router.RouteHandler;

@withAuthentication
class Admin extends React.Component {
  static contextTypes = {
    router: React.PropTypes.func
  };
  static childContextTypes = {
    muiTheme: React.PropTypes.object,
    setProgressVisibility: React.PropTypes.func,
    inProgress: React.PropTypes.func
  };
  componentWillMount(){
    LoginStore.setDashboardVal(AuthConstants.Dashboards.ADMIN);
  }
  constructor(){
    super();
    this.setProgressVisibility = this.setProgressVisibility.bind(this);
    this.state = {
      inProgress: false
    }
  }
  setProgressVisibility(value){
    this.setState({inProgress:value});
  }
  inProgress(){
    return this.state.inProgress;
  }
  getChildContext() {
    return {
      muiTheme: ThemeManager.getCurrentTheme(),
      setProgressVisibility: this.setProgressVisibility
    };
  }
  render(){
    return(
      <div style={{display:'flex', flexFlow:'row',background: '#fff',fontWeight:'300'}}>
        <Menu Constants = {MenuConstants}/>
        {this.state.inProgress && <CircularProgress color='green' style={{zIndex:'9999',position:'absolute',left:'50%',top:'30%'}} mode="indeterminate" />}
        <RouteHandler />
      </div>
    )
  }
}
//Designer.childContextTypes = {
//  muiTheme: React.PropTypes.object,
//  setProgressVisibility: React.PropTypes.func,
//  inProgress: React.PropTypes.func
//};
export default Admin;
