/**
 * Created by vikramaditya on 8/10/15.
 */
import React from 'react';
import Helper from '../helper.js';
import styles from './DashBoards.scss';
var {LoginStore, RouteCategories, AppConstants: Constants,withAuthentication, withStyles} = Helper;
@withAuthentication
@withStyles(styles)
class DashBoards extends React.Component {
  static contextTypes = {
    router: React.PropTypes.func
  };
  constructor() {
    super();

    this.state = {
      roles: []
    }
  }
  routeToHome(value) {
    LoginStore.setDashboard(value);
  }

  render() {
    let type = LoginStore.getDashboards();
    let loginButtons = [];
    type.forEach((item, index) => {
      loginButtons.push(
        <div key={index} className="LoginComponent">
          <button className="LoginComponent-button" onClick={this.routeToHome.bind(this,item)}>{item}</button>
        </div>
      );
    });
    return (
      <div className="DashBoards">
        <h3 className="DashBoards-title">Login as</h3>

        <div className="DashBoards-subtitle"> Choose a dashboard</div>
        <div className="DashBoards-content">
          {loginButtons}
        </div>
      </div>
    )
  }
}

export default DashBoards;
