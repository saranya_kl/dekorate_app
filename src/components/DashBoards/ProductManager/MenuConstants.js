/**
 * Created by vikramaditya on 9/9/15.
 */
import Helper from '../../helper.js';

var {ResourceConstants : Resource, AuthConstants} = Helper;
let Roles = AuthConstants.Roles;
export default ({
  MENU_ICONS: [
    {
      path: 'M5.5,21C4.72,21 4.04,20.55 3.71,19.9V19.9L1.1,10.44L1,10A1,1 0 0,1 2,9H6.58L11.18,2.43C11.36,2.17 11.66,2 12,2C12.34,2 12.65,2.17 12.83,2.44L17.42,9H22A1,1 0 0,1 23,10L22.96,10.29L20.29,19.9C19.96,20.55 19.28,21 18.5,21H5.5M12,4.74L9,9H15L12,4.74M12,13A2,2 0 0,0 10,15A2,2 0 0,0 12,17A2,2 0 0,0 14,15A2,2 0 0,0 12,13Z',
      payload: '/',
      regex: '^\/operations\/products',
      title: 'Product Repository',
      resourceName: Resource.ProductRepository
    },
    {
      path: 'M10,20V14H14V20H19V12H22L12,3L2,12H5V20H10Z',
      payload: 'projects',
      regex: '^\/operations\/projects',
      title: 'Home',
      roles: [Roles.OPERATIONS_LEAD]
    },
    {
      path: 'M17,18C15.89,18 15,18.89 15,20A2,2 0 0,0 17,22A2,2 0 0,0 19,20C19,18.89 18.1,18 17,18M1,2V4H3L6.6,11.59L5.24,14.04C5.09,14.32 5,14.65 5,15A2,2 0 0,0 7,17H19V15H7.42A0.25,0.25 0 0,1 7.17,14.75C7.17,14.7 7.18,14.66 7.2,14.63L8.1,13H15.55C16.3,13 16.96,12.58 17.3,11.97L20.88,5.5C20.95,5.34 21,5.17 21,5A1,1 0 0,0 20,4H5.21L4.27,2M7,18C5.89,18 5,18.89 5,20A2,2 0 0,0 7,22A2,2 0 0,0 9,20C9,18.89 8.1,18 7,18Z',
      payload: 'catalog',
      regex: '^\/operations\/catalog',
      title: 'Product Catalog',
      resourceName: Resource.ProductCatalog
    },
    {
      path: 'M12,4A4,4 0 0,1 16,8A4,4 0 0,1 12,12A4,4 0 0,1 8,8A4,4 0 0,1 12,4M12,14C16.42,14 20,15.79 20,18V20H4V18C4,15.79 7.58,14 12,14Z',
      payload: 'profile',
      regex: '^\/operations\/profile',
      title: 'Profile',
      resourceName: Resource.Profile
    }
  ]
})
