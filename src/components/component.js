/**
 * Created by vikramaditya on 8/6/15.
 */
export default {
  App: require('./App'),
  DashBoards: require('./DashBoards'),
  Login: require('./Login'),
  NotFoundPage: require('./NotFoundPage'),
  UnAuthorized: require('./UnAuthorized'),
  ProfileViewer: require('./ProfileViewer'),
  AdminStats: require('./AdminStats'),
  Modeller: require('./DashBoards/Modeller'),
  ModellerView: require('./Modules/ModellerView/ModellerView'),
  Designer: require('./Roles/Designer'),
  Visualiser: require('./DashBoards/Visualiser'),
  VisualiserView: require('./Modules/VisualiserView/VisualiserView.js'),
  Admin: require('./DashBoards/Admin'),
  ProductManager: require('./DashBoards/ProductManager/ProductManager'),
  ProductManagerView: require('./Modules/ProductManagerView/ProductManagerView'),
  AllUsers: require('./AllUsers'),
  DesignerStats: require('./DesignerStats'),
  ProductCatalog: require('./Shared/ProductCatalog'),
  Moodboard: require('./Shared/Moodboard/Moodboard'),

  Postman: require('./Roles/Admin/Postman'),

  HomePage: require('./Roles/Designer/components/HomePage'),
  ProductScreen: require('./Roles/Designer/components/ProductScreen'),
  ProductsScreen: require('./Roles/Designer/components/ProductScreen/ProductsScreen')
}
