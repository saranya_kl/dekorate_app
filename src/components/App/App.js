/*! React Starter Kit | MIT License | http://www.reactstarterkit.com/ */

import React, { PropTypes } from 'react';
import mui from 'material-ui';
import Router from 'react-router';
import styles from './App.less';
import customTheme from '../../lib/MUI_Theme/custom-theme';
import withStyles from '../../decorators/withStyles';
import Helper from '../helper.js';
var PUBNUB = require('pubnub');
global.PUBNUB = PUBNUB;

var RouteHandler = Router.RouteHandler;
var ThemeManager = new mui.Styles.ThemeManager();

var {LoginStore,TriggerTypes,RouteStore,NotificationActions, AppConstants} = Helper;


@withStyles(styles) class App extends React.Component {
  static contextTypes = {
    router: React.PropTypes.func,
    muiTheme: React.PropTypes.object
  };
  static childContextTypes = {
    muiTheme: React.PropTypes.object
  };

  constructor() {
    super();
    let that = this;
    that.state = {
      showError: false
    };
    //that.printConsoleMessage();
    ThemeManager.setTheme(customTheme);
  }
  printConsoleMessage(){
    console.log('%cIf you have got any issues in website report here ->%c www.xyz.com',
      'color: red; font-weight: bold; font-size: 20px','color: skyblue;font-weight: bold; font-size: 18px');
  }
  componentDidMount() {
    this.unsubscribe = LoginStore.listen(this.onLoginChange.bind(this))
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  subscribeRealtimeApi() {

    let channel = LoginStore.getUserID();

    let pubnub = PUBNUB({
      subscribe_key: AppConstants.PUBNUB_SUBSCRIBE_KEY
    });

    pubnub.subscribe({
      channel: channel,
      message: function (message, env, ch, timer, magic_ch) {
        console.log(message);
        NotificationActions.onNewNotification(message);
        //if(message.type != 'PRODUCT_UPDATED'){
        //  NotificationActions.FETCH_ALL_NOTIFICATIONS(LoginStore.getUserID());
        //}
      }
    });
    pubnub.subscribe({
      channel: 'dekorate_global',
      message: function (message, env, ch, timer, magic_ch) {
        console.log(message);
        NotificationActions.onNewNotification(message);
      }
    });

  }
  tryAgain(){
    this.setState({showError: false});
    this.context.router.refresh();
  }
  onLoginChange(props) {

    //console.log('Change from Login Store', props);
    if (props == TriggerTypes.LOGIN_SUCCESS) {

      this.subscribeRealtimeApi();

      let transition_path = RouteStore.getnextTransitionPath() || '/dashboard';
      //console.log(RouteStore.getnextTransitionPath());
      this.context.router.transitionTo(transition_path);
    }
    if (props == TriggerTypes.CHANGE_ROUTE) {
      this.context.router.transitionTo(LoginStore.getCurrentDashboard());
      //this.context.router.transitionTo('/designer');
      // TODO hard coded value to designer
    }
    if (props == TriggerTypes.LOGOUT) {
      this.context.router.transitionTo('/login');
    }
    if (props == TriggerTypes.UNAUTHORIZED) {
      this.context.router.transitionTo('/error');
    }
    if (props == TriggerTypes.LOGIN_ERROR) {
      this.setState({showError: true});
    }
  }

  getChildContext() {
    return {
      muiTheme: ThemeManager.getCurrentTheme()
    };
  }

  render() {
    return (
      <div>
        {this.state.showError && <div style={{width: '100%', textAlign:'center'}}><p><b>Login Failed</b><br />Refresh page </p> </div>}
        <RouteHandler />
      </div>
    );
  }
}
App.childContextTypes = {
  muiTheme: React.PropTypes.object
};
export default App;
