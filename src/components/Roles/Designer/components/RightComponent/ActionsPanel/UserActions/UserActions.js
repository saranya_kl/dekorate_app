/**
 * Created by vikramaditya on 7/24/15.
 */
import React from 'react';
import mui from 'material-ui';
import Helper from '../../../../../../helper.js';
import styles from './UserActions.scss';
import QuizViewer from '../Project_Actions/QuizViewer';
var {AppConstants: Constants, UsersStore, ProjectStore, ProjectActions, withStyles, TriggerTypes, UsersActions: User_Actions} = Helper;
var {SvgIcon, ListDivider, Dialog} = mui;

@withStyles(styles)
class UserActions extends React.Component{
  static contextTypes = {
    setProgressVisibility: React.PropTypes.func
  };
  constructor(){
    super();
    this.userdetails = '';
    this.state = {
      user_details: {},
      quiz_response:[],
      currentQuizIndex:-1,
      ques: {},
      showQuizResponse: false
    };
  }
  componentWillMount(){
    let self = this;
    //let userid = ProjectStore.getProject().relationship.owner.id;
    if(ProjectStore.getProject().relationship){
      this.initialFetch();
    }
    self.unsubscribe_project_store = ProjectStore.listen(self.onStatusChange.bind(self));
    this.unsubscribe_user_store = UsersStore.listen(this.handleUserStore.bind(this));
    this.onInitialLoad()
  }
  componentWillUnmount(){
    this.unsubscribe_project_store();
    this.unsubscribe_user_store();
  }
  initialFetch(){
    let userid = ProjectStore.getProject().relationship.owner.id;
    User_Actions.fetchUserDetails(userid);
  }
  onInitialLoad(){
    let userid = ProjectStore.getProject().relationship.owner.id;
    let userdetails = UsersStore.getUser(userid);
    let quizresponse = userdetails.actions ? userdetails.actions : [];
    this.userdetails = userdetails;
    this.setState({quiz_response: quizresponse, user_details: userdetails});
  }
  handleUserStore(){
    this.context.setProgressVisibility(false);
    this.onInitialLoad();
  }
  onStatusChange(props){
    let userid = ProjectStore.getProject().relationship.owner.id;
    if(props == TriggerTypes.SENT_STYLE_QUIZ || props == TriggerTypes.USER_ACTION_TOGGLE){
      User_Actions.onUserChange(userid);
    }
    if(props == TriggerTypes.FETCHED_PROJECT){
      this.initialFetch();
    }
    if(props == TriggerTypes.RECEIVED_QUIZ){
      this.setState({ques: ProjectStore.getCurrentQuiz()});
    }
  }
  showQuiz(index){
    if(this.state.currentQuizIndex == index){
      this.setState({currentQuizIndex:-1, showQuizResponse: false});
    }else{
      ProjectActions.GET_QUIZ(this.state.quiz_response[index].quizid);
      this.setState({currentQuizIndex:index, showQuizResponse: true});
    }
  }
  onQuizCancel(qid){
    var r = confirm(`Cancel Style Quiz`);
    if (r == true) {
      let uid = ProjectStore.getProject().relationship.owner.id;
      ProjectActions.CANCEL_USER_ACTION(qid, uid);
      this.context.setProgressVisibility(true);
    }
  }
  sendStyleQuiz(){
    var r = confirm(`Send Style Quiz`);
    if (r == true) {
      let userid = ProjectStore.getProject().relationship.owner.id;
      let pid = ProjectStore.getProject().id;
      ProjectActions.REQUEST_STYLE_QUIZ(userid,pid);
      this.context.setProgressVisibility(true);
    }
  }

  dismiss(){
    this.setState({currentQuizIndex:-1,showQuizResponse:false});
  }
  render(){
    let self = this;
    let userid = ProjectStore.getProject().relationship ? ProjectStore.getProject().relationship.owner.id : '';
    return (
      <div className ='user-action'>
        <div className ='container'>
            <div className ='style-quiz-action'>
              <div className = 'send-action'>
                <span className ='send-label'>Style Quiz </span>
                <button className='send-button' onClick={self.sendStyleQuiz.bind(self)}>SEND</button>
              </div>
            </div>
          </div>
          <div className ='flex-column'>
            {
              userid != this.state.user_details.id ? <span>Loading...</span> :
              self.state.quiz_response.map((quiz,index) => {
                return (
                  <div key={index}>
                    <ListDivider className='user-action-list-divider'/>
                     <div className ='quiz-response-list'>
                      <div>Quiz Response-{index}</div>
                       <div>
                         {
                           quiz.response ?
                           (index == self.state.currentQuizIndex ?
                           <button className='show-button' onClick={self.showQuiz.bind(self,index)}>HIDE</button>
                            :
                           <button className='show-button' onClick={self.showQuiz.bind(self,index)}>VIEW</button>)
                            :
                           <button className='show-button' onClick = {self.onQuizCancel.bind(self,quiz.id)}>SENT</button>
                         }
                       </div>
                    </div>
                    <div className = 'quiz-response-container'>
                      {index == self.state.currentQuizIndex && <div className ='quiz-viewport'>
                        {this.state.showQuizResponse ? <QuizViewer question={this.state.ques} response={this.state.quiz_response[this.state.currentQuizIndex].response} onDismiss={this.dismiss.bind(this)}/> : ''}
                      </div>}
                    </div>
                  </div>
                );
              })
            }
          </div>
        <ListDivider className = 'user-action-list-divider'/>
      </div>
    );
  }
}
export default UserActions;
