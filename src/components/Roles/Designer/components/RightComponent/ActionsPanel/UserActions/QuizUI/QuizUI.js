/**
 * Created by vikramaditya on 7/16/15.
 */
import React from 'react';

import Helper from '../../../../../../../helper.js';
var {QuizUIConstants: Quiz, UsersStore, withStyles} = Helper;
import styles from './QuizUI.scss';

@withStyles(styles)
class QuizUI extends React.Component {

  constructor() {
    super();
    this.quiz = Quiz.QUIZ;
    this.user_data = [];
    this.state = {
      user_data: []
    };
  }

  //componentWillReceiveProps(nextProps){
  //  this.user_data = UserStore.getUserDetails().quizresponse[nextProps.index].response;
  //  console.log(this.user_data);
  //  this.processData();
  //}
  componentWillMount() {
    let userid = '';
    this.user_data = UsersStore.getUser(userid).actions[this.props.index].response.answers;
    this.processData();
  }

  processData() {
    var data = [];
    for (var i = 0; i < this.user_data.length; i++) {
      data[i] = this.user_data[i][0]; // confirm if this is always gonna be [0]
    }
    this.setState({user_data: data});
  }

  render() {
    var self = this;
    return (
      <div className ='quiz-ui'>
        {self.quiz.questions ? self.quiz.questions.map(function (question, index) {
          return (
            <div key={index}>
              {/*<h3 style= {self.centerStyle} >Questions</h3>*/}
              <p className ='quiz-ui-center-align'><strong>{(index + 1) + '. '}</strong>{question.question.text}</p>
              {/*<h3 style= {self.centerStyle}>Options</h3>
               <div className ='flex-row'>
               {question.options.map(function(option,index){
               return(
               <div key={index}>
               <img className ='question-image' src = {option.img}/>
               </div>
               )
               })}
               </div>
               <h3 className = 'quiz-ui-center-align'>User Answer</h3>
               */}

              <div>
                {
                  self.state.user_data.length > 0 ? (function () {
                    var img_url = [];
                    var ans = [];
                    for (var j = 0; j < question.options.length; j++) {
                      if (question.options[j].value === self.state.user_data[index]) {
                        img_url.push(question.options[j].img);
                        ans.push(question.options[j].value);
                        break;
                      }
                    }
                    return (
                      <div className = 'flex-column'>
                        <img src={img_url} className ='answer-image'/>

                        <p className='quiz-ui-center-align'>{ans}</p>
                      </div>
                    );
                  })() : <p className ='quiz-ui-center-align'>No Answer</p>
                }
              </div>
            </div>
          );
        }) : ''
        }
      </div>
    );
  }
}
export default QuizUI;
