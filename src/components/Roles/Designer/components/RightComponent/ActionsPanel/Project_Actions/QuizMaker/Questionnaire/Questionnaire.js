import React from 'react';
import {ListDivider, SvgIcon, Dialog,ListItem, TextField} from 'material-ui';
import Helper from '../../../../../../../../helper.js';
import styles from './Questionnaire.scss';
import NewQuestionFiller from './NewQuestionFiller';
import Subjective from './Subjective';
import StarRate from './StarRate';
import ImageOptions from './ImageOptions';

var {withStyles, TriggerTypes, AppConstants: Constants, ProjectActions: QuizAction, ProjectStore} = Helper;

@withStyles(styles) class Questionnaire extends React.Component {
  constructor() {
    super();
    this.question_types = ['single-text', 'subjective', 'starrate', 'single-image'];
    this.single_text_sample = {
      type: 'single-text-image',
      question: {
        text: '',
        img: ''
      },
      options: [{}, {}]
    };
    this.state = {
      close: true,
      showIndex: 0,
      showQues: 0,
      showNew: false,
      questions: [],
      createnew: true,
      current: 0,
      tempobj: {},
      edit: false,
      name: '',
      subtitle: '',
      title: ''
    }
  }

  componentWillMount() {
    var x = this.props.data.questions;
    var y = this.props.data.title;
    var z = this.props.title;
    var a = this.props.subtitle;
    this.setState({questions: x, name: y, title: z, subtitle: a});
  }

  componentWillReceiveProps(nextProps) {
    var x = nextProps.data.questions;
    var y = nextProps.data.title;
    var z = nextProps.title;
    var a = nextProps.subtitle;
    this.setState({questions: x, name: y, title: z, subtitle: a});
  }

  openQuestion(text) {
    if (this.question_types[text] == 'subjective') {
      var obj = {
        type: 'subjective',
        question: {
          text: ''
        },
        options: []
      };
      var x = _.cloneDeep(this.state.questions);
      x.push(obj);
      var curr = x.length - 1;
      this.setState({questions: x, createnew: false, showIndex: text, showQues: curr, current: curr});
    }
    else if (this.question_types[text] == 'starrate') {
      var obj = {
        type: 'single-text-starrate',
        question: {
          text: ''
        },
        options: [{
          starcount: ''
        }]
      };
      var x = _.cloneDeep(this.state.questions);
      x.push(obj);
      var curr = x.length - 1;
      this.setState({questions: x, createnew: false, showIndex: text, showQues: curr, current: curr});

    }
    else if (this.question_types[text] == 'single-text') {
      var obj = {
        type: 'single-image-text',
        question: {
          text: '',
          img: ''
        },
        options: [{}, {}]
      };
      var x = _.cloneDeep(this.state.questions);
      x.push(obj);
      var curr = x.length - 1;
      this.setState({questions: x, createnew: false, showIndex: text, showQues: curr, current: curr});

    }
    else if (this.question_types[text] == 'single-image') {
      var obj = {
        type: 'single-text-image',
        question: {
          text: ''
        },
        options: [{
          img: '',
          value: ''
        }, {
          img: '',
          value: ''
        },
          {
            img: '',
            value: ''
          }, {
            img: '',
            value: ''
          }]
      };
      var x = _.cloneDeep(this.state.questions);
      x.push(obj);
      var curr = x.length - 1;
      this.setState({questions: x, createnew: false, showIndex: text, showQues: curr, current: curr});

    }
  }

  onsubmitQuestion(obj) {
    if (this.state.edit) {
      var x = _.cloneDeep(this.state.questions);
      x[this.state.showQues] = obj;
      this.setState({questions: x, createnew: true, edit: false});
    }
    else {
      var x = _.cloneDeep(this.state.questions);
      x[this.state.current] = obj;
      this.setState({questions: x, createnew: true});
    }
  }

  showQuestion(i) {
    if (this.state.questions[i].type == 'single-text-starrate') {
      this.setState({showQues: i, showIndex: 2, edit: true, createnew: false});
    }
    else if (this.state.questions[i].type == 'single-text-text') {
      this.setState({showQues: i, showIndex: 0, edit: true, createnew: false});
    }
    else if (this.state.questions[i].type == 'single-image-text') {
      this.setState({showQues: i, showIndex: 0, edit: true, createnew: false});
    }
    else if (this.state.questions[i].type == 'subjective') {
      this.setState({showQues: i, showIndex: 1, edit: true, createnew: false});
    }
    else if (this.state.questions[i].type == 'single-text-image') {
      this.setState({showQues: i, showIndex: 3, edit: true, createnew: false});
    }
  }

  deleteQues(ind) {
    var result = confirm('Are you sure you want to delete this question?');
    if (result) {
      if (ind > 0) {
        var i = ind - 1;
      }
      else {
        var i = 0;
      }
      var x = _.cloneDeep(this.state.questions);
      x.splice(ind, 1);
      this.setState({createnew: true, showQues: i, questions: x});
    }
  }

  add() {
    this.setState({createnew: true});
  }

  getValueInput(tag, e) {
    if (tag == 'NAME') {
      var t = e.target.value;
      this.setState({name: t});
    }
    else if (tag == 'SUBTITLE') {
      var t = e.target.value;
      this.setState({subtitle: t});
    }
    else if (tag == 'TITLE') {
      var t = e.target.value;
      this.setState({title: t});
    }
  }

  getValuename() {
    var name = React.findDOMNode(this.refs.name).value;
    this.setState({name: name});
  }

  getValuetitle(e) {
    var t = e.target.value;
    this.setState({title: t});
  }

  getValuesubtitle(e) {
    var s = e.target.value;
    this.setState({subtitle: s});
  }

  goBack() {
    var result = confirm('Going back would delete this template! Are you sure?');
    if (result) {
      this.props.back();
    }
  }

  submit() {
    var obj = {};
    obj.name = this.state.name;
    obj.type = 'linear-compulsory';
    obj.title = this.state.title;
    obj.subtitle = this.state.subtitle;
    obj.questions = this.state.questions;
    if ((this.props.title == '') && (this.props.subtitle == '')) { //new quiz
      if (this.state.name && this.state.title && this.state.subtitle) {
        this.props.onsubmit(obj);
      }
      else {
        alert('Make sure to fill name, title and subtitle fields!');
      }
    }
    else { //review
      var update = confirm('Do you want to update these changes to this template?');
      if (update) {
        this.props.update(obj);
      }
      else {
        var newone = confirm('Do you want create a new template with these changes?');
        if (newone) {
          this.props.onsubmit(obj);
        }
        // else{
        //   this.props.back();
        // }
      }
    }


  }

  render() {
    let siht = this;
    return (
      <div>
        <div className="Questionnaire">
          <div onClick={siht.goBack.bind(siht)} className="Questionnaire-back">
            <SvgIcon className='left-icon' style={{fill:'black', zoom:'0.75'}}>
              <path d="M15.41 16.09l-4.58-4.59 4.58-4.59L14 5.5l-6 6 6 6z"/>
            </SvgIcon> Back
          </div>
          <TitleInputs name={siht.state.name} title={siht.state.title} subtitle={siht.state.subtitle}
                       getValueInput={siht.getValueInput.bind(siht)}/>

          <div className="Questionnaire-content flexrow">
            <div className="flexcol Questionnaire-questions">
              <span> Questions: </span>

              <div className="questions-list">
                {
                  (siht.state.questions && siht.state.questions.map(function (ques, index) {
                    let currentStyle = siht.state.showQues == index ? {border: '3px solid #3498db'} : {};
                    return (
                      <div className="questions-list-eachItem flexrow" key={index} style={currentStyle}>
                        <div key={index} onClick={siht.showQuestion.bind(siht,index)} className="content">
                          <span className="index"> {index + 1}.  </span>

                          <p className="ques"> {ques.question.text} </p>
                        </div>
                        <div className="delete-icon">
                          <SvgIcon className='cross-icon'
                                   style={{fill:'black', zoom:'0.75', cursor:'pointer'}}
                                   onClick={siht.deleteQues.bind(siht,index)}>
                            <path d={Constants.ICONS.close_icon}></path>
                          </SvgIcon>
                        </div>
                      </div>
                    );
                  }))
                }
              </div>
              <div className="AddWrapper" onClick={siht.add.bind(siht)}>
                <SvgIcon className="add-icon">
                  <path d="M19 13h-6v6h-2v-6H5v-2h6V5h2v6h6v2z"/>
                </SvgIcon>
              </div>
              <div className="buttonWrapper"
                   style={{display:'flex', flexFlow: 'row', justifyContent:'space-between', alignItems:'center'}}>
                <button className="submit-button add" onClick={siht.submit.bind(siht)}> Submit</button>
              </div>
            </div>
            <div className="Questionnaire-createnew">
              { siht.state.createnew ?
                <div>
                  <span className="questions"> Pick a type </span>
                  <table className="createnew-grid">
                    <tr className="first-row">
                      <td className="first-child">
                        <div className="type-grid-each" onClick={siht.openQuestion.bind(this,0)}>
                          Text options
                        </div>
                      </td>
                      <td className="second-child">
                        <div className="type-grid-each" onClick={siht.openQuestion.bind(this,2)}>
                          Star Rate
                        </div>
                      </td>
                    </tr>
                    <tr className="second-row">
                      <td className="first-child">
                        <div className="type-grid-each" onClick={siht.openQuestion.bind(this,1)}>
                          Subjective
                        </div>
                      </td>
                      <td className="second-child">
                        <div className="type-grid-each" onClick={siht.openQuestion.bind(this,3)}>
                          Image Options
                        </div>
                      </td>
                    </tr>
                  </table>
                </div>
                :
                <div className="fill-question">
                  { siht.question_types[siht.state.showIndex] == 'single-text' ?
                    <NewQuestionFiller data={ this.state.questions[this.state.showQues]}
                                       submitQuestion={siht.onsubmitQuestion.bind(siht)}/> : ''}
                  { siht.question_types[siht.state.showIndex] == 'subjective' ?
                    <Subjective data={this.state.questions[this.state.showQues]}
                                submitQuestion={siht.onsubmitQuestion.bind(siht)}/> : ''}
                  { siht.question_types[siht.state.showIndex] == 'starrate' ?
                    <StarRate data={this.state.questions[this.state.showQues]}
                              submitQuestion={siht.onsubmitQuestion.bind(siht)}/> : ''}
                  { siht.question_types[siht.state.showIndex] == 'single-image' ?
                    <ImageOptions data={this.state.questions[this.state.showQues]}
                                  submitQuestion={siht.onsubmitQuestion.bind(siht)}/> : ''}
                </div> }
            </div>

          </div>


        </div>

      </div>
    );
  }
}
class TitleInputs extends React.Component {
  render() {
    return (
      <div className="Questionnaire-inputlist flexrow">
        <div className="flexrow Questionnaire-inputlist-eachInput">
          <span className="name-label"> Name: </span>
          <TextField ref='name' type='text' className="name-input" hintText='Name of the template'
                     style={{width:'200px'}}
                     value={this.props.name} onChange={this.props.getValueInput.bind(this,'NAME')}/>
        </div>
        <div className="flexrow Questionnaire-inputlist-eachInput">
          <span className="name-label"> Title: </span>
          <TextField ref='title' type='text' className="name-input" hintText='Title of the template'
                     style={{width:'200px'}}
                     value={this.props.title} onChange={this.props.getValueInput.bind(this,'TITLE')}/>
        </div>
        <div className="flexrow Questionnaire-inputlist-eachInput">
          <span className="name-label"> Subtitle: </span>
          <TextField ref='subtitle' type='text' className="name-input" hintText='Subtitle of the template'
                     style={{width:'200px'}}
                     value={this.props.subtitle} onChange={this.props.getValueInput.bind(this,'SUBTITLE')}/>
        </div>
      </div>
    );
  }
}
class QuestionsList extends React.Component {
  render() {
    return (
      <div></div>
    );
  }
}
export default Questionnaire;
