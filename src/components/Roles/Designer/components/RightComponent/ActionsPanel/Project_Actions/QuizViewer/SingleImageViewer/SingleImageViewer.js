import React from 'react';
import {ListDivider, SvgIcon, Dialog,ListItem} from 'material-ui';
import Helper from '../../../../../../../../helper.js';
import styles from './SingleImageViewer.scss';


var {withStyles, TriggerTypes, AppConstants: Constants, ProjectActions: QuizAction, LoginStore, ProjectStore} = Helper;

@withStyles(styles)
class SingleImageViewer extends React.Component {
  constructor() {
    super();
    this.state = {}
  }

  render() {
    var self = this;
    return (
      <div className="single-image">
        <div>{this.props.ques.question.text}</div>
        <table>
          <tr>
            <td>

              <div className="single-image-image-holder">
                { self.props.resp.length == 1 ? <div>
                  {self.props.resp[0] == self.props.ques.options[0].value ?
                    <img src={self.props.ques.options[0].img} className="single-image-image-select"/>:
                    <img src={self.props.ques.options[0].img} className="single-image-image"/>}</div> :
                  <img src={self.props.ques.options[0].img} className="single-image-image"/> }
                <span>{self.props.ques.options[0].value}</span>
              </div>

            </td>
            <td>
              <div className="single-image-image-holder">
                { self.props.resp.length == 1 ? <div>
                  {self.props.resp[0] == self.props.ques.options[1].value ?
                    <img src={self.props.ques.options[1].img} className="single-image-image-select"/>:
                    <img src={self.props.ques.options[1].img} className="single-image-image"/>} </div>:
                  <img src={self.props.ques.options[1].img} className="single-image-image"/> }
                <span>{self.props.ques.options[1].value}</span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div className="single-image-image-holder">
                { self.props.resp.length == 1 ? <div>
                  {self.props.resp[0] == self.props.ques.options[2].value ?
                    <img src={self.props.ques.options[2].img} className="single-image-image-select"/>:
                    <img src={self.props.ques.options[2].img} className="single-image-image"/>} </div>:
                  <img src={self.props.ques.options[2].img} className="single-image-image"/> }
                <span>{self.props.ques.options[2].value}</span>
              </div>
            </td>
            <td>
              <div className="single-image-image-holder">
                { self.props.resp.length == 1 ? <div>
                  {self.props.resp[0] == self.props.ques.options[3].value ?
                    <img src={self.props.ques.options[3].img} className="single-image-image-select"/>:
                    <img src={self.props.ques.options[3].img} className="single-image-image"/>}</div> :
                  <img src={self.props.ques.options[3].img} className="single-image-image"/> }
                <span>{self.props.ques.options[3].value}</span>
              </div>
            </td>
          </tr>
        </table>
      </div>
    );
  }
}
export default SingleImageViewer;
