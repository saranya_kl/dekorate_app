import React from 'react';
import _ from 'lodash';
import Helper from '../../../../../../helper.js';
var {AppConstants             : Constants, ProjectActions} = Helper;
class Quiz_Sender extends React.Component{
  constructor(){
    super();
  }
  sendQuiz(){
    var x = confirm('Send Quiz?');
    if(x == true){
      var data = _.cloneDeep(Constants.PROJECT_ACTION);
      data.data.title = 'Take our style quiz';
      data.data.subtitle = 'Answer a few questions to help us understand your taste better';
      data.data.data = {quizid: '717381fe0d74962cce60'};
      data.data.type = 'quiz';
      data.data.action = 'Submit';
      data.data.projectid = '9a7f6934f373cacd6429';
      data.data.userid = '2eb7472f7a6557899d47';
      ProjectActions.SEND_PROJECT_ACTION(data);
    }
  }
  render(){
    return(
      <div>
        <button onClick = {this.sendQuiz.bind(this)}>Send</button>
      </div>
    )
  }
}
export default Quiz_Sender;
