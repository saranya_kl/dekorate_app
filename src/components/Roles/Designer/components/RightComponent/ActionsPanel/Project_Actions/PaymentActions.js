/*
 * Created by vikramaditya on 8/21/15.
 */
import React from 'react';
import {Dialog, SvgIcon} from 'material-ui';
import Helper from '../../../../../../helper.js';
import CustomModal from '../../../../../../../lib/Modal';
import Svg from '../../../../../../Shared/Svg/Svg';
import styles from './PaymentActions.scss';
var {AppConstants: Constants, ProjectsActions: QuizAction, ProjectsStore, withStyles} = Helper;

@withStyles(styles)
class PaymentActions extends React.Component {
  static contextTypes = {
    setProgressVisibility: React.PropTypes.func
  };
  constructor(props) {
    super(props);
    let that = this;
    that.quiz_data = {
      data: {
        title: '',
        subtitle: '',
        projectid: '',
        action: 'Pay',
        type: 'payment',
        data: {
          amounts: [
            {
              MRP: {
                INR: '5000',
                USD: '75.97'
              },
              SP: {
                INR: '2000',
                USD: '30.40'
              }
            }
          ]
        }
      }
    };
    that.data = {title: '', subtitle: ''};
    that.unsubscribe = ProjectsStore.listen(that.onStatusChange.bind(that));
  }
  componentWillUnmount(){
    this.unsubscribe();
  }
  onStatusChange(trigger){
    let that = this;
    if(trigger.project){
      that.context.setProgressVisibility(false);
      that.modalDismiss();
    }
  }
  modalDismiss(){
    this.props.onDismiss();
  }
  getQuizData(){
    this.data.title = React.findDOMNode(this.refs.title).value;
    this.data.subtitle = React.findDOMNode(this.refs.subtitle).value;
    return this.data;
  }
  setPaymentPrice(tag, e){
    let price = e.target.value, that = this;
    if(isNaN(price) || parseFloat(price) < 0){
      alert(`Enter correct price.`);
    }else{
      let amount = _.cloneDeep(that.quiz_data.data.data.amounts[0]);
      switch (tag) {
      case 'MRP INR':
        amount.MRP.INR = price;
        break;
      case 'MRP USD':
        amount.MRP.USD = price;
        break;
      case 'SP INR':
        amount.SP.INR = price;
        break;
      case 'SP USD':
        amount.SP.USD = price;
        break;
      default:
        console.log('Some error in actions');
      }
      that.quiz_data.data.data.amounts[0] = amount; //TODO currently hardcoded in 0th object.
    }
  }
  sendQuiz(){
    let that = this;
    let project = ProjectsStore.getProject(that.props.pid);
    if(project.relationship.owner) {
      let data1 = that.getQuizData();
      that.quiz_data.data.title = data1.title;
      that.quiz_data.data.subtitle = data1.subtitle;
      that.quiz_data.data.projectid = project.id;
      let r = confirm(`Send Project Action to ${project.relationship.owner.name}`);
      if (r == true) {
        QuizAction.sendProjectAction(that.quiz_data);
        //QuizAction.SEND_PROJECT_ACTION(this.quiz_data);
        that.context.setProgressVisibility(true);
      }
    }else{
      alert('Unable to send action');
    }
  }
  render() {
    let self = this;
    return (
      <div className="PaymentActions">
        <CustomModal isOpen={true} onClose={self.props.onDismiss.bind(self)} style={{top:'12%', height:'70%',left:'25%', width:'50%', borderRadius: '3px',padding:'0'}}>
          <div onClick={self.modalDismiss.bind(self)}>
            <Svg iconType={'close_icon'} className='cross-icon' style={{fill:'#808080', zoom:'0.75', position: 'absolute', top: '20px', right: '20px',cursor:'pointer'}}/>
          </div>
          <div className="PaymentActions-content">
            <h6 className="PaymentActions-content-title"> Payment Action </h6>
            <div className="PaymentActions-content-wrapper">
              <span className="PaymentActions-content-inputLabel"> Title </span>
              <input ref = 'title' type='text' placeholder='Title' defaultValue={'Please make the payment to begin designing with dekorate'} className="PaymentActions-content-input"/>
              <span className="PaymentActions-content-inputLabel"> Message </span>
              <input ref = 'subtitle' type='text' placeholder='subtitle' defaultValue={''} className="PaymentActions-content-input"/>

              <div className="price-wrapper">
                <div className='price-wrapper-mrp'>
                  <div className='flex-row'>
                    <span className="price-label">MRP INR:  </span>
                    <input type='text' className='price-input' onChange={self.setPaymentPrice.bind(self,'MRP INR')} style={{textAlign:'right'}} defaultValue={'5000'}/>
                  </div>
                  <br />
                  <div className='flex-row'>
                    <span className="price-label">Selling Price INR:  </span>
                    <input type='text' className='price-input' onChange={self.setPaymentPrice.bind(self,'SP INR')} style={{textAlign:'right'}} defaultValue={'2000'}/>
                  </div>
                </div>
                <div className='price-wrapper-sp'>
                  <div className='flex-row'>
                    <span className="price-label">USD:  </span>
                    <input type='text' className='price-input' onChange={self.setPaymentPrice.bind(self,'MRP USD')} style={{textAlign:'right'}} defaultValue={'75.97'}/>
                  </div>
                  <br />
                  <div className='flex-row'>
                    <span className="price-label">USD:  </span>
                    <input type='text' className='price-input' onChange={self.setPaymentPrice.bind(self,'SP USD')} style={{textAlign:'right'}} defaultValue={'30.40'}/>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="PaymentActions-footer">
            <div className="input-divider"></div>
            <div className="PaymentActions-footer-buttonWrapper">
              <button onClick={self.modalDismiss.bind(self)} className="cancel">Cancel</button>
              <button onClick={self.sendQuiz.bind(self)} className="send">Send</button>
            </div>
          </div>
        </CustomModal>
      </div>
    )
  }
}
export default PaymentActions;
