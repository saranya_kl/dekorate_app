/**
 * Created by vikramaditya on 8/25/15.
 */
import React from 'react';
import _ from 'lodash';
import {Dialog, SvgIcon} from 'material-ui';
import Helper from '../../../../../../helper.js';
import styles from './DesignerOptionResponse.scss';
import CustomDialog from '../../../../../../../lib/Modal';
var {AppConstants: Constants, UsersActions, UsersStore, withStyles} = Helper;

@withStyles(styles) class DesignerOptionResponse extends React.Component {
  constructor(props) {
    super(props);
    let data = UsersStore.getDesigners();
    this.state = {
      isLoading: data.length == 0,
      all_designers: data.length == 0 ? []:data,
      designer_options: [],
      answer: {}
    };
    this.unsubscribe = UsersStore.listen(this.onStatusChange.bind(this));
  }

  componentWillMount() {
    let data = UsersStore.getDesigners();
    if (data.length != 0) {
      this.setResponse(this.props, data);
    } else {
      UsersActions.fetchDesigners();
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.data !== this.props.data) {
      this.setResponse(nextProps);
    }
  }

  setResponse(props, all_designers) {
    let response = props.data.response;
    let designers_options = props.data.data.designers;
    let arr = [];
    all_designers.map((designer) => {
      designers_options.map((dr) => {
        if (dr.id == designer.id) {
          arr.push({id: designer.id, name: `${designer.firstname} ${designer.lastname}`, imageUrl: designer.imageUrl, amount: dr.amount});
        }
      })
    });
    this.setState({designer_options: arr, answer: response ? response : 'PENDING'});
  }

  onStatusChange(trigger) {
    if(trigger.designers){
      let data = UsersStore.getDesigners();
      this.setResponse(this.props, data);
      this.setState({isLoading: false, all_designers: data});
    }
  }

  modalDismiss() {
    // this.refs.designer_dialog.dismiss();
    this.props.show(false);
  }

  render() {
    let response = this.props.data.response ? this.props.data.response : '';
    let title = this.props.data.title;
    let subtitle = this.props.data.subtitle;
    // let user = this.state.user;
    let options = this.state.designer_options || [];
    return (
      <div className="DesignerOptionResponse">
        <CustomDialog isOpen={true} onClose={this.modalDismiss.bind(this)} style={{top:'25%', borderRadius: '3px', height:'50%',width:'50%', left:'25%',padding:'0'}}>
          <div>
            <SvgIcon className='custom-cross-icon' onClick={this.modalDismiss.bind(this)}>
              <path d={Constants.ICONS.close_icon}></path>
            </SvgIcon>
          </div>
          <div className="DesignerOptionResponse-header">
            Designer choice response
          </div>
          <div className="DesignerOptionResponse-content">
            <div className="DesignerOptionResponse-designers">
              {
                options.map(function(designer, index){
                  let temp = response.designerid;
                  let mrp_inr = _.result(designer,'amount.MRP.INR','NIL');
                  let mrp_usd = _.result(designer,'amount.MRP.USD','NIL');
                  let sp_inr = _.result(designer,'amount.SP.INR','NIL');
                  let sp_usd = _.result(designer,'amount.SP.USD','NIL');
                  return (
                    <div key={index} className="DesignerOptionResponse-designers-wrapper" style={{border:  (temp == designer.id) ? '1px solid #6e92cc': 'none'}}>
                      <img width='100' height='100' src={designer.imageUrl} className="DesignerOptionResponse-designers-image"/>
                      <div className="DesignerOptionResponse-designers-name">
                        <span style={{textAlign:'center'}}><strong>{designer.name}</strong></span>
                        <br />
                        <span><strong>MRP :</strong>&nbsp;&#8377;{mrp_inr} &nbsp;/&nbsp;${mrp_usd}</span>
                        <br />
                        <span><strong>Selling Price :</strong>&nbsp;&#8377;{sp_inr}&nbsp;/&nbsp;${sp_usd}</span>
                      </div>
                    </div>
                  )
                })
              }
            </div>
            <div>Response: {response ? response.designername : 'Waiting for Response'}</div>
          </div>
          <div className="input-divider"></div>
          <div className="DesignerOptionResponse-buttonwrapper">
            <button className="done" onClick={this.modalDismiss.bind(this)}> Done</button>
          </div>
        </CustomDialog>
      </div>
    )
  }
}
export default DesignerOptionResponse;
