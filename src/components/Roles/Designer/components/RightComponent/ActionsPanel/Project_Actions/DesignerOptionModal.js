/**
 * Created by vikramaditya on 8/25/15.
 */
import React from 'react';
import {Dialog, SvgIcon, RefreshIndicator, Checkbox} from 'material-ui';
import styles from './DesignerOptionModal.scss';
import Helper from '../../../../../../helper.js';
import CustomDialog from '../../../../../../../lib/Modal';

var {AppConstants: Constants, UsersStore, UsersActions, ProjectsActions: QuizAction, ProjectsStore, withStyles} = Helper;
let dummyAmount = {MRP: {INR: '0', USD: '0'}, SP: {INR: '0', USD: '0'}};
@withStyles(styles)
class DesignerOptionModal extends React.Component {
  static contextTypes = {
    setProgressVisibility: React.PropTypes.func
  };
  constructor(props) {
    super(props);
    let that = this;
    let data = UsersStore.getDesigners();

    that.priority = 1;
    that.selected_user = {};
    that.data = {};
    that.quiz_data = _.cloneDeep(Constants.PROJECT_ACTION);
    that.non_state_designers = data.length == 0 ? [] : data;
    that.state = {
      all_designers: data.length == 0 ? [] : data,
      filtered_list: data.length == 0 ? [] : data,
      selected_user: {},
      isLoading: data.length == 0
    };
    data.length == 0 && UsersActions.fetchDesigners();
    that.unsubscribe = ProjectsStore.listen(that.onStatusChange.bind(that));
    that.unsubscribe_userStore = UsersStore.listen(that.handleUsersStore.bind(that));
  }
  componentWillUnmount(){
    this.unsubscribe();
    this.unsubscribe_userStore();
  }
  handleUsersStore(trigger){
    let that = this;
    if(trigger.designers){
      let users = UsersStore.getDesigners();
      that.non_state_designers = users;
      that.setState({all_designers: users, filtered_list: users, isLoading: false});
    }
  }
  onStatusChange(trigger){
    let that = this;
    if(trigger.project){
      that.context.setProgressVisibility(false);
      that.modalDismiss();
    }
  }
  modalDismiss() {
    this.props.onDismiss();
  }
  setDesignerPrice(id, name, tag, e){
    let price = e.target.value;
    if(isNaN(price) || parseInt(price) < 0){
      alert(`Enter correct price for ${name}`);
    }
    else{
      let amount = _.cloneDeep(this.selected_user[id].amount);
      switch (tag) {
      case 'MRP INR':
        amount.MRP.INR = price;
        break;
      case 'MRP USD':
        amount.MRP.USD = price;
        break;
      case 'SP INR':
        amount.SP.INR = price;
        break;
      case 'SP USD':
        amount.SP.USD = price;
        break;
      default:
        console.log('Some error in actions');
      }
      this.selected_user[id].amount = amount;
    }
  }
  selectDesigner(id, name) {
    if (this.selected_user[id] && this.selected_user[id].priority > 0) {
      this.selected_user[id].priority = 0;
    } else {
      this.selected_user[id] = {priority: this.priority++, amount: dummyAmount, designername: name};
    }
    this.setState({selected_user: this.selected_user});
  }

  getQuizData() {
    this.data.title = React.findDOMNode(this.refs.title).value;
    this.data.subtitle = React.findDOMNode(this.refs.subtitle).value;
    let arr = [];
    //Sort
    let sorted_keys = Object.keys(this.selected_user).sort((a,b) => {return this.selected_user[a].priority - this.selected_user[b].priority});
    sorted_keys.map((id, index) => {if(this.selected_user[id].priority != 0){arr.push({id: id, designername: this.selected_user[id].designername, amount: this.selected_user[id].amount})}});
    //for (var designer in sorted_keys) {
    //  if (this.selected_user[designer] != 0) {
    //    arr.push({id: designer});
    //  }
    //}
    this.data.designers = arr;
    return this.data;
  }
  sendQuiz(){
    let that = this;
    let project = ProjectsStore.getProject(that.props.pid);
    if(project.relationship.owner) {
      let data4 = that.getQuizData();
      that.quiz_data.data.title = data4.title;
      that.quiz_data.data.subtitle = data4.subtitle;
      that.quiz_data.data.data = {designers: data4.designers};
      that.quiz_data.data.action = 'Select';
      that.quiz_data.data.type = 'choosedesigner';
      that.quiz_data.data.projectid = project.id;
      that.quiz_data.data.userid = project.relationship.owner.id;

      let r = confirm(`Send Designer Option to ${project.relationship.owner.name}`);
      if (r == true) {
        QuizAction.sendProjectAction(that.quiz_data);
        //QuizAction.SEND_PROJECT_ACTION(this.quiz_data);
        that.context.setProgressVisibility(true);
      }
    }else{
      alert('Unable to send action');
    }
  }
  searchDesigner(e){
    let value = (e.target.value).toLowerCase();
    function isEqual(str) {
      let name = (`${str.firstname} ${str.lastname}`).toLowerCase();
      if (name.indexOf(value) >= 0) {
        return true;
      }
      return false;
    }
    let x = this.non_state_designers;
    let filtered = x.filter(isEqual);
    this.setState({filtered_list: filtered});
  }
  render() {
    let self = this;
    let inputStyle = {width:'80%',margin:'5px 30px', padding: '6px',fontSize: '0.8rem', border:'1px solid #ababab',outline:'none'};
    //let action = [{text: 'Send', onTouchTap: this.sendQuiz.bind(this)}];
    return (
      <div style={{textAlign:'left'}} className="DesignerOptionModal">
        <CustomDialog ref='designer_dialog' isOpen={true} onClose={this.modalDismiss.bind(this)} style={{top:'12%', borderRadius: '3px', height:'70%',width:'50%', left:'25%',padding:'0'}}>
          <SvgIcon className='custom-cross-icon'
                   style={{fill:'black', zoom:'0.75', position: 'absolute', top: '10px', right: '10px',cursor:'pointer'}}
                   onClick={this.modalDismiss.bind(this)}>
            <path d={Constants.ICONS.close_icon} />
          </SvgIcon>
          <h6 className="DesignerOptionModal-title"> Offer to choose a designer </h6>
          <div className="DesignerOptionModal-content">
            {this.state.isLoading && <h3 className="Loading">Loading Designers please wait...</h3>}
            <div className="DesignerOptionModal-inputWrapper">
              <span className="custom-input-label"> Title </span>
              <input ref='title' type='text' placeholder='Title'
                     defaultValue={'Choose your designer'} className="DesignerOptionModal-input"/>
              <span className="custom-input-label"> Sub-title </span>
              <input ref='subtitle' type='text' placeholder='subtitle'
                     defaultValue={'Choose the designer you want to get your room designed with'} className="DesignerOptionModal-input"/>
              <span className="custom-input-label"> Designers </span>
              <div className='DesignerOptionModal-searchBox'>
                <input className="DesignerOptionModal-input" type='text' placeholder='Search' onChange={self.searchDesigner.bind(self)}/>
              </div>
            </div>
            <div className='DesignerOptionModal-designer-wrapper' style={{height:'100%',overflowY: 'visible'}}>
              {
                self.state.filtered_list.map((designer, index) => {
                  return (
                    <div key={index} className='DesignerOptionModal-designer-wrapper-container' style={{backgroundColor: self.state.selected_user[designer.id] && self.state.selected_user[designer.id].priority > 0 ? 'rgb(234, 248, 246)' : 'white'}}>
                      <div className='image-container'>
                        <div className='image-container' onClick={self.selectDesigner.bind(self,designer.id,`${designer.firstname} ${designer.lastname}`)}>
                          <img src={designer.imageUrl} className='designer-image'/>
                          <span className='designer-image-label'>{`${designer.firstname} ${designer.lastname}`}</span>
                          {self.state.selected_user[designer.id] && self.state.selected_user[designer.id].priority > 0 && <SvgIcon className='tick' color={'white'}>
                            <path d='M9 16.2L4.8 12l-1.4 1.4L9 19 21 7l-1.4-1.4L9 16.2z'/>
                          </SvgIcon>}
                        </div>
                        {self.state.selected_user[designer.id] && self.state.selected_user[designer.id].priority > 0 && <div className="price-wrapper">
                          <div className='price-wrapper-mrp'>
                            <div className='flex-row'>
                              <span className="price-label">MRP INR:  </span>
                              <input type='text' className='price-input' onChange={self.setDesignerPrice.bind(self,designer.id, `${designer.firstname} ${designer.lastname}`, 'MRP INR')} style={{textAlign:'right'}} defaultValue={dummyAmount.MRP.INR}/>
                            </div>
                            <br />
                            <div className='flex-row'>
                              <span className="price-label">Selling Price INR:  </span>
                              <input type='text' className='price-input' onChange={self.setDesignerPrice.bind(self,designer.id, `${designer.firstname} ${designer.lastname}`, 'SP INR')} style={{textAlign:'right'}} defaultValue={dummyAmount.SP.INR}/>
                            </div>
                          </div>
                          <div className='price-wrapper-sp'>
                            <div className='flex-row'>
                              <span className="price-label">USD:  </span>
                              <input type='text' className='price-input' onChange={self.setDesignerPrice.bind(self,designer.id, `${designer.firstname} ${designer.lastname}`, 'MRP USD')} style={{textAlign:'right'}} defaultValue={dummyAmount.MRP.USD}/>
                            </div>
                            <br />
                            <div className='flex-row'>
                              <span className="price-label">USD:  </span>
                              <input type='text' className='price-input' onChange={self.setDesignerPrice.bind(self,designer.id, `${designer.firstname} ${designer.lastname}`, 'SP USD')} style={{textAlign:'right'}} defaultValue={dummyAmount.SP.USD}/>
                            </div>
                          </div>
                        </div>
                        }
                      </div>
                    </div>
                  )
                })
              }
            </div>
          </div>
          <div className="input-divider"> </div>
          <div className="DesignerOptionModal-buttonWrapper">
            <button onClick={this.modalDismiss.bind(this)} className="cancel">Cancel</button>
            <button onClick={this.sendQuiz.bind(this)} className="send">Send</button>
          </div>
        </CustomDialog>
      </div>
    )
  }
}
export default DesignerOptionModal;
{/*
 function () {
 let total = self.state.all_designers.length;
 let col = 6;
 let row = Math.ceil(total / col);
 var row_arr = [];
 var designer_count = 0;

 for (let i = 0; i < row; i++) {
 row_arr.push(
 <div key={i} style={{display:'flex', flexFlow:'row'}}>
 {
 function () {
 var col_arr = [];
 for (let j = 0; j < col && designer_count < total; j++) {
 col_arr.push(
 <div key={j} style={{margin:'10px', position: 'relative'}}>
 <Checkbox
 onCheck={self.selectDesigner.bind(self,self.state.all_designers[designer_count].id)}
 name={`${i*col+j}`}
 style={{position:'absolute', left:'5px', top:'5px', stroke: 'white'}}
 value="checkboxValue1"/>
 <img width='80' height='80'
 src={self.state.all_designers[designer_count].imageUrl}/>
 <div
 style={{position:'absolute', bottom: '0px', left: '0px', height:'16px', width: '80px',zIndex: '2', backgroundColor:'black', opacity: '0.8'}}></div>
 <small
 style={{position:'absolute', bottom: '0px', left: '0px', width: '80px',zIndex: '3', color:'white',textAlign:'center',fontSize:'0.6rem'}}>{`${self.state.all_designers[designer_count].firstname} ${self.state.all_designers[designer_count].lastname}`}</small>
 </div>
 );
 designer_count++;
 }
 return col_arr;
 }(i)
 }
 </div>
 )
 }
 return row_arr;
 }()
 */
}
