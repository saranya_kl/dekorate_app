/**
 * Created by vikramaditya on 9/15/15.
 */
import React from 'react';
import _ from 'lodash';
import Helper from '../../../../../../helper.js';
import styles from './PaymentActionsResponse.scss';
import CustomModal from '../../../../../../../lib/Modal';
var {withStyles} = Helper;
@withStyles(styles)
class PaymentActionsResponse extends React.Component {
  constructor() {
    super();
  }

  componentWillMount() {

  }

  componentWillUnmount() {

  }
  modalDismiss(){
    this.props.show(false);
  }
  render() {
    let title = this.props.data.title;
    let subtitle = this.props.data.subtitle;
    let answer = _.result(this.props.data, 'response.designername', 'PENDING');
    let mrp = _.result(this.props.data, 'data.amounts[0].MRP', '0');
    let sp = _.result(this.props.data, 'data.amounts[0].SP', '0');
    return (
      <div className='PaymentActionsResponse'>
        <CustomModal isOpen={true} onClose={this.modalDismiss.bind(this)}
                     style={{left:'30%', width:'40%', top:'25%',height:'50%'}}>
          <p><strong> Title </strong>: <span>{title}</span></p>
          <p><strong> Subtitle </strong>: <span>{subtitle}</span></p>
          <div className="price-wrapper">
            <div className='price-wrapper-mrp'>
              <div className='flex-row'>
                <span className="price-label">MRP INR:  </span>
                <input disabled={true} type='text' className='price-input' style={{textAlign:'right'}} value={mrp.INR}/>
              </div>
              <br />
              <div className='flex-row'>
                <span className="price-label">Selling Price INR:  </span>
                <input disabled={true} type='text' className='price-input' style={{textAlign:'right'}} value={sp.INR}/>
              </div>
            </div>
            <div className='price-wrapper-sp'>
              <div className='flex-row'>
                <span className="price-label">USD:  </span>
                <input disabled={true} type='text' className='price-input' style={{textAlign:'right'}} value={mrp.USD}/>
              </div>
              <br />
              <div className='flex-row'>
                <span className="price-label">USD:  </span>
                <input disabled={true} type='text' className='price-input' style={{textAlign:'right'}} value={sp.USD}/>
              </div>
            </div>
          </div>
          <span>Response : {answer} </span>
        </CustomModal>
      </div>
    )
  }
}
export default PaymentActionsResponse;
