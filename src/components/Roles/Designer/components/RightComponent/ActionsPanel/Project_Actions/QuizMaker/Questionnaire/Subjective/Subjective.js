import React from 'react';
import _ from 'lodash';
import {ListDivider, SvgIcon, Dialog,ListItem, TextField} from 'material-ui';
import Helper from '../../../../../../../../../helper.js';
import styles from './Subjective.scss';

var {withStyles, TriggerTypes, AppConstants: Constants, ProjectActions: QuizAction, ProjectStore} = Helper;

@withStyles(styles) class Subjective extends React.Component {
  constructor() {
    super();
    this.state = {
      obj: {
        type: 'subjective',
        question: {},
        options: []
      },
      value: ''
    };
  }

  componentWillReceiveProps(nextProps) {
    var x = nextProps.data.question.text;
    var xy = _.cloneDeep(this.state.obj);
    xy.question.text = x;
    this.setState({value: x, obj: xy});
  }

  componentDidMount() {
    var x = this.props.data.question.text;
    var xy = _.cloneDeep(this.state.obj);
    xy.question.text = x;
    this.setState({value: x, obj: xy});
  }

  getValue(e) {
    var questext = e.target.value;
    var x = _.cloneDeep(this.state.obj);
    x.question.text = questext;
    this.setState({obj: x, value: questext});
  }

  submit() {
    this.props.submitQuestion(this.state.obj);
  }

  render() {
    let self = this;
    let value = this.state.value;
    return (
      <div className="subjective">
        <div className="question">
          <span className="question-title"> Question: </span>

          <div className="question-text">
            <span className="question-label"> Text: </span>
            <TextField ref='questionText' type='text' className="question-input" placeholder='Question' value={value}
                   onChange={self.getValue.bind(self)}/>
          </div>
        </div>
        <div className="submit">
          <button className="submit-button" onClick={self.submit.bind(self)}> Submit</button>
        </div>
      </div>
    );
  }
}
export default Subjective;
