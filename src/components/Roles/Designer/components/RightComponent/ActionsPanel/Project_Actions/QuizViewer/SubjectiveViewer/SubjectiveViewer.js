import React from 'react';
import {ListDivider, SvgIcon, Dialog,ListItem} from 'material-ui';
import Helper from '../../../../../../../../helper.js';
import styles from './SubjectiveViewer.scss';


var {withStyles, TriggerTypes, AppConstants: Constants, ProjectActions: QuizAction, LoginStore, ProjectStore} = Helper;

@withStyles(styles) class SubjectiveViewer extends React.Component {
  constructor() {
    super();
    this.state = {

    }
  }
  render(){

    var self = this;
    return(
      <div className="subjective">
        <div>{this.props.ques.question.text}</div>
        <div><span>Answer:</span><div className="subjective-option">{self.props.resp}</div></div>
      </div>
    );
  }
}
export default SubjectiveViewer;
