/**
 * Created by vikramaditya on 8/22/15.
 */
import React from 'react';
import {Dialog, SvgIcon} from 'material-ui';
import Helper from '../../../../../../helper.js';
var {AppConstants: Constants} = Helper;

class SubjectiveQuestionResponse extends React.Component {
  constructor() {
    super();
  }
  modalDismiss(){
    this.refs.text_dialog.dismiss();
    this.props.show(false);
  }
  render() {
    let action = [{text:'Dismiss', onTouchTap: this.modalDismiss.bind(this)}];
    let title = this.props.data.title,subtitle =this.props.data.subtitle,question= this.props.data.data.text;
    let answer= this.props.data.response ? this.props.data.response.textAnswer : 'PENDING';
    let img_url = this.props.data.data.img ? this.props.data.data.img : '';
    return (
      <div>
        <Dialog ref ='text_dialog' actions = {action} modal={true} openImmediately={true} autoDetectWindowHeight={true}>
          <div>
            <SvgIcon className='cross-icon'
                     style={{fill:'black', zoom:'0.75', position: 'absolute', top: '10px', right: '10px',cursor:'pointer'}}
                     onClick={this.modalDismiss.bind(this)}>
              <path d={Constants.ICONS.close_icon}></path>
            </SvgIcon>
          </div>
          <div style={{display: 'flex', flexFlow:'column'}}>
            <p>Title - {title}</p>
            <p>Subtitle - {subtitle}</p>
            {img_url != '' ? <img src={img_url} width='150' height='120'/> : ''}
            <p>Question - {question}</p>
            <p>Answer - {answer}</p>
          </div>
        </Dialog>
      </div>
    )
  }
}
export default SubjectiveQuestionResponse;
