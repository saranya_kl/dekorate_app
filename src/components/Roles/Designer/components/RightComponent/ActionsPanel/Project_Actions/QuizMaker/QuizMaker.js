import React from 'react';
import _ from 'lodash';
import {SvgIcon, Dialog,ListItem} from 'material-ui';
import Helper from '../../../../../../../helper.js';
import styles from './QuizMaker.scss';
import Modal from '../../../../../../../../lib/Modal';
import Questionnaire from './Questionnaire';


var {withStyles, TriggerTypes, AppConstants: Constants, ProjectActions: QuizAction, LoginStore, ProjectStore} = Helper;

@withStyles(styles)
class QuizMaker extends React.Component {
  constructor() {
    super();
    this.state = {
      quizs: [],
      current_quiz: {},
      new_quiz: {},
      show: true,
      current: 0,
      buttons: false,
      title: '',
      subtitle: '',
      name: '',
      open: true
    }
  }

  componentWillMount() {
    this.unsubscribe = ProjectStore.listen(this.onStatusChange.bind(this));
    QuizAction.GET_ALL_QUIZ(LoginStore.getUserID());
  }

  modalDismiss() {
    this.props.onDismiss();
    this.setState({open:false});
  }

  onStatusChange(props) {
    if (props == TriggerTypes.RECEIVED_ALL_QUIZ) {
      var all_quiz = ProjectStore.getQuizData();
      var q = all_quiz[this.state.current];
      // if(all_quiz!=[])
      // {
      //   this.getQuiz(all_quiz[this.state.current].id);
      // }
      this.setState({quizs: all_quiz, title: ' ' || q.title, name: ' ' || q.name, subtitle: ' ' || q.subtitle});
    }
    if (props == TriggerTypes.RECEIVED_QUIZ) {
      var q = ProjectStore.getCurrentQuiz();
      var quiz = this.state.quizs[this.state.current];
      this.setState({current_quiz: q, title:quiz.title, subtitle:quiz.subtitle});
    }
    if (props == TriggerTypes.QUIZ_DELETED) {
      QuizAction.GET_ALL_QUIZ(LoginStore.getUserID());
    }
    if(props == TriggerTypes.UPLOADED_QUIZ){
      console.log('uploadQuiz');
      QuizAction.GET_ALL_QUIZ(LoginStore.getUserID());
    }
    if( props == TriggerTypes.QUIZ_UPDATED){
      QuizAction.GET_ALL_QUIZ(LoginStore.getUserID());
    }
  }

  add() {
    var x = {
      title: '',
      name: '',
      subtitle: '',
      questions: []
    };
    var y = _.cloneDeep(this.state.quizs);
    y.push(x);
    var curr = y.length - 1;
    var emp = '';
    this.setState({show: false, current: curr, current_quiz: x,subtitle:emp,title:emp});
  }

  OpenQuiz(index) {
    this.setState({
      current: index,
      buttons: true,
      title: this.state.quizs[index].title,
      name: this.state.quizs[index].name,
      subtitle: this.state.quizs[index].subtitle
    });
    this.getQuiz(this.state.quizs[index].id);
  }

  getQuiz(id){
    QuizAction.GET_QUIZ(id);
  }

  addQuiz(obj) {
    var x = _.cloneDeep(this.state.quizs);
    x[this.state.current] = obj;
    this.setState({show: true, title: x[this.state.current].title, subtitle: obj.subtitle,name:x[this.state.current].name});
    this.uploadQuiz(obj);

  }

  uploadQuiz(obj) {
    var data = {};
    data.backenduserid = LoginStore.getUserID();
    data.title = obj.title;
    data.subtitle = obj.subtitle;
    data.name = obj.name;
    data.data = {};
    data.data.questions = obj.questions;
    data.data.title = obj.name;
    data.data.type = 'linear-compulsory';
    var r = confirm(`Make this Quiz Global?`);
    if (r == true) {
      data.global = true;
      QuizAction.UPLOAD_QUIZ(data);
    }else{
      data.global = false;
      QuizAction.UPLOAD_QUIZ(data);
    }
  }

  getQuizData() {
    return this.state.quizs;
  }

  review(index) {
    this.setState({
      show: false,
      current: index,
      title: this.state.quizs[index].title,
      name: this.state.quizs[index].name,
      subtitle: this.state.quizs[index].subtitle
    });
    this.getQuiz(this.state.quizs[index].id);
  }

  changeAttributes(type, e) {
    if (type == 'TITLE') {
      let title = e.target.value;
      this.setState({title: title});
    }
    if (type == 'SUBTITLE') {
      let subtitle = e.target.value;
      this.setState({subtitle: subtitle});
    }
  }
  update(obj){
    var data = {};
    data.backenduserid = LoginStore.getUserID();
    data.title = obj.title;
    data.subtitle = obj.subtitle;
    data.name = obj.name;
    data.data = {};
    data.data.questions = obj.questions;
    data.data.title = obj.name;
    data.data.type = 'linear-compulsory';
    data.id = this.state.quizs[this.state.current].id;
    QuizAction.UPDATE_QUIZ(data);
    this.setState({show: true, title: obj.title, subtitle: obj.subtitle,name:obj.name});
  }
  goBack(){
    this.setState({show:true, buttons:false});
  }
  deleteQuiz(ind){
    var result = confirm('Are you sure you want to delete?');
    if (result) {
      this.setState({current:0, buttons: false});
      QuizAction.DELETE_QUIZ(this.state.quizs[ind].id);
    }
  }
  SendQuiz() {
    var data = _.cloneDeep(Constants.PROJECT_ACTION);
    data.data.title = this.state.title;
    data.data.subtitle = this.state.subtitle && this.state.quizs[this.state.current].subtitle;
    data.data.data = {quizid: this.state.quizs[this.state.current].id};
    data.data.type = 'quiz';
    data.data.action = 'Submit';
    data.data.projectid = ProjectStore.getProject().id;
    data.data.userid =  ProjectStore.getProject().relationship.owner.id;
    QuizAction.SEND_PROJECT_ACTION(data);
    this.modalDismiss();
  }

  render() {
    let self = this;
    var style={left:'20%', width: '60%',height: '80%',top: '7%'};
    return (
      <Modal isOpen={self.state.open} onClose={self.modalDismiss.bind(self)} style={style}>
        <SvgIcon className='cross-icon'
                 style={{fill:'black', zoom:'0.75', position: 'absolute', top: '10px', right: '10px',cursor:'pointer'}}
                 onClick={self.modalDismiss.bind(self)}>
          <path d={Constants.ICONS.close_icon}></path>
        </SvgIcon>
        <div style={{height:'100%'}}>
          { self.state.show ? <div className="quiz-maker">
            <div className="quiz-stored">
              <span className="quiz-maker-title"> Stored Templates </span>
              <StoredTemplates quizs={self.state.quizs} current={self.state.current} OpenQuiz={self.OpenQuiz.bind(self)} deleteQuiz={self.deleteQuiz.bind(self)} add={self.add.bind(self)} review={self.review.bind(self)} send={self.SendQuiz.bind(self)}/>
            </div>
          </div> : <div style={{height:'100%'}}><Questionnaire data={self.state.current_quiz} onsubmit={self.addQuiz.bind(self)} back={self.goBack.bind(self)}
          subtitle={self.state.subtitle} title={self.state.title} update={self.update.bind(self)} className="height"/></div>}
        </div>
      </Modal>
    );
  }
}

class StoredTemplates extends React.Component {
  OpenQuiz(index){
    this.props.OpenQuiz(index);
  }
  review(index){
    this.props.review(index);
  }
  render() {
    let quizlist = this.props.quizs;
    let self = this;
    let showEachQuiz = (data, index) => <EachQuiz data={data.name} key={index} index={index} selected={this.props.current==index} deleteQuiz={this.props.deleteQuiz.bind(this)} OpenQuiz={this.OpenQuiz.bind(this)} review={this.review.bind(this)} send={this.props.send.bind(this)}/>;
    return(
      <div className="quiz-stored-list">
        {quizlist.map(showEachQuiz)}
        <div className="add-wrapper" onClick={self.props.add.bind(self)}>
          <SvgIcon className="add-box">
            <path d="M19 13h-6v6h-2v-6H5v-2h6V5h2v6h6v2"/>
          </SvgIcon>
        </div>
      </div>
    );
  }
}

class EachQuiz extends React.Component {

  render() {
    let index = this.props.index;
    let title = this.props.data;
    let currentStyle = this.props.selected ? {
      display: 'flex',
      flexFlow: 'column',
      justufyContent: 'space-between',
      alignItems:'flex-start',
      backgroundColor:'grey',
      margin: '15px',
      width: '200px',
      height: '150px',
      position:'relative',
      color:'white'
    } : {
      display: 'flex',
      flexFlow: 'column',
      justufyContent: 'space-between',
      alignItems:'flex-start',
      backgroundColor:'#f2f2f2',
      margin: '15px',
      width: '200px',
      height: '150px',
      position:'relative'
    }
    return (
      <div className="EachQuiz" style={currentStyle}>
        <div className="EachQuiz-listitem" key={index} onClick={this.props.OpenQuiz.bind(this,index)}>
          <div className="EachQuiz-listitem-title">
            <span className="index"> {index + 1}.  </span>
            <span className="ques"> {title} </span>
          </div>
        </div>
        <SvgIcon className='cross-icon'
                   style={{fill:'black', zoom:'0.75',cursor:'pointer', position: 'absolute', right: '3%', top: '3%'}}
                   onClick={this.props.deleteQuiz.bind(this,index)}>
          <path d={Constants.ICONS.close_icon}></path>
        </SvgIcon>
        {this.props.selected && <div className="EachQuiz-buttons flex-row">
          <button onClick={this.props.send.bind(this)}> Send </button>
          <button onClick={this.props.review.bind(this,index)}> Review </button>
        </div>}
      </div>
    );
  }
}


export default QuizMaker;
