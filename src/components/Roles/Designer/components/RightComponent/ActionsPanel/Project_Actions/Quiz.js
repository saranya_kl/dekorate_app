/**
 * Created by vikramaditya on 8/21/15.
 */
import React from 'react';
import {Dialog, SvgIcon} from 'material-ui';
import Helper from '../../../../../../helper.js';
var {AppConstants: Constants} = Helper;

class Quiz extends React.Component {
  constructor() {
    super();
    this.data = {
      title: '',
      subtitle: ''
    }
  }
  modalDismiss(){
    this.props.onDismiss();
    this.refs.images_dialog.dismiss();
  }
  getQuizData(){
    this.data.title = React.findDOMNode(this.refs.title).value;
    this.data.subtitle = React.findDOMNode(this.refs.subtitle).value;
    this.data.url = React.findDOMNode(this.refs.quiz_url).value;
    return this.data;
  }

  render() {
    let action = [{text:'Send', onTouchTap: this.props.onSubmit.bind(this)}];
    return (
      <div>
        <Dialog ref ='images_dialog' actions = {action} modal={true} openImmediately={true} autoDetectWindowHeight={true}>
          <div>
            <SvgIcon className='cross-icon'
                     style={{fill:'black', zoom:'0.75', position: 'absolute', top: '10px', right: '10px',cursor:'pointer'}}
                     onClick={this.modalDismiss.bind(this)}>
              <path d={Constants.ICONS.close_icon}></path>
            </SvgIcon>
          </div>
          <div style={{display: 'flex', flexFlow:'column'}}>
            <input ref = 'title' type='text' placeholder='Title' defaultValue={'Give Style quiz'}/>
            <input ref = 'subtitle' type='text' placeholder='subtitle' defaultValue={'Subtitle'}/>
            <input ref = 'quiz_url' type='text' placeholder='http://www.example.com'/>
          </div>
        </Dialog>
      </div>
    )
  }
}
export default Quiz;
