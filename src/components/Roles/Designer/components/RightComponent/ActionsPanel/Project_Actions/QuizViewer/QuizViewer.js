import React from 'react';
import {ListDivider, SvgIcon, Dialog,ListItem} from 'material-ui';
import Helper from '../../../../../../../helper.js';
import styles from './QuizViewer.scss';
import SingleTextViewer from './SingleTextViewer';
import SubjectiveViewer from './SubjectiveViewer';
import StarrateViewer from './StarrateViewer';
import SingleImageViewer from './SingleImageViewer';
import Services from '../../../../../../../../services/Services';
//TODO self fetch the quiz question for given quiz id.
var {withStyles, AppConstants: Constants} = Helper;

@withStyles(styles)
class QuizViewer extends React.Component {
  constructor() {
    super();
    this.state = {
      quiz: [],
      quizTitle: '',
      response: [],
      current: 0,
      left: false,
      right: true
    }
  }
  fetchQuiz(id){
    Services.getAPI(Constants.SERVER_URL + 'quiz/' + id)
      .then((response)=> {
        let quiz = response.body.data;
        this.setState({quiz: quiz.questions, quizTitle: quiz.title});
      });
  }
  componentWillMount() {
    this.props.quizid && this.fetchQuiz(this.props.quizid);
    var x = [];
    var y = this.props.response ? this.props.response.answers : [];
    this.setState({quiz: x, response: y});
  }

  componentWillReceiveProps(nextProps) {
    var x = [];
    var y = nextProps.response ? nextProps.response.answers : [];
    this.setState({quiz: x, response: y});
  }

  componentDidMount() {
    var x = [];
    var y = this.props.response ? this.props.response.answers : [];
    this.setState({quiz: x, response: y});
  }

  sendQuiz() {

  }

  modalDismiss() {
    this.props.onDismiss();
  }

  goLeft() {

    if (this.state.current > 0 && this.state.current <= (this.state.quiz.length - 1)) {
      var x = this.state.current - 1;
      if (x == 0) {
        this.setState({left: false, current: x, right: true});
      }
      else {
        this.setState({left: true, current: x, right: true});
      }
    }
  }

  goRight() {

    if (this.state.current >= 0 && this.state.current < (this.state.quiz.length - 1)) {
      var x = this.state.current + 1;
      if (x == (this.state.quiz.length - 1)) {
        this.setState({right: false, current: x, left: true});
      }
      else {
        this.setState({right: true, current: x, left: true});
      }
    }
  }

  render() {
    let self = this;
    //let response = self.state.response ? self.state.response[self.state.current] : {};
    let response = self.state.response ?  self.state.response.length==0 ? {} : self.state.response[self.state.current] : {};
    let action = [{text: '  ', onTouchTap: this.sendQuiz.bind(this)}];
    let quiz_type = self.state.quiz ? self.state.quiz[self.state.current] ? self.state.quiz[self.state.current].type : '' : '';
    return (
      <div className="quizresponse">
        <Dialog actions={action} openImmediately={true} onDismiss={self.modalDismiss.bind(self)}
                title={self.state.quizTitle}
                autoDetectWindowHeight={true}>
          <SvgIcon className='cross-icon'
                   style={{fill:'black', zoom:'0.75', position: 'absolute', top: '10px', right: '10px',cursor:'pointer'}}
                   onClick={self.modalDismiss.bind(self)}>
            <path d={Constants.ICONS.close_icon}></path>
          </SvgIcon>
          { self.state.left ? <SvgIcon className='left-icon'
                                       style={{fill:'black', zoom:'0.75', position: 'absolute', top: '50%', left: '10px',cursor:'pointer'}}
                                       onClick={self.goLeft.bind(self)}>
            <path d="M15.41 16.09l-4.58-4.59 4.58-4.59L14 5.5l-6 6 6 6z"/>
          </SvgIcon> : ''}
          { self.state.right ? <SvgIcon className='right-icon'
                                        style={{fill:'black', zoom:'0.75', position: 'absolute', top: '50%', right: '10px',cursor:'pointer'}}
                                        onClick={self.goRight.bind(self)}>
            <path d="M8.59 16.34l4.58-4.59-4.58-4.59L10 5.75l6 6-6 6z"/>
          </SvgIcon> : ''}
          <div className='quizresponse-viewer'>
            {self.state.quiz ? <div>{self.state.current+1}/{self.state.quiz.length}</div>: ''}
            {
              (quiz_type == 'single-text-text') ? <SingleTextViewer ques={self.state.quiz[self.state.current]}
                                                                    resp={response}/> : ''
            }
            {
              (quiz_type == 'single-image-text') ? <SingleTextViewer ques={self.state.quiz[self.state.current]}
                                                                     resp={response}/> : ''
            }
            {
              (quiz_type == 'subjective') ? <SubjectiveViewer ques={self.state.quiz[self.state.current]}
                                                              resp={response}/> : ''
            }
            {
              (quiz_type == 'single-text-starrate') ? <StarrateViewer ques={self.state.quiz[self.state.current]}
                                                                      resp={response}/> : ''
            }
            {
              (quiz_type == 'single-text-image') ? <SingleImageViewer ques={self.state.quiz[self.state.current]}
                                                                      resp={response}/> : ''
            }
          </div>
        </Dialog>
      </div>
    );
  }
}
export default QuizViewer;
