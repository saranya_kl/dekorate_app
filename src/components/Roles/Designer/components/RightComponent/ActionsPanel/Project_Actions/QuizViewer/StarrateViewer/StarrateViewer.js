import React from 'react';
import {ListDivider, SvgIcon, Dialog,ListItem} from 'material-ui';
import Helper from '../../../../../../../../helper.js';
import styles from './StarrateViewer.scss';


var {withStyles, TriggerTypes, AppConstants: Constants, ProjectActions: QuizAction, LoginStore, ProjectStore} = Helper;

@withStyles(styles) class StarrateViewer extends React.Component {
  constructor() {
    super();
    this.state = {
      fill:[],
      empty:[],
      total:[]
    }
  }
  componentWillMount(){
    var arr = [];
    for(var i = 0;i<parseInt(this.props.resp);i++){
      arr.push(' ');
    }
    var array = [];
    var rest = parseInt(this.props.ques.options[0].starcount) - parseInt(this.props.resp);
    for(var i = 0;i<rest;i++){
      array.push(' ');
    }
    var arra = [];
    for(var i = 0;i<parseInt(this.props.ques.options[0].starcount);i++){
      arra.push(' ');
    }
    this.setState({fill:arr,empty:array,total:arra});
  }
  componentDidMount(){
    var arr = [];
    for(var i = 0;i<parseInt(this.props.resp);i++){
      arr.push(' ');
    }
    var array = [];
    var rest = parseInt(this.props.ques.options[0].starcount) - parseInt(this.props.resp);
    for(var i = 0;i<rest;i++){
      array.push(' ');
    }
    this.setState({fill:arr,empty:array});
  }
  componentWillReceiveProps(nextProps){
    var arr = [];
    for(var i = 0;i<parseInt(nextProps.resp);i++){
      arr.push(' ');
    }
    var array = [];
    var rest = parseInt(nextProps.ques.options[0].starcount) - parseInt(nextProps.resp);
    for(var i = 0;i<rest;i++){
      array.push(' ');
    }
    this.setState({fill:arr,empty:array});
  }
  render(){
    var self = this;
    var length = this.state.fill + this.state.empty;
    return(
      <div className="starrate">
        <div>{this.props.ques.question.text}</div>
        <div className="starrate-stars">
          { self.props.resp.length == 1 ?
            <div style={{display: 'flex'}}>
              <div className="starrate-fill">
                {
                  this.state.fill.map((option,index) => {
                    return(
                      <div key={index}>
                        <SvgIcon style={{fill:'black'}}>
                          <path d="M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z"/>
                        </SvgIcon>
                      </div>
                    )
                  })
                }
              </div>
              <div className="starrate-empty">
                {
                  this.state.empty.map((option,index) => {
                    return(
                      <div key={index}>
                        <SvgIcon style={{fill:'black'}}>
                          <path d="M22 9.24l-7.19-.62L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21 12 17.27 18.18 21l-1.63-7.03L22 9.24zM12 15.4l-3.76 2.27 1-4.28-3.32-2.88 4.38-.38L12 6.1l1.71 4.04 4.38.38-3.32 2.88 1 4.28L12 15.4z"/>
                        </SvgIcon>
                      </div>
                    )
                  })
                }
              </div>
            </div> :
            <div className="starrate-empty">
              {
                this.state.total.map((option,index) => {
                  return(
                    <div key={index}>
                      <SvgIcon style={{fill:'black'}}>
                        <path d="M22 9.24l-7.19-.62L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21 12 17.27 18.18 21l-1.63-7.03L22 9.24zM12 15.4l-3.76 2.27 1-4.28-3.32-2.88 4.38-.38L12 6.1l1.71 4.04 4.38.38-3.32 2.88 1 4.28L12 15.4z"/>
                      </SvgIcon>
                    </div>
                  )
                })
              }
            </div>
          }

        </div>
      </div>
    );
  }
}
export default StarrateViewer;
