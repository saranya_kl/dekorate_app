import React from 'react';
import _ from 'lodash';
import {ListDivider, SvgIcon, Dialog,ListItem, TextField} from 'material-ui';
import Helper from '../../../../../../../../../helper.js';
import styles from './StarRate.scss';

var {withStyles, TriggerTypes, AppConstants: Constants, ProjectActions: QuizAction, ProjectStore} = Helper;

@withStyles(styles) class StarRate extends React.Component {
  constructor() {
    super();
    this.state = {
      obj: {
        type: 'single-text-starrate',
        question: {
          text: ''
        },
        options: [{
          starcount: ''
        }]
      },
      qvalue: '',
      cvalue: ''
    };
  }

  componentDidMount() {
    var x = this.props.data.question.text;
    var y = this.props.data.options[0].starcount;
    var xy = _.cloneDeep(this.state.obj);
    xy.question.text = x;
    xy.options[0].starcount = y;
    this.setState({qvalue: x, cvalue: y, obj: xy});
  }

  componentWillReceiveProps(nextProps) {
    console.log(nextProps);
    var x = nextProps.data.question.text;
    var y = nextProps.data.options[0].starcount;
    var xy = _.cloneDeep(this.state.obj);
    xy.question.text = x;
    xy.options[0].starcount = y;
    this.setState({qvalue: x, cvalue: y, obj: xy});
  }

  getValue(e) {
    var questext = e.target.value;
    var x = _.cloneDeep(this.state.obj);
    x.question.text = questext;
    this.setState({obj: x, qvalue: questext});
  }

  getValueStars(e) {
    var strcnt = e.target.value;
    var x = _.cloneDeep(this.state.obj);
    x.options[0].starcount = strcnt;
    this.setState({obj: x, cvalue: strcnt});
  }

  submit() {
    this.props.submitQuestion(this.state.obj);
  }

  render() {
    let self = this;
    let qvalue = this.state.qvalue;
    let cvalue = this.state.cvalue;
    return (
      <div className="star-rate">
        <div className="question">
          <span className="question-title"> Question: </span>

          <div className="question-text">
            <span className="question-label"> Text: </span>
            <TextField ref='questionText' type='text' className="question-input" placeholder='Question' value={qvalue}
                   onChange={self.getValue.bind(self)}/>
          </div>
        </div>
        <div className="options">
          <span className="options-title"> Options: </span>

          <div>
            <span className="options-label"> Star Count: </span>
            <TextField ref='option' type='text' className="options-input" placeholder='No. of stars' value={cvalue}
                   onChange={self.getValueStars.bind(self)}/>
          </div>
          <div className="buttons">
            <div className="submit">
              <button className="submit-button" onClick={self.submit.bind(self)}> Submit</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default StarRate;
