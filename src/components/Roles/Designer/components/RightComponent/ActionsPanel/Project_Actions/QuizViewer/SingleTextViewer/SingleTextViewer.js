import React from 'react';
import {ListDivider, SvgIcon, Dialog,ListItem} from 'material-ui';
import Helper from '../../../../../../../../helper.js';
import styles from './SingleTextViewer.scss';


var {withStyles, TriggerTypes, AppConstants: Constants, ProjectActions: QuizAction, LoginStore, ProjectStore} = Helper;

@withStyles(styles) class SingleTextViewer extends React.Component {
  constructor() {
    super();
    this.state = {

    }
  }
  render(){

    var self = this;
    return(
      <div className="single-text">
        <div>{this.props.ques.question.text}</div>
        {this.props.ques.question.img ? <img className="single-text-image" src={this.props.ques.question.img} /> : ''}
        <div>
          {
            this.props.ques.options.map((option,index) => {
              return(
                <div key={index}>
                  { self.props.resp.length == 1 ? self.props.resp[0] == option.value ? <div className="single-text-option-select">{option.text}</div> : <div className="single-text-option">{option.text}</div> :  <div className="single-text-option">{option.text}</div>}
                </div>
              )
            })
          }
        </div>
      </div>
    );
  }
}
export default SingleTextViewer;
