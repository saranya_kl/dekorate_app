import React from 'react';
import _ from 'lodash';
import DropZone from 'react-dropzone';
import {ListDivider, SvgIcon, Dialog,ListItem, TextField, RefreshIndicator} from 'material-ui';
import Helper from '../../../../../../../../../helper.js';
import Services from '../../../../../../../../../../services/Services.js';
import styles from './NewQuestionForm.scss';

var {withStyles, TriggerTypes, AppConstants: Constants, ProjectActions: QuizAction, ProjectStore} = Helper;

@withStyles(styles) class NewQuestionFiller extends React.Component {
  constructor() {
    super();
    this.state = {
      obj: {
        question: {
          text: '',
          img: ''
        },
        options: []
      },
      add: true,
      imageUrls: {}
    };
  }

  componentWillMount() {
    var x = _.cloneDeep(this.state.obj);
    x.options.push({});
    x.options.push({});
    this.setState({obj: x});
  }

  componentWillReceiveProps(nextProps) {
    var x = nextProps.data;
    this.setState({obj: x});
  }

  componentDidMount() {
    var x = this.props.data;
    this.setState({obj: x});
  }

  getValueQuestion(tag, e) {
    var x = _.cloneDeep(this.state.obj);
    if (tag == 'T') {
      var questext = e.target.value;
      x.question.text = questext;
    }
    else if (tag == 'I') {
      var quesimg = e.target.value;
      x.question.img = quesimg;
    }
    this.setState({obj: x});
    console.log(x);
  }

  getValueOptions(ind, e) {
    var opt = e.target.value;
    var x = _.cloneDeep(this.state.obj);
    x.options[ind].text = opt;
    x.options[ind].value = opt;
    this.setState({obj: x, value: opt});
    console.log('getValueOptions', ind);
  }

  addOption() {
    if (this.state.obj.options.length < 6) {
      var x = _.cloneDeep(this.state.obj);
      x.options.push({});
      this.setState({obj: x});
    }
    else if (this.state.obj.options.length == 5) {
      this.setState({add: false});
    }
  }

  submit() {
    if (this.state.imageUrls == '') {
      var x = _.cloneDeep(this.state.obj);
      x.type = 'single-text-text';
    }
    else {
      var x = _.cloneDeep(this.state.obj);
      x.type = 'single-image-text';
      x.question.img = this.state.imageUrls.url;
    }
    this.setState({obj: x});
    console.log(x);
    this.props.submitQuestion(x);
  }

  removeOption() {
    let x = {};
    this.setState({imageUrls: x});
  }

  onDragOver(value) {
    this.setState({isDragActive: value})
  }

  onDrop(files) {
    console.log(files);
    let arr = {};
    if (files.length == 1) {
      arr.isUploading = true;
      arr.preview = files[0].preview;
      arr.url = '';
      this.uploadImage(files[0]);
      this.setState({imageUrls: arr});
    }
    else {
      alert('Make sure you upload only one image');
    }
  }

  uploadImage(file) {
    let pid = ProjectStore.getProject().id;
    Services.uploadAPI(Constants.SERVER_URL + 'upload', file, `?type=quiz&projectid=${pid}`)
      .then((response) => {
        let url = response.body.url;
        let x = this.state.imageUrls;
        if (x) {
          x.url = url;
          x.isUploading = false;
          this.setState({imageUrls: x});
        }
      })

  }

  uploadComponent() {
    let imageUrl = '';
    let drop_style = {
      textAlign: 'center',
      height: '120px',
      width: '150px',
      border: this.state.isDragActive ? '1px solid green' : '1px dashed black'
    };
    let data = this.state.imageUrls;
    return (
      <div>
        <div style={{position:'relative'}} className="eachUpload">
          {
            Object.keys(data).length != 0 ?
              <div>
                {data.isUploading ?
                  <div>
                    <img src={data.preview} style={{'WebkitFilter': 'blur(5px)'}} width='150' height='120'/>
                    <RefreshIndicator size={40} left={55} top={40} status='loading'/>
                  </div>
                  :
                  <img src={data.preview} width='150' height='120'/>}
              </div>
              :
              <DropZone style={drop_style} onDrop={this.onDrop.bind(this)} onDragOver={this.onDragOver.bind(this, true)}
                        onDragLeave={this.onDragOver.bind(this, false)}>
                {!this.state.isDragActive && <div>
                  <SvgIcon>
                    <path fill='#000000' d={Constants.PLUS_ICON}/>
                  </SvgIcon>
                  <span style={{fontSize:'0.6em'}}> Drop your files here, or click to select files to upload. </span>
                </div>
                }
              </DropZone>

          }
        </div>
      </div>
    );
  }

  render() {
    let self = this;
    let value = this.state.value;
    return (
      <div className="Form">
        <div className="question">
          <span className="question-title"> Question: </span>

          <div className="question-text">
            <span className="question-label"> Text: </span>
            <TextField ref='questionText' type='text' className="question-input" placeholder='Question'
                       value={this.state.obj.question.text} onChange={self.getValueQuestion.bind(self,'T')}/>
          </div>
          <div className="question-img">
            <span className="question-label"> ImageUrl: </span>
            {self.uploadComponent()}
            {self.state.obj.question.img && <img src={self.state.obj.question.img} width={150} height={150}/>}
          </div>
        </div>
        <div className="options">
          <span className="options-title"> Options: </span>

          <div className="options-list">
            {
              (self.state.obj.options.map(function (opt, index) {

                return (
                  <div key={index}>
                    <span className="options-label"> Option{index + 1}: </span>
                    <TextField ref='option' type='text' className="options-input" placeholder='option' value={opt.text}
                               onChange={self.getValueOptions.bind(self,index)}/>
                  </div>
                );
              }))
            }
          </div>
          <div className="buttons">
            { self.state.add ? <SvgIcon onClick={self.addOption.bind(self)} className="add">
              <path d="M10 7H8v4H4v2h4v4h2v-4h4v-2h-4V7zm10 11h-2V7.38L15 8.4V6.7L19.7 5h.3v13z"/>
            </SvgIcon> : ''}
            <div className="submit">
              <button className="submit-button" onClick={self.submit.bind(self)}> Submit</button>
            </div>
          </div>

        </div>
      </div>
    );
  }
}
export default NewQuestionFiller;
