/**
 * Created by vikramaditya on 8/21/15.
 */
import React from 'react';
import Services from '../../../../../../../services/Services.js';
import DropZone from 'react-dropzone';
import {Dialog, SvgIcon, RefreshIndicator} from 'material-ui';
import Helper from '../../../../../../helper.js';
import CustomModal from '../../../../../../../lib/Modal';
import Svg from '../../../../../../Shared/Svg/Svg';
import styles from './SubjectiveQuestion.scss';
var {AppConstants: Constants, ProjectsStore, ProjectsActions: QuizAction, withStyles} = Helper;

@withStyles(styles)
class SubjectiveQuestion extends React.Component {
  static contextTypes = {
    setProgressVisibility: React.PropTypes.func
  };
  constructor(props) {
    super(props);
    let that = this;
    that.img = '';
    that.quiz_data = _.cloneDeep(Constants.PROJECT_ACTION);
    that.state = {
      isUploading: false,
      isDragActive:false,
      showPreview: false,
      img_url: ''
    };
    that.data = {
      title: '',
      subtitle: '',
      text: ''
    };
    that.unsubscribe = ProjectsStore.listen(that.onStatusChange.bind(that));
  }
  componentWillUnmount(){
    this.unsubscribe();
  }
  onStatusChange(trigger){
    let that = this;
    if(trigger.project){
      that.context.setProgressVisibility(false);
      that.modalDismiss();
    }
  }
  modalDismiss(){
    this.props.onDismiss();
  }
  uploadImage(file){
    let that = this;
    let pid = that.props.pid;
    Services.uploadAPI(Constants.SERVER_URL+'upload',file,`?type=subjectivequiz&projectid=${pid}`)
      .then((response)=>{that.img = JSON.parse(response.text).url; that.setState({isUploading: false})});
  }
  onDrop(files) {
    this.uploadImage(files[0]);
    this.setState({isDragActive:false, isUploading: true, showPreview: true, img_url: files[0].preview});
  }
  onDragOver(){
    this.setState({isDragActive:true});
  }
  onDragLeave(){
    this.setState({isDragActive:false});
  }
  removeImage(){
    this.setState({isDragActive:false, showPreview: false, img_url: ''});
  }
  getQuizData(){
    this.data.title = React.findDOMNode(this.refs.title).value;
    this.data.subtitle = React.findDOMNode(this.refs.subtitle).value;
    this.data.text = React.findDOMNode(this.refs.question).value;
    if(this.img != '') {this.data.img = this.img}
    return this.data;
  }
  sendQuiz(){
    let that = this;
    let project = ProjectsStore.getProject(that.props.pid);
    if(project.relationship.owner) {

      var data2 = this.getQuizData();
      this.quiz_data.data.title = data2.title;
      this.quiz_data.data.subtitle = data2.subtitle;
      this.quiz_data.data.type = 'text';
      this.quiz_data.data.projectid = project.id;
      this.quiz_data.data.userid = project.relationship.owner.id;

      if(data2.img != ''){this.quiz_data.data.data = {text: data2.text, img: data2.img} }
      else{this.quiz_data.data.data = {text: data2.text}}

      var r = confirm(`Send Project Action to ${project.relationship.owner.name}`);
      if (r == true) {
        QuizAction.sendProjectAction(this.quiz_data);
        //QuizAction.SEND_PROJECT_ACTION(this.quiz_data);
        this.context.setProgressVisibility(true);
      }
    }else{
      alert('Unable to send action');
    }
  }
  render() {
    let self = this;
    let img_w = '150px',img_h = '120px';
    let labelStyle = {margin:'5px 0 0 30px',fontSize: '0.9rem',fontFamily:'Lato',fontWeight:'400',color:'#545454',alignSelf: 'flex-start'};
    let inputStyle = {width:'90%',margin:'5px auto', padding: '6px',fontSize: '0.9rem',fontFamily:'Lato', border:'none',backgroundColor:'#f2f2f2', outline:'none', marginBottom:'25px', borderRadius:'3px'};
    //let action = [{text:'Send', onTouchTap: this.sendQuiz.bind(this)}];
    let drop_style = {textAlign: 'center', height: '120px', width: '92%', margin:'0 auto', border: this.state.isDragActive ? '1px solid green' : '1px dashed #e6e6e6', borderRadius:'4px'};
    let dropZoneStyle = { display: 'flex', flexFlow: 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: '#f2f2f2', height: '100%'};
    let dividerstyle = { width:'100%', height:'1px', backgroundColor:'#e6e6e6', marginTop:'30px'};
    return (
      <div className="SubjectiveQuestion">
        <CustomModal isOpen={true} onClose={this.props.onDismiss.bind(this)} style={{top:'12%', height:'70%',left:'25%', width:'50%', borderRadius: '3px', padding:'0'}}>
          <div>
            <div onClick={this.modalDismiss.bind(this)}>
              <Svg className='cross-icon' iconType={'close_icon'} style={{fill:'#808080', zoom:'0.75', position: 'absolute', top: '20px', right: '20px',cursor:'pointer'}}/>
            </div>
          </div>
          <h6 className="SubjectiveQuestion-title"> Subjective Question </h6>
          <div className="SubjectiveQuestion-content">
            <span className="custom-input-label"> Design Title </span>
            <input ref = 'title' type='text' placeholder='Title' defaultValue={''} className="custom-input"/>
            <span className="custom-input-label"> Design Subtitle </span>
            <input ref = 'subtitle' type='text' placeholder='Sub-title' defaultValue={''} className="custom-input"/>
            <span className="custom-input-label"> Question </span>
            <textarea ref='question' rows='3' cols='50' placeholder='Question' defaultValue={''} className="custom-input"/>
            <span className="custom-input-label"> Image(Optional) </span>
            <div style={{width:'100%', margin:'5px 0',marginBottom:'25px', textAlign:'center'}}>
              {this.state.showPreview ?
                <div style={{display: 'inline-block', position:'relative'}}>
                  {this.state.isUploading ? <img style={{'WebkitFilter': 'blur(5px)'}} src={this.state.img_url} width={img_w} height={img_h}/>
                    : <img src={this.state.img_url} width={img_w} height={img_h}/>
                  }
                  <Svg iconType={'close_icon'} style={{position: 'absolute', top:'2px', right:'2px', fill: 'black'}}/>
                  {this.state.isUploading && <RefreshIndicator size={40} left={55} top={20} status='loading' />}
                </div> :
                <DropZone style= {drop_style} onDrop={self.onDrop.bind(self)} onDragOver={self.onDragOver.bind(self)} onDragLeave = {self.onDragLeave.bind(self)}>
                  {!self.state.isDragActive && <div style={dropZoneStyle}>
                    <Svg iconType={'plus-icon'} style={{width: '30px',display: 'block',fill: '#ccc'}}/>
                    <span style={{fontSize:'0.8em', color:'#ccc'}}> Drop your files here, or click to select files to upload. </span>
                  </div>
                  }
                </DropZone>
              }
            </div>
          </div>
          <div className="input-divider"> </div>
          <div className="SubjectiveQuestion-buttonWrapper">
            <button onClick={this.modalDismiss.bind(this)} className="custom-secondary-button">Cancel</button>
            <button onClick={this.sendQuiz.bind(this)} className="custom-primary-button">Send</button>
          </div>

        </CustomModal>
      </div>
    )
  }
}
export default SubjectiveQuestion;
