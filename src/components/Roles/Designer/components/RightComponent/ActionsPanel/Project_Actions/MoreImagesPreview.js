/**
 * Created by vikramaditya on 9/15/15.
 */
import React from 'react';
import CustomModal from '../../../../../../../lib/Modal';
class MoreImagesPreview extends React.Component {
  constructor() {
    super();
  }

  componentWillMount() {

  }

  componentWillUnmount() {

  }
  modalDismiss(){
    this.props.onDismiss();
  }
  render() {
    let title = this.props.data.title;
    let subtitle = this.props.data.subtitle;
    return (
      <CustomModal isOpen={true} onClose={this.modalDismiss.bind(this)} style={{left:'30%', width:'40%', top:'25%',height:'50%'}}>
        <p>{title}</p>
        <p>{subtitle}</p>
      </CustomModal>
    )
  }
}
export default MoreImagesPreview;
