import React from 'react';
import _ from 'lodash';
import DropZone from 'react-dropzone';
import {ListDivider, SvgIcon, Dialog,ListItem, RefreshIndicator, TextField} from 'material-ui';
import Helper from '../../../../../../../../../helper.js';
import Services from '../../../../../../../../../../services/Services.js';
import styles from './ImageOptions.scss';
var {withStyles, TriggerTypes, AppConstants: Constants, ProjectActions: QuizAction, ProjectStore} = Helper;

@withStyles(styles) class ImageOptions extends React.Component {
  constructor() {
    super();
    this.state = {
      isDragActive: false,
      imageUrls: [],
      obj: {
        type: 'single-text-image',
        question: {},
        options: []
      },
      add: true
    };
  }

  componentWillMount() {
    var x = _.cloneDeep(this.state.obj);
    x.options.push({
      img: '',
      value: ''
    });
    x.options.push({
      img: '',
      value: ''
    });
    x.options.push({
      img: '',
      value: ''
    });
    x.options.push({
      img: '',
      value: ''
    });
    this.setState({obj: x});
  }

  componentWillReceiveProps(nextProps) {
    var x = nextProps.data;
    if (x.options.length == 2) {
      this.setState({obj: x, add: true});
    }
    else {
      this.setState({obj: x, add: false});
    }

  }

  componentDidMount() {
    var x = this.props.data;
    if (x.options.length == 2) {
      this.setState({obj: x, add: true});
    }
    else {
      this.setState({obj: x, add: false});
    }
  }

  getValueQuestion(e) {
    var questext = e.target.value;
    var x = _.cloneDeep(this.state.obj);
    x.question.text = questext;
    this.setState({obj: x});
  }

  getValueOptions(ind, e) {
    var opt = e.target.value;
    var x = _.cloneDeep(this.state.obj);
    x.options[ind].img = opt;
    this.setState({obj: x});
  }

  getValueOptionsVal(ind, e) {
    var opt = e.target.value;
    var x = _.cloneDeep(this.state.obj);
    x.options[ind].value = opt;
    this.setState({obj: x});
  }

  addOption() {
    if (this.state.obj.options.length == 2) {
      var x = _.cloneDeep(this.state.obj);
      x.options.push({
        img: '',
        value: ''
      });
      x.options.push({
        img: '',
        value: ''
      });
      this.setState({obj: x, add: false});
    }
  }

  submit() {
    var x = _.cloneDeep(this.state.obj);
    var y = _.cloneDeep(this.state.imageUrls);
    if (this.state.imageUrls.length == 4) {
      for (var i = 0; i < y.length; i++) {
        x.options[i].img = y[i].url;
      }
      console.log(x);
      this.setState({obj: x});
      this.props.submitQuestion(x);
    }
    else {
      alert('Make sure you uploaded four images!');
    }

  }

  onDrop(files) {
    console.log(files);
    let arr = [];
    let length = this.state.imageUrls.length;
    for (let i = 0; i < files.length; i++) {
      arr.push({isUploading: true, preview: files[i].preview, url: ''})
      this.uploadImage(files[i], length + i);
    }
    this.setState({imageUrls: arr});
  }

  uploadImage(file, index) {
    let pid = ProjectStore.getProject().id;
    Services.uploadAPI(Constants.SERVER_URL + 'upload', file, `?type=quiz&projectid=${pid}`)
      .then((response) => {
        let url = response.body.url;
        let x = this.state.imageUrls;
        if (x[index]) {
          x[index].url = url;
          x[index].isUploading = false;
          this.setState({imageUrls: x});
        }
      })

  }

  onDragOver(value) {
    this.setState({isDragActive: value})
  }

  handleChange(index, e) {
    let x = this.state.obj;
    x.options[index].value = e.target.value;
    this.setState({obj: x});
  }

  removeOption(index) {
    let x = this.state.imageUrls;
    x.splice(index, 1);
    this.setState({imageUrls: x});
  }

  uploadComponent() {
    let imageUrl = '';
    let drop_style = {
      textAlign: 'center',
      height: '120px',
      width: '150px',
      border: this.state.isDragActive ? '1px solid green' : '1px dashed black'
    };
    return (
      <div>
        {this.state.imageUrls.map((data, index)=> {
          return (
            <div key={index} style={{position:'relative'}} className="eachUpload">
              {data.isUploading ?
                <img src={data.preview} style={{'WebkitFilter': 'blur(5px)'}} width='150' height='120'/> :
                <img src={data.preview} width='150' height='120'/>}
              {data.isUploading && <RefreshIndicator size={40} left={55} top={40} status='loading'/>}
              <TextField type='text' placeholder='value' onChange={this.getValueOptionsVal.bind(this,index)}
                         className="eachUpload-input" value={data.value}/>
              <button onClick={this.removeOption.bind(this,index)} className="eachUpload-delete"> Delete</button>
            </div>)
        })
        }
        <DropZone style={drop_style} onDrop={this.onDrop.bind(this)} onDragOver={this.onDragOver.bind(this, true)}
                  onDragLeave={this.onDragOver.bind(this, false)}>
          {!this.state.isDragActive && <div>
            <SvgIcon>
              <path fill='#000000' d={Constants.PLUS_ICON}/>
            </SvgIcon>
            <span style={{fontSize:'0.6em'}}> Drop your files here, or click to select files to upload. </span>
          </div>
          }
        </DropZone>
      </div>
    );
  }

  render() {
    let self = this;
    let value = this.state.value;
    return (
      <div className="Form">
        <div className="question">
          <span className="question-title"> Question: </span>

          <div className="question-text">
            <span className="question-label"> Text: </span>
            <TextField ref='questionText' type='text' className="question-input" placeholder='Question'
                       value={this.state.obj.question.text} onChange={self.getValueQuestion.bind(self)}/>
          </div>
        </div>
        <div className="options">
          <span className="options-title"> Options: </span>
          {self.props.data.options[0].img == '' ? self.uploadComponent() :
            <OptionsViewer data={self.props.data.options}/>}
          <div className="buttons">
            <div className="submit">
              <button className="submit-button" onClick={self.submit.bind(self)}> Submit</button>
            </div>
          </div>

        </div>
      </div>
    );
  }
}
export default ImageOptions;

class OptionsViewer extends React.Component {
  render() {
    return (
      <div className="OptionsViewer">
        {
          this.props.data.map(function (item, index) {
            return (
              <div className="OptionsViewer-wrapper">
                <img className="OptionsViewer-image" src={item.img}/>

                <div className="OptionsViewer-label">{item.value}</div>
              </div>
            );
          })
        }
      </div>
    );
  }
}
/*
 (self.state.obj.options.map(function (opt, index) {

 return (
 <div key={index}>

 {
 // <TextField ref='option' type='text' className="options-input" placeholder='Image URL' value={opt.img}
 //        onChange={self.getValueOptions.bind(self,index)}/>
 // <TextField ref='OptValue' type='text' className="options-input-label" placeholder='Value' value={opt.value}
 //        onChange={self.getValueOptionsVal.bind(self,index)}/>
 }
 </div>
 );
 }))
 */
