/**
 * Created by vikramaditya on 6/20/15.
 */

import React from 'react'
var ReactCSSTransitionGroup = React.addons.CSSTransitionGroup;
import mui from 'material-ui';
import _ from 'lodash';
import SmartTimeAgo from 'react-smart-time-ago';
import Helper from '../../../../../helper.js';
import Svg from '../../../../../Shared/Svg/Svg';
var {DRPStore, DRPActions, AppConstants: Constants, withStyles, LoginStore, UnreadDataStore, UnreadDataActions} = Helper;

import styles from './Comments.scss';
var {ListDivider, Avatar, SvgIcon} = mui;
let adminImageUrl = Constants.ADMIN_IMAGE_URL;

let UNREAD_STATUS = '01';
let READ_STATUS = '11';
let NONE_STATUS = '00';
@withStyles(styles)
class Comments extends React.Component {
  static contextTypes = {
    setProgressVisibility: React.PropTypes.func
  };

  constructor(props) {
    super(props);
    let that = this;
    that.unread_comments = {};
    that.state = {
      comments: _.result(props.drp, 'attributes.comment',[]),
      unread_color: '#C1CCD0'
    };
    that.unsubscribe_drp_store = DRPStore.listen(that.handleDesignResponse.bind(that));
  }
  componentWillReceiveProps(nextProps){
    let that = this;
    that.setState({comments: _.result(nextProps.drp, 'attributes.comment',[])});
  }
  componentWillUnmount() {
    this.unsubscribe_drp_store();
  }
  handleDesignResponse(trigger) {
    let that = this;
    if (trigger.drp) {
      let drp = DRPStore.getDesignResponse(that.props.drp.id);
      that.setState({comments:  _.result(drp, 'attributes.comment',[])});
    }
  }
  //onStatusChange(props) {
    //if(props == TriggerTypes.COMMENT_UNREAD){
    //  this.setUnreadComments();
    //  let drid = ProjectStore.getDesignResponseID();
    //  Object.keys(this.unread_comments).map((id, index) => {
    //    if(index == 0 ){
    //      ProjectActions.READ_DOCUMENT(id, 'COMMENT', drid);
    //      this.setState({unread_color: 'transparent'});
    //    }else{
    //      ProjectActions.READ_DOCUMENT(id);
    //    }
    //  });
    //}
  //}
  //setUnreadComments(){
  //  this.unread_comments = {};
  //  let ids = ProjectStore.getUnreadComments();  // NOT giving unread comments, giving all comments
  //  let drid = ProjectStore.getDesignResponseID();
  //  if(ids[drid] && Object.keys(ids[drid]).length > 0){
  //    Object.keys(ids[drid]).map((id,index) => {
  //      if(ids[drid][id] == UNREAD_STATUS){
  //        this.unread_comments[id] = UNREAD_STATUS;
  //      }
  //    })
  //  }
  //}
  postComment(comment) {
    let that = this, userid = LoginStore.getUserID(), imgurl = adminImageUrl;
    let projectid = that.props.project.id;
    let drid = that.props.drp.id;
    let new_comment = {
      attributes: {
        created_at: Date.now(),
        imageUrl: imgurl,
        text: comment,
        userid: userid,
        username: 'Dekorate'
      }, id: '', type: ''
    };
    if (comment.trim() == '') {
      alert('Empty Comment');
    }
    else {
      //React.findDOMNode(this.refs.commentbox).value = '';
      let prev_comments = that.props.drp.attributes.comment;
      prev_comments.unshift(new_comment);
      DRPActions.postComment(drid, comment.trim(), userid, imgurl, projectid);
      that.setState({comments: prev_comments});
    }
  }

  sendComment(e) {
    let that = this;
    if (e.keyCode == 13) {
      let comment = _.cloneDeep(React.findDOMNode(that.refs.commentbox).value);
      React.findDOMNode(that.refs.commentbox).value = '';
      e.preventDefault();
      that.postComment(comment);
    }
  }
  deleteComment(drid, id, index, text){
    let that = this;
    if(!(drid && id)){
      alert('Error');
    }else {
      let r = confirm(`Delete comment ? \n${text}`);
      if (r == true) {
        let prev_comments = that.state.comments;
        prev_comments.splice(index, 1);
        that.setState({comments: prev_comments});
        let projectid = that.props.project.id;
        DRPActions.deleteComment(projectid, drid, id);
      }
    }
  }
  getStatus(cid){
    let status = UnreadDataStore.getStatus(cid) == UNREAD_STATUS;
    status && UnreadDataActions.readData(cid);
    return status;
  }
  getCommentSection(comments){
    let self = this;
    return (
      comments.map((comment,index) => {
        return (
          [<div className ='comment-row'>
            <div className = 'left-avatar'>
              <Avatar style={{zoom:'0.6'}} src={comment.attributes.imageUrl}/>
            </div>
            <div className = 'right-container'>
              <div className = 'top-header'>
                <span className = 'username'>{comment.attributes.username}</span>
                <span className = 'separator'> . </span>
                <span className ='time-ago'><SmartTimeAgo value={comment.attributes.created_at}/></span>
              </div>
              <div className ='comment-text'>{comment.attributes.text}</div>
              <div onClick={self.deleteComment.bind(self,comment.drid,comment.id,index, comment.attributes.text)} className = 'comment-cross'>
                <Svg className='cross-icon' iconType={'close_icon'}/>
              </div>
            </div>
          </div>,
          <ListDivider />]
        );
      })
    )
  }
  render() {
    let self = this;
    let comments = self.state.comments;
    let unreadComments = comments.filter((comment)=> self.getStatus(comment.id));
    let readComments = comments.filter((comment)=> !self.getStatus(comment.id));
    return (
      <div className = 'comments-container'>
        <div className ='body'>
          <div className = 'top-container'>
            <div className ='top-label'>
              <span style={{color:'#666666'}}>Comments</span>
            </div>
          </div>
          <textarea rows ='4' className ='comment-box' ref='commentbox' onKeyDown={self.sendComment.bind(self)} placeholder='Add a comment...' />

          {/*<input  type='text' className ='comment-box' ref='commentbox' onKeyDown={self.sendComment.bind(self)} placeholder='Add a comment...'/>*/}

          <div className ='comments-list'>
            {comments.length == 0 && <div className ='no-comment-list'>No Comments yet</div>}
            <ReactCSSTransitionGroup transitionName="example" transitionAppear={true}>
             {self.getCommentSection(unreadComments)}
            </ReactCSSTransitionGroup>
             {self.getCommentSection(readComments)}
          </div>
        </div>
      </div>
    );
  }
}
export default Comments;
