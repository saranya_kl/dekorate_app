/**
 * Created by vikramaditya on 7/23/15.
 */
/*eslint-disable*/
import React from 'react';
import _ from 'lodash';
import mui from 'material-ui';
import Moment from 'moment';
import QuizViewer from '../../RightComponent/ActionsPanel/Project_Actions/QuizViewer/QuizViewer';
import Helper from '../../../../../helper.js';
import styles from './TimeLine.scss';
import Svg from '../../../../../Shared/Svg/Svg';
var {AppConstants: Constants, TimeLineConstants, withStyles, UsersStore, TimelineStore, TimelineActions, UsersActions} = Helper;

let TYPE = TimeLineConstants.TIMELINE_TYPE;
@withStyles(styles) class TimeLine extends React.Component {

  static contextTypes = {
    router: React.PropTypes.func,
    muiTheme: React.PropTypes.object
  };

  constructor() {
    super();
    this.state = {
      timeline: [],
      isLoading: true,
      user: {},
      userImg: '',
      style_quiz_message: '',
      styleQuizResponse: {},
      showStyleQuiz: false
    }
  }

  componentWillMount() {
    let that = this;
    let project = that.props.project;
    let pid = project.id, uid = _.result(project, 'relationship.owner.id', undefined);
    if (pid && uid) {
      that.initialFetch(that.props);
      that.setStyleQuiResponse(that.props);
    }
    that.unsubscribe = UsersStore.listen(that.handleUsersStore.bind(that));
    that.unsubscribe_timeline = TimelineStore.listen(that.handleTimeLine.bind(that));
  }
  componentWillReceiveProps(nextProps){
    let that = this;
    if(nextProps.project.id !== that.props.project.id){
      let pid = nextProps.project.id, uid = _.result(nextProps, 'project.relationship.owner.id', undefined);
      if (pid && uid) {
        that.initialFetch(nextProps);
        that.setStyleQuiResponse(nextProps);
      }
    }
  }
  initialFetch(props) {
    let that = this;
    let project = props.project;
    let pid = project.id, uid = _.result(project, 'relationship.owner.id', undefined);
    if (TimelineStore.getTimeLine(pid) == Constants.ITEM_LOADING) {
      TimelineActions.fetchTimeLine(uid, pid);
      UsersActions.fetchUserDetails(uid);
      that.setState({user: project.relationship.owner, timeline: [], isLoading: true});
    } else {
      that.setState({timeline: TimelineStore.getTimeLine(pid).data, user: project.relationship.owner, isLoading: false});
    }
  }
  handleUsersStore(){
    let that = this;
    that.setStyleQuiResponse(that.props);
  }
  handleTimeLine(trigger){
    let that = this;
    let project = that.props.project;
    let pid = project.id;
    if(trigger.timeline) {
      that.setState({timeline: TimelineStore.getTimeLine(pid).data, isLoading: false});
    }
    if(trigger.error){
      that.setState({timeline: [], isLoading: false});
    }
  }
  //handleProjectStore(trigger){
  //  let that = this;
  //  if(trigger.project){
  //    //console.log(trigger.project);
  //    let pid = trigger.project.id, uid = _.result(trigger, 'project.relationship.owner.id', undefined);
  //    TimelineActions.fetchTimeLine(uid, pid);
  //    UsersActions.fetchUserDetails(uid);
  //  }
  //  if(trigger.error){
  //
  //  }
  //}
  //onStatusChange(props) {
  //  let that = this;
  //  let project = that.props.project;
  //  let pid = project.id, uid = _.result(project, 'relationship.owner.id', undefined);
  //  //TODO handle actions trigger
  //  if (props == TriggerTypes.FETCHED_PROJECT) {
  //    this.initialFetch();
  //  }
  //  if (props == TriggerTypes.SENT_STYLE_QUIZ) {
  //    TimelineActions.fetchTimeLine(uid, pid);
  //  }
  //  if (props == TriggerTypes.PROJECT_ACTION_SENT) {
  //    TimelineActions.fetchTimeLine(uid, pid);
  //  }
  //  if(props == TriggerTypes.FETCHED_USER_DETAILS){
  //    this.setStyleQuiResponse();
  //  }
  //}

  setStyleQuiResponse(props){
    let that = this;
    let project = props.project;
    let userid =  _.result(project, 'relationship.owner.id', undefined);
    let userdetails = UsersStore.getUser(userid);
    let num_actions = userdetails.actions ? userdetails.actions.length : 0;
    let quizresponse = userdetails.actions ? userdetails.actions : [];
    var message = '';
    if(num_actions == 0){
      message = 'Style quiz not yet sent';
    }else{
      let msg = quizresponse[0].response ?
        'Response <b style="color:#666">available</b> for style quiz'
        :
        '<b style="color:#666">Awaiting response</b> on Style quiz';
      message = msg;
    }
    that.setState({style_quiz_message: message, styleQuizResponse: quizresponse});
  }

  componentWillUnmount() {
    this.unsubscribe();
    this.unsubscribe_timeline();
    //this.unsubscribe_project_store();
  }

  parseDate(str) {
    var mdy = str.split('/')
    return new Date(mdy[2], mdy[0]-1, mdy[1]);
  }

  daydiff(first, second) {
    return Math.round((second-first)/(1000*60*60*24));
  }

  calcTime(time){
    var time1 = this.parseDate(Moment(time).format('l'));
    var time2 = this.parseDate(Moment().format('l'));
    var daydiff = this.daydiff(time1,time2);
    if(daydiff > 3)
    {
      return Moment(time).format("MMM Do");
    }
    else{
      var time = Moment(time).fromNow();
      var space = time.indexOf(' ');
      var part1 = time.substring(0,space) == 'a' ? '1' : time.substring(0,space);
      var part2 = time.substring(space+1,space+2);
      return part1+part2;
    }
    return 0;
  }
  hideQuizViewer(){
    this.setState({showStyleQuiz: false});
  }
  render() {
    let self = this;
    let timeline = self.state.timeline ? self.state.timeline : [];
    let isLoading = self.state.isLoading;
    let user = self.state.user;
    let svgiconStyle = {fill: 'white', backgroundColor: 'coral', borderRadius: '50%', marginLeft: '4px', padding: '4px', zIndex: '1', width:'13px', height:'13px'};
    let style_quiz_message = self.state.style_quiz_message;
    let awaiting = self.state.style_quiz_message=='<b style="color:#666">Awaiting response</b> on Style quiz';
    let styleQuizResponse = self.state.styleQuizResponse;
    return (
      <div className="TimelineViewer">
        <div className='TimelineViewer-stylequiz'>
          <img src={user.imageUrl} className="TimelineViewer-list-img"/>
          <div className="TimelineViewer-listitem" style={{border:  awaiting ? '1px dashed #e6e6e6' : 'none', backgroundColor:awaiting ? '#f5f5f5' : 'white'}}>
            <Svg className="TimelineViewer-listitem-icon" iconType={'left-icon'} style={{fill: awaiting ? '#f5f5f5' : 'white'}}/>
            <div className="TimelineViewer-stylequiz-content">
              <div className="flex-row">
                <div className='header-icon'>
                  <Svg iconType={'style-quiz'} style={svgiconStyle}/>
                </div>
                <div className="wrapper">
                  <span dangerouslySetInnerHTML={{__html: style_quiz_message}}></span>
                </div>
              </div>
              <div>
                {styleQuizResponse[0] && styleQuizResponse[0].response && <button style={{margin:'0', marginRight:'5px', padding:'4px'}} className='custom-secondary-button' onClick={()=>self.setState({showStyleQuiz: true})}>VIEW</button>}
              </div>
            </div>
          </div>
        </div>
        {self.state.showStyleQuiz && <QuizViewer quizid={styleQuizResponse[0].quizid} response={styleQuizResponse[0].response} onDismiss={self.hideQuizViewer.bind(self)}/>}
        <div className="TimelineViewer-content">

          {
            (timeline.map(function (item, i) {
              var title = '';
              var my_msg = '';
              var url = '';
              var type = TYPE[item.type] ? TYPE[item.type].key : '';
              switch (type) {
                case 'USER':
                  title = `${user.name}`;
                  my_msg = `${TYPE[item.type].my_msg}`;
                  url = user.imageUrl;
                  break;
                case 'SERVER':
                  title = 'Dekorate';
                  my_msg = `${TYPE[item.type].my_msg}`;
                  url = Constants.ADMIN_IMAGE_URL;
                  break;
                case 'ANY':
                  title = `${(item.customdata && item.customdata.commentername) || item.text }`;
                  my_msg = `${TYPE[item.type].my_msg}`;
                  if(item.customdata){
                    if(item.customdata.commentername=='Dekorate')
                    {
                      url = Constants.ADMIN_IMAGE_URL;
                    }
                    else{
                      url = item.customdata.imageUrl;
                    }
                  }
                  else{
                    url = user.imageUrl;
                  }
                  break;
                default:
                  title = 'Something else happened unable to show on timeline';
              }

              {
                svgiconStyle = {fill: 'white',backgroundColor: 'coral', borderRadius: '50%', marginLeft: '4px', padding: '4px', zIndex: '1', width:'13px', height:'13px'};
                switch (item.type) {
                  case 'designresponse-submitted':
                    svgiconStyle.backgroundColor= '#3498db';
                    break;
                  case 'userfeedback-submitted':
                    svgiconStyle.backgroundColor= '#27ae60';
                    break;
                  case 'projectaction-submit':
                    svgiconStyle.backgroundColor= '#3498db';
                    break;
                  case 'project-created':
                    svgiconStyle.backgroundColor= '#1abc9c';
                    break;
                  case 'project-close':
                    svgiconStyle.backgroundColor= '#2ecc71';
                    break;
                  case 'comment-created':
                    svgiconStyle.backgroundColor= '#e74c3c';
                    break;
                  default:
                    svgiconStyle.backgroundColor= 'coral';
                    break;
                }
              }
              var time = Moment(item.created_at).fromNow();
              var space = time.indexOf(' ');
              var part1 = time.substring(0,space) == 'a' ? '1' : time.substring(0,space);
              var date1 = Moment(item.created_at).format('l');
              var date2 = Moment().format('l');
              var cut_time = self.calcTime(item.created_at);
              return (
                <div key={i} className="TimelineViewer-list">
                  <div>
                    <img src={url} className="TimelineViewer-list-img"/>
                  </div>
                  <div className="TimelineViewer-listitem">
                    <Svg className="TimelineViewer-listitem-icon" iconType={'left-icon'} style={{fill:'#fff'}}/>
                    <div className="TimelineViewer-listitem-header">
                      <div>
                        <div className='header-icon'>
                          <Svg iconType={item.type} style={svgiconStyle}/>
                        </div>
                        <div className="wrapper">
                          <span className="TimelineViewer-listitem-header-title">{title}</span><span
                          className="TimelineViewer-listitem-header-subtitle">{my_msg} &nbsp;</span>
                          {
                            item.type == 'project-created' ? <span onClick={self.props.switchMenu.bind(self, 0)} className="TimelineViewer-listitem-link">"{item.text}"</span>
                            :
                            item.text && <span className="TimelineViewer-listitem-text">"{item.text}"</span>
                          }
                        </div>
                      </div>
                      <span className="TimelineViewer-date"> {cut_time}</span>
                    </div>

                    {
                      item.imageUrl && <div className="TimelineViewer-listitem-thumbnails">
                        { item.imageUrl.map(function (image, j) {
                          return (
                            <div key={j} className='image-container'>
                              {j <= 2 ? <img key={j} src={image} height={50} width={50} className="image"/>
                                : item.imageUrl.length == 4 ?
                                <img src={image} height={50} width={50} className="image"/>
                                : j == 3 && [<img key={j} src={image} height={50} width={50} className="image"/>,
                                <div key={j*j} className='image-overlay'></div>,
                                <div key={j+j} className='overlay-text'><p>{`+${item.imageUrl.length - 3}`}</p></div>]
                              }
                            </div>
                          )
                        }) }
                      </div>
                    }
                  </div>


                </div>
              );
            }))
          }
          {isLoading && <h3 className='TimelineViewer-loading-text'>Loading Timeline ...</h3>}
        </div>
      </div>
    );
  }
}

export default TimeLine;
