import React from 'react';
import mui from 'material-ui';
import _ from 'lodash';
import styles from './AssignProject.scss';
import Helper from '../../../../../helper.js';
import CustomModal from '../../../../../../lib/Modal';
var {AppConstants: Constants, withStyles, ProjectAssignedUserStore, ProjectAssignedUserActions, ProjectsStore, ProjectsActions} = Helper;
var {SvgIcon, Avatar, ListDivider} = mui;
const ALLOWED_ROLES = {'designer': 'Designer', 'visualizer': 'Visualizer', 'modeller': 'Modeller'};

@withStyles(styles) class AssignProject extends React.Component {

  static contextTypes = {
    setProgressVisibility: React.PropTypes.func,
    router: React.PropTypes.func,
    muiTheme: React.PropTypes.object
  };

  constructor() {
    super();
    this.state = {
      backend_users: [],
      project_users: [],
      non_project_users: [],
      user_with_roles: {},
      roles: []
    };
  }

  componentWillMount() {
    let that = this;
    let pid = that.props.pid;
    //TODO use some conditions in fetch all backend users
    let allBackendUsers = ProjectAssignedUserStore.getAllBackendUsers();
    if(allBackendUsers.length == 0){
      ProjectAssignedUserActions.fetchAllBackendUsers();
    }else{
      that.separateUsers(pid);
      that.setState({backend_users: allBackendUsers});
    }
    ProjectAssignedUserActions.fetchAssignedUsers(pid);
    that.unsubscribe_assignedUserStore = ProjectAssignedUserStore.listen(that.handleAssignedUserStore.bind(that));
    that.unsubscribe = ProjectsStore.listen(that.handleProjectStore.bind(that));
  }

  componentWillUnmount() {
    let that = this;
    that.unsubscribe();
    that.unsubscribe_assignedUserStore();
  }
  handleProjectStore(trigger){
    let that = this;
    if(trigger.project){
      that.context.setProgressVisibility(false);
      that.modalDismiss();
    }
    if(trigger.error){
      that.context.setProgressVisibility(false);
    }
  }
  handleAssignedUserStore(trigger){
    let that = this;
    if(trigger.error){
      that.context.setProgressVisibility(false);
    }
    if(trigger.users){
      that.separateUsers(that.props.pid);
      that.context.setProgressVisibility(false);
    }
    if(trigger.allBackendUsers){
      that.separateUsers(that.props.pid);
      that.setState({backend_users: ProjectAssignedUserStore.getAllBackendUsers()});
      that.context.setProgressVisibility(false);
    }
  }

  assignProject(user, role) {
    let r = confirm(`Assign Project to ${user['firstname']} ${user['lastname']}`), that = this;
    let pid = that.props.pid;
    if (r == true) {
      ProjectsActions.assignProject(user.id, pid, role);
      this.context.setProgressVisibility(true);
    }
  }

  unAssignUser(user) {
    var r = confirm(`Remove ${user['firstname']} ${user['lastname']} from Project`), that = this;
    let pid = that.props.pid;
    if (r == true) {
      ProjectsActions.unAssignProject(user.id, pid);
      this.context.setProgressVisibility(true);
    }
  }

  separateUsers(pid) {
    let data = ProjectAssignedUserStore.getAllBackendUsers();
    let ids = ProjectAssignedUserStore.getAssignedUsers(pid);
    let non_project_users = [];
    let project_users = [];
    data.map((user) => {
      non_project_users.push(user);
      for (let i = 0; i < ids.length; i++) {
        if (user.id == ids[i].userid) {
          let tmp = _.cloneDeep(user);
          tmp.currentRole = ids[i].role;
          project_users.push(tmp);
          non_project_users.pop();
        }
      }
    });
    this.splitUsers(non_project_users);
    this.setState({project_users: project_users, non_project_users: non_project_users});
  }

  splitUsers(non_project_users) {
    let users = non_project_users;
    var all_roles = {};
    users.map((user) => {
      let role = user.roles;
      role.map((value)=> {
        let current_role = value.role;
        if (!all_roles[current_role]) {
          all_roles[current_role] = [];
        }
        all_roles[current_role].push(user);
      })
    });
    var roles = Object.keys(all_roles);
    this.setState({user_with_roles: all_roles, roles: roles});
  }

  //filter(e){
  //var data = this.state.data;
  //let val = e.target.value;
  //var reg = (val == '') ? new RegExp('(.)*','g') : new RegExp(`${val}(.)*`,'g');
  //console.log(val,reg);
  //var filtered = [];
  //for(let i=0;i<data.length;i++){
  //  if(reg.test(data[i])){
  //    filtered.push(data[i]);
  //  }
  //}
  //this.setState({data: filtered});
  //}
  modalDismiss() {
    this.props.onDismiss();
  }

  render() {
    let self = this;
    return (
      <CustomModal isOpen={true} style={{left:'30%', top:'10%', height:'80%',width: '40%'}}
                   onClose={this.modalDismiss.bind(this)}>
        <div className="AssignProject">
          <div className="AssignProject-header">
            <span>Assign Project</span>
          </div>
          <div className="AssignProject-content">
            <h5 style={{padding: '10px 10px',margin:'0',borderBottom: '1px solid #e6e6e6'}}>Assigned Users</h5>
            {
              self.state.project_users.map(function (user, i) {
                //TODO change for more roles
                var badgeColor = '';
                switch (user.currentRole) {
                case 'designer':
                  badgeColor = '#27ae60';
                  break;
                case 'visualizer':
                  badgeColor = '#3498db';
                  break;
                case 'modeller':
                  badgeColor = '#e67e22';
                  break;
                default:
                  badgeColor = '#e74c3c'
                }
                return (
                  <div key={i} className="AssignProject-assigned-users-list">
                    <div className='flex-row AssignProject-assigned-users-list-align-center'>
                      <Avatar style={{zoom: '0.6',margin: '0 5px'}} src={user.imageUrl}/>
                      <span> {`${user.firstname} ${user.lastname}`} </span>
                      <span
                        style={{color:badgeColor, alignSelf: 'center', marginLeft: '15px', borderRadius: '2px', padding: '0px 5px',border: '1px solid'}}>{user.currentRole.toUpperCase()}</span>
                    </div>
                    <div style={{alignSelf: 'center'}}>
                      <SvgIcon onClick={self.unAssignUser.bind(self, user)} className='cross-icon'
                               style={{cursor:'pointer', fill:'#666666', zoom:'0.75'}}>
                        <path d={Constants.ICONS.close_icon}></path>
                      </SvgIcon>
                    </div>
                  </div>
                );
              })
            }

            {
              /*
               <h5 style={{padding: '10px 10px',margin:'0',borderBottom: '1px solid #e6e6e6'}}>Users</h5>
               <div className="AssignProject-searchlist Search">
               <input type="text" className="AssignProject-searchbar" onChange={self.filter.bind(self)} placeholder="Search for the designer.."/>
               </div>

               self.state.non_project_users.map(function (user, i) {
               return (
               <div onClick = {self.assignProject.bind(self,user.id,i)} key={i} className="AssignProject-searchlist">
               <Avatar style={{zoom: '0.6',margin: '0 5px'}} src={user.imageUrl}/>
               <span> {`${user.firstname} ${user.lastname}`} </span>
               </div>
               );
               })*/
            }
            <div className="AssignProject-assign">
              {
                Object.keys(self.state.user_with_roles).map((role, ind)=> {
                  let arr = self.state.user_with_roles[role];
                  return (
                    ALLOWED_ROLES[role] && <div key={ind} className="AssignProject-assign-usersList">
                      <h5 style={{padding: '10px 10px',margin:'0',borderBottom: '1px solid #e6e6e6'}}>{ALLOWED_ROLES[role]}</h5>
                      {arr.map((user, i) => {
                        return (
                          <div onClick={self.assignProject.bind(self, user, role)} key={i}
                               className="AssignProject-searchlist">
                            <Avatar style={{zoom: '0.6',margin: '0 5px'}} src={user.imageUrl}/>
                            <span> {`${user.firstname} ${user.lastname}`} </span>
                          </div>)
                      })
                      }
                    </div>
                  )
                })
              }
            </div>
          </div>
        </div>
      </CustomModal>
    );
  }
}
/*
 <SvgIcon onClick={self.props.showRightComponent.bind(false,'USER_ASSIGN')} className='cross-icon'
 style={{cursor:'pointer', fill:'#666666', zoom:'0.75'}}>
 <path d={Constants.ICONS.close_icon}></path>
 </SvgIcon>
 *
 * <div className="AssignProject-notes">
 <div className="AssignProject-notes-title"> Notes</div>
 <textarea placeholder="Type text here.." className="AssignProject-notes-input"/>

 </div>
 <div className="AssignProject-notes-buttons">
 <button className="AssignProject-notes-button">Cancel</button>
 <button className="AssignProject-notes-button">Assign</button>
 </div>*/
export default AssignProject;
