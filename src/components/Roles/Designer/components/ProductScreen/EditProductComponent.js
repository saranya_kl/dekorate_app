/**
 * Created by vikramaditya on 10/13/15.
 */
import React from 'react';
import mui from 'material-ui';
import _ from 'lodash';
import DropZone from 'react-dropzone';
import Services from '../../../../../services/Services.js';
import ImageGrid from '../../../../Shared/ImageGrid/ImageGrid.js';
import Helper from '../../../../helper.js';
import ProductItemView from './ProductRepository/ProductItemView.js';
import ProductRenderedView from './ProductRepository/ProductRenderedView.js';
import styles from './ProductRepository/ProductRepository.scss';
import CascadeSelect from './CascadeSelect/CascadeSelect.js';
import Grid from './Grid/Grid.js';
var {DownloadUtility, CustomUtility, ProductConstants, TriggerTypes, ImageUploadConstants, ProductsStore, ProductsAction, withStyles, AppConstants: Constants} = Helper;

var {SvgIcon} = mui;

var {CircularProgress} = mui;

let CURRENT_PASS = Constants.ADMIN_PASS;

@withStyles(styles) class EditProductComponent extends React.Component {

  constructor() {
    super();
    this.state = {
      editable: true,
      isDirty: true,
      product: _.cloneDeep(ProductConstants.DUMMY_PRODUCT),
      inProgress: false
    };
  }

  componentWillMount() {
    var self = this;
    self.props.data.productid && self.setState({product: _.cloneDeep(self.props.data)});
    //self.unsubscribe_product = ProductStore.listen(self.onStatusChange.bind(self));
  }

  componentWillUnmount() {

  }

  setEditableProperty() {
    this.setState({editable: !this.state.editable});
  }

  setDirtyProperty(e, value) {
    var self = this;
    var data = _.cloneDeep(this.state.product);
    data.isdirty = value;
    this.refs.dirty.setToggled(value);
    this.setState({data: data, dirty: value});
  }


  componentWillReceiveProps(nextProps) {
    let self = this;
    self.setState({product: _.cloneDeep(nextProps.data)});
  }
  onProductSave(){
    let that = this;
    that.unsubscribe_productsstore();
    that.setState({inProgress: false});
  }
  uploadImage(tag, files) {
    this.setState({inProgress: true});
    CustomUtility.uploadProductImage(files, tag, this.state.product.productcode)
      .then((response) => this.setImage(response))
      .catch(() => this.setState({inProgress: false}))
  }
  setImage(r){
    let {url, tag} = r;
    if (tag == ImageUploadConstants.IMAGE) {
      let data = _.cloneDeep(this.state.product);
      this.image_count = this.state.product.assets.image.length;
      data.assets.image[this.image_count++] = url;
      this.setState({data: data, previewImage: url, inProgress: false});
    }
    if (tag == ImageUploadConstants.RENDERED) {
      let data = _.cloneDeep(this.state.product);
      this.render_count = this.state.product.assets.renderedimage.length;
      data.assets.renderedimage[this.render_count++] = url;
      this.setState({data: data, previewRendered: url, inProgress: false});
    }
    if (tag == ImageUploadConstants._3D) {
      let data = _.cloneDeep(this.state.product);
      this.model_count = this.state.product.assets.models.length;
      data.assets.models[this.model_count++] = url;
      this.setState({data: data, previewZip: url, inProgress: false});
    }
  }

  handleChange(key, event) {
    let that = this;
    if (that.state.editable) {
      var x = _.assign(that.state.product, {[key]: event.target.value});
      that.setState({product: x});
    }
  }

  setSize(key, event) {
    let x = _.cloneDeep(this.state.product);
    x.size[key] = event.target.value;
    this.setState({product: x});
  }

  parseProduct(product) {
    let that = this;
    let currentProduct = that.state.product;
    _.assign(currentProduct, {name: product.title}, {price: product.price}, {url: product.url},
      {originproductid: product.originproductid}, {brand: product.brand},
      {marketplace: product.marketplace}, {colour: product.colour}, {material: product.material});
    _.set(currentProduct, 'assets.image', product.image);
    //console.log(currentProduct);
    that.setState({product: currentProduct, inProgress: false});
  }

  fetchProduct() {
    let that = this;
    let value = this.refs.inputUrl.getDOMNode().value.trim();
    if (value) {
      Services.postAPI(Constants.SERVER_URL + 'scrap', {data: {url: value}})
        .then(response => {
          that.parseProduct(response.body)
        })
        .catch(() => {
          that.setState({inProgress: false});
        });
      that.setState({inProgress: true});
    }
  }

  setPreview(index, tag) {
    if (tag == ImageUploadConstants.IMAGE) {
      if (index < this.state.product.assets.image.length)
        this.setState({previewImage: this.state.product.assets.image[index]});
    }
    if (tag == ImageUploadConstants.RENDERED) {
      if (index < this.state.product.assets.renderedimage.length)
        this.setState({previewRendered: this.state.product.assets.renderedimage[index]});
    }
    if (tag == ImageUploadConstants._3D) {
      if (index < this.state.product.assets.models.length)
        this.setState({previewZip: this.state.product.assets.models[index]});
    }
  }

  removeImage(index, tag) {

    if (tag == ImageUploadConstants.IMAGE) {
      let data = _.cloneDeep(this.state.product);
      data.assets.image.splice(index, 1);
      this.setState({product: data, previewImage: data.displayImage.url});
    }
    if (tag == ImageUploadConstants.RENDERED) {
      let data = _.cloneDeep(this.state.product);
      data.assets.renderedimage.splice(index, 1);
      this.setState({product: data, previewRendered: data.assets.renderedimage[0]});
    }
    if (tag == ImageUploadConstants._3D) {
      let data = _.cloneDeep(this.state.product);
      data.assets.models.splice(index, 1);
      this.setState({product: data, previewZip: data.assets.models[0]});
    }
  }
  saveProduct(flag){
    let that = this;
    let data = _.cloneDeep(this.state.product);
    let cascadeData = that.refs.cascade.refs.withStyles.getCascadeData();
    data.type = cascadeData.type;
    data.space = cascadeData.space;
    data.category = cascadeData.category;
    data.itemtype = cascadeData.itemtype;
    console.log('Product will be', data.productid == '' ? 'added' : 'updated');
    if (data.productid == '' || flag == 'NEW') {
      ProductsAction.addProduct(data);
    } else {
      ProductsAction.updateProduct(data);
    }
    that.unsubscribe_productsstore = ProductsStore.listen(that.onProductSave.bind(that));
    that.setState({inProgress: true, product: data});
  }

  downLoadZip() {
    let that = this;
    that.setState({inProgress: true});
    DownloadUtility.downloadProductZIP(that.state.product)
      .then(() => that.setState({inProgress: false}))
      .catch(() => that.setState({inProgress: false}));
  }

  render() {
    let self = this;
    return (
      <div className="ProductRepository">

        <div style={{margin:'10px 0px', display:'flex',flexFlow:'row nowrap' ,justifyContent:'space-around'}}>
          <div>
            <button onClick={self.saveProduct.bind(self)}>SAVE</button>
          </div>
          <div>
            <button onClick={self.saveProduct.bind(self,'NEW')}>SAVE AS NEW</button>
          </div>
          <div className="toggle">
            {/*<Toggle
             ref="dirty"
             name="editable12"
             value="value12"
             label={self.state.data.isdirty ? 'Dirty' :'Non-Dirty'}
             onToggle={self.setDirtyProperty.bind(self)}
             />*/}
          </div>
        </div>
        {this.getProductTable()}
        {(self.state.inProgress) &&
        <div style={{position: 'fixed',top:'40%',right:'50%'}}><CircularProgress color='green' mode="indeterminate"/>
        </div>}
      </div>
    );
  }

  getProductTable() {
    let self = this;
    return (
      <div className="product-table">
        <table style={{width:'100%'}}>
          <tr>
            <td className="first-row">URL</td>
            <td><input type="text" ref='inputUrl' placeholder="https://www.urbanladder.com/products/lavu-floor-lamp"/>
            </td>
            <td>
              <button type="button" className="fetch" onClick={self.fetchProduct.bind(self)}>Fetch</button>
            </td>
          </tr>
          <tr>
            <td className="first-row">Code</td>
            <td><input type="text" placeholder="Code" disabled={this.state.editable ? false : 'disabled' }
                       value={self.state.product.productcode} onChange={this.handleChange.bind(this,'productcode')}/>
            </td>
            <td>
              <CascadeSelect ref = 'cascade' editable={this.state.editable}
                             data={Object.keys(self.state.product).length > 0 ? self.state.product:{}}/>
            </td>
          </tr>
          <tr>
            <td className="first-row">Origin Product Code</td>
            <td><input type="text" placeholder="SKU" disabled={this.state.editable ? false : 'disabled' }
                       value={self.state.product.originproductid}
                       onChange={this.handleChange.bind(this,'originproductid')}/></td>
          </tr>
          <tr>
            { self.state.product.productid != '' &&
            [<td key={1}>Product ID</td>,
              <td key={2}>{self.state.product.productid}</td>]
            }
          </tr>
          <tr>
            <td className="first-row">Name</td>
            <td><input type="text" placeholder="Name" disabled={this.state.editable ? false : 'disabled' }
                       value={self.state.product.name} onChange={this.handleChange.bind(this,'name')}/></td>
          </tr>
          <tr>
            <td className="first-row">Price</td>
            <td><input type="text" placeholder="Price" disabled={this.state.editable ? false : 'disabled' }
                       value={self.state.product.price} onChange={this.handleChange.bind(this,'price')}/></td>
          </tr>
          <tr>
            <td className="first-row">Color</td>
            <td><input type="text" placeholder="Color" disabled={this.state.editable ? false : 'disabled' }
                       value={self.state.product.colour} onChange={this.handleChange.bind(this,'colour')}/></td>
          </tr>
          <tr>
            <td className="first-row">Market Place</td>
            <td><input type="text" placeholder="Market place" disabled={this.state.editable ? false : 'disabled' }
                       value={self.state.product.marketplace} onChange={this.handleChange.bind(this,'marketplace')}/>
            </td>
          </tr>
          <tr>
            <td className="first-row">Brand</td>
            <td><input type="text" placeholder="Brand" disabled={this.state.editable ? false : 'disabled' }
                       value={self.state.product.brand} onChange={this.handleChange.bind(this,'brand')}/></td>
          </tr>
          <tr>
            <td className="first-row">Dimensions</td>
            <td>
              <div style={{display:'flex',flexFlow:'row nowrap',justifyContent:'space-between'}}>
                <input type='text' placeholder='width' disabled={this.state.editable ? false : 'disabled' }
                       value={self.state.product.size? self.state.product.size.w:''}
                       onChange={this.setSize.bind(this,'w')}/>
                <input type='text' placeholder='height' disabled={this.state.editable ? false : 'disabled' }
                       value={self.state.product.size?self.state.product.size.h:''}
                       onChange={this.setSize.bind(this,'h')}/>
                <input type='text' placeholder='depth' disabled={this.state.editable ? false : 'disabled' }
                       value={self.state.product.size?self.state.product.size.d:''}
                       onChange={this.setSize.bind(this,'d')}/>
              </div>
            </td>
          </tr>
          <tr>
            <td className="first-row">Style</td>
            <td><input type="text" placeholder="Style" disabled={this.state.editable ? false : 'disabled' }
                       value={self.state.product.style} onChange={this.handleChange.bind(this,'style')}/></td>
          </tr>
          <tr>
            <td className="first-row">Material</td>
            <td><input type="text" placeholder="Material" disabled={this.state.editable ? false : 'disabled' }
                       value={self.state.product.material} onChange={this.handleChange.bind(this,'material')}/></td>
          </tr>
          <tr>
            <td className="first-row">URL</td>
            <td><input type="text" placeholder="URL" disabled={this.state.editable ? false : 'disabled' }
                       value={self.state.product.url} onChange={this.handleChange.bind(this,'url')}/></td>
          </tr>
          <tr>
            <td className="first-row">Notes</td>
            <td>
                <textarea value={this.state.product.notes} rows="4" style={{width:'80%'}}
                          disabled={this.state.editable ? false : 'disabled' } placeholder='Notes'
                          onChange={this.handleChange.bind(this,'notes')}>
                </textarea>
            </td>
          </tr>
          <tr>
            <td className="first-row">Images</td>
            <td className="Images">
              <div className='preview-image'
                   style={{background:'url("'+self.state.previewImage +'")'}}>
              </div>
              <DropZone style={{paddingTop: '10px',paddingLeft:'55px'}}
                        onDrop={self.uploadImage.bind(self,ImageUploadConstants.IMAGE)}>
                <button>Upload Image</button>
              </DropZone>
            </td>
            <td style={{maxHeight: '100px',overflowY:'auto',maxWidth: '310px',border: '1px dashed grey'}}>
              <Grid data={Object.keys(self.state.product).length > 0 ? self.state.product.assets.image:[]}
                    onClick={self.setPreview.bind(self)} onRemove={self.removeImage.bind(self)}
                    tag={ImageUploadConstants.IMAGE}
                />
            </td>
          </tr>
          <tr>
            <td className="first-row">2D</td>
            <td className="Images">
              <img src={self.state.previewRendered}
                   style={{width: '200px',height:'200px',border:'1px dashed black'}}/>
              <DropZone style={{paddingTop: '10px',paddingLeft:'55px'}}
                        onDrop={self.uploadImage.bind(self,ImageUploadConstants.RENDERED)}>
                <button>Upload Rendered Image</button>
              </DropZone>
            </td>
            <td><Grid data={Object.keys(self.state.product).length > 0 ? self.state.product.assets.renderedimage:[]}
                      onClick={self.setPreview.bind(self)} onRemove={self.removeImage.bind(self)}
                      tag={ImageUploadConstants.RENDERED}/></td>
          </tr>
          <tr>
            <td className="first-row">3D</td>
            <td className="Images">
              <DropZone style={{paddingTop: '10px',paddingLeft:'55px'}}
                        onDrop={self.uploadImage.bind(self,ImageUploadConstants._3D)}>
                <button>Upload 3D Max File</button>
              </DropZone>
            </td>
            <td>
              <ol>
                { self.state.product.assets ?
                  function () {
                    var row = [];
                    for (var i = 0; i < self.state.product.assets.models.length; i++) {
                      row.push(<li key={i} style={{position:'relative'}}>
                        <SvgIcon onClick={self.removeImage.bind(self,i,ImageUploadConstants._3D)}
                                 style={{backgroundColor: 'white',position: 'absolute',width: '10px',height: '10px',left: '-30px',top:'3px',cursor: 'pointer'}}>
                          <path d={Constants.ICONS.close_icon}></path>
                        </SvgIcon>
                        <a className='product-rep-a' target='_blank'
                           href={self.state.product.assets.models[i]}>{self.state.product.assets.models[i]}</a>
                      </li>);
                    }
                    return row;
                  }() : ''
                }
              </ol>
            </td>
          </tr>
        </table>
        <ImageGrid />
        <button onClick={self.downLoadZip.bind(self)} style={{position:'relative',left:'40%', margin:'10px 0px'}}>
          Download Product as Zip
        </button>
      </div>
    )
  }
}
export default EditProductComponent;
