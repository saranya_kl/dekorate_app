import Services from '../../../../../../services/Services.js';
class productSchema{
  constructor(){
    this.getSchema();
  }
  getSchema(){
    //Services.getAPI('https://s3.amazonaws.com/deko-repo-dev/Product-Schema/productSchema.json')
    //.then(response => {
    //  console.log(response.body);
    //});
  }
}
export default {
  "type": {
    "Furniture": {
      "space": {
        "Living Room": {
          "category": {
            "Seating": {
              "itemtype": [
                "Chairs",
                "Benches, Stools",
                "Rocking Chair",
                "Futons",
                "Ottomans",
                "Recliner",
                "Wing Chair",
                "Accent Chair",
                "Folding Chair",
                "Bean Bags"
              ]
            },
            "Sofa and Sectionals": {
              "itemtype": [
                "One Seater",
                "Two Seater",
                "Three Seater",
                "L Shaped Sofa",
                "Loveseats",
                "Sofa cum Bed"
              ]
            },
            "Tables": {
              "itemtype": [
                "Coffee Tables",
                "Side Tables",
                "Console Tables"
              ]
            },
            "Storage and Accessories": {
              "itemtype": [
                "Wall Shelves",
                "TV Units",
                "Shoesracks",
                "Bookshelves and Display unit"
              ]
            }
          }
        },
        "Bed Room": {
          "category": {
            "Beds": {
              "itemtype": [
                "King Size",
                "Single Bed",
                "Queen Bed",
                "Storage Bed",
                "Futon Bed",
                "Sofa cum Bed",
                "Platform Bed",
                "Four-Poster Bed"
              ]
            },
            "Storage and Accessories": {
              "itemtype": [
                "Bedside Tables",
                "Chest of Drawers",
                "Dresser and Mirrors",
                "Wardrobes",
                "Wall Shelves",
                "Holders",
                "Trunks and Boxes"
              ]
            }
          }
        },
        "Dinning Room": {
          "category": {
            "Dining Sets": {
              "itemtype": [
                "4 Seater",
                "6 Seater",
                "8 Seater",
                "Dining Chairs",
                "Dining Tables",
                "Bar Stools"
              ]
            },
            "Storage and Accessories": {
              "itemtype": [
                "Cabinets and Sideboards",
                "Bar Unit"
              ]
            }
          }
        },
        "Garden and Balcony": {
          "itemtype": [
            "Swings",
            "Outside Chair",
            "Outside Table",
            "Seatings",
            "Gazebos"
          ]
        },
        "Kids Room": {
          "category": {
            "Beds": {
              "itemtype": [
                "Beds",
                "Bunk Bed",
                "Bed with Tables",
                "Theme Beds"
              ]
            },
            "Table and Chair": {
              "itemtype": [
                "Study Tables and Desk",
                "Play Tables",
                "Chairs",
                "Stools"
              ]
            },
            "Storage and Accessories": {
              "itemtype": [
                "Wardrobes",
                "Chest of Drawers",
                "Bedside Table",
                "Toybox",
                "Shelves"
              ]
            }
          }
        },
        "Study Room": {
          "itemtype": [
            "Tables",
            "Chairs"
          ]
        }
      }
    },
    "Furnishing": {
      "category": {
        "Fabrics": {
          "itemtype": [
            "Upholstery"
          ]
        },
        "Window Furnishing": {
          "itemtype": [
            "Window Curtains",
            "Door Curtains",
            "Curtain Accessories",
            "Sheer Curtains",
            "Curtain Rods",
            "Blinds and Shades"
          ]
        },
        "Bed Linen": {
          "itemtype": [
            "Bed Sheets",
            "Bed Cover",
            "Comforter",
            "Pillow Covers",
            "Pillow Inserts"
          ]
        },
        "Cushion and Throws": {
          "itemtype": [
            "Cushion Covers",
            "Cushion Inserts",
            "Filled Cushion",
            "Throws"
          ]
        },
        "Rugs and Carpets": {
          "itemtype": [
            "Multipurpose Mats",
            "Area Rugs",
            "Doormats"
          ]
        },
        "Table Linen": {
          "itemtype": [
            "Table Cloths",
            "Placements",
            "Table Runners",
            "Table Linen Sets",
            "Napkin and Tissue",
            "Coasters",
            "Trivets"
          ]
        },
        "Kids Furnishing": {
          "itemtype": [
            "Kids Bed Sheet",
            "Kids Blanket and Quilts",
            "Kids Pillows and Pillow Cover",
            "Kids Cushion Cover",
            "Kids Curtains"
          ]
        }
      }
    },
    "Decor": {
      "category": {
        "Wall Decor": {
          "itemtype": [
            "Wall Paper",
            "Framed Poster",
            "Paints",
            "Paintings",
            "Wall Accents",
            "Wall Decals and Stickers",
            "Art Prints and Panels",
            "Decorative Mirrors",
            "Key Hodler",
            "Wall Clock",
            "Kids Wall Decoration",
            "Photoframe"
          ]
        },
        "Lighting": {
          "itemtype": [
            "Lanterns",
            "Candles",
            "Candle Holders",
            "Defuser and Fragrances",
            "Table Lamps",
            "Lamp Shades",
            "Chandeliers",
            "Ceiling Lamps",
            "Floor Lamps",
            "Hanging Lights",
            "Wall Mounted"
          ]
        },
        "Home Accents": {
          "itemtype": [
            "Statues and Sculptures",
            "Desk Accessories",
            "Decorative Trays and Bowls",
            "Knik Knacks",
            "Artifacts",
            "Fridge Magnets",
            "Religion and Spirituality",
            "Room divider",
            "Ceiling Fan"
          ]
        },
        "Vase and Flowers": {
          "itemtype": [
            "Natural Plants and Pots",
            "Natural Flowers",
            "Vases",
            "Artificial Plants and Pots",
            "Artificial Flowers",
            "Garden Accents"
          ]
        }
      }
    }
  }
}
