/**
 * Created by vikramaditya on 7/13/15.
 */
import React from 'react';
import Select from 'react-select';
import Helper from '../../../../../helper.js';
import styles from './CascadeSelect.scss';
var {TriggerTypes, withStyles} = Helper;
const ProductSchema = require('./productSchema.js');

let Constants = {
  TYPE: 'type',
  SPACE: 'space',
  CATEGORY: 'category',
  ITEM: 'itemtype'
};

@withStyles(styles) class CascadeSelect extends React.Component {

  constructor() {
    super();
    let that = this;
    that.cascadeOptions = {type: [], space: [], category: [], itemtype: []};
    that.menuIndex = {type: null, space: null, category: null, itemtype: null};
    that.CascadeType = ['type', 'space', 'category', 'itemtype'];
  }

  parseData(object) {
    let arr = [];
    let arr_object = (object.length) ? object : Object.keys(object);
    for (let i = 0; i < arr_object.length; i++) {
      arr.push({value: arr_object[i], label: arr_object[i]});
    }
    return arr;
  }

  componentWillMount() {
    let that = this;
    that.initialLoad(that.props.data);
  }

  componentWillReceiveProps(nextProps) {
    let that = this;
    if(nextProps.data.productid !== that.props.data.productid){
      that.initialLoad(nextProps.data);
    }
  }

  initialLoad(prop) {
    let that = this;
    that.setCascadeData(ProductSchema);
    prop.type ? that.setPropsData(prop.type, Constants.TYPE) : that.menuIndex.type = null;
    prop.space ? that.setPropsData(prop.space, Constants.SPACE) : that.menuIndex.space = null;
    prop.category ? that.setPropsData(prop.category, Constants.CATEGORY) : that.menuIndex.category = null;
    prop.itemtype ? that.menuIndex.itemtype = prop.itemtype : that.menuIndex.itemtype = null;
    that.forceUpdate();
  }

  setPropsData(value, tag) {
    let that = this;
    that.setCascadeData(that.cascadeOptions[tag][value]);
    that.menuIndex[tag] = value;
  }

  getCascadeData() {
    let that = this;
    return ({
      type: that.menuIndex.type || '',
      space: that.menuIndex.space || '',
      category: that.menuIndex.category || '',
      itemtype: that.menuIndex.itemtype || ''
    });
  }

  setCascadeData(data) {
    let that = this;
    let key = Object.keys(data)[0]; // should be single always.
    that.cascadeOptions[key] = data[key];
  }

  onSchemaSelection(tag, selectedIndex) {
    let that = this;
    let index = that.CascadeType.indexOf(tag);
    //reset the lower cascades.
    for (let i = index + 1; i < that.CascadeType.length; i++) {
      that.cascadeOptions[that.CascadeType[i]] = [];
      that.menuIndex[that.CascadeType[i]] = null;
    }
    (index + 1 != that.CascadeType.length) && that.setCascadeData(that.cascadeOptions[tag][selectedIndex]);
    that.menuIndex[tag] = selectedIndex;
    that.forceUpdate();
  }

  render() {
    var self = this;
    let schemaType = self.cascadeOptions['type'] ? self.parseData(self.cascadeOptions['type']) : [];
    let schemaSpace = self.cascadeOptions['space'] ? self.parseData(self.cascadeOptions['space']) : [];
    let schemaCategory = self.cascadeOptions['category'] ? self.parseData(self.cascadeOptions['category']) : [];
    let schemaItem = self.cascadeOptions['itemtype'] ? self.parseData(self.cascadeOptions['itemtype']) : [];
    return (
      <div>
        <Select name={Constants.TYPE} clearable={false} placeholder='type' value={self.menuIndex.type}
                options={schemaType} searchable={false}
                onChange={self.onSchemaSelection.bind(self,Constants.TYPE)}/>
        <Select name={Constants.SPACE} clearable={false} placeholder='Space' value={self.menuIndex.space}
                options={schemaSpace} searchable={false}
                onChange={self.onSchemaSelection.bind(self,Constants.SPACE)}/>
        <Select name={Constants.CATEGORY} clearable={false} placeholder='Category'
                value={self.menuIndex.category}
                options={schemaCategory} searchable={false}
                onChange={self.onSchemaSelection.bind(self,Constants.CATEGORY)}/>
        <Select name={Constants.ITEM} clearable={false} placeholder='Item' value={self.menuIndex.itemtype}
                options={schemaItem} searchable={false}
                onChange={self.onSchemaSelection.bind(self,Constants.ITEM)}/>
      </div>
    );
  }
}
export default CascadeSelect;
{/*
 <div>
 <Select name={Constants.TYPE} clearable={false} placeholder='type' value={self.typeSelectedIndex}
 options={self.schemaType} searchable={false}
 onChange={self.onSchemaSelection.bind(self,Constants.TYPE)}/>
 <Select name={Constants.SPACE} clearable={false} placeholder='Space' value={self.spaceSelectedIndex}
 options={self.schemaSpace} searchable={false}
 onChange={self.onSchemaSelection.bind(self,Constants.SPACE)}/>
 <Select name={Constants.CATEGORY} clearable={false} placeholder='Category'
 value={self.categorySelectedIndex}
 options={self.schemaCategory} searchable={false}
 onChange={self.onSchemaSelection.bind(self,Constants.CATEGORY)}/>
 <Select name={Constants.ITEM} clearable={false} placeholder='Item' value={self.itemSelectedIndex}
 options={self.schemaItem} searchable={false}
 onChange={self.onSchemaSelection.bind(self,Constants.ITEM)}/>
 </div>
 */
}
