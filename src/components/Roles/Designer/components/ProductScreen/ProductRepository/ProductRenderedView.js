/**
 * Created by vikramaditya on 9/18/15.
 */
import React from 'react';
import {SvgIcon} from 'material-ui';
import ProductImageViewer from './ProductImageViewer.js';
import SigningServices from '../../../../../../services/SigningServices.js';
import Helper from '../../../../../helper.js';
var {ResourceConstants, LoginStore} = Helper;
class ProductRenderedView extends React.Component {
  constructor() {
    super();
    this.state = {
      data: [],
      models: [],
      renderedimage: []
    }
  }

  componentWillMount() {
    this.setState({
      data: this.props.data,
      renderedimage: this.props.data.assets.renderedimage,
      models: this.props.data.assets.models
    });
  }

  componentWillReceiveProps(newprops) {
    this.setState({
      data: newprops.data,
      renderedimage: newprops.data.assets.renderedimage,
      models: newprops.data.assets.models
    });
  }
  download3D(url){
    SigningServices.downloadProduct(url);
  }
  render() {
    let that = this;
    let rendered = this.state.renderedimage;
    let models = this.state.models;
    let allRendered = rendered.map((img, index) => {
      return (
        <div key={index}>
          <img src={img} style={{margin: '2px'}} width='100px' height='100px'/>
        </div>
      )
    });
    let allModels = models.map((model, index) => {
      let canDownload = LoginStore.checkPermission(ResourceConstants.ProductCatalog_ProductDownload);
      return (
      canDownload && <button key={index} className='custom-secondary-button' style={{width:'220px', margin:'5px'}} onClick={that.download3D.bind(that,model)}>
          Download 3dMax File - {index+1}
          <SvgIcon>
            <path fill="#808080" d="M5,20H19V18H5M19,9H15V3H9V9H5L12,16L19,9Z"/>
          </SvgIcon>
        </button>
      )
    });
    return (
      <div style={{display:'flex', flexFlow:'row', marginTop : '5px', marginLeft:'5px', backgroundColor:'#ffffff'}}>
        {rendered.length > 0 && <div style={{flex:'1', overflowX:'scroll'}}>
          {allRendered}
        </div>}
        {models.length > 0 && <div style={{flex:'1', wordBreak: 'break-word', margin:'30px'}}>
          {allModels}
        </div>
        }
      </div>
    )
  }
}
export default ProductRenderedView;
