/**
 * Created by rick on 13/09/15.
 */
import React from 'react';
import Helper from '../../../../../helper.js';
import styles from './ProductItemView.scss';

import ProductImageViewer from './ProductImageViewer.js'
var {withStyles} = Helper;

@withStyles(styles)
class ProductItemView extends React.Component {
  constructor() {
    super();
  }
  state = {
    editMode: true
  };
  componentWillMount(){
    this.setState({
      data: this.props.data
    });
  }
  componentWillReceiveProps(newprops){
    this.setState({
      data: newprops.data
    });
  }
  getTopMenuBar(items){
    return (
      <div className='ProductItemView-menuBar'>
        {items.map((item, index) => {
          return (
            <button className='menuButton' key={index}>{item}</button>
          )
        })}
      </div>
    )
  }
  getCategoryBar(data){
    return (
      <div className="categoricalData">
        {data.type||'TYPE'} > {data.space ? `${data.space} >` : ''}  {data.category ? `${data.category} >` : ''} {data.itemtype || 'ITEM'}
      </div>
    )
  }
  render() {
    let data = this.state.data;
    let editMode = this.state.editMode;

    //console.log(editMode);
    return (
      <div className="ProductItemView">
        <div className="ProductItemView-basicProductInfo">
          <div className="imageViewerPanel">
            <ProductImageViewer data={data.assets.image}/>
          </div>
          <div className="infoPanel">
            <div className="productName">{data.name || 'Product Name'}</div>
            <div className="brandName">{data.brand}</div>
              {this.getCategoryBar(data)}
            <div className="priceField">
              <span>{data.amount.INR && `₹${data.amount.INR}`}</span>
              {data.amount.USD && <span className="separator">|</span>}
              <span>{data.amount.USD && `$${data.amount.USD}`}</span>
            </div>
            <div className="urlField"><a target="_blank" href={data.url}>{data.url}</a></div>
            <div className="detailsTable">
              <div className="header">PRODUCT DETAILS</div>
              <div className="tr"><div className="td-left">Material</div> <div className="td-right">{data.material}</div> </div>
              <div className="tr"><div className="td-left">Color</div> <div className="td-right">{data.colour}</div> </div>
              <div className="tr"><div className="td-left">Size</div> <div className="td-right">{data.size.w}W x {data.size.h}H x {data.size.d}D</div> </div>
              <div className="tr"><div className="td-left">Origin SKU</div> <div className="td-right">{data.originproductid}</div> </div>
            </div>


            <div className="detailsTable">
              <div className="header">TECH DETAILS</div>
              <div className="tr"><div className="td-left">Product ID</div> <div className="td-right">{data.productid}</div> </div>
              <div className="tr"><div className="td-left">Product Code</div> <div className="td-right">{data.productcode}</div> </div>
            </div>


          </div>
        </div>
      </div>
    )

  }

}


export default ProductItemView;
