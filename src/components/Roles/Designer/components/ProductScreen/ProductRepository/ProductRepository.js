/**
 * Created by vikramaditya on 7/8/15.
 */
import React from 'react';
import mui from 'material-ui';
import _ from 'lodash';
import DropZone from 'react-dropzone';

import Helper from '../../../../../helper.js';
import Components from '../../../../../component.js';
import ProductItemView from './ProductItemView.js';
import ProductRenderedView from './ProductRenderedView.js';
import styles from './ProductRepository.scss';
import CascadeSelect from '../CascadeSelect';
import Grid from '../Grid';
import SigningServices from '../../../../../../services/SigningServices.js';
var {CustomUtility, DownloadUtility, LoginStore, ResourceConstants, ProductConstants,TriggerTypes,
  ImageUploadConstants, ProductsStore, ProductsAction, withStyles, AppConstants: Constants} = Helper;

var {SvgIcon,Dialog,TextField} = mui;

var {Toggle,CircularProgress} = mui;

let CURRENT_PASS = Constants.ADMIN_PASS;

@withStyles(styles) class ProductRepository extends React.Component {

  constructor() {
    super();
    let that = this;
    that.image_count = 0;
    that.render_count = 0;
    that.model_count = 0;
    that.cascadeData = {};
    that.state = {
      editable: false,
      data: _.cloneDeep(ProductConstants.DUMMY_PRODUCT),
      previewRendered: null,
      previewZip: null,
      inProgress: false,
      dirty: true
    };
  }

  componentWillMount() {
    let self = this;
    self.unsubscribe_store = ProductsStore.listen(self.handleProductStore.bind(self));
  }

  componentWillUnmount() {
    this.unsubscribe_store();
  }

  _onDialogSubmit(action) {
    //console.log(action)
    let that = this;
    let pass = that.refs.password;
    //console.log(pass,pass.getValue());
    if (action == ProductConstants.SUBMIT && pass.getValue() == CURRENT_PASS) {
      let data = _.cloneDeep(that.state.data);
      data.isdirty = false;
      that.setState({data: data, dirty: false});
      that.refs.dirty.setToggled(true);
      that.refs.dialog.dismiss(); // do this on password verification.
    } else if (pass.getValue() != CURRENT_PASS) {
      alert('wrong password');
      that.refs.dirty.setToggled(false);
      that.refs.dialog.dismiss();
    }
    if (action == ProductConstants.CANCEL) {
      that.refs.dirty.setToggled(false);
      that.refs.dialog.dismiss();
    }
  }

  setEditableProperty() {
    let that = this;
    //this.refs.toggle.isToggled()
    that.setState({editable: !that.state.editable});
  }

  setDirtyProperty(e, value) {
    let self = this;
    let data = _.cloneDeep(self.state.data);
    data.isdirty = value;
    self.refs.dirty.setToggled(value);
    self.setState({data: data, dirty: value});
    /*
     if (this.refs.dirty.isToggled()) {
     this.refs.dialog.show();
     } else {
     var data = _.cloneDeep(this.state.data);
     data.isdirty = true;
     this.setState({data: data, dirty: true});
     }
     */
  }

  componentWillReceiveProps(nextProps) {
    let self = this;
    //console.log('same props',this.props.data, nextProps.data,_.isEqual(this.props.data, nextProps.data));
    if (!_.isEqual(self.props.data, nextProps.data)) {
      self.refs.dirty.setToggled(nextProps.data.isdirty);
      self.setState({
        inProgress: false,
        data: _.cloneDeep(nextProps.data),
        //previewImage: _.clone(nextProps.data.assets.image[0]),
        previewRendered: _.clone(nextProps.data.assets.renderedimage[0]),
        previewZip: _.clone(nextProps.data.assets.models[0])
      });
    }else{
      self.setState({inProgress: false});
    }
  }

  handleChange(key, event) {
    let that = this;
    if (that.state.editable) {
      let x = _.assign(that.state.data, {[key]: event.target.value});
      that.setState({data: x});
    }
  }
  handleAvailablity(available){
    let that = this;
    if (that.state.editable) {
      let x = that.state.data;
      x.available = available;
      that.setState({data: x});
    }
  }
  handlePriceChange(tag, e){
    let that = this, price = e.target.value;
    if (that.state.editable) {
      let x = that.state.data;
      x['amount'][tag] = price;
      that.setState({data: x});
    }
  }
  handleProductStore(trigger){
    let that = this;
    if(trigger.fetched){
      that.setState({inProgress: false, data: trigger.fetched});
    }
    if(trigger.error){
      that.setState({inProgress: false});
    }
    if(trigger.update){
      that.refs.toggle.setToggled(false);
      that.setState({inProgress: false, editable: false});
    }
  }
  setSize(key, event) {
    let that = this, x = _.cloneDeep(that.state.data);
    x.size[key] = event.target.value;
    that.setState({data: x});
  }

  fetchProduct() {
    let that = this;
    let url = that.refs.inputUrl.getDOMNode().value.trim();
    if (url) {
      //(that.state.data.productid == '') && ProductStore.reset();
      if(ProductsStore.getUrlFetchedProduct(url) == Constants.ITEM_LOADING){
        ProductsAction.scrapeProductURL(url);
        that.setState({inProgress: true});
        //ProductActions.FETCH_URL_PRODUCTS(url);
      }else{
        let product = ProductsStore.getUrlFetchedProduct(url);
        that.setState({data: product});
      }
    }
  }

  setPreview(index, tag) {
    let that = this;
    if (tag == ImageUploadConstants.IMAGE) {
      if (index < that.state.data.assets.image.length){
        let url = that.state.data.assets.image[index];
        let data = that.state.data;
        data.displayImage.url = url;
        that.setState({data: data});
      }
    }
    if (tag == ImageUploadConstants.RENDERED) {
      if (index < that.state.data.assets.renderedimage.length)
        that.setState({previewRendered: that.state.data.assets.renderedimage[index]});
    }
    if (tag == ImageUploadConstants._3D) {
      if (index < that.state.data.assets.models.length)
        that.setState({previewZip: that.state.data.assets.models[index]});
    }
  }

  removeImage(index, tag) {
    let that = this;
    if (tag == ImageUploadConstants.IMAGE) {
      let data = _.cloneDeep(that.state.data);
      //data.assets.image[index] = null;
      data.assets.image.splice(index, 1); //change to prev if this is confusing..
      data.displayImage.url = data.assets.image[0];
      that.setState({data: data});
    }
    if (tag == ImageUploadConstants.RENDERED) {
      let data = _.cloneDeep(that.state.data);
      //data.assets.renderedimage[index] = null;
      data.assets.renderedimage.splice(index, 1); //change to prev if this is confusing..
      that.setState({data: data, previewRendered: data.assets.renderedimage[0]});
    }
    if (tag == ImageUploadConstants._3D) {
      let data = _.cloneDeep(that.state.data);
      //data.assets.renderedimage[index] = null;
      data.assets.models.splice(index, 1); //change to prev if this is confusing..
      that.setState({data: data, previewZip: data.assets.models[0]});
    }
  }

  addNewProduct() {
    let that = this;
    let data = _.cloneDeep(that.state.data);
    let cascadeData = that.refs.cascade.refs.withStyles.getCascadeData();
    data.type = cascadeData.type;
    data.space = cascadeData.space;
    data.category = cascadeData.category;
    data.itemtype = cascadeData.itemtype;
    ProductsAction.addProduct(data);

    //ProductActions.setCurrentProduct(0, ProductConstants.NEW, data);
  }

  updateProduct() {
    let that = this;
    let data = _.cloneDeep(that.state.data);
    if(that.refs.cascade){
      let cascadeData = that.refs.cascade.refs.withStyles.getCascadeData();
      data.type = cascadeData.type;
      data.space = cascadeData.space;
      data.category = cascadeData.category;
      data.itemtype = cascadeData.itemtype;
    }
    console.log('Product will be', data.productid == '' ? 'added' : 'updated');
    if (data.productid == '') {
      ProductsAction.addProduct(data);
      that.setState({inProgress: true});
    }else{
      ProductsAction.updateProduct(data);
      that.setState({inProgress: true});
    }
  }

  createNewProduct() {
    let that = this;
    that.refs.toggle.setToggled(true);
    that.setState({editable: true, previewRendered: null, previewZip: null, data: _.cloneDeep(ProductConstants.DUMMY_PRODUCT)});
  }

  setCascadeData(props) {

    this.cascadeData = _.cloneDeep(this.state.data);
    this.cascadeData.type = props.type ? props.type : '';
    this.cascadeData.space = props.space ? props.space : '';
    this.cascadeData.category = props.category ? props.category : '';
    this.cascadeData.itemtype = props.itemtype ? props.itemtype : '';

  }

  uploadImage(tag, files) {
    let that = this;
    that.setState({inProgress: true});
    CustomUtility.uploadProductImage(files, tag, that.state.data.productcode)
    .then((response) => that.setImage(response))
    .catch(() => that.setState({inProgress: false}))
  }
  setImage(r){
    let {url, tag} = r, that = this;
    if (tag == ImageUploadConstants.IMAGE) {
      let data = _.cloneDeep(that.state.data);
      that.image_count = that.state.data.assets.image.length;
      data.assets.image[that.image_count++] = url;
      data.displayImage.url = url;
      that.setState({data: data, inProgress: false});
    }
    if (tag == ImageUploadConstants.RENDERED) {
      let data = _.cloneDeep(that.state.data);
      that.render_count = that.state.data.assets.renderedimage.length;
      data.assets.renderedimage[that.render_count++] = url;
      that.setState({data: data, previewRendered: url, inProgress: false});
    }
    if (tag == ImageUploadConstants._3D) {
      let data = _.cloneDeep(that.state.data);
      that.model_count = that.state.data.assets.models.length;
      data.assets.models[that.model_count++] = url;
      that.setState({data: data, previewZip: url, inProgress: false});
    }
  }
  download3D(url){
    SigningServices.downloadProduct(url);
  }
  downLoadZip() {
    let that = this;
    that.setState({inProgress: true});
    DownloadUtility.downloadProductZIP(that.state.data)
    .then(() => that.setState({inProgress: false}))
    .catch(() => that.setState({inProgress: false}));
  }
  render() {
    let self = this;
    //TODO this.renderCss(this.getStyles());
    return (
      <div className="ProductRepository">
        <div style={{margin:'10px 0px', display:'flex',flexFlow:'row nowrap' ,justifyContent:'space-around'}}>
            <div style={{ textAlign:'center'}}>
              <button onClick={self.createNewProduct.bind(self)}>New Product</button>
            </div>
            {/*<div>
             <button onClick={self.addNewProduct.bind(self)}>Add Product</button>
             </div>*/}
            <div>
              <button onClick={self.updateProduct.bind(self)}>SAVE</button>
            </div>
            <div>
              <button onClick={self.addNewProduct.bind(self)}>SAVE AS NEW</button>
            </div>
            <div className="toggle">
              <Toggle
                ref="toggle"
                name="editable"
                value="value"
                label={self.state.editable ? 'Editable':'Non-Editable'}
                onToggle={self.setEditableProperty.bind(self)}
                />
            </div>
            <div className="toggle">
              <Toggle
                ref="dirty"
                name="editable12"
                value="value12"
                label={self.state.data.isdirty ? 'Dirty' :'Non-Dirty'}
                onToggle={self.setDirtyProperty.bind(self)}
                />
            </div>
          </div>
        <div>
          {
            (self.state.inProgress) &&
            <div style={{position: 'fixed',top:'40%',right:'50%'}}><CircularProgress color='green' mode="indeterminate"/></div>
          }
        </div>
        {self.state.editable ? self.getProductTable() : <div>
          <ProductItemView data={self.state.data}/>
          <ProductRenderedView data={self.state.data}/>
        </div>}
      </div>
    );
  }

  getProductTable(){
    let self = this;
    return (
      <div className="product-table">
        <table style={{width:'100%'}}>
          <tr>
            <td className="first-row">URL</td>
            <td><input type="text" ref='inputUrl' placeholder="https://www.urbanladder.com/products/lavu-floor-lamp"/>
            </td>
            <td>
              <button type="button" className="fetch" onClick={self.fetchProduct.bind(self)}>Fetch</button>
            </td>
          </tr>
          <tr>
            <td className="first-row">Code</td>
            <td><input type="text" placeholder="Code" disabled={self.state.editable ? false : 'disabled' }
                       value={self.state.data.productcode} onChange={self.handleChange.bind(self,'productcode')}/>
            </td>
            <td>
              <CascadeSelect ref = 'cascade' editable={self.state.editable}
                             data={Object.keys(self.state.data).length > 0 ? self.state.data:{}}/>
            </td>
          </tr>
          <tr>
            <td className="first-row">Origin Product Code</td>
            <td><input type="text" placeholder="SKU" disabled={self.state.editable ? false : 'disabled' }
                       value={self.state.data.originproductid}
                       onChange={self.handleChange.bind(self,'originproductid')}/></td>
          </tr>
          <tr>
            { self.state.data.productid != '' &&
            [<td key={1}>Product ID</td>,
              <td key={2}>{self.state.data.productid}</td>]
            }
          </tr>
          <tr>
            <td className="first-row">Name</td>
            <td><input type="text" placeholder="Name" disabled={self.state.editable ? false : 'disabled' }
                       value={self.state.data.name} onChange={self.handleChange.bind(self,'name')}/></td>
          </tr>
          <tr>
            <td className="first-row">Available</td>
            <td>
              <input style={{width:'20px'}} type="radio" name='available' disabled={self.state.editable ? false : 'disabled' }
                       checked={self.state.data.available} onChange={self.handleAvailablity.bind(self, true)}/>
              <span>Yes</span>
              <input style={{width:'20px'}} type="radio" name='available' disabled={self.state.editable ? false : 'disabled' }
                     checked={!self.state.data.available} onChange={self.handleAvailablity.bind(self, false)}/>
              <span>No</span>
            </td>
          </tr>
          <tr>
            <td className="first-row">Price</td>
            <td>
              <span>INR</span><input style={{width: '26%', margin: '0 5px'}} type="text" placeholder="Price (INR)" disabled={self.state.editable ? false : 'disabled' }
                       value={self.state.data.amount['INR']} onChange={self.handlePriceChange.bind(self, 'INR')}/>
              <span>USD</span><input style={{width: '26%', marginLeft:'5px'}} type="text" placeholder="Price (USD)" disabled={self.state.editable ? false : 'disabled' }
                       value={self.state.data.amount['USD']} onChange={self.handlePriceChange.bind(self,'USD')}/>
            </td>
          </tr>
          <tr>
            <td className="first-row">Color</td>
            <td><input type="text" placeholder="Color" disabled={self.state.editable ? false : 'disabled' }
                       value={self.state.data.colour} onChange={self.handleChange.bind(self,'colour')}/></td>
          </tr>
          <tr>
            <td className="first-row">Market Place</td>
            <td><input type="text" placeholder="Market place" disabled={self.state.editable ? false : 'disabled' }
                       value={self.state.data.marketplace} onChange={self.handleChange.bind(self,'marketplace')}/></td>
          </tr>
          <tr>
            <td className="first-row">Brand</td>
            <td><input type="text" placeholder="Brand" disabled={self.state.editable ? false : 'disabled' }
                       value={self.state.data.brand} onChange={self.handleChange.bind(self,'brand')}/></td>
          </tr>
          <tr>
            <td className="first-row">Dimensions</td>
            <td>
              <div style={{display:'flex',flexFlow:'row nowrap',justifyContent:'space-between'}}>
                <input type='text' placeholder='width' disabled={self.state.editable ? false : 'disabled' }
                       value={self.state.data.size? self.state.data.size.w:''}
                       onChange={self.setSize.bind(self,'w')}/>
                <input type='text' placeholder='height' disabled={self.state.editable ? false : 'disabled' }
                       value={self.state.data.size?self.state.data.size.h:''} onChange={self.setSize.bind(self,'h')}/>
                <input type='text' placeholder='depth' disabled={self.state.editable ? false : 'disabled' }
                       value={self.state.data.size?self.state.data.size.d:''} onChange={self.setSize.bind(self,'d')}/>
              </div>
            </td>
          </tr>
          <tr>
            <td className="first-row">Style</td>
            <td><input type="text" placeholder="Style" disabled={self.state.editable ? false : 'disabled' }
                       value={self.state.data.style} onChange={self.handleChange.bind(self,'style')}/></td>
          </tr>
          <tr>
            <td className="first-row">Material</td>
            <td><input type="text" placeholder="Material" disabled={self.state.editable ? false : 'disabled' }
                       value={self.state.data.material} onChange={self.handleChange.bind(self,'material')}/></td>
          </tr>
          <tr>
            <td className="first-row">URL</td>
            <td><input type="text" placeholder="URL" disabled={self.state.editable ? false : 'disabled' }
                       value={self.state.data.url} onChange={self.handleChange.bind(self,'url')}/></td>
          </tr>
          <tr>
            <td className="first-row">Notes</td>
            <td>
                <textarea value={self.state.data.notes} rows="4" style={{width:'80%'}}
                          disabled={self.state.editable ? false : 'disabled' } placeholder='Notes'
                          onChange={self.handleChange.bind(self,'notes')}>
                </textarea>
            </td>
          </tr>
          <tr>
            <td className="first-row">Images</td>
            <td className="Images">
              <div className='preview-image'
                   style={{background:'url("'+_.result(self.state.data,'displayImage.url','') +'")'}}>
              </div>
              <DropZone style={{paddingTop: '10px',paddingLeft:'55px'}}
                        onDrop={self.uploadImage.bind(self,ImageUploadConstants.IMAGE)}>
                <button>Upload Image</button>
              </DropZone>
            </td>
            <td style={{maxHeight: '100px',overflowY:'auto',maxWidth: '310px',border: '1px dashed grey'}}>
              <Grid data={Object.keys(self.state.data).length > 0 ? self.state.data.assets.image:[]}
                    onClick={self.setPreview.bind(self)} onRemove={self.removeImage.bind(self)}
                    tag={ImageUploadConstants.IMAGE}
                />
            </td>
          </tr>
          <tr>
            <td className="first-row">2D</td>
            <td className="Images">
              <img src={self.state.previewRendered}
                   style={{width: '200px',height:'200px',border:'1px dashed black'}}/>
              <DropZone style={{paddingTop: '10px',paddingLeft:'55px'}}
                        onDrop={self.uploadImage.bind(self,ImageUploadConstants.RENDERED)}>
                <button>Upload Rendered Image</button>
              </DropZone>
            </td>
            <td><Grid data={Object.keys(self.state.data).length > 0 ? self.state.data.assets.renderedimage:[]}
                      onClick={self.setPreview.bind(self)} onRemove={self.removeImage.bind(self)}
                      tag={ImageUploadConstants.RENDERED}/></td>
          </tr>
          <tr>
            <td className="first-row">3D</td>
            <td className="Images">
              <DropZone style={{paddingTop: '10px',paddingLeft:'55px'}}
                        onDrop={self.uploadImage.bind(self,ImageUploadConstants._3D)}>
                <button>Upload 3D Max File</button>
              </DropZone>
            </td>
            <td>
              <ol>
                { self.state.data.assets ?
                  function () {
                    let row = [];
                    let canDownload = LoginStore.checkPermission(ResourceConstants.ProductCatalog_ProductDownload);
                    for (let i = 0; i < self.state.data.assets.models.length; i++) {
                      canDownload &&
                      row.push(
                        <li key={i} style={{position:'relative'}}>
                        <SvgIcon onClick={self.removeImage.bind(self,i,ImageUploadConstants._3D)}
                                 style={{backgroundColor: 'white',position: 'absolute',width: '10px',height: '10px',left: '-30px',top:'3px',cursor: 'pointer'}}>
                          <path d={Constants.ICONS.close_icon} />
                        </SvgIcon>
                        <button className='custom-secondary-button' onClick={self.download3D.bind(self, self.state.data.assets.models[i])}>Download {i+1}</button>
                        {/*<a className='product-rep-a' target='_blank'
                           href={self.state.data.assets.models[i]}>{self.state.data.assets.models[i]}</a>*/}
                      </li>);
                    }
                    return row;
                  }() : ''
                }
              </ol>
            </td>
          </tr>
        </table>
        <button onClick={self.downLoadZip.bind(self)} style={{position:'relative',left:'40%', margin:'10px 0px'}}>
          Download Product as Zip
        </button>
      </div>
    )
  }
}
export default ProductRepository;
