/**
 * Created by rick on 13/09/15.
 */
import React from 'react';
import {SvgIcon} from 'material-ui';
import Helper from '../../../../../helper.js';
import styles from './ProductImageViewer.scss';
var {withStyles} = Helper;
let left_arrow = 'M15.41,16.58L10.83,12L15.41,7.41L14,6L8,12L14,18L15.41,16.58Z';
let right_arrow = 'M8.59,16.58L13.17,12L8.59,7.41L10,6L16,12L10,18L8.59,16.58Z';
let downloadBtnSvg = 'M5,20H19V18H5M19,9H15V3H9V9H5L12,16L19,9Z';
@withStyles(styles)
class ProductImageViewer extends React.Component {

  state = {
    currentImageIndex: 0
  };

  setCurrentImageIndex(x) {
    this.setState({
      currentImageIndex: x
    });
  }

  changeImageIndex(tag) {
    if (tag == 'LEFT') {
      React.findDOMNode(this.refs.imageListCarousel).scrollLeft -= 87;
    } else {
      React.findDOMNode(this.refs.imageListCarousel).scrollLeft += 87;
    }
  }

  componentWillReceiveProps() {
    this.setState({
      currentImageIndex: 0
    });
  }

  getImageListCarouselItems(data, currentIndex) {
    let self = this;
    return (
      data.map((item, index) => {
        let classname = `imageListCarousel-item ${index == currentIndex ? 'selected' : ''}`;
        return (
          <div className={classname} key={index} onMouseOver={self.setCurrentImageIndex.bind(self,index)}>
            <div className="responsiveImage" style={{backgroundImage: `url( '${item}' )`}}/>
          </div>
        );
      })
    )

  }

  render() {
    let data = this.props.data || [];
    let currentImageIndex = this.state.currentImageIndex;
    let self = this;
    return (
      <div className="ProductImageViewer">
        <div className="imgGallery">
          <div className="imgHolder">
            <div className="downloadBtn">
              <a href={data[currentImageIndex]} download="image">
                <SvgIcon>
                  <path d={downloadBtnSvg}></path>
                </SvgIcon>
              </a>
            </div>
            <div className="responsiveImage" style={{backgroundImage: `url( '${data[currentImageIndex]}' )`}}/>
          </div>
          <div className='imageListWrapper'>
            <SvgIcon onClick={self.changeImageIndex.bind(self,'LEFT')} className='arrowController'>
              <path d={left_arrow}></path>
            </SvgIcon>

            <div ref='imageListCarousel' className="imageListCarousel">
              {self.getImageListCarouselItems(data, currentImageIndex)}
            </div>
            <SvgIcon onClick={self.changeImageIndex.bind(self,'RIGHT')} className='arrowController'>
              <path d={right_arrow}></path>
            </SvgIcon>
          </div>
        </div>
      </div>
    )


  }

}

export default ProductImageViewer;
