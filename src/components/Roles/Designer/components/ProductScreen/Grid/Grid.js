/**
 * Created by vikramaditya on 7/10/15.
 */
import React from 'react';
import mui from 'material-ui';
import styles from './Grid.scss';
import Helper from '../../../../../helper.js';
var {withStyles, AppConstants: Constants} = Helper;

var {SvgIcon} = mui;

@withStyles(styles)
class Grid extends React.Component{
  constructor(){
    super();
  }
  setPreview(index){
    this.props.onClick(index,this.props.tag);
  }
  removeImage(index){
    this.props.onRemove(index,this.props.tag);
  }
  render(){
    var self = this;
    let row = 3;
    var image_array = (self.props.data.length > 0) ? self.props.data : [];
    let col = Math.ceil(image_array.length*1.0/row);
    return (
      <table style={{width:'300px'}}>
        <tbody className='grid-body'>
          {
            (image_array.length > 0) ? (function(){
              var col_arr = [];
              for(var i = 0; i < row; i++){
                col_arr.push(<tr key = {i}>
                  {
                    (function(i){
                      var row_arr = [];
                      for(var j = 0; j < col; j++){
                        row_arr.push(<td key={j}>
                          <div className='grid-cell' style={{position: 'relative'}}>
                            <img onClick = {self.setPreview.bind(self,i * col + j)} src={image_array[i * col + j]} style={{width: '100px', height: '100px'}} />
                            {image_array[i*col+j] != null ? <SvgIcon onClick={self.removeImage.bind(self,i*col+j)} style={{backgroundColor: 'white', position: 'absolute', width: '10px', height: '10px', right: '3px', top: '3px', cursor: 'pointer'}}>
                              <path d = {Constants.ICONS.close_icon}> </path>
                            </SvgIcon> : ''
                            }
                          </div>
                        </td>);
                      }
                      return (row_arr);
                    })(i)
                  }
                </tr>);
              }
              return col_arr;
            })() : ''
          }
        </tbody>
      </table>
    );
  }
}
export default Grid;
