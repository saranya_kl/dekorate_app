/**
 * Created by vikramaditya on 7/15/15.
 */
//TODO Make and use this component wherever required
import React from 'react';
import mui from 'material-ui';
var {Dialog,TextField} = mui;

let ModalConstants = {
  CANCEL:'CANCEL',
  SUBMIT:'SUBMIT'
};
class PasswordModal extends React.Component{

  constructor(){
    super();
  }
  DialogAction(action){
    if(action == ModalConstants.SUBMIT){
      this.refs.dialog.dismiss();
      //this.props.onSubmit('1');
    }
    if(action == ModalConstants.CANCEL){
      this.refs.dialog.dismiss();
      //this.props.onCancel('2');
    }
  }
  render(){
    let standardActions = [
      { text: 'Cancel',onTouchTap: this.DialogAction.bind(this,ModalConstants.CANCEL)},
      { text: 'Submit',onTouchTap: this.DialogAction.bind(this,ModalConstants.SUBMIT)}
    ];
    return(
      <div>
        {this.props.show ? <Dialog ref = 'dialog'
              title="Dialog With Standard Actions"
              actions={standardActions}
              actionFocus="submit"
              openImmediately = {true}
              modal={true}>
        <TextField ref='password'
                   hintText="Password"
                   floatingLabelText="Password">
          <input type="password" />
        </TextField>
      </Dialog>:''}
      </div>
    )
  }
}
export default PasswordModal;
