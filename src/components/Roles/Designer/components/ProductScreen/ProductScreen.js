/**
 * Created by vikramaditya on 7/8/15.
 */
import React from 'react';
import mui from 'material-ui';
import Moment from 'moment';
import Helper from '../../../../helper.js';
import Components from '../../../../component.js';
import styles from './ProductScreen.scss';
import ProductList from './ProductList';
import ProductRepository from './ProductRepository';

var {withStyles, ProductsAction, ProductCatalogStore, ProductCatalogActions, ProductsStore, AppConstants: Constants, ProductConstants} = Helper;

let ADMIN_PASS = Constants.ADMIN_PASS;
var {Tabs,Tab,SvgIcon, Dialog, TextField, CircularProgress} = mui;

@withStyles(styles)
class ProductScreen extends React.Component {
  //static contextTypes = {
  //  setProgressVisibility: React.PropTypes.func,
  //  inProgress: React.PropTypes.func
  //};

  constructor(){
    super();
    this.searchTerm = '';
    this.state = {
      dirty_products: [],
      non_dirty_products: [],
      currentPreviewProduct: {},
      inProgress: false,
      searchResult:[]
    }
  }
  componentWillMount(){
    let that = this;

    //let non_dirty = ProductStore.getNonDirtyproducts(),dirty = ProductStore.getDirtyProducts();
    //if(non_dirty && dirty && (non_dirty.length || dirty.length > 0)){
    //  self.setState({non_dirty_products: non_dirty,dirty_products: dirty});
    //}else{
    //  //ProductActions.FETCH_PRODUCTS('0');
    //}
    //self.unsubscribe = ProductStore.listen( self.onStatusChange.bind( self ));
    ProductsAction.fetchRecentProducts(0, false);
    ProductsAction.fetchRecentProducts(0, true);
    that.unsubscribe = ProductsStore.listen(that.handleProductStore.bind( that ));
    that.unsubscribe_catalog = ProductCatalogStore.listen(that.handleProductCatalog.bind(that));
  }
  componentWillUnmount(){
    this.unsubscribe();
    this.unsubscribe_catalog();
  }
  setProgressVisibility(value){
    this.setState({inProgress:value});
  }
  _onDialogSubmit(){
    if(this.refs.password.getValue() == ADMIN_PASS){
      this.refs.dialog.dismiss();
      //ProductStore.setAdmin(true);
    }
    else{
      alert('wrong password');
      this.refs.dialog.dismiss();
    }
  }
  showModal(){
    this.refs.dialog.show();
  }
  handleProductStore(trigger){
    function mapToArray(map){let arr = [];for(let x of map){arr.push(x[1]);}return arr;}
    let that = this;
    if(trigger.error){
      that.setProgressVisibility(false);
    }
    if(trigger.products){
      if(trigger.searchResults){
        let results = ProductsStore.getSearchResults();
        that.setState({searchResult: results});
        that.setProgressVisibility(false);
      }else{
        let dirty = mapToArray(ProductsStore.getDirtyProducts()), non_dirty = mapToArray(ProductsStore.getNonDirtyProducts());
        that.setState({dirty_products: dirty, non_dirty_products: non_dirty});
      }
    }
    if(trigger.update){
      let dirty = mapToArray(ProductsStore.getDirtyProducts()), non_dirty = mapToArray(ProductsStore.getNonDirtyProducts());
      that.setState({dirty_products: dirty, non_dirty_products: non_dirty});
    }
  }
  handleProductCatalog(trigger){
    let that = this;
    if(trigger.error){
      that.setProgressVisibility(false);
    }
    if(trigger.search){
      let list = ProductCatalogStore.getList();
      ProductsAction.fetchProducts(list);
      list.length == 0 && that.setProgressVisibility(false);
    }
  }
  requestMore(is_dirty){
    let offsetId = ProductsStore.getOffset(is_dirty);
    ProductsAction.fetchRecentProducts(offsetId, is_dirty);
  }
  //searchHandler(e){this.setState({searchTerm:e.target.value})}
  searchProduct(e){
    let value = React.findDOMNode(this.refs.searchbox).value.trim();
    if(e.keyCode == undefined || e.keyCode == 13){
      if( value != ''){
        this.searchTerm = value;
        this.setProgressVisibility(true);
        let data = {query: value};
        ProductCatalogActions.getSearchResults(data);
        //ProductActions.SEARCH_PRODUCTS(value);
      }
    }
    //console.log('Searching',e.keyCode);
  }
  clearProductList(){this.searchTerm = ''; React.findDOMNode(this.refs.searchbox).value = ''; this.setState({searchResult: []})}

  //setCurrentProduct(index,initiator){
  //  if(initiator == ProductConstants.ONLINE){
  //    this.setProgressVisibility(true);
  //  }
  //  //ProductActions.setCurrentProduct(index,initiator);
  //}
  setCurrentPreviewProduct(product){
    this.setState({currentPreviewProduct: product});
  }
  render() {
    var self = this;
    var TabContainerStyle = {backgroundColor:'silver',border: '1px solid silver'};
    var TabItemStyle = {color:'black', backgroundColor:'white',fontSize:'0.75em',fontWeight:'200',overflowY:'auto'};
    let standardActions = [
      {text: 'Cancel'},
      {text: 'Submit', onTouchTap: this._onDialogSubmit.bind(this)}
    ];
    return (
      <div className='ProductScreen-page'>
        <Dialog ref='dialog'
                title="Dialog With Standard Actions"
                actions={standardActions}
                actionFocus="submit"
                modal={true}>
          <TextField ref='password'
                     hintText="Password"
                     floatingLabelText="Password" type='password'>
          </TextField>
        </Dialog>
        <div className='ProductScreen-container'>
          <div className="ProductScreen-rowwrapper">
            <div className="ProductScreen-columnwrapper">
              <Tabs initialSelectedIndex={1}
                    tabItemContainerStyle={TabContainerStyle} className = "ProductScreen-tabs">

                <Tab label='Latest Products'
                     style={TabItemStyle}>
                  <ProductList onScrollBottom={self.requestMore.bind(self)} listType={ProductConstants.NON_DIRTY} notFoundHeader={'No Products Found'} data={self.state.non_dirty_products} onClick={self.setCurrentPreviewProduct.bind(self)}/>
                </Tab>

                <Tab label='Dirty Products'
                     style={TabItemStyle}>
                  <ProductList onScrollBottom={self.requestMore.bind(self)} listType={ProductConstants.DIRTY} notFoundHeader={'No Products Found'} data={self.state.dirty_products} onClick={self.setCurrentPreviewProduct.bind(self)}/>
                </Tab>

                <Tab label='Search Products'
                     style={TabItemStyle}>
                  <div className="ProductScreen-search">
                    <div className="searchbar">
                      <div>
                        <SvgIcon className="search-icon">
                          <path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z" />
                        </SvgIcon>
                        <input onKeyDown = {self.searchProduct.bind(self)} ref='searchbox' type="text" className="searchBox" placeholder="Search"/>
                        <SvgIcon onClick={self.clearProductList.bind(self)} className="cross-icon">
                          <path d={Constants.ICONS.close_icon} />
                        </SvgIcon>
                      </div>
                    </div>
                    <button onClick={self.searchProduct.bind(self)} className="search-button"> Search </button>
                    {/*<button onClick={self.clearProductList.bind(self)} className="clear-button"> Clear </button>*/}
                  </div>
                  <ProductList onScrollBottom={self.requestMore.bind(self)} listType={ProductConstants.SEARCH} notFoundHeader={self.searchTerm != '' ? 'Searching for "'+self.searchTerm+'"':'Not Found'} data={self.state.searchResult} onClick={self.setCurrentPreviewProduct.bind(self)}/>
                </Tab>
              </Tabs>
            </div>
            <div className="ProductScreen-repo">
              <div className="ProductScreen-repo-wrapper">
                <ProductRepository data={self.state.currentPreviewProduct}/>
              </div>
            </div>
          </div>

        </div>
        {this.state.inProgress && <CircularProgress color='green' style={{zIndex:'9999',position:'absolute',left:'50%',top:'30%'}} mode="indeterminate" />}
      </div>
    );
  }
}
export default ProductScreen;
