/**
 * Created by vikramaditya on 7/8/15.
 */
import React from 'react';
import ExecutionEnvironment from 'react/lib/ExecutionEnvironment';
import mui from 'material-ui';
import Moment from 'moment';
import _ from 'lodash';
import Helper from '../../../../../helper.js';
var {ProductsAction, withStyles, AppConstants: Constants, ProductConstants, ResourceConstants, LoginStore} = Helper;

import styles from './ProductList.scss';

let ADMIN_PASS = Constants.ADMIN_PASS;
var {List, LinearProgress, ListDivider, Avatar, SvgIcon, Dialog, TextField} = mui;

@withStyles(styles) class ProductList extends React.Component {

  constructor() {
    super();
    this.currentListLength = 0;
    this.state = {
      currentIndex: -1,
      products_list: [],
      isLoading: false,
      item: {
        img: '',
        name: '',
        id: ''
      }
    };

  }
  componentDidMount(){
    let that = this;
    let dirty = that.props.listType == ProductConstants.DIRTY;
    let list = React.findDOMNode(that.refs.productList);
    list.onscroll = function(ev) {
      //console.log(ev.target.scrollHeight, list.offsetHeight, ev.target.scrollTop);
      if ((list.offsetHeight + ev.target.scrollTop + 1) >= (ev.target.scrollHeight)) {
        !that.state.isLoading && that.props.onScrollBottom(dirty);
        !that.state.isLoading && that.setState({isLoading: true});
        //console.log('AT BOTTOM REQUESTING', that.props.listType);
      }else{
        //console.log('NOT AT BOTTOM')
      }
    };
  }
  componentWillUnmount(){
    //window.removeEventListener('scroll', this.handleScroll.bind(this));
  }
  componentWillMount() {
    this.setProducts(this.props.data);
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.data !== this.props.data) {
      this.setProducts(nextProps.data);
    }
  }

  setProducts(props) {
    this.setState({products_list: props, isLoading: false});
  }

  showCurrent(index) {
    let product = this.state.products_list[index];
    this.props.onClick(product, this.props.listType);
    this.setState({currentIndex: index});
  }

  _onDialogSubmit() {
    console.log(this.refs.password.getValue());
    if (this.refs.password.getValue() == ADMIN_PASS) {
      this.refs.dialog.dismiss();
      ProductsAction.deleteProduct(this.state.item.id);
      //ProductActions.DELETE_PRODUCT(this.state.item.id);
    }
  }
  //loadMore(){
  //  this.currentListLength += 30;
  //  ProductActions.FETCH_PRODUCTS(`${this.currentListLength}`);
  //}
  deleteItem(pid, name, image, index) {
    if (LoginStore.checkPermission(ResourceConstants.Products_ProductDelete)) {
      //var item = _.cloneDeep(this.state.item);
      //item.id = pid;
      //item.name = name;
      //item.img = image;
      //this.setState({item:item});
      //this.refs.dialog.show();
      let r = confirm('Delete item ' + name);
      if (r == true) {
        //ProductActions.DELETE_PRODUCT(pid);
        ProductsAction.deleteProduct(pid);
        let x = this.state.products_list;
        x.splice(index,1);
        this.setState({products_list: x});
      }
    } else {
      alert('Access denied');
    }
  }

  render() {
    var self = this;
    let standardActions = [
      {text: 'Cancel'},
      {text: 'Submit', onTouchTap: this._onDialogSubmit.bind(this), ref: 'submit'}
    ];
    let isLoading = self.state.isLoading;
    let ItemList = (product, index) => {
      if(!product){return;}
      else {
        return (
          <div key={index} className='itemList'
               style={{backgroundColor: this.state.currentIndex == index ? 'gray' : 'white'}}>
            <div onClick={self.showCurrent.bind(self,index)} className='item-info'>
              <div className='item-left'>
                <img className='product-image' src={_.result(product,'displayImage.url','')}/>

                <div className='product-name'>
                  {product.name}
                </div>
              </div>
              <div className='product-id'>
                {product.productid}
              </div>
            </div>
            <div className='itemList-item-right'>

              <button className='delete-button'>
                <SvgIcon style={{top: '25%',right: '2%'}}
                         onClick={self.deleteItem.bind(self,product.productid,product.name,product.assets.image[0], index)}>
                  <path d={Constants.ICONS.delete_icon} />
                </SvgIcon>
              </button>
            </div>
          </div>
        )
      }
    };
    return (
      <div className='ProductList' ref="productList">
        <Dialog
          ref='dialog'
          title={ 'Delete ' + self.state.item.name}
          actions={standardActions}
          modal={true} type='password'>
          <TextField
            ref='password'
            hintText="Password Field"
            floatingLabelText="Password"
            type="password"/>
        </Dialog>

        <List className='ProductList-list'>
          {ItemList}
          {
            (self.state.products_list.length > 0) ? self.state.products_list.map(function (product, index) {
              return ([
                ItemList(product, index),
                <ListDivider />
              ])
            }) : <h4 style={{textAlign: 'center' }}>{self.props.notFoundHeader}</h4>
          }
        </List>
        {/*<button className='ProductList-loadMore' onClick={self.loadMore.bind(self)}>LoadMore</button>*/}
        {isLoading && <LinearProgress style={{height:'8px', left:'5px'}} mode="indeterminate"  />}
      </div>
    )
  }
}
export default ProductList;
