/**
 * Created by vikramaditya on 8/12/15.
 */
import React from 'react';
import mui from 'material-ui';
import _ from 'lodash';
import CanvasPlayer from '../TicketInbox/ProjectViewer/CanvasPlayer';
import ThumbNails from '../TicketInbox/ProjectViewer/ThumbNails';
import Helper from '../../../../helper.js';
import CustomModal from '../../../../../lib/Modal';
var {AppConstants: Constants} = Helper;
var {Dialog, SvgIcon} = mui;

class CanvasPlayerModal extends React.Component {
  constructor() {
    super();
    this.isVisible = false;
    this.state = {
      data: [],
      angle: 0,
      index: 0
    }
  }

  componentWillMount() {
    this.isVisible = true;
    this.setState({data: _.cloneDeep(this.props.data),index: this.props.initialIndex ? this.props.initialIndex:0});
  }
  componentWillUnmount(){
    this.isVisible = false;
    window.removeEventListener('keydown',this.handleKeyEvent.bind(this));
  }
  componentDidMount(){
    window.addEventListener('keydown',this.handleKeyEvent.bind(this));
  }
  modalDismiss() {
    this.props.show(false)
  }

  changeIndex(index) {
    this.setState({index: index})
  }
  handleKeyEvent(e){
    let NEXT_KEY = '39';
    let PREV_KEY = '37';
    let that = this;
    if(e.keyCode == NEXT_KEY){
      let length = Infinity, index = that.state.index;
      if(that.state.data.length){
        length = that.state.data.length;
        index = (that.state.index + 1) % length;
      }
      that.isVisible && that.setState({index: index})
    }
    if(e.keyCode == PREV_KEY){
      let length = Infinity, index = that.state.index;
      if(that.state.data.length){
        length = that.state.data.length;
        index = (that.state.index - 1) < 0 ? length-1 : (that.state.index-1);
      }
      that.isVisible && that.setState({index: index})
    }
  }
  render() {
    let self = this;
    let img_url = self.state.data[self.state.index].url ? self.state.data[self.state.index].url : self.state.data[self.state.index].imageUrl.url ?  self.state.data[self.state.index].imageUrl.url : self.state.data[self.state.index].imageUrl;
    return (
      <div>
        <CustomModal ref='dialog' style={{width:'97%', height:'90%', left:'0', top:'5%'}} isOpen={true} onClose={this.modalDismiss.bind(this)} >
          <div style={{height: '100%', width:'100%'}}>
            <SvgIcon className='cross-icon'
                     style={{fill:'black', zoom:'0.75', position: 'absolute', top: '10px', right: '10px',cursor:'pointer'}}
                     onClick={this.modalDismiss.bind(this)}>
              <path d={Constants.ICONS.close_icon} />
            </SvgIcon>
            <CanvasPlayer imageUrl={img_url}
                          audioUrl={self.state.data[self.state.index].audioUrl ? self.state.data[self.state.index].audioUrl : 'nil'}
                          annUrl={self.state.data[self.state.index].annUrl ? self.state.data[self.state.index].annUrl: 'nil'}
                          fullscreen={false} height={600}/>
            <ThumbNails onClick={self.changeIndex.bind(self)} data={self.state.data}
                        current={self.state.index}/>
          </div>
        </CustomModal>
      </div>
    )
  }
}
export default CanvasPlayerModal;
