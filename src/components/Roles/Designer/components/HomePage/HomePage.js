/**
 * Created by vikramaditya on 7/23/15.
 */
import React from 'react';
import mui from 'material-ui';
import Helper from '../../../../helper.js';
import TicketInbox from '../TicketInbox';
import CustomModal from '../../../../Shared/Modal/Modal';
import ProjectViewerComponent from '../TicketInbox/ProjectViewer/ProjectViewerComponent.js';

var {ProjectsActions, withStyles, LoginStore} = Helper;
import styles from './HomePage.scss';

@withStyles(styles)
class HomePage extends React.Component {
  static contextTypes = {
    router: React.PropTypes.func,
    setProgressVisibility: React.PropTypes.func
  };

  constructor() {
    super();
    this.state = {
      showProjectList: true,
      showModalList: false,
      pid: ''
    }
  }
  componentWillReceiveProps(nextProps){
    this.setLayout();
  }
  componentWillMount() {
    this.setLayout();
  }
  setLayout(){
    let that = this;
    if (this.context.router.getCurrentParams().pid && this.context.router.getCurrentPath() != `/${LoginStore.getCurrentDashboard()}`) {
      let path = this.context.router.getCurrentPath();
      let pid = this.context.router.getCurrentParams().pid; //path.replace('/designer/projects/', '');
      that.onShowProject(true, pid);
    } else {
      that.onShowProject(false, '');
    }
  }
  onShowProject(value, pid) {
    if (value) {
      //(pid != this.state.pid) && ProjectsActions.fetchProject(pid);
      this.setState({
        pid: pid,
        showProjectList: false
      });
    }
    else {
      //this.context.router.transitionTo('/'+LoginStore.getCurrentDashboard());
      this.setState({
        pid: pid,
        showProjectList: true
      });
    }
  }
  showProjectModalList(value){
    if(value){
      document.documentElement.style.overflow = 'hidden';
    }else{
      document.documentElement.style.overflow = 'auto';
    }
    this.setState({showModalList: value});
  }
  render() {
    let self = this;
    let showModalList = self.state.showModalList;
    return (
      <div className="home-page">
        <div className="home-page-main">
          <div className="home-page-main-container">
              {
                self.state.showProjectList ?
                <TicketInbox pid={self.state.pid} showProject={self.onShowProject.bind(self)}/>
                  :
                  <div>
                    <CustomModal isOpen={true} modalStyle={{display: showModalList? 'block' : 'none'}} overlayStyle={{backgroundColor: 'rgba(0,0,0,0.3)'}} onClose={self.showProjectModalList.bind(self,false)} style={{display:'flex', overflow:'hidden', flexFlow:'column', bottom:'0', height:'100%', left: '55px',backgroundColor:'white',border:'1px solid rgba(0,0,0,0.1)',maxHeight:'100vh',padding:'0',width:'300px'}}>
                      <TicketInbox pid={self.state.pid} expanded={true} showProject={self.onShowProject.bind(self)}/>
                    </CustomModal>
                    <div style={{zIndex: '1',position:'fixed', left: '55px', top:'50%'}}>
                      <button onClick={self.showProjectModalList.bind(self,true)} style={{backgroundColor: '#fff', height: '80px', width: '17px', border: '1px solid #666'}}>&lt;</button>
                    </div>
                    <ProjectViewerComponent pid={self.state.pid} onClose={self.onShowProject.bind(self,false)}/>
                  </div>
              }
          </div>
        </div>
      </div>
    )
  }
}
export default HomePage
