/**
 * Created by vikramaditya on 6/19/15.
 */
import React from 'react';
import Router from 'react-router';
import mui from 'material-ui';
import _ from 'lodash';
import styles from './SideMenu.scss';
import Helper from '../../../../helper.js';
import Notification from '../Notification';
import UserAccount from './UserAccount.js';
import MenuElement from './MenuElement.js';
import Constants from './MenuConstants.js';
var {withStyles, LoginStore} = Helper;
var {Avatar, Menu,Toolbar,ToolbarGroup,ToolbarTitle,SvgIcon,List, ListItem,MenuItem,ListDivider, Toggle} = mui;

var {Colors,Spacing} = mui.Styles;
var ThemeManager = new mui.Styles.ThemeManager();

let ALLOWED_MODULES = {
  'admin': ['designer', 'postman', 'products', 'profile', 'adminstats'],
  'designer': ['designer', 'products', 'profile'],
  'lead-designer': ['designer', 'products', 'profile', 'adminstats'],
  'viewer': ['viewer'],
  'operations': ['operations']
};
@withStyles(styles) class SideMenu extends React.Component {

  static contextTypes = {
    router: React.PropTypes.func,
    muiTheme: React.PropTypes.object
  };
  static childContextTypes = {
    muiTheme: React.PropTypes.object
  };

  getChildContext() {
    return {
      muiTheme: ThemeManager.getCurrentTheme()
    }
  }

  componentWillMount() {
    ThemeManager.setPalette({textColor: 'rgba(255,255,255,0.5)'});

    ThemeManager.setComponentThemes({
      menu: {
        backgroundColor: Colors.white
        //containerBackgroundColor: this.context.muiTheme.palette.primary1Color
      },
      menuItem: {
        dataHeight: 32,
        height: 48,
        hoverColor: 'rgba(0, 0, 0, .35)',
        padding: Spacing.desktopGutter,
        selectedTextColor: Colors.white,
        color: Colors.white
      }
    });
    this.setMenuSelection();
  }

  componentWillReceiveProps(nextProps) {
    this.setMenuSelection();
  }

  setMenuSelection() {
    //TODO temporary fix modify it later.
    var d_profile = /[A-Za-z0-9.://-_]+profile/;
    if (d_profile.test(this.context.router.getCurrentPath())) {
      this.setState({menuSelectedIndex: 1});
    }
    else {
      var currentPayload = this.context.router.getCurrentPath().slice(1 + this.context.router.getCurrentPath().lastIndexOf('/'));
      for (let i = 0; i < Constants.MENU_ICONS.length; i++) {
        if (Constants.MENU_ICONS[i].payload == currentPayload) {
          this.setState({menuSelectedIndex: i});
          break;
        }
      }
    }
    //switch (currentPayload){
    //  case Constants.MENU_ICONS[0].payload:
    //    this.setState({menuSelectedIndex: 0});
    //    break;
    //  case Constants.MENU_ICONS[1].payload:
    //    this.setState({menuSelectedIndex: 1});
    //    break;
    //  default:
    //    this.setState({menuSelectedIndex: 0});
    //}
  }

  constructor() {

    super();
    this.state = {
      menuSelectedIndex: 0
    };
  }

  menuClickHandler(index, menuItem) {
    if (menuItem.payload == 'profile') {
      this.context.router.transitionTo(`/${LoginStore.getCurrentDashboard()}/${menuItem.payload}/${LoginStore.getUserID()}`);
    }
    else if (menuItem.payload == LoginStore.getCurrentDashboard()) {
      this.context.router.transitionTo(`/${LoginStore.getCurrentDashboard()}`);
    }
    else {
      //this.context.router.replaceWith(menuItem.payload, this.context.router.getCurrentParams());
      this.context.router.transitionTo(`/${LoginStore.getCurrentDashboard()}/${menuItem.payload}`, this.context.router.getCurrentParams());
    }
  }

  getTheme() {
    return this.context.muiTheme.getCurrentTheme();
  }

  navigateToHome() {
    this.context.router.transitionTo(LoginStore.getCurrentDashboard());
    //this.context.router.transitionTo('designer'); //TODO make this generic
  }

  render() {

    var theme = this.getTheme();
    var self = this;
    //let dashboard = LoginStore.getCurrentDashboard();
    return (
      <div className="SideMenu">
        <div>
          <div onClick={self.navigateToHome.bind(self)}
               style={{backgroundSize: '40px 40px', backgroundPosition: '5px', backgroundRepeat:'no-repeat', cursor:'pointer',color: theme.palette.canvasColor,textAlign: 'center',width:'50px', height: '50px', backgroundImage: 'url(https://s3.amazonaws.com/deko-repo-dev/icon/Icon-40%402x.png)'}}>
          </div>
          {/*<Toolbar style={{cursor:'pointer',background: '#323031',color: theme.palette.canvasColor,textAlign: 'center',width:'55px', height: '62px'}}>
           <ToolbarTitle onClick={self.navigateToHome.bind(self)} text = "D" style={{paddingRight:'0', paddingTop:'2px', fontSize: '0.9em',lineHeight:'58px'}}/>
           </Toolbar>*/}
          <ListDivider style={{backgroundColor:'#5A5A5A'}}/>

          <div className="menu-bar">
            <div style={{backgroundColor :'#323031'}}>
              {
                (Constants.MENU_ICONS.map(function (iconpath, i) {
                  return (
                    <div key={i} title={iconpath.title} onClick={self.menuClickHandler.bind(self,i,iconpath)}>
                      <div
                        style={(i == self.state.menuSelectedIndex) ? {display: 'flex', cursor:'pointer',position:'relative', textAlign:'center', flexFlow:'row nowrap',justifyContent:'center', alignItems:'center',height:'55px', backgroundColor: 'rgba(255,255,255,0.1)'} : {display: 'flex', cursor:'pointer',position:'relative', textAlign:'center', flexFlow:'row nowrap',height:'55px', alignItems:'center', justifyContent:'center'}}>
                        <SvgIcon color={Colors.white} style={{display:'block',zoom:'0.8',padding: '8px 0'}}>
                          <path d={iconpath.path}/>
                        </SvgIcon>
                      </div>
                    </div>
                  )
                }))
              }
            </div>
          </div>
        </div>

        <div className='lower-section'>
          <div className='avatar'>
            <UserAccount />
          </div>
          <div className='notification'>
            <Notification />
          </div>
          {/*<div className='popup-menu'>
            <MenuElement />
          </div>*/}

        </div>
      </div>
    );
  }
}
export default SideMenu;

/*
 render(){
 var self = this;
 var theme = this.getTheme();
 return(
 <div className="menu-list">
 <Toolbar style={{background: theme.palette.accent1Color,color: theme.palette.canvasColor}}>
 <ToolbarTitle text = "CRM" onClick={this.navigateToHome.bind(this)}/>
 </Toolbar>
 <Menu ref="menu" menuItems={iconMenuItems} selectedIndex={this.state.menuSelectedIndex} autoWidth={false} onItemTap={this.menuClickHandler.bind(this)}/>
 </div>
 );
 }
 */
