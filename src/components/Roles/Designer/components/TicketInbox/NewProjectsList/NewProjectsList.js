import React from 'react';
import mui from 'material-ui';
import Moment from 'moment';
import _ from 'lodash';
import styles from './NewProjectsList.scss';

import Helper from '../../../../../helper.js';
var {withStyles, CustomUtility, AppConstants: Constants} = Helper;

var {ListItem,ListDivider,List,Checkbox,Avatar} = mui;
var {Colors,Spacing} = mui.Styles;
var ThemeManager = new mui.Styles.ThemeManager();
let UNREAD_PROJECT = '01';
let READ_PROJECT = '11';

@withStyles(styles) class NewProjectsList extends React.Component {

  static contextTypes = {
    router: React.PropTypes.func,
    muiTheme: React.PropTypes.object
  }

  constructor() {
    super();
    this.non_state_project_list = [];
    this.state = {
      readFlag: {},
      data:[],
      hoverCheckBox: false,
      hoverCheckBoxId: ''
    };
  }
  componentWillMount() {
    this.setUnreadProjects(this.props);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.data !== this.props.data) {
      this.setUnreadProjects(nextProps);
    }
    if (nextProps.expanded !== this.props.expanded) {
      React.findDOMNode(this.refs.project_list).scrollTop = 0;
    }
  }

  setUnreadProjects(props) {
    let unread_data = [];//TicketStore.getProjectUnreadStatus();
    let readFlag = {};
    props.data.map((project) => {
      readFlag[project.id] = unread_data[project.id] ? unread_data[project.id] : READ_PROJECT;
    });
    this.non_state_project_list = props.data;
    this.setState({readFlag: readFlag, data: props.data});
  }

  routeToProject(pid) {
    this.context.router.transitionTo(this.context.router.getCurrentPath() + '/projects/' + pid);
  }

  onHover(arg, pid) {
    this.setState({hoverCheckBox: arg, hoverCheckBoxId: pid});
  }
  onSearchProject(e){
    let value = (e.target.value).toLowerCase();
    function isEqual(project) {
      let name = (`${project.relationship.owner.name}`).toLowerCase();
      let title = (`${project.attributes.title}`).toLowerCase();
      let id = (`${project.id}`).toLowerCase();
      let project_status = (`${CustomUtility.getProjectStatus(project)}`).toLowerCase();
      if (name.indexOf(value) >= 0 || title.indexOf(value) >= 0 || id.indexOf(value) >= 0 || project_status.indexOf(value) >= 0) {
        return true;
      }
      return false;
    }
    let x = this.non_state_project_list;
    let filtered = x.filter(isEqual);
    this.setState({data: filtered});
  }
  shrinkTab(pid, index) {
    if (this.state.readFlag[pid] == UNREAD_PROJECT) {
      //ProjectActions.READ_DOCUMENT(pid);
      let readFlag = this.state.readFlag;
      readFlag[pid] = READ_PROJECT;
      this.setState({readFlag: readFlag});
    }
    this.props.onClick(pid, index);
  }

  render() {
    let self = this;
    //let UserProject = this.props.data;
    let UserProject = this.state.data;
    return (
      <div ref='project_list' className="NewProjectsList">
        <div className='NewProjectsList-searchBox'>
          <input type='text' placeholder='Search' onChange={this.onSearchProject.bind(this)} style={{width : self.props.expanded ? '80%' : '95%'}}/>
        </div>
        <List style={{paddingBottom:'0'}}>
          {
            (UserProject.length > 0) ? UserProject.map(function (project, index) {
              var userObject = {};
              let project_status = CustomUtility.getProjectStatus(project);
              let descStyle = (project.id == self.props.pid) ? {
                color: 'black'
              } : {
                color: '#999'
              };
              let titleStyle = self.props.expanded ? {
                order: '2',alignSelf:'center', flex: '13.5',padding:'0px 10px', fontSize: '0.7em',textOverflow: 'ellipsis',whiteSpace:'nowrap', overflow:'hidden'
              } :
                {
                  order: '2',alignSelf:'center', flex: '1.5',padding:'0px 10px', fontSize: '0.7em',textOverflow: 'ellipsis',whiteSpace:'nowrap', overflow:'hidden'
                }
              let currentStyle = (project.id == self.props.pid) ? {
                backgroundColor: '#f1f7fc',
                marginBottom: '1px',
                paddingLeft: '5px',
                paddingRight: '5px',
                borderRight: '2px solid #4c98d8',
                color: 'black',
                fontWeight:'400'
              } :
                self.state.readFlag[project.id] && self.state.readFlag[project.id] == READ_PROJECT ?
                {backgroundColor: 'white', marginBottom: '1px', paddingLeft: '5px', paddingRight: '5px'} :
                {backgroundColor: '#f2f2f2', marginBottom: '1px', paddingLeft: '5px', paddingRight: '5px'};
              //if (Object.keys(self.props.userDetails).length > 0) {
              //  userObject.imgurl = 'http://graph.facebook.com/' + self.props.userDetails[project.relationship.owner.id].clientid + '/picture';
              //  userObject.name = self.props.userDetails[project.relationship.owner.id].first_name + ' ' +
              //    self.props.userDetails[project.relationship.owner.id].last_name;
              //}
              userObject.imgurl = project.relationship.owner.imageUrl;//'http://graph.facebook.com/' + self.props.userDetails[project.relationship.owner.id].clientid + '/picture';
              userObject.name = project.relationship.owner.name;

              return ([

                <ListItem onClick={self.shrinkTab.bind(self,project.id,index)}
                          style={currentStyle}>

                  <div style={{display: 'flex', flexFlow: 'column nowrap', justifyContent:'center'}}>
                    <div style={{display: 'flex', flexFlow: 'row nowrap', justifyContent:'space-between', fontFamily: 'Lato'}}>
                      <span style={{fontSize: '0.8rem', paddingRight: '8px',alignSelf: 'center',fontWeight: 'bold'}}>{index+1}.</span>
                      <Avatar style={{height:'22px', width:'22px',objectFit: 'cover'}} src={userObject.imgurl}/>
                      {!self.props.expanded &&
                      <div style={{order: '1', flex: '1.5',padding:'0px 10px',fontSize: '0.7em',alignSelf:'center'}}>
                        {userObject.name}
                      </div>}
                      <div
                        style={titleStyle}>
                        {project.attributes.title}
                        <br /> {self.props.expanded && userObject.name}
                        { !self.props.expanded &&
                      <span style={descStyle}> {project.attributes.description}</span> }
                      </div>
                      {
                        <div style={{order: '3', flex: '1',alignSelf:'center', textAlign:'right',fontSize: '0.7em'}}>
                          { project_status == Constants.PROJECT_STATUS.UNASSIGNED && <div style={{display:'flex', justifyContent:'flex-end'}}>{
                            !self.props.expanded ? <span className="project-status un-assigned">Un-Assigned</span> :
                              <div className="dot un-assigned"></div> }</div>
                          }
                          { project_status == Constants.PROJECT_STATUS.ASSIGNED && <div style={{display:'flex', justifyContent:'flex-end'}}>{
                            !self.props.expanded ? <span className="project-status assigned">Assigned</span> :
                              <div className="dot assigned"></div> }</div>
                          }
                          { project_status == Constants.PROJECT_STATUS.WORKING && <div style={{display:'flex', justifyContent:'flex-end'}}>{
                            !self.props.expanded ? <span className="project-status working">Working</span> :
                              <div className="dot working"></div> }</div>
                          }
                          { project_status == Constants.PROJECT_STATUS.CLOSED && <div style={{display:'flex', justifyContent:'flex-end'}}>{
                            !self.props.expanded ? <span className="project-status closed">Closed</span> :
                              <div className="dot closed"></div> }</div>
                          }
                          { project_status == Constants.PROJECT_STATUS.DESIGN_SENT && <div style={{display:'flex', justifyContent:'flex-end'}}>{
                            !self.props.expanded ? <span className="project-status design">Design Sent</span> :
                              <div className="dot design"></div> }</div>
                          }
                        </div>
                      }
                      {!self.props.expanded && <div
                        style={{order: '4', alignSelf:'center', flex: '1', fontSize: '0.7em', textAlign: 'end',marginRight: '20px',overflow: 'hidden', whiteSpace: 'nowrap',textOverflow: 'ellipsis',color:'#808080'}}>
                        {Moment(project.attributes.meta.created_at).calendar()}
                        <span>{project.attributes.meta.read ? 'N' : ''}</span>
                      </div>}
                    </div>
                  </div>
                </ListItem>,
                <ListDivider style={{backgroundColor: '#f2f2f2'}}/>
              ])
            }) : ''
          }
        </List>
      </div>
    );
  }
}
export default NewProjectsList;
/*
 * <ListItem onClick={self.shrinkTab.bind(self,project.id,index)}
 leftCheckbox={<div onMouseOver={self.onHover.bind(self,true,project.id)} onMouseOut={self.onHover.bind(self,false,project.id)}>
 {(self.state.hoverCheckBox && self.state.hoverCheckBoxId == project.id) ? <Checkbox />:
 <Avatar style={{transform:'translate(-20%,0%)',zoom:'0.6'}} src={userObject.imgurl} />} </div>
 } style={{backgroundColor:'#f2f2f2',marginBottom: '1px',paddingLeft:'54px',paddingRight:'5px'}}>
 <div
 style={{color: Colors.black, display: 'flex', flexFlow: 'row nowrap', justifyContent:'space-between'}}>
 {self.props.expanded ? "": <div style={{order: '1', flex: '1.5', fontSize: '0.7em'}}>
 {userObject.name}
 </div>}
 <div
 style={{order: '2', flex: '1.5', fontSize: '0.7em',textOverflow: 'ellipsis',whiteSpace:'nowrap', overflow:'hidden'}}>
 {project.attributes.title} <span style={{color:'grey'}}> {project.attributes.description}</span>
 </div>
 { self.props.expanded ? '': <div style={{order: '3', flex: '1', fontSize: '0.7em'}}>
 Actions
 </div>}
 <div style={{order: '4', flex: '1', fontSize: '0.7em', textAlign: 'end',marginRight: '20px',
 overflow: 'hidden', whiteSpace: 'nowrap',textOverflow: 'ellipsis',color:'#808080'}}>
 {Moment(project.attributes.meta.created_at).calendar()}
 </div>
 </div>
 </ListItem>
 * */

//leftAvatar={(<Avatar onMouseEnter={self.onHover.bind(self,true)} onMouseLeave={self.onHover.bind(self,false)} style={AvatarStyle}> A </Avatar>)}
//leftCheckbox={ <Checkbox onClick={self.routeToProject.bind(self)} style={CheckBoxStyle} iconStyle={{width:'20px', height: '20px', fill: '#bdc3c7', marginLeft:'6px'}}/> }
