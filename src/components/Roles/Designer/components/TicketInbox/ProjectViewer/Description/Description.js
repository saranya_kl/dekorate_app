/**
 * Created by vikramaditya on 7/21/15.
 */
import React from 'react';
import {Avatar, List, ListItem, ListDivider} from 'material-ui';
import styles from './Description.scss';
import Helper from '../../../../../../helper.js';
var {withStyles,ProjectActions, ProjectStore, TriggerTypes} = Helper;

@withStyles(styles)
class Description extends React.Component {
  constructor() {
    super();
    this.all_backend_users = [];
    this.current_pid = '';
    this.state = {
      project_backend_users: []
    }
  }
  componentWillUnmount(){
    this.unsubscribe();
  }
  componentWillMount(){
    if(ProjectStore.getBackendUsers() && ProjectStore.getBackendUsers().length == 0){
      ProjectActions.GET_BACKEND_USERS();
    }else{this.all_backend_users =  ProjectStore.getBackendUsers()}

    if(this.current_pid != this.props.data.id || ProjectStore.getProjectBackendUsers().length == 0){
      this.current_pid = this.props.data.id;
      ProjectActions.GET_PROJECT_BACKEND_USERS(this.props.data.id);
    }else{this.current_pid = this.props.data.id; this.setBackendUsers();}
    this.unsubscribe = ProjectStore.listen(this.onStatusChange.bind(this));
  }
  setBackendUsers(){
    let data = ProjectStore.getProjectBackendUsers();
    var backend_user_data = [];
    this.all_backend_users.map((user) => {
      for (let i = 0; i < data.length; i++) {
        if (user.id == data[i].userid) {
          let tmp = _.cloneDeep(user);
          tmp.currentRole = data[i].role;
          backend_user_data.push(tmp);
        }
      }
    });
    this.setState({project_backend_users: backend_user_data})
  }
  componentWillReceiveProps(nextProps){
    if(nextProps.data.id !== this.props.data.id){
      ProjectActions.GET_PROJECT_BACKEND_USERS(nextProps.data.id);
    }
  }
  onStatusChange(props){
    if (props == TriggerTypes.FETCHED_ALL_BACKEND_USERS) {
      this.all_backend_users =  ProjectStore.getBackendUsers();
    }
    if (props == TriggerTypes.FETCHED_PROJECT_BACKEND_USERS) {
      this.setBackendUsers();
    }
  }
  render(){
    let self = this;
    let des = this.props.data.attributes ? this.props.data.attributes.description : '';
    let budget = this.props.data.attributes ? this.props.data.attributes.budget : '';

    return (
      <div className ='Description'>
        <div className='Description-wrapper'>
          <span className='Description-title'>Description: </span>
          {des === '' ? <span className='Description-empty-title'>No Description</span> :
            <div className='Description-content'>
              <small>{des}</small>
            </div>
          }
        </div>
        <ListDivider />
        <div className='Description-wrapper'>
          <span className='Description-title'>Budget: </span>
          <div className='Description-content'>
            <small>{budget}</small>
          </div>
        </div>
        <ListDivider />
        <div className='Description-collaborators'>
          <span className='Description-title'>Collaborators: </span>
          {
            self.props.data.attributes && self.props.data.attributes.collaborators.map((person, index)=> {
              return (
                <ListItem key={index} disabled={true} style={{paddingLeft:'55px', paddingRight:'16px', paddingBottom:'15px', paddingTop:'10px', fontSize:'0.8rem'}} leftAvatar={<Avatar title={`${person.name}`} src={person.imageUrl} style={{width:'28px', height:'28px', top:'4px'}}/>} primaryText={person.name}>
                </ListItem>
              )
            })
          }
          {self.props.data.attributes && self.props.data.attributes.collaborators.length == 0 && <span className='Description-empty-title'>No Collaborators</span>}
        </div>
        <ListDivider />
        <div className='Description-assigned-users'>
          <span className='Description-title'>Assigned Users: </span>
          {
            self.state.project_backend_users.map((user, index) => {
              return (
                <ListItem key={index} disabled={true} style={{paddingLeft:'55px', paddingRight:'16px', paddingBottom:'7px', paddingTop:'10px', fontSize:'0.8rem'}} leftAvatar={<Avatar key={index} style={{width:'28px', height:'28px', top:'11px'}} title={`${user.firstname} ${user.lastname}`}
                src={user.imageUrl}/>} primaryText={`${user.firstname} ${user.lastname}`} secondaryText={<span style={{fontSize:'11px', marginTop:'0'}}>{user.currentRole}</span>}>
                </ListItem>
              )
            })
          }
          {self.state.project_backend_users.length == 0 && <span className='Description-empty-title'>No Assigned Users</span>}
        </div>
        <ListDivider />
      </div>
    );
  }
}
export default Description;
