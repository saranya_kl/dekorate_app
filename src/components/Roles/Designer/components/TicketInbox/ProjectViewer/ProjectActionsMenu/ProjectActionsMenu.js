/**
 * Created by vikramaditya on 7/21/15.
 */
import React from 'react';
import mui from 'material-ui';
import styles from './ProjectActionsMenu.scss';
import AssignProject from '../../../RightComponent/AssignProject';
import Helper from '../../../../../../helper.js';
import PasswordModal from '../../PasswordModal';
var {ProjectsActions, withStyles, CustomUtility, ResourceConstants, AppConstants: Constants, LoginStore} = Helper;
var {SvgIcon, Toggle, Avatar} = mui;
let Menu = require('material-ui/lib/menus/menu');
let IconMenu = require('material-ui/lib/menus/icon-menu');
let MenuItem = require('material-ui/lib/menus/menu-item');
let MenuDivider = require('material-ui/lib/menus/menu-divider');

let ADMIN_PASSWORD = Constants.ADMIN_PASS;
let MENU_ITEMS = {
  COMMENT: 'COMMENT',
  TIMELINE: 'TIMELINE',
  STYLE_QUIZ: 'STYLE_QUIZ',
  USER_ASSIGN: 'USER_ASSIGN'
};
let MODAL_INITIATOR = {
  DELETE_PROJECT: 'DELETE_PROJECT',
  LOCK_PROJECT: 'LOCK_PROJECT',
  CLOSE_PROJECT: 'CLOSE_PROJECT'
};
const ROLE = LoginStore.getCurrentDashboard();
@withStyles(styles) class ProjectActionsMenu extends React.Component {
  static contextTypes = {
    setProgressVisibility: React.PropTypes.func
  };

  constructor(props) {
    super(props);
    let that = this;
    that.MENU_LIST = {
      'ASSIGN_PROJECT': {text: 'Assign Project', color: '#000'},
      'DELETE_PROJECT': {text: 'Delete Project', color:'red'},
      'UN_WORK': {text: `Un-Working`, color: '#000'},
      'CLOSE': {text: 'Collapse View', color: '#000'},
      'PROJECT_CLOSE': {text: props.meta.isopen? 'Close Project' : 'Open Project', color: 'red'}
    };
    that.currentModal = '';
    that.state = {
      isProjectOpen: props.meta.isopen,
      showAssignProject: false,
      collaborators: [],
      owner: ''
    };
    //that.unsubscribe_ticketstore = TicketsStore.listen(that.handleTicketStore.bind(that));
    //that.unsubscribe_projectstore = ProjectsStore.listen(that.handleProjectStore.bind(that));
    that.checkPermission(props);
  }
  checkPermission(props){
    let that = this;
    if(!LoginStore.checkPermission(ResourceConstants.Projects_ProjectDelete)){
      delete that.MENU_LIST.DELETE_PROJECT;
    }
    if(!LoginStore.checkPermission(ResourceConstants.Projects_ProjectClose)){
      delete that.MENU_LIST.PROJECT_CLOSE;
    }
    else{
      that.MENU_LIST.PROJECT_CLOSE.text = props.meta.isopen ? 'Close Project' : 'Open Project'
    }
  }
  componentWillUnmount() {
    //this.unsubscribe_projectstore();
    //this.unsubscribe_ticketstore();
  }

  componentWillReceiveProps(nextProps) {
    this.checkPermission(nextProps);
    this.setState({isProjectOpen: nextProps.meta.isopen});
  }
  //doAllRequests() {
  //  let userid = LoginStore.getUserID();
  //  //this.context.setProgressVisibility(true);
  //  setTimeout(()=> {
  //    //TicketActions.FETCH_PROJECTS();
  //    //TicketActions.FETCH_MY_PROJECTS(userid, ROLE);
  //    ProjectActions.fetchProject(this.props.pid);
  //  }, 500);
  //}
  //
  //requestAllClosed() {
  //  let userid = LoginStore.getUserID();
  //  setTimeout(()=> {
  //    TicketActions.FETCH_MY_CLOSED_PROJECTS(userid, ROLE, 'me');
  //    TicketActions.FETCH_MY_CLOSED_PROJECTS(userid, ROLE, 'global');// for all closed projects
  //  }, 500);
  //
  //}

  /*onStatusChange(props) {
    //on reload
    let userid = LoginStore.getUserID();
    if (props == TriggerTypes.PROJECT_LOCKED) {
      this.doAllRequests();
    }
    if (props == TriggerTypes.PROJECT_UNLOCKED) {
      this.doAllRequests();
    }
    if (props == TriggerTypes.PROJECT_DELETED) {
      TicketActions.FETCH_PROJECTS();
      TicketActions.FETCH_MY_PROJECTS(userid, ROLE);
      this.props.onClose();
      this.context.setProgressVisibility(false);
    }
    if (props == TriggerTypes.PROJECT_CLOSED) {
      this.doAllRequests();
      this.requestAllClosed();
    }
    if (props == TriggerTypes.PROJECT_OPEN) {
      this.doAllRequests();
      this.requestAllClosed();
    }
    if (props == TriggerTypes.PROJECT_STARTED) {
      this.doAllRequests();
    }
  }
  */
  deleteProject() {
    this.currentModal = MODAL_INITIATOR.DELETE_PROJECT;
    this.refs.passmodal.refs.withStyles.show();
  }

  lockProject(e, value) {
    this.currentModal = MODAL_INITIATOR.LOCK_PROJECT;
    this.refs.passmodal.refs.withStyles.show();
  }

  closeProject() {
    this.currentModal = MODAL_INITIATOR.CLOSE_PROJECT;
    this.closeToggleValue = false;
    this.refs.passmodal.refs.withStyles.show();
  }

  openProject() {
    this.currentModal = MODAL_INITIATOR.CLOSE_PROJECT;
    this.closeToggleValue = true;
    this.refs.passmodal.refs.withStyles.show();
  }

  modalAction(type) {
    if (this.validateUser(type)) {
      switch (this.currentModal) {
      case MODAL_INITIATOR.DELETE_PROJECT:
        //this.context.setProgressVisibility(true);
        ProjectsActions.deleteProject(this.props.pid);
        //ProjectActions.DELETE_DB_PROJECT(this.props.pid);
        break;
      case MODAL_INITIATOR.CLOSE_PROJECT:
        //this.context.setProgressVisibility(true);
        //false -> close, true -> open.
        ProjectsActions.closeProject(this.props.pid, this.closeToggleValue);
        //ProjectActions.CLOSE_PROJECT(this.props.pid, this.closeToggleValue);
        break;
      default :
        console.log('NONE');
      }
    }
  }

  validateUser(type) {
    let that = this;
    if (type == 'SUBMIT') {
      if (that.refs.passmodal.refs.withStyles.getValue() == ADMIN_PASSWORD) {
        that.refs.passmodal.refs.withStyles.dismiss();
        return true;
      } else {
        alert('Wrong Password');
        that.refs.passmodal.refs.withStyles.dismiss();
        return false;
      }
    } else {
      that.refs.passmodal.refs.withStyles.dismiss();
      return false;
    }
  }

  stopWorking() {
    ProjectsActions.startProject(this.props.pid, false);
  }

  onMenuSelect(e) {
    let that = this;
    switch(e.target.innerHTML){
    case that.MENU_LIST.CLOSE && that.MENU_LIST.CLOSE.text:
      that.props.onClose();
      break;
    case that.MENU_LIST.ASSIGN_PROJECT && that.MENU_LIST.ASSIGN_PROJECT.text:
      that.setState({showAssignProject: true});
      //that.props.showRightComponent(true, MENU_ITEMS.USER_ASSIGN);
      break;
    case that.MENU_LIST.UN_WORK && that.MENU_LIST.UN_WORK.text:
      that.stopWorking();
      break;
    case that.MENU_LIST.DELETE_PROJECT && that.MENU_LIST.DELETE_PROJECT.text:
      that.deleteProject();
      break;
    case that.MENU_LIST.PROJECT_CLOSE && that.MENU_LIST.PROJECT_CLOSE.text:
      that.state.isProjectOpen ? that.closeProject() :  that.openProject();
      break;
    default :
      console.log('error in project actions menu');
      //
    }
    //if (e.target.innerHTML == this.MENU_LIST.CLOSE.text) {
    //  this.props.onClose();
    //}
    //else if (e.target.innerHTML == this.MENU_LIST.ASSIGN_PROJECT.text) {
    //  this.props.showRightComponent(true, MENU_ITEMS.USER_ASSIGN);
    //}
    //else if (e.target.innerHTML == this.MENU_LIST.UN_WORK.text) {
    //  this.stopWorking();
    //}
    //else if (e.target.innerHTML == this.MENU_LIST.DELETE_PROJECT.text) {
    //  this.deleteProject();
    //}
    //else if (e.target.innerHTML == this.MENU_LIST.PROJECT_CLOSE.text) {
    //  this.state.isProjectOpen ? this.closeProject() :  this.openProject();
    //}
  }

  render() {
    let self = this;
    let project_status = self.props.project.attributes ? CustomUtility.getProjectStatus(self.props.project) : ''; //!self.props.meta.isopen ? 'Closed' : !self.props.meta.isassign ? 'Unassigned' : self.props.meta.islocked? 'Locked' : 'In Progress';
    var status_color = '#000000';
    switch (project_status) {
    case Constants.PROJECT_STATUS.UNASSIGNED:
      status_color = '#e74c3c';
      break;
    case Constants.PROJECT_STATUS.ASSIGNED:
      status_color = '#f1c40f';
      break;
    case Constants.PROJECT_STATUS.WORKING:
      status_color = '#27ae60';
      break;
    case Constants.PROJECT_STATUS.CLOSED:
      status_color = '#3498db';
      break;
    case Constants.PROJECT_STATUS.DESIGN_SENT:
      status_color = '#000000';
      break;
    default:
      status_color = '#000000';
    }
    let showAssignProject = self.state.showAssignProject;
    let AssignProjectComponent = <AssignProject pid={self.props.pid} onDismiss={() => self.setState({showAssignProject: false})}/>;

    let ProjectTitle = self.props.project.attributes ? self.props.project.attributes.title : '';
    let RightDropDownMenu = <IconMenu onItemTouchTap={self.onMenuSelect.bind(self)}
                                      iconButtonElement={<div className="RightDropDownMenu"><SvgIcon style={{cursor:'pointer', fill:'#666'}}><path d={Constants.ICONS.menu_vertical} /></SvgIcon></div>}>
      {Object.keys(self.MENU_LIST).map((key, index) => {
        return <MenuItem key={index} innerDivStyle={{fontSize:'0.8rem', color: self.MENU_LIST[key].color}} primaryText={self.MENU_LIST[key].text}/>;
      })}
    </IconMenu>;

    let ProjectDescription = <div style={{display:'flex',flexFlow:'column',paddingLeft:'5px'}}>
      <div style={{display:'flex',flexFlow:'row'}}>
        <span style={{fontSize: '1rem', fontWeight: '400', padding: 'none', marginLeft: '5px',color:'black'}}>
          {ProjectTitle}
        </span>
        <button disabled={true} className="assign"
                style={{outline:'none', minWidth: '80px', marginLeft: '15px', border: '1px solid #cccccc', backgroundColor: 'transparent', fontSize: '0.7em', borderRadius:'2px', color: status_color}}>
          {project_status}
        </button>
      </div>
      <span> <a className=''
                href={`https://www.facebook.com/${self.props.project.relationship && self.props.project.relationship.owner.fbid}`}
                target='_blank'>
          <span
            style={{margin:'4px 6px', paddingRight:'12px', fontSize:'12px', color:'#008AE6'}}>{self.props.project.relationship ? self.props.project.relationship.owner.name : ''}</span>
      </a>
      </span>
    </div>;
    return (
      <div className='project-action-menu-container'>
        <div className='flex-row' style={{width: '100%'}}>
          <div className="avatar">
            <div
              style={{width:'30px', height:'30px', backgroundSize:'cover', backgroundPosition:'50% 50%', borderRadius:'50%', backgroundImage: `url(${self.props.project.relationship ? self.props.project.relationship.owner.imageUrl : ''})`}}></div>
          </div>
          <div>
            {ProjectDescription}
          </div>

          <div className='toggle' style={{width:'100%'}}>
            {
              (project_status == Constants.PROJECT_STATUS.WORKING || project_status == Constants.PROJECT_STATUS.DESIGN_SENT)
              && <button className="newdesign-button" onClick={self.props.onSwitch.bind(self,-1)}> New Design </button>
            }
            {/*<button className="close-button"
                    onClick={() => {self.state.isProjectOpen ? self.closeProject() : self.openProject()}}> {self.state.isProjectOpen ? 'Close Project' : 'Open Project'}</button>*/}
            <div style={{marginLeft: '8px'}}>
              {RightDropDownMenu}
            </div>
          </div>
        </div>
        {showAssignProject && AssignProjectComponent}
        <PasswordModal ref='passmodal' onAction={self.modalAction.bind(self)}/>
      </div>
    )
  }
}
export default ProjectActionsMenu;
