import React from 'react';
import _ from 'lodash';
import {Avatar, List, ListItem, ListDivider} from 'material-ui';
import SubjectiveQuestionResponse from '../../../../RightComponent/ActionsPanel/Project_Actions/SubjectiveQuestionResponse';
import CanvasPlayerModal from '../../../../CanvasPlayerModal/CanvasPlayerModal.js';
import DesignerOptionResponse from '../../../../RightComponent/ActionsPanel/Project_Actions/DesignerOptionResponse';
import MoreImagesPreview from '../../../../RightComponent/ActionsPanel/Project_Actions/MoreImagesPreview';
import QuizViewer from '../../../../RightComponent/ActionsPanel/Project_Actions/QuizViewer/QuizViewer';
import PaymentActionsResponse from '../../../../RightComponent/ActionsPanel/Project_Actions/PaymentActionsResponse';

import styles from './ActionResponses.scss';
import Helper from '../../../../../../../helper.js';
var {withStyles, ProjectsActions, ProjectsStore} = Helper;
let ResponseType = {
  MORE_IMAGES : 'More Images',
  SUBJECTIVE: 'Subjective',
  DESIGNER_OPTION: 'Designer Option',
  PAYMENT: 'Payment',
  QUIZ: 'Quiz'
};
@withStyles(styles)
class ActionResponses extends React.Component {
  static contextTypes = {
    setProgressVisibility: React.PropTypes.func
  };
  constructor(props) {
    super(props);
    let that = this;
    that.state = {
      more_images_response: [],
      subjective_response: [],
      quiz_response: [],
      designer_response: [],
      payment_response: [],
      currentType: null,
      currentIndex: 0
    };
    that.unsubscribe = ProjectsStore.listen(that.handleProjectsStore.bind(that));
  }
  setActionResponse(project){
    let more_images = [], subjective = [], quiz = [], designer = [], payment = [], that = this;
    _.result(project,'attributes.actions',[]).map((action) => {
      if(action.type == 'images'){more_images.push(action)}
      if(action.type == 'text'){subjective.push(action)}
      if(action.type == 'quiz'){quiz.push(action)}
      if(action.type == 'choosedesigner'){designer.push(action)}
      if(action.type == 'payment'){payment.push(action)}
    });
    that.setState({more_images_response: more_images, subjective_response: subjective, quiz_response: quiz, designer_response: designer, payment_response: payment});
  }
  componentDidMount(){
    let that = this;
    that.setActionResponse(that.props.project);
  }
  componentWillReceiveProps(nextProps){
    let that = this;
    that.setActionResponse(nextProps.project);
  }
  handleProjectsStore(trigger){
    let that = this;
    if(trigger.project){
      that.context.setProgressVisibility(false);
    }
    if(trigger.error){
      that.context.setProgressVisibility(false);
    }
  }
  hideQuizResponseViewer(){
    let that = this;
    that.setState({currentType: null, currentIndex: 0});
  }
  setCurrentQuizResponse(type, index){
    let that = this;
    that.setState({currentType: type, currentIndex: index});
  }
  onQuizCancel(quiz){
    let that = this;
    let r = confirm(`Cancel Project Action`);
    if (r == true) {
      let pid = that.props.project.id, qid = quiz.id;
      ProjectsActions.cancelProjectAction(qid, pid);
      this.context.setProgressVisibility(true);
    }
  }
  getResponseViewer(responseType, index=0){
    let component = null, that = this;
    component = responseType == ResponseType.PAYMENT ? <div style={{position:'absolute'}}><PaymentActionsResponse show={that.hideQuizResponseViewer.bind(that)} data = {that.state.payment_response[index]} /></div> : component;
    component = responseType == ResponseType.SUBJECTIVE ? <div style={{position:'absolute'}}><SubjectiveQuestionResponse show={that.hideQuizResponseViewer.bind(that)} data = {that.state.subjective_response[index]} /></div> : component;
    component = responseType == ResponseType.DESIGNER_OPTION ? <div style={{position:'absolute'}}><DesignerOptionResponse show={that.hideQuizResponseViewer.bind(that)} data = {that.state.designer_response[index]} /></div> : component;
    component = responseType == ResponseType.QUIZ ? <div><QuizViewer question={{}} response={that.state.quiz_response[index].response} onDismiss={that.hideQuizResponseViewer.bind(that)}/> </div> : component;
    component = responseType == ResponseType.MORE_IMAGES ?
      <div style={{position:'absolute'}}>
      {
        that.state.more_images_response[index].response ?
          <CanvasPlayerModal show={that.hideQuizResponseViewer.bind(that)} data={_.result(that.state.more_images_response[index], `response.images`,[])} />
          :
          <MoreImagesPreview onDismiss={that.hideQuizResponseViewer.bind(that)} data = {that.state.more_images_response[index]} />
      }
      </div> : component;
    return component;
  }

  getResponseList(object, title, index){
    let that = this;
    let component = object.response ?
      <button onClick={that.setCurrentQuizResponse.bind(that, title, index)} className="ActionResponses-response-view">View</button>
      :
      <div>
        <button onClick={that.onQuizCancel.bind(that, object)} className="ActionResponses-response-cancel">Cancel</button>
        <button onClick={that.setCurrentQuizResponse.bind(that, title, index)} className="ActionResponses-response-view">View</button>
      </div>;

    return(
      <div key={index}>
        <div className="ActionResponses-response">
          <div className="ActionResponses-response-title"> {title} - {index+1}</div>
          {component}
        </div>
        <div className="input-divider"></div>
      </div>
    )
  }

  render() {
    let that = this;
    let actionResponse = that.state.currentType && that.getResponseViewer(that.state.currentType, that.state.currentIndex);
    let totallength = that.state.payment_response.length + that.state.more_images_response.length +
      that.state.subjective_response.length + that.state.designer_response.length + that.state.quiz_response.length;
    return(
    totallength != 0 && <div className="ActionResponses">
  			<div className="ActionResponses-title"> Recent Responses </div>
        <div className="input-divider"></div>
        {that.state.payment_response.map((object, index) => that.getResponseList(object, ResponseType.PAYMENT,index))}
  			{that.state.more_images_response.map((object, index) => that.getResponseList(object, ResponseType.MORE_IMAGES, index))}
        {that.state.subjective_response.map((object, index) => that.getResponseList(object, ResponseType.SUBJECTIVE, index))}
        {that.state.designer_response.map((object, index) => that.getResponseList(object, ResponseType.DESIGNER_OPTION,index))}
        {that.state.quiz_response.map((object, index) => that.getResponseList(object, ResponseType.QUIZ, index))}
        {actionResponse}
  		</div>
  	)
  }
}

export default ActionResponses;
