import React from 'react';
import _ from 'lodash';
import {SvgIcon, ListDivider, Toggle, Tabs, Tab} from 'material-ui';
import styles from './ProjectDetails.scss';
import ActionResponses from './ActionResponses';
import Helper from '../../../../../../helper.js';

var {withStyles, AppConstants: Constants, ProjectAssignedUserActions, ProjectAssignedUserStore} = Helper;

@withStyles(styles)
class ProjectDetails extends React.Component {
  constructor() {
    super();
    this.all_backend_users = ProjectAssignedUserStore.getAllBackendUsers();
    this.state = {
      project_backend_users: []
    }
  }
  componentWillUnmount(){
    this.unsubscribe();
  }
  componentWillMount(){
    let that = this;
    ProjectAssignedUserActions.fetchAllBackendUsers();
    ProjectAssignedUserActions.fetchAssignedUsers(that.props.data.id);
    that.setBackendUsers(that.props.data.id);
    that.unsubscribe = ProjectAssignedUserStore.listen(that.handleAssignedUsers.bind(that));
  }
  handleAssignedUsers(trigger){
    let that = this;
    if(trigger.users){
      that.setBackendUsers(that.props.data.id);
    }
    if(trigger.allBackendUsers){
      that.all_backend_users =  ProjectAssignedUserStore.getAllBackendUsers();
      that.setBackendUsers(that.props.data.id);
    }
    if(trigger.error){

    }
  }
  setBackendUsers(pid){
    let data = ProjectAssignedUserStore.getAssignedUsers(pid);
    var backend_user_data = [];
    this.all_backend_users.map((user) => {
      for (let i = 0; i < data.length; i++) {
        if (user.id == data[i].userid) {
          let tmp = _.cloneDeep(user);
          tmp.currentRole = data[i].role;
          backend_user_data.push(tmp);
        }
      }
    });
    this.setState({project_backend_users: backend_user_data})
  }
  componentWillReceiveProps(nextProps){
    let that = this;
    if(nextProps.data.id !== that.props.data.id){
      ProjectAssignedUserActions.fetchAssignedUsers(nextProps.data.id);
      that.setBackendUsers(nextProps.data.id);
      //ProjectActions.GET_PROJECT_BACKEND_USERS(nextProps.data.id);
    }
  }
  render(){
    let that = this;
    let des = _.result(that.props,'data.attributes.description','No description');
    let budget = _.result(that.props,'data.attributes.budget','');
    let owner = _.result(that.props,'data.relationship.owner','');
    let country_location = _.result(that.props,'data.attributes.meta.location.country','Not Available');
    let source = _.result(that.props.data,'attributes.meta.source','ios');
    return(
  		<div className="ProjectDetails">
        <ActionResponses project={that.props.data}/>
  			<div className="description">
  				<div className="ProjectDetails-wrapper">
  					<div className="ProjectDetails-label">Project description</div>
  					<div className="ProjectDetails-text" style={{marginTop:'5px'}}>{des}</div>
  				</div>
  				<div className="input-divider"></div>
  				<div className="ProjectDetails-wrapper">
          <div className="ProjectDetails-label">Budget</div>
            {/*budget.substring(0,3)=='INR' ? <div className="ProjectDetails-text" style={{marginTop:'5px'}}>{budget}</div> : <div className="ProjectDetails-text" style={{marginTop:'5px'}}>INR &nbsp; {budget}</div>*/}
            <div className="ProjectDetails-text" style={{marginTop:'5px'}}>{budget}</div>
          </div>
          <div className="input-divider"></div>
          <div className="ProjectDetails-wrapper">
            <div className="ProjectDetails-label">Source</div>
            <div className="ProjectDetails-text" style={{marginTop:'5px'}}>
              {source}
            </div>
          </div>
          <div className="input-divider"></div>
          <div className="ProjectDetails-wrapper">
            <div className="ProjectDetails-label">Location</div>
            <div className="ProjectDetails-text" style={{marginTop:'5px'}}>
              {country_location}
            </div>
          </div>
  				<div className="input-divider"></div>
  				{owner!='' && <div className="user">
  					<img src={owner.imageUrl} className="user-image owner-image"/>
  					<div className="user-details">
  						<div className="user-name ProjectDetails-label">{owner.name}</div>
  						<div className="user-designation ProjectDetails-text">Owner</div>
  					</div>
  				</div>}
  			</div>
  			<div className="relatives">
  				<div className="ProjectDetails-wrapper">
  					<div className="ProjectDetails-label">Assigned users</div>
  					<div className="relatives-list">
  						{
                that.state.project_backend_users.map((user, index) => {
                  return (
				          <div key={index} disabled={true} className="user">
                    <img src={user.imageUrl} className="user-image"/>
				            <div className="user-details">
				  						<div className="user-name ProjectDetails-label">{`${user.firstname} ${user.lastname}`}</div>
				  						<div className="user-designation ProjectDetails-text">{user.currentRole}</div>
				  					</div>
				          </div>
                  )
                })
				      }
				      {that.state.project_backend_users.length == 0 && <span className='ProjectDetails-text'>No Assigned Users</span>}
  					</div>
  				</div>
  				<div className="input-divider"></div>
  				<div className="ProjectDetails-wrapper">
  					<div className="ProjectDetails-label">Collaborators</div>
  					<div className="relatives-list">
  						{
                that.props.data.attributes && that.props.data.attributes.collaborators.map((user, index) => {
                  return (
                    <div key={index} disabled={true} className="user">
                      <img src={user.imageUrl} className="user-image"/>
                      <div className="user-details">
				  					  	<div className="collab-name ProjectDetails-text user-name">{user.name}</div>
				  					  </div>
				            </div>
                  )
                })
				      }
				      {that.props.data.attributes && that.props.data.attributes.collaborators.length == 0 && <span className='ProjectDetails-text'>No Collaborators</span>}
  					</div>
  				</div>
  			</div>
  		</div>
  	)
  }
}

export default ProjectDetails;
