/**
 * Created by vikramaditya on 7/21/15.
 */
import React from 'react';
import {SvgIcon} from 'material-ui';
import _ from 'lodash';
import NotFoundPage from '../../../../../NotFoundPage';
import Helper from '../../../../../helper.js';
import ProjectTopHeader from './ProjectTopHeader';
import ProjectActionsMenu from './ProjectActionsMenu';
import ProjectItemsLists from './ProjectItemsLists';
import ThumbNails from './ThumbNails';
import Description from './Description';
import CanvasPlayer from './CanvasPlayer';
import CanvasPlayerModal from '../../CanvasPlayerModal';
import DesignResponse from './DesignResponse';
//import FilesHolder from './FilesHolder';
import WorkSpace from './WorkSpace/WorkSpace.js';
import TimeLine from '../../RightComponent/TimeLine';
import ActionsPanel from '../../RightComponent/ActionsPanel';
import NextAction from './NextAction';

var {ProjectActions, NotificationTypes,TicketActions, LoginStore, NotificationStore, ProjectStore, TriggerTypes, AppConstants: Constants} = Helper;
const ROLE = LoginStore.getCurrentDashboard();
class ProjectViewer extends React.Component {
  static contextTypes = {
    setProgressVisibility: React.PropTypes.func,
    router: React.PropTypes.func
  };

  constructor() {
    super();
    this.isVisible = false;
    this.state = {
      showModal: false,
      project: {},
      submissions: [],
      currentSubmissionIndex: 0,
      currentMenuIndex: -3, // 0 for submission and 1,2 3... for drp
      allDesignResponse: [],
      allDraftDesignResponse: [],
      currentDesignResponse: _.cloneDeep(Constants.DESIGN_RESPONSE_OBJECT).data,
      new_drp: false,
      designResponseList: [],
      draftList: [],
      feedbackObject: {},
      feedbackList: [],
      project_backend_users: [],
      //all_backend_users: [],
      unread_status: {},
      unread_comments: {},
      showNotFound: false
    }
  }

  componentWillReceiveProps(nextProps) {
    let newpid = this.context.router.getCurrentParams().pid;
    if (newpid != nextProps.pid) {
      nextProps.showProject(true, newpid);
      this.setState({currentMenuIndex: -3, showNotFound: false});
    } else {
      if (nextProps.pid != this.props.pid) {
        this.setState({currentMenuIndex: -3, showNotFound: false});
      }
    }
  }

  componentDidMount() {
    this.isVisible = true;
    window.addEventListener('keydown', this.onKeyPress.bind(this));
  }

  componentWillMount() {
    var self = this;
    ProjectActions.GET_PROJECT(this.props.pid);
    this.context.setProgressVisibility(true);
    self.unsubscribe_projectStore = ProjectStore.listen(self.onStatusChange.bind(self));
    self.unsubscribe_notification = NotificationStore.listen(self.onNotification.bind(self));
  }

  componentWillUnmount() {
    this.isVisible = false;
    window.removeEventListener('keydown', this.onKeyPress.bind(this));
    this.unsubscribe_projectStore();
    this.unsubscribe_designresponse_store();
    this.unsubscribe_notification();
  }

  onKeyPress(e) {
    if (e.keyCode == 37) { //left
      let c_index = (this.state.currentSubmissionIndex - 1) % this.state.submissions.length;
      var index = c_index >= 0 ? c_index : this.state.submissions.length - 1;
      this.showSubmission(index);
    }
    if (e.keyCode == 39) { //right
      let c_index = (this.state.currentSubmissionIndex + 1) % this.state.submissions.length;
      this.showSubmission(c_index);
    }
  }

  onNotification(props) {
    //console.log(props);
    let Project = ProjectStore.getProject();
    let pid = Project.id;
    let userid = LoginStore.getUserID();
    if (pid == props.pid) {
      switch (props.type) {
      case NotificationTypes.COMMENT:
        ProjectActions.REFRESH_DESIGN_RESPONSE([props.drid]);
        break;
      case NotificationTypes.USERFEEDBACK_SUBMITTED:
        ProjectActions.GET_PROJECT(pid); // think of optimization in future
        break;
      case NotificationTypes.PROJECT_ACTION:
        ProjectActions.onProjectActionCancel();
        break;
      case NotificationTypes.PROJECT_ACTION_SUBMITTED:
        ProjectActions.onProjectActionCancel();
        break;
      case NotificationTypes.NEW_DESIGN_RESPONSE:
        ProjectActions.GET_PROJECT(pid);
        break;
      case NotificationTypes.PROJECT_ASSIGNED:
        TicketActions.FETCH_PROJECTS(); // TODO should be commented..
        TicketActions.FETCH_MY_PROJECTS(userid, ROLE);
        ProjectActions.GET_PROJECT(pid);
        break;
      case NotificationTypes.PROJECT_CLOSE:
        TicketActions.FETCH_PROJECTS(); // TODO should be commented..
        TicketActions.FETCH_MY_PROJECTS(userid, ROLE);
        ProjectActions.GET_PROJECT(pid);
        break;
      case NotificationTypes.LOCK_PROJECT:
        TicketActions.FETCH_PROJECTS(); // TODO should be commented..
        TicketActions.FETCH_MY_PROJECTS(userid, ROLE);
        ProjectActions.GET_PROJECT(pid);
        break;
      default:
        break;
      }
    }
  }

  onStatusChange(props) {
    if (props == TriggerTypes.FETCHED_PROJECT) {
      this.context.setProgressVisibility(false);
      let Project = ProjectStore.getProject();
      var drplist = Project.relationship.designresponses.map(function (x) {
        return x.id
      });
      var draft_drplist = Project.relationship.draftdesignresponses ? Project.relationship.draftdesignresponses.map(function (x) {
        return x.id
      }) : [];
      //TODO set drafts and non-drafts
      ProjectStore.setNonDrafts(drplist); //storing ids in store.... may be helpful in future.
      ProjectStore.setDrafts(draft_drplist);
      (drplist.length > 0) && ProjectActions.GET_DESIGN_RESPONSE(drplist); // get design response
      (draft_drplist.length > 0) && ProjectActions.GET_DRAFT_DESIGN_RESPONSE(draft_drplist);
      //ProjectActions.GET_BACKEND_USERS(); // fetched by sidemenu
      ProjectActions.GET_PROJECT_BACKEND_USERS(this.props.pid);
      this.setState({
        designResponseList: _.cloneDeep(drplist.reverse()),
        draftList: _.cloneDeep(draft_drplist.reverse()),
        project: Project,
        submissions: ProjectStore.getProject().attributes.submission,
        currentSubmissionIndex: 0
      });
    }
    if (props == TriggerTypes.FETCHED_DESIGN_RESPONSES) {
      //let drafts = ProjectStore.getDrafts();
      let non_draft = ProjectStore.getNonDrafts();
      for (let i = 0; i < non_draft.length; i++) {
        ProjectActions.GET_USER_FEEDBACK(non_draft[i]);
      }
      let drp = ProjectStore.getAllDesignResponse();
      this.setState({
        feedbackObject: {},
        unread_status: ProjectStore.getUnreadStatus(),
        unread_comments: ProjectStore.getUnreadComments(),
        feedbackList: [],
        allDesignResponse: drp ? _.cloneDeep(drp.reverse()) : [],
        currentDesignResponse: drp ? drp[this.state.currentMenuIndex] ? drp[this.state.currentMenuIndex] : _.cloneDeep(Constants.DESIGN_RESPONSE_OBJECT).data : _.cloneDeep(Constants.DESIGN_RESPONSE_OBJECT).data
      });
    }
    if (props == TriggerTypes.FETCHED_DRAFT_DESIGN_RESPONSES) {
      let draft = ProjectStore.getAllDraftDesignResponse();
      this.setState({allDraftDesignResponse: draft});
    }
    if (props.split(' ')[0] == TriggerTypes.FEEDBACK_RECEIVED) {
      let fd = {};
      var fd_clone = _.cloneDeep(this.state.feedbackObject);
      fd_clone[props.split(' ')[1]] = fd; //hashing with design response drid for feedback
      let list = Object.keys(fd_clone);
      this.setState({feedbackObject: fd_clone, feedbackList: list, unread_status: ProjectStore.getUnreadStatus()});
    }
    if (props == TriggerTypes.COMMENT_READ) {
      var x = this.state.unread_comments;
      let drid = ProjectStore.getDesignResponseID();
      delete x[drid];
      this.setState({unread_comments: x});
    }

    if (props == TriggerTypes.REFRESH_DESIGN_RESPONSE) {
      /* for updating comment*/
      let drp = ProjectStore.getAllDesignResponse();
      let menuIndex = this.state.currentMenuIndex;
      if (menuIndex > 0 && menuIndex <= this.state.designResponseList.length) {
        ProjectStore.setCommentData(drp[menuIndex - 1]);
      }
      this.setState({
        allDesignResponse: drp,
        unread_comments: ProjectStore.getUnreadComments(),
        unread_status: ProjectStore.getUnreadStatus()
      });
    }
    if (props == TriggerTypes.BACKEND_ERROR) {
      this.context.setProgressVisibility(false);
      this.setState({showNotFound: true});
    }
  }

  switchMenu(index, id) {
    // -1 for new drp, 0 for submission and others for drps.
    if (index == 0) {
      this.setState({currentMenuIndex: index, new_drp: (index == -1), currentSubmissionIndex: 0});
    }
    else if (index == -1) {
      this.setState({
        currentMenuIndex: index, new_drp: (index == -1),
        currentDesignResponse: _.cloneDeep(Constants.DESIGN_RESPONSE_OBJECT).data
      });
    }
    else if (index == -2) {
      this.setState({
        currentMenuIndex: index, new_drp: (index == -1)
      });
    }
    else if (index == -3) {
      this.setState({
        currentMenuIndex: index, new_drp: (index == -1)
      });
    }
    else if (index > this.state.designResponseList.length) { // for drafts
      let current = this.state.allDraftDesignResponse[(index - this.state.designResponseList.length) - 1];
      if(current){
        this.setState({
          currentMenuIndex: index, new_drp: true,
          currentDesignResponse: current
        });
      }
    }
    else if (index <= this.state.designResponseList.length) {
      ProjectStore.setCommentData(this.state.allDesignResponse[index - 1]);
      let current = this.state.allDesignResponse[index - 1];
      if(current) {
        this.setState({
          currentMenuIndex: index, new_drp: (index == -1),
          currentDesignResponse: current
        });
      }
    }
    else {
      //let fd = this.state.feedbackObject[id][0] ? this.state.feedbackObject[id][0].data : [];
      this.setState({currentMenuIndex: index, new_drp: (index == -1)});
    }
  }

  downloadZip() {
    var img_url = [];
    var self = this;
    for (var i = 0; i < this.state.submissions.length; i++) {
      img_url[i] = this.state.submissions[i].imageUrl.url;
    }
    //this.context.setProgressVisibility(true);
  }

  showSubmission(index) {
    if (this.state.currentMenuIndex == 0) { //only for submission
      this.isVisible && this.setState({currentSubmissionIndex: index});
    }
  }

  onShowModal(value) {
    if (value) {
      this.isVisible = false
    } else {
      this.isVisible = true;
    }
    this.setState({showModal: value})
  }

  showRightComponent(value, tag) {
    this.props.showRightComponent(value, tag);
  }

  getProjectDetails() {
    let self = this;
    return (
      <div style={{display:'flex', flexFlow:'column'}}>
        {self.state.currentMenuIndex == 0 ?
          <Description data={self.state.project}/> : ''}
      </div>
    )
  }

  getSubmissionView() {
    let self = this;
    return (
      <div
        style={{display:'flex', flexFlow:'row', justifyContent:'space-between', height:'100%', backgroundColor:'#e6e6e6'}}>
        <div style={{flex:'2'}}>
          <div style={{display:'flex', flexFlow:'column'}}>
            <div
              style={{width:'100%', display:'flex', flexFlow:'row', justifyContent:'space-between', borderBottom: '1px solid #bdc3c7'}}>
              <h6
                style={{color:'#545454', fontSize:'0.9rem', fontWeight:'400', fontFamily:'Lato', padding:'15px', margin:'0', paddingLeft:'25px'}}>
                {self.state.project.attributes ? self.state.project.attributes.title : ''} </h6>
              <button onClick={self.downloadZip.bind(self)}
                      style={{alignSelf:'center', backgroundColor:'transparent', padding:'3.5px', marginRight:'15px', border:'none'}}>
                <SvgIcon style={{fill:'#545454'}}>
                  <path
                    d="M19.35 10.04C18.67 6.59 15.64 4 12 4 9.11 4 6.6 5.64 5.35 8.04 2.34 8.36 0 10.91 0 14c0 3.31 2.69 6 6 6h13c2.76 0 5-2.24 5-5 0-2.64-2.05-4.78-4.65-4.96zM17 13l-5 5-5-5h3V9h4v4h3z"/>
                </SvgIcon>
              </button>
            </div>
            {self.state.submissions.length > 0 && <CanvasPlayer showModal={self.onShowModal.bind(self)}
                                                                imageUrl={self.state.submissions[self.state.currentSubmissionIndex].imageUrl.url}
                                                                audioUrl={self.state.submissions[self.state.currentSubmissionIndex].audioUrl}
                                                                annUrl={self.state.submissions[self.state.currentSubmissionIndex].annUrl}
                                                                fullscreen={true}/> }
            <ThumbNails onClick={self.showSubmission.bind(self)} data={self.state.submissions}
                        current={self.state.currentSubmissionIndex}/>
          </div>
        </div>
        <div style={{flex:'1', borderLeft:'1px solid #bdc3c7'}}>
          {this.getProjectDetails()}
        </div>
      </div>
    )
  }

  getDesignResponseComponent() {
    let self = this;
    return (
      <DesignResponse onSwitch={self.switchMenu.bind(self)} currentMenuIndex={self.state.currentMenuIndex}
                      feedback={self.state.feedbackObject[self.state.currentDesignResponse ? self.state.currentDesignResponse.id : 0] ? self.state.feedbackObject[self.state.currentDesignResponse.id][0] ? self.state.feedbackObject[self.state.currentDesignResponse ? self.state.currentDesignResponse.id : 0][0].data :[] :[]}
                      newDesignResponse={self.state.new_drp} list={self.state.designResponseList}
                      data={self.state.currentDesignResponse}/>
    )
  }

  getOverviewComponent() {
    return (
      <div style={{display:'flex', flexFlow:'row',justifyContent:'space-between', backgroundColor:'#e6e6e6'}}>
        <div style={{flex:'2'}}>
          <div>

            <NextAction data={this.state.project} switchMenu={this.switchMenu.bind(this)}/>

            <div style={{minHeight: '64vh'}}>
              <TimeLine switchMenu={this.switchMenu.bind(this)}/>
            </div>
          </div>
        </div>
        <div style={{flex:'1', borderLeft:'1px solid #bdc3c7'}}>
          {/*<ActionsPanel />*/}
        </div>
      </div>
    )
  }

  getCurrentComponent(index) {

    switch (index) {
    case -3:
      return this.getOverviewComponent();
      break;
    case -2:
      return <WorkSpace pid={this.props.pid}/>;
      break;
    case 0:
      return this.state.submissions.length > 0 ? this.getSubmissionView() : '';
      break;
    default:
      return this.getDesignResponseComponent();
    }
  }

  getPage() {
    let self = this;
    let Modal = <CanvasPlayerModal initialIndex={this.state.currentSubmissionIndex} show={this.onShowModal.bind(this)}
                                   data={self.state.currentMenuIndex == 0 ? self.state.submissions : self.state.feedbackObject[self.state.currentDesignResponse ? self.state.currentDesignResponse.id : 0]}/>;
    return (
      <div style={{display:'flex',flexFlow:'column'}}>
        <ProjectActionsMenu project={self.state.project} onClose={self.props.onClose}
                            meta={self.state.project.attributes ? self.state.project.attributes.meta:{}}
                            pid={self.state.project.id}
                            showRightComponent={self.showRightComponent.bind(self)}
                            onSwitch={self.switchMenu.bind(self)}/>
        <ProjectItemsLists project={self.state.project} fd_data={self.state.feedbackObject}
                           showRightComponent={self.showRightComponent.bind(self)}
                           onClick={self.switchMenu.bind(self)} current={self.state.currentMenuIndex}
                           drp_data={self.state.designResponseList} drafts={self.state.draftList}
                           unread_status={self.state.unread_status}
                           unread_comments={self.state.unread_comments}/>
        {self.getCurrentComponent(self.state.currentMenuIndex)}
        {self.state.showModal && <div style={{position:'absolute'}}>{Modal}</div>}
      </div>
    )
  }

  render() {
    var self = this;
    let CurrentPage = this.state.showNotFound ? <NotFoundPage /> : this.getPage();
    return (
      <div>
        {self.props.pid ?
          CurrentPage
          : <h3>Loading...</h3>}
      </div>
    )
  }
}
export default ProjectViewer;
