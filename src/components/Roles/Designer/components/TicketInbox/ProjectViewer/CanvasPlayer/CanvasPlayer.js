/**
 * Created by vikramaditya on 6/19/15.
 */

/* eslint-disable */
import React from 'react'
import Router from 'react-router';
import mui from 'material-ui';
import styles from './CanvasPlayer.scss'
import request from 'superagent';
import _ from 'lodash';
import Helper from '../../../../../../helper.js';
//import Cookie from 'react-cookie';
var {withStyles, AppConstants: Constants} = Helper;

var {LinearProgress,SvgIcon,FontIcon,Paper,CircularProgress} = mui;
var {Colors,Spacing} = mui.Styles;


@withStyles(styles) class CanvasPlayer extends React.Component {


  constructor() {
    super();
    this.canvasHeight = 500;
    this.doTransform = false;
    this.state = {
      angle: 0,
      canvasWidth: 290,
      totalDuration: "00:00",
      currentTime: "00:00",
      playBtnText: "Play",
      audioSrc: "",
      loopTimeout: 0,
      isPlaying: false,
      initialized: false,
      isAudioNil: false,
      progress: 0,
      isLoading: false
    }
  }

  componentWillMount() {

    this.canvasHeight = this.props.height ? _.cloneDeep(this.props.height) : this.canvasHeight;
  }

  clearTime(resolve) {
    var time;
    if (this.timeout == null) {
      time = 0;
    }
    else {
      time = this.timeout;
    }
    // console.log(time , "time");
    clearTimeout(time);
    this.timeout = 0;

    resolve("Timeout cleared");

  }

  audioReady(resolve) {

    var audio = React.findDOMNode(this.refs.sound);
    audio.oncanplaythrough = function () {
      //console.log("Audio Ready");
      resolve("Audio ready");
    };
  }

  loadRoom() {


    var self = this;
    var x = function () {
      return new Promise(function (resolve) {
        self.clearTime(resolve);
      });
    }


    x().then(function (response) {

      return new Promise(function (resolve) {

        self.setState({isLoading: true});
        var bgurl = self.props.imageUrl;
        var canvas = React.findDOMNode(self.refs.canvas);
        var context = canvas.getContext("2d");
        context.clearRect(0, 0, canvas.width, canvas.height);
        context.putImageData(context.getImageData(0, 0, canvas.width, canvas.height), 0, 0);
        //  console.log("cleared canvas");
        var bg = new Image();
        bg.src = bgurl;

        bg.onload = function () {
          self.setState({
            backgroundImage: bgurl

          });

          resolve('done image');
        };

      });

    }).then(function (response) {

      return new Promise(function (resolve, reject) {


        if (self.props.audioUrl == "nil") {
          reject("No Audio");
        } else {

          self.state.audioSrc = self.props.audioUrl;
          React.findDOMNode(self.refs.sound).src = self.state.audioSrc;
          React.findDOMNode(self.refs.sound).load();


          self.audioReady(resolve)
          //resolve("resolved second promise");

        }

      });

    }).then(function (response) {


      return new Promise(function (resolve) {

        function setTimeFormat(seconds) {
          var totalSec = seconds;
          //var hours = parseInt( totalSec / 3600 ) % 24;
          var minutes = parseInt(totalSec / 60) % 60;
          var seconds = totalSec % 60;

          var result = /*(hours < 10 ? "0" + hours : hours) + "-" +*/ (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds);
          return result;
        }

        self.setState({
          currentTime: '00:00',
          totalDuration: setTimeFormat(Math.floor(React.findDOMNode(self.refs.sound).duration))
        })
        resolve("Set time format");
      });


    }).then(function (response, reject) {

      return new Promise(function (resolve, reject) {

        if (self.props.annUrl == "nil") {
          reject("No text file");
        } else {
          var points_url = self.props.annUrl;
          self.getFileData(points_url, resolve);
        }

      });

    }).then(function (response) {

      self.loadPoints();
      self.setState({
        isLoading: false,
        assetLoaded: true
      })
    });
  }


  getFileData(points, resolve) {

    var self = this;
    request.get(points).end((err, res)=> {
      if (err) throw err;
      // console.log(res);
      self.tmp_x = [];
      self.tmp_y = [];
      self.tmp_drag = [];
      self.factor = 1;
      var x_offset = 0;
      var y_offset = 0;

      var line_arr = [];
      var allText = res.text;
      allText = allText.replace("[", ",");
      allText = allText.replace("]", "");
      line_arr = allText.split("ne");

      var viewport = allText.substring(allText.indexOf("=") + 1);
      viewport = viewport.replace("[", "");
      viewport = viewport.replace("]", "");
      var size = viewport.split(",");
      for (var i = 0; i < size.length; i++) {
        size[i] = parseInt(size[i]);
      }
      var imageWidth = size[0];
      var imageHeight = size[1];

      self.factor = self.canvasHeight / imageHeight;

      var myHeight = self.canvasHeight;
      var myWidth = self.factor * imageWidth;

      self.setState({
        canvasWidth: myWidth,
        canvasHeight: myHeight
      })
      // console.log(size , "size");

      self.tmp_x[0] = (-3 + x_offset) * self.factor;
      self.tmp_y[0] = (0 + y_offset) * self.factor;
      self.tmp_drag[0] = undefined;

      for (var i = 0; i < line_arr.length - 1; i++) {
        var tmp = line_arr[i].split(",");
        var p_x = parseFloat(tmp[1]);
        var p_y = parseFloat(tmp[2]);

        self.tmp_x[i + 1] = (p_x + x_offset) * self.factor;
        self.tmp_y[i + 1] = (p_y + y_offset) * self.factor;
        self.tmp_drag[i + 1] = (tmp[2] == 'p') ? undefined : undefined;

        //console.log("Points: ",self.tmp_x[i+1],self.tmp_y[i+1]);
      }

      //console.log(self.tmp_x);

      resolve(8.359);
    });
  }

  componentDidMount() {
    if (this.props.audioUrl == 'nil') {
      let img = new Image();
      img.src = this.props.imageUrl;
      img.onload = () => {
        let r = img.height / this.canvasHeight;
        this.setState({canvasWidth: img.width / r});
      };
      this.setState({
        backgroundImage: this.props.imageUrl,
        isAudioNil: true,
        progress: 0
      })
    }
    else {
      this.loadRoom();
    }
  }

  componentWillReceiveProps(nextProps) {

    //componentWillReceiveProps(nextProps) {
    //if (this.props.imageUrl != nextProps.imageUrl) {
    //this.doTransform = false;

    this.doTransform = false;
    let propsDidChange = false;
    if (this.props.imageUrl != nextProps.imageUrl) propsDidChange = true;
    if (this.props.audioUrl != nextProps.audioUrl) propsDidChange = true;
    if(propsDidChange){
      if (nextProps.audioUrl == "nil") {
        let img = new Image();
        img.src = nextProps.imageUrl;
        img.onload = () => {
          let r = img.height / this.canvasHeight;
          this.setState({canvasWidth: img.width / r});
        };
        this.setState({
          backgroundImage: nextProps.imageUrl,
          isAudioNil: true,
          angle: 0
        })
      }
      else {
        this.setState({
          audioSrc: "",
          loopTimeout: 0,
          isPlaying: false,
          initialized: false,
          isAudioNil: false,
          playBtnText: 'Play',
          progress: 0,
          angle: 0
        });
        this.loadRoom();
      }
    }
  }

  loadPoints() {

    var self = this;
    var length = self.tmp_x.length;
    var i = 0;
    var tagCount = 0;
    var dotCount = 0;
    var current_tool_id = 0;
    var canvas = React.findDOMNode(this.refs.canvas);
    var context = canvas.getContext('2d');
    for (var i = 0; i < length;) {
      if (self.tmp_x[i] == -3.0 * self.factor) {
        //Change Tool

        switch (self.tmp_y[i]) {

          case 1.0:
            current_tool_id = 1; //black
            context.strokeStyle = '#000000';
            context.fillStyle = "#000000";
            context.lineJoin = "round";
            context.lineWidth = 5;
            context.globalAlpha = 1;
            break;
          case 0.0:
            current_tool_id = 0;
            context.strokeStyle = '#000000';
            context.fillStyle = "#ffff00";
            context.lineJoin = "round";
            context.lineWidth = 1;
            context.globalAlpha = 0.5;
            break;
          default :
            context.strokeStyle = '#000000';
            context.fillStyle = "#ffff00";
            context.globalAlpha = 0.5;
        }
        i++;
      }
      else if (self.tmp_x[i] == -1.0 * self.factor) {
        i++;
      }
      else {

        if (self.tmp_drag[i] && i) {
          // drag
          context.beginPath();
          context.moveTo(self.tmp_x[i - 1], self.tmp_y[i - 1]);
          context.lineTo(self.tmp_x[i], self.tmp_y[i]);
          context.closePath();
          context.stroke();

        } else {
          //dot
          dotCount++;
          context.beginPath();
          context.arc(self.tmp_x[i], self.tmp_y[i], 10, 0, 2 * Math.PI);
          //context.closePath();
          context.stroke();
          context.fill();
          context.font = "13px Roboto";
          context.fillStyle = 'black';
          context.globalAlpha = 0.7;
          context.fillText(dotCount, self.tmp_x[i] - 3, self.tmp_y[i] + 5);

          //console.log("Point @ ", self.tmp_x[i], self.tmp_y[i]);
        }


        if (current_tool_id == 0) {

          var offset = context.lineWidth;
          context.textAlign = 'left';
          context.strokeStyle = 'black';
          context.lineWidth = 2;
          context.fillStyle = 'white';
          context.globalAlpha = 1;

          tagCount++;

          context.lineJoin = "round";
          context.lineWidth = 1;
          context.strokeStyle = '#000000';
          context.fillStyle = "#ffff00";
          context.globalAlpha = 0.5;
        }
        i++;
      }
    }

  }


  drawCanvas(totalTime, fps, tmp_x, tmp_y, tmp_drag) {

    var self = this;
    var length = tmp_x.length;
    var i = 0;
    var tagCount = 0;
    var dotCount = 0;
    var current_tool_id = 0;
    var start = Date.now();
    var canvas = React.findDOMNode(this.refs.canvas);
    var context = canvas.getContext('2d');

    React.findDOMNode(this.refs.sound).play();

    var loop_function = function () {
      //console.log("in loop");
      if (i < length) {

        if (tmp_x[i] == -3.0 * self.factor) {
          //Change Tool

          switch (tmp_y[i]) {

            case 1.0:
              current_tool_id = 1; //black
              context.strokeStyle = '#000000';
              context.fillStyle = "#000000";
              context.lineJoin = "round";
              context.lineWidth = 5;
              context.globalAlpha = 1;
              break;
            case 0.0:
              current_tool_id = 0;
              context.strokeStyle = '#000000';
              context.fillStyle = "#ffff00";
              context.lineJoin = "round";
              context.lineWidth = 1;
              context.globalAlpha = 0.5;
              break;
            default :
              context.strokeStyle = '#000000';
              context.fillStyle = "#ffff00";
              context.globalAlpha = 0.5;
          }
          i++;
        }
        else if (tmp_x[i] == -1.0 * self.factor) {
          i++;
        }
        else {

          if (tmp_drag[i] && i) {
            // drag
            context.beginPath();
            context.moveTo(tmp_x[i - 1], tmp_y[i - 1]);
            context.lineTo(tmp_x[i], tmp_y[i]);
            context.closePath();
            context.stroke();

          } else {
            //dot
            dotCount++;
            context.beginPath();
            context.arc(tmp_x[i], tmp_y[i], 10, 0, 2 * Math.PI);
            //context.closePath();
            context.stroke();
            context.fill();
            context.font = "15px Georgia";
            context.fillStyle = 'black';
            context.globalAlpha = 1;
            context.fillText(dotCount, tmp_x[i] - 4, tmp_y[i] + 3);

            //console.log("Point @ ", tmp_x[i],tmp_y[i]);
          }


          if (current_tool_id == 0) {

            var offset = context.lineWidth;
            context.textAlign = 'left';
            context.strokeStyle = 'black';
            context.lineWidth = 2;
            context.fillStyle = 'white';
            context.globalAlpha = 1;

            tagCount++;

            context.lineJoin = "round";
            context.lineWidth = 1;
            context.strokeStyle = '#000000';
            context.fillStyle = "#ffff00";
            context.globalAlpha = 0.5;
          }
          i++;
        }
      }
      else {
        //Stop Timer

        React.findDOMNode(self.refs.sound).pause();
        //console.log("Stopped");
        var end = Date.now();
        var time = end - start;
        //console.log('Execution time: ' + time);
      }
    };
    self.func = function doTimer(length, resolution, oninstance, oncomplete) {
      self.steps = (length / 100) * (resolution / 10);
      self.totalLength = self.steps;
      var audio = React.findDOMNode(self.refs.sound)
      self.totalTime = audio.duration;
      self.currentTime = 0
      self.currentProgress = 0;
      self.count = 0,
        self.start = Date.now();
      self.speed = length / self.steps;
      self.flag = 0;

      self.inst = function instance() {
        if (++self.count >= self.steps) {
          self.currentProgress = self.totalLength;
          oncomplete(self.steps, self.count);
        }
        else {
          self.currentProgress++;
          self.currentTime = Math.floor(audio.currentTime);
          oninstance(self.steps, self.count);

          self.setState({
            progress: (self.currentProgress / self.totalLength) * 100,
            currentTime: setTimeFormat(self.currentTime),
            totalDuration: setTimeFormat(Math.floor(audio.duration))
          })
          self.diff = (Date.now() - self.start) - (self.count * self.speed);
          self.timeout = window.setTimeout(self.inst, (self.speed - self.diff))

        }
      }
      window.setTimeout(self.inst, (self.speed))

    }

    function setTimeFormat(seconds) {
      var totalSec = seconds;
      //var hours = parseInt( totalSec / 3600 ) % 24;
      var minutes = parseInt(totalSec / 60) % 60;
      var seconds = totalSec % 60;

      var result = /*(hours < 10 ? "0" + hours : hours) + "-" +*/ (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds);
      return result;
    }

    var startTime = Date.now();

    self.func(totalTime, fps, loop_function,
      function () {
        //img.style.opacity = 0;
        var audio = React.findDOMNode(self.refs.sound);
        audio.pause();
        //audio.currentTime = 0;
        self.currentProgress = self.totalLength;
        self.setState({
          isPlaying: false,
          initialized: false,
          playBtnText: 'Replay',
          progress: 100
        });
        //console.log("Completed: ",Date.now() - startTime);

      });
  }

  pauseRecording() {
    //console.log("pause")
    if (this.state.isPlaying) {
      React.findDOMNode(this.refs.sound).pause();
      clearTimeout(this.timeout);
      this.timeout = 0;
      this.setState({
        isPlaying: false,
        playBtnText: 'Play'
      })
    }
    else {
      if (this.state.initialized == false) {

        var audio = React.findDOMNode(this.refs.sound);
        audio.currentTime = 0;
        var tot = audio.duration;
        var timeInterval = tot / this.tmp_x.length;
        let fps = 1.0 / timeInterval;

        var canvas = React.findDOMNode(this.refs.canvas);
        var context = canvas.getContext("2d");
        context.clearRect(0, 0, canvas.width, canvas.height);
        context.putImageData(context.getImageData(0, 0, canvas.width, canvas.height), 0, 0);
        this.setState({
          initialized: true,
          isPlaying: true,
          playBtnText: 'Pause',
          progress: 0
        })
        this.drawCanvas(tot * 1000, fps, this.tmp_x, this.tmp_y, this.tmp_drag)
      } else {
        React.findDOMNode(this.refs.sound).play();
        this.resume();
        this.setState({
          isPlaying: true,
          playBtnText: 'Pause'
        })
      }
    }
  }

  resume() {
    this.start = Date.now();
    this.steps = this.steps - this.count;
    this.count = 0;
    this.timeout = window.setTimeout(this.inst, 0)
  }

  showFullScreen() {
    this.props.showModal(true);
  }

  rotate(dir) {
    this.doTransform = true;
    if (dir == 'LEFT') {
      var ang = this.state.angle - 90;
      this.setState({angle: ang});
    }
    if (dir == 'RIGHT') {
      var ang = this.state.angle + 90;
      this.setState({angle: ang});
    }
  }

  render() {
    //style = {{transition: 'transform 0.5s ease-in'}}
    let self = this;
    let imgUrl = this.state.backgroundImage;
    let progressBarWidth = this.state.canvasWidth - 70;
    let cover_contain = 'contain'; //this.props.cover_contain;
    let transform = 'rotate(' + self.state.angle + 'deg)';
    let canvas_style = {
      transform: transform,
      backgroundImage: 'url(' + imgUrl + ')',
      backgroundPosition: "50% 50%",
      backgroundSize: cover_contain,
      backgroundRepeat: "no-repeat"
    };
    let img_style = {
      transform: transform,
      backgroundImage: `url(${this.state.backgroundImage})`,
      backgroundRepeat: "no-repeat",
      backgroundSize: 'contain',
      width: `${self.state.canvasWidth}`,
      height: `${self.canvasHeight}`
    };
    if (this.doTransform) {canvas_style.transition = 'transform 0.5s ease-in';img_style.transition = 'transform 0.5s ease-in';}
    else {canvas_style.transition = 'none';img_style.transition = 'none';}

    let currentTime = (self.state.progress == 0 || self.state.progress == 100) ? (self.state.totalDuration).slice(2) : (self.state.currentTime).slice(2);

    return (<div className="submission-view" style={{backgroundColor:'#e6e6e6', overflow: 'hidden'}}>

      {this.state.isAudioNil ?
        <div style={{position: 'relative'}}>
          <div style={img_style}>
          </div>

          <div style={{position: 'relative',bottom:'0px'}}>
            <div className='img-overlay'></div>
            <div className="playerDiv">
              <div>
                <button onClick={self.rotate.bind(self,'LEFT')}
                        style={{background: 'transparent',border: 'none', outline: 'none'}}>
                  <SvgIcon style={{fill: '#fff',alignSelf: 'center', display:'block', zoom: '0.9'}}>
                    <path d={Constants.ICONS.rotate_left}/>
                  </SvgIcon>
                </button>
                <button onClick={self.rotate.bind(self,'RIGHT')}
                        style={{background: 'transparent',border: 'none', outline: 'none'}}>
                  <SvgIcon style={{fill: '#fff',alignSelf: 'center', display:'block', zoom: '0.9'}}>
                    <path d={Constants.ICONS.rotate_right}/>
                  </SvgIcon>
                </button>
              </div>
              {self.props.fullscreen && <button style={{background:'transparent', border:'none',outline:'none'}}
                                                onClick={this.showFullScreen.bind(this)}>
                <SvgIcon color={Colors.white} style={{fill: '#fff',alignSelf: 'center', display:'block'}}>
                  <path d={Constants.SUBMISSION_ICON['fullscreen']}/>
                </SvgIcon>
              </button>}
            </div>
          </div>
        </div>
        :
        <div style={{position: 'relative'}}>

          <div>
            {
              !self.state.isPlaying && !self.state.isLoading &&
              <button className='play-icon' onClick={self.pauseRecording.bind(self)}>
                <SvgIcon style={{width:'50px', height:'50px', fill:'#fff'}}>
                  <path className='play-icon-svg' d={Constants.ICONS.play_icon}></path>
                </SvgIcon>
              </button>
            }
            {(self.state.isLoading) ? <div style={{zIndex:'6',position:'absolute',left:'40%',top:'40%'}}>
              <CircularProgress mode="indeterminate" size={0.5}/></div> : null}

            <canvas width={this.state.canvasWidth} height={self.canvasHeight} ref='canvas'
                    style={canvas_style}>
              <audio ref="sound" type={"audio/wav"} src={""}></audio>
            </canvas>
          </div>

          <div style={{position: 'relative',bottom:'0px'}}>
            <progress className="progressBar" max={'100'} value={this.state.progress}
                      style={{width: progressBarWidth}}></progress>
            <div className="overlay"></div>
            <div className="playerDiv">
              <button disabled={self.state.isLoading} style={{background:'transparent',border:'none',outline:'none'}}
                      onClick={this.pauseRecording.bind(this)}>
                <SvgIcon color={Colors.white} style={{fill: '#fff',alignSelf: 'center', display:'block' }}>
                  <path d={Constants.SUBMISSION_ICON[this.state.playBtnText]}/>
                </SvgIcon>
              </button>
              <span id="tracktime"
                    style={{color: 'white', width:'20px', bottom: '6px',alignSelf: 'center' , fontSize:'12px' , fontWeight:'300'}}>{currentTime}</span>

              <div>
                <button onClick={self.rotate.bind(self,'LEFT')}
                        style={{background: 'transparent',border: 'none', outline: 'none'}}>
                  <SvgIcon style={{fill: '#fff',alignSelf: 'center', display:'block', zoom: '0.9'}}>
                    <path d={Constants.ICONS.rotate_left}/>
                  </SvgIcon>
                </button>
                <button onClick={self.rotate.bind(self,'RIGHT')}
                        style={{background: 'transparent',border: 'none', outline: 'none'}}>
                  <SvgIcon style={{fill: '#fff',alignSelf: 'center', display:'block', zoom: '0.9'}}>
                    <path d={Constants.ICONS.rotate_right}/>
                  </SvgIcon>
                </button>
              </div>
              {self.props.fullscreen && <button style={{background:'transparent', border:'none',outline:'none'}}
                                                onClick={this.showFullScreen.bind(this)}>
                <SvgIcon color={Colors.white} style={{fill: '#fff',alignSelf: 'center', display:'block'}}>
                  <path d={Constants.SUBMISSION_ICON['fullscreen']}/>
                </SvgIcon>
              </button>}
            </div>
          </div>
        </div>
      }
    </div>)
  }
}
;
export default CanvasPlayer;
