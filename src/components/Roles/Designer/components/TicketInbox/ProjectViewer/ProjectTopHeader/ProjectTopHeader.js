/**
 * Created by vikramaditya on 7/21/15.
 */
import React from 'react';
import mui from 'material-ui';
import Helper from '../../../../../../helper.js';
var {AppConstants: Constants, TicketStore, TriggerTypes } = Helper;
var {SvgIcon, Avatar} = mui;

let Menu = require('material-ui/lib/menus/menu');
let IconMenu = require('material-ui/lib/menus/icon-menu');
let MenuItem = require('material-ui/lib/menus/menu-item');
let MenuDivider = require('material-ui/lib/menus/menu-divider');


class ProjectTopHeader extends React.Component {
  constructor() {
    super();
    this.MENU_LIST = {
      'ASSIGN_PROJECT': 'Assign Project',
      'DELETE_PROJECT': 'Delete Project',
      'TIMELINE' : 'TimeLine',
      'CLOSE': 'Collapse View',
      'ACTION': 'Send/View Actions'
    };
    this.state = {
      collaborators: [],
      owner: '',
      owner_img: ''
    };
  }

  componentWillMount() {
    let self = this;
    self.unsubscribe = TicketStore.listen(self.onStatusChange.bind(self));
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  onStatusChange(props) {
    //on reload
    if (props == TriggerTypes.FETCHED_ALL_USER_DETAILS) {
      this.unsubscribe();
      let userid = (Object.keys(TicketStore.getAllUsers()).length > 0 && this.props.project.relationship) ? TicketStore.getAllUsers()[this.props.project.relationship.owner.id].clientid : '';
      this.setState({owner_img: userid});
    }
  }
  onMenuSelect(e){
    if(e.target.innerHTML == this.MENU_LIST.CLOSE){
      this.props.onClose();
    }
    if(e.target.innerHTML == this.MENU_LIST.ASSIGN_PROJECT){

    }
    if(e.target.innerHTML == this.MENU_LIST.DELETE_PROJECT){

    }
    if(e.target.innerHTML == this.MENU_LIST.TIMELINE){

    }
    if(e.target.innerHTML == this.MENU_LIST.ACTION){

    }
  }

  render() {
    let self = this;
    let userid = (Object.keys(TicketStore.getAllUsers()).length > 0 && this.props.project.relationship) ? TicketStore.getAllUsers()[this.props.project.relationship.owner.id].clientid : '';
    let owner_img = userid ? userid : this.state.owner_img;
    let RightMenu = <IconMenu onItemTouchTap ={this.onMenuSelect.bind(this)}
                              iconButtonElement={<SvgIcon style={{cursor:'pointer', fill:'#000000'}}><path d={Constants.ICONS.menu_vertical}></path></SvgIcon>}>
                            {Object.keys(this.MENU_LIST).map((key, index) => {return <MenuItem key={index} primaryText={this.MENU_LIST[key]} />})}
                    </IconMenu>;
    return (
      <div style={{display:'flex',flexFlow:'column',backgroundColor:'#e6e6e6'}}>
        <div style={{display:'flex',flexFlow:'row',justifyContent:'space-between',padding:'10px'}}>
          <span
            style={{fontSize: '16px', fontWeight: '300', padding: 'none', marginLeft: '10px',color:'#666666'}}>{self.props.project.attributes ? self.props.project.attributes.title : ''}</span>
          <div>
            {RightMenu}
          </div>
        </div>
        <div style={{display:'flex',flexFlow:'row',paddingLeft:'19px'}}>
          <Avatar style={{zoom: '0.6'}} src={'http://graph.facebook.com/'+ owner_img+ '/picture'}/>
          <span
            style={{margin:'4px 6px', paddingRight:'12px', borderRight:'1px solid #a6a6a6',fontSize:'12px', color:'#008AE6'}}>{self.props.project.relationship ? self.props.project.relationship.owner.name : ''}</span>
          {
            self.props.project.attributes ? self.props.project.attributes.collaborators.map((person, index)=> {

              return (
                <Avatar key={index} title={`${person.name}`} style={{zoom: '0.6',margin: '0 5px'}}
                        src={person.imageUrl}/>
              )
            }) : ''
          }
          {self.props.backend_users.length > 0 && <span style={{borderRight: '1px solid #a6a6a6', margin:'4px 10px'}}/>}
          {
            self.props.backend_users.map((user, index) => {

              return (
                <Avatar key={index} title={`${user.firstname} ${user.lastname}`} style={{zoom: '0.6',margin: '0 5px'}}
                        src={user.imageUrl}/>
              )
            })
          }
        </div>
      </div>
    )
  }
}
export default ProjectTopHeader;
