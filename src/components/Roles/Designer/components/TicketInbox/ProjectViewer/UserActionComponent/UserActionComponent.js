/**
 * Created by vikramaditya on 11/4/15.
 */
import React from 'react';
import mui from 'material-ui';
import _ from 'lodash';
import Helper from '../../../../../../helper.js';
import Components from '../../../../../../component.js';
import styles from './UserActionComponent.scss';
import SubjectiveQuestion from '../../../RightComponent/ActionsPanel/Project_Actions/SubjectiveQuestion.js';
import MoreImages from '../../../RightComponent/ActionsPanel/Project_Actions/MoreImages.js';
import DesignerOptionModal from '../../../RightComponent/ActionsPanel/Project_Actions/DesignerOptionModal.js';
import PaymentActions from '../../../RightComponent/ActionsPanel/Project_Actions/PaymentActions.js';

var {AppConstants: Constants, withStyles, LoginStore, ProjectsStore, ProjectsActions} = Helper;
let {SvgIcon, RefreshIndicator} = mui;

@withStyles(styles)
class UserActionComponent extends React.Component {

  constructor(props) {
    super(props);
    let that = this;
    this.state = {
      expand: false,
      dropdown: false,
      showSubjective: false,
      showMoreImages: false,
      showDesignerOption: false,
      showPaymentOption: false,
      styleQuizStatus: 'Send Style Quiz'
    };
    that.user = LoginStore.getUserInfo();
  }
  componentWillReceiveProps(nextProps){
    this.setState({styleQuizStatus: 'Send Style Quiz'});
  }
  expand(){
    this.setState({expand: true});
  }
  send(){
    this.setState({expand:false});
  }
  askDropdown(){
    let that = this;
    that.setState({dropdown: !that.state.dropdown});
  }
  sendStyleQuiz(){
    let r = confirm(`Send Style Quiz`), that = this;
    if (r) {
      let pid = that.props.pid;
      let uid = _.result(ProjectsStore.getProject(pid),'relationship.owner.id',null);
      ProjectsActions.sendUserAction(uid,pid);
      that.setState({styleQuizStatus: 'Sending...'});
    }
  }
  showModal(type, value){
    let that = this;
    if(type == 'MORE_IMAGES'){
      that.setState({showMoreImages: value, dropdown: false});
    }
    if(type == 'SUBJECTIVE_QUESTION'){
      that.setState({showSubjective: value, dropdown: false});
    }
    if(type == 'QUIZ'){
      that.setState({showQuiz: value, dropdown: false});
    }
    if(type == 'DESIGNER'){
      that.setState({showDesignerOption: value, dropdown: false});
    }
    if(type == 'PAYMENT'){
      that.setState({showPaymentOption: value, dropdown: false});
    }
  }
  render() {
    let that = this;
    let user = that.user;
    let styleQuizStatus = that.state.styleQuizStatus;
    let sendingStyleQuiz = styleQuizStatus != 'Send Style Quiz';
    return(
      <div>
        { that.state.expand ?
          <div className="UserActions-expanded">
            <div className="UserActions-expanded-input">
              <img src={user.imageUrl} className="UserActions-img" />
              <textarea rows={4} placeholder="Add your response here" className="UserActions-expanded-text"/>
            </div>
            <div className="UserActions-expanded-actionwrapper">
              <div className="UserActions-expanded-iconWrapper">
                <SvgIcon className="UserActions-icon">
                  <path d="M4 14h4v-4H4v4zm0 5h4v-4H4v4zM4 9h4V5H4v4zm5 5h12v-4H9v4zm0 5h12v-4H9v4zM9 5v4h12V5H9z"/>
                </SvgIcon>
                <button className="style-quiz" onClick={that.sendStyleQuiz.bind(that)}>
                  {sendingStyleQuiz && <RefreshIndicator size={20} left={10} top={5} status="loading" />}
                  {styleQuizStatus}
                </button>
              </div>
              <div className="UserActions-expanded-sendWrapper">
                <button className="ask-wrapper" onClick={that.askDropdown.bind(that)}>
                  <span className="ask">Ask</span>
                  <SvgIcon className="down-icon">
                    <path d="M7 10l5 5 5-5z"/>
                  </SvgIcon>
                </button>
                <button className="send" onClick={that.send.bind(that)}>Send</button>
              </div>
            </div>
            {
              that.state.dropdown ?
                <div className="dropdown-wrapper">
                  <div className="action-type" onClick={that.showModal.bind(that, 'SUBJECTIVE_QUESTION', true)}>A Question </div>
                  <div className="input-divider"></div>
                  <div className="action-type" onClick={that.showModal.bind(that, 'MORE_IMAGES', true)}>For More Images </div>
                  <div className="input-divider"></div>
                  <div className="action-type" onClick={that.showModal.bind(that, 'DESIGNER', true)}>For Designer Choice </div>
                  <div className="input-divider"></div>
                  <div className="action-type" onClick={that.showModal.bind(that, 'PAYMENT', true)}>For Payment Option </div>
                </div> : ''
            }
          </div>
          :
          <div className="UserActions">
            <img src={user.imageUrl} className="UserActions-img"/>
            <button onClick={that.expand.bind(that)} className="UserActions-inputbutton">Add your response here </button>
            <div className="UserActions-iconWrapper">
              <SvgIcon className="UserActions-icon">
                <path d="M4 14h4v-4H4v4zm0 5h4v-4H4v4zM4 9h4V5H4v4zm5 5h12v-4H9v4zm0 5h12v-4H9v4zM9 5v4h12V5H9z"/>
              </SvgIcon>
            </div>
          </div>
        }
        {that.state.showSubjective && <div style={{position:'absolute'}}><SubjectiveQuestion pid={that.props.pid} ref='subjective' onDismiss= {that.showModal.bind(that, 'SUBJECTIVE_QUESTION', false)} /></div>}
        {that.state.showMoreImages && <div style={{position:'absolute'}}><MoreImages pid={that.props.pid} ref='moreimages' onDismiss = {that.showModal.bind(that, 'MORE_IMAGES', false)} /></div>}
        {that.state.showDesignerOption && <div style={{position:'absolute'}}><DesignerOptionModal pid={that.props.pid} ref='designer' onDismiss= {that.showModal.bind(that, 'DESIGNER', false)} /></div>}
        {that.state.showPaymentOption && <div style={{position:'absolute'}}><PaymentActions pid={that.props.pid} ref='payment' onDismiss= {that.showModal.bind(that, 'PAYMENT', false)} /></div>}
      </div>
    )
  }

}

export default UserActionComponent;
