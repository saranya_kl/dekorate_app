/**
 * Created by vikramaditya on 7/21/15.
 */
import React from 'react';
import {SvgIcon, CircularProgress} from 'material-ui';
import _ from 'lodash';
import NotFoundPage from '../../../../../NotFoundPage';
import Helper from '../../../../../helper.js';
import ProjectActionsMenu from './ProjectActionsMenu';
import ProjectItemsLists from './ProjectItemsLists/MenuLists';
import ThumbNails from './ThumbNails';
import CanvasPlayer from './CanvasPlayer';
import CanvasPlayerModal from '../../CanvasPlayerModal';
import DesignResponse from './DesignResponse';
import ProjectDetails from './ProjectDetails/ProjectDetails.js';
import WorkSpace from './WorkSpace/WorkSpace.js';
import TimeLine from '../../RightComponent/TimeLine';
import NextAction from './NextAction';
import UserActionComponent from './UserActionComponent/UserActionComponent.js';

var {ProjectsActions, DownloadUtility, ProjectsStore, ProjectAssignedUserActions, DRPActions, AppConstants: Constants} = Helper;
class ProjectViewerComponent extends React.Component {
  static contextTypes = {
    setProgressVisibility: React.PropTypes.func,
    router: React.PropTypes.func
  };

  constructor(props) {
    super(props);
    let that = this;
    that.isVisible = false;

    let project = typeof(ProjectsStore.getProject(props.pid)) == 'object' ? ProjectsStore.getProject(props.pid): {};
    that.state = {
      showModal: false,
      isLoading: ProjectsStore.getProject(props.pid) == 'string',
      project: project,
      submissions: _.result(project,'attributes.submission',[]),
      currentSubmissionIndex: 0,
      currentMenuIndex: -3, // 0 for submission and 1,2 3... for drp
      currentDesignResponse: _.cloneDeep(Constants.DESIGN_RESPONSE_OBJECT).data,
      designResponseList: _.result(project,'relationship.designresponses',[]).map(x => x.id),
      draftList: _.result(project,'relationship.draftdesignresponses',[]).map(x => x.id),
      currentDRID: null,
      new_drp: false,
      showNotFound: false
    };
    that.unsubscribe_projectstore = ProjectsStore.listen(that.handleProjectStore.bind(that));
  }
  componentWillMount() {
    let self = this;
    let pid = self.props.pid;
    self.setProject(pid);
  }
  componentWillReceiveProps(nextProps) {
    let self = this;
    if(nextProps.pid != self.props.pid){
      let pid = nextProps.pid;
      self.setProject(nextProps.pid);
      self.setState({currentMenuIndex: -3});
    }
  }
  //componentWillReceiveProps(nextProps) {
  //  let that = this;
  //  let newpid = that.context.router.getCurrentParams().pid;
    //console.log(newpid, nextProps.pid, that.props.pid);
    //that.setState({currentMenuIndex: -3});
    //ProjectAssignedUserActions.fetchAssignedUsers(nextProps.pid);
  //}

  componentDidMount() {
    let that = this;
    that.isVisible = true;
    window.addEventListener('keydown', that.onKeyPress.bind(that));
  }

  componentWillUnmount() {
    let that = this;
    that.isVisible = false;
    window.removeEventListener('keydown', that.onKeyPress.bind(that));
    that.unsubscribe_projectstore();
  }

  onKeyPress(e) {
    let that = this;
    if (e.keyCode == 37) { //left
      let c_index = (that.state.currentSubmissionIndex - 1) % that.state.submissions.length;
      let index = c_index >= 0 ? c_index : that.state.submissions.length - 1;
      that.showSubmission(index);
    }
    if (e.keyCode == 39) { //right
      let c_index = (that.state.currentSubmissionIndex + 1) % that.state.submissions.length;
      that.showSubmission(c_index);
    }
  }

  setProject(pid){
    let that = this;
    let project = ProjectsStore.getProject(pid);
    if(typeof (project) == 'object'){

      let drp = _.result(project,'relationship.designresponses',[]);
      let draft_drp = _.result(project,'relationship.draftdesignresponses',[]);
      let drplist = drp.map(x => x.id);
      let draft_drplist = draft_drp.map(x =>  x.id);

      DRPActions.fetchDesignResponse(drplist);
      DRPActions.fetchDraftDesignResponse(draft_drplist);
      that.setState({
        designResponseList: _.cloneDeep(drplist.reverse()),
        draftList: _.cloneDeep(draft_drplist.reverse()),
        project: project,
        submissions: project.attributes.submission,
        currentSubmissionIndex: 0,
        isLoading: false
      });
    }else{
      ProjectsActions.fetchProject(pid);
      ProjectAssignedUserActions.fetchAssignedUsers(pid);
      that.setState({isLoading: true});
    }
  }
  handleProjectStore(trigger){
    let that = this;
    let pid = that.props.pid;
    if(trigger.project){
      that.setProject(pid);
    }
    if(trigger.error){
      that.setState({isLoading: false, showNotFound: true});
    }
  }

  switchMenu(index, id) {
    let that = this;
    // -1 for new drp, 0 for submission and others for drps.
    if (index == 0) {
      that.setState({currentMenuIndex: index, new_drp: (index == -1), currentSubmissionIndex: 0});
    }
    else if (index == -1) {
      that.setState({
        currentMenuIndex: index, new_drp: (index == -1),
        currentDRID: null,
        currentDesignResponse: _.cloneDeep(Constants.DESIGN_RESPONSE_OBJECT).data
      });
    }
    else if (index == -2) {
      that.setState({
        currentMenuIndex: index, new_drp: (index == -1)
      });
    }
    else if (index == -3) {
      that.setState({
        currentMenuIndex: index, new_drp: (index == -1)
      });
    }
    else if (index > that.state.designResponseList.length) { // for drafts
      that.setState({
        currentDRID: id,
        currentMenuIndex: index, new_drp: true,
        currentDesignResponse: {}
      });
    }
    else if (index <= that.state.designResponseList.length) {
      that.setState({
        currentDRID: id,
        currentMenuIndex: index, new_drp: (index == -1),
        currentDesignResponse: {}
      });
    }
    else {
      that.setState({currentMenuIndex: index, new_drp: (index == -1)});
    }
  }

  downloadZip() {
    let img_url = [];
    let self = this;
    for (let i = 0; i < self.state.submissions.length; i++) {
      img_url[i] = self.state.submissions[i].imageUrl.url;
    }
    self.context.setProgressVisibility(true);
    DownloadUtility.downloadImageZIP(img_url, self.props.pid)
      .then(() => self.context.setProgressVisibility(false))
      .catch(() => self.context.setProgressVisibility(false));
  }

  showRight(){
    let that = this;
    let c_index = (that.state.currentSubmissionIndex + 1) % that.state.submissions.length;
    that.showSubmission(c_index);
  }

  showLeft(){
    let that = this;
    let c_index = (that.state.currentSubmissionIndex - 1) % that.state.submissions.length;
    let index = c_index >= 0 ? c_index : that.state.submissions.length - 1;
    that.showSubmission(index);
  }

  showSubmission(index) {
    let that = this;
    if (that.state.currentMenuIndex == 0) { //only for submission
      that.isVisible && that.setState({currentSubmissionIndex: index});
    }
  }

  onShowModal(value) {
    let that = this;
    if (value) {
      that.isVisible = false
    } else {
      that.isVisible = true;
    }
    that.setState({showModal: value})
  }

  //getProjectDetails() {
  //  let self = this;
  //  return (
  //    <div style={{display:'flex', flexFlow:'column'}}>
  //      {self.state.currentMenuIndex == 0 ?
  //        <Description data={self.state.project}/> : ''}
  //    </div>
  //  )
  //}

  getSubmissionView() {
    let self = this;
    return (
      <div
        style={{display:'flex', flexFlow:'row', justifyContent:'space-between', height:'100%', backgroundColor:'#e6e6e6'}}>
        <div style={{flex:'2'}}>
          <div style={{display:'flex', flexFlow:'column'}}>
            <div
              style={{width:'100%', display:'flex', flexFlow:'row', justifyContent:'space-between', borderBottom: '1px solid #bdc3c7'}}>
              <h6
                style={{color:'#545454', fontSize:'0.9rem', fontWeight:'400', fontFamily:'Lato', padding:'15px', margin:'0', paddingLeft:'25px'}}>
                {self.state.project.attributes ? self.state.project.attributes.title : ''} </h6>
              <div style={{display:'flex'}}><button onClick={self.downloadZip.bind(self)}
                                                    style={{alignSelf:'center', backgroundColor:'#545454', display:'flex',padding:'5px', marginRight:'15px', border:'none',borderRadius:'2px'}}>
                <SvgIcon style={{fill:'#fff', width:'20px', height:'20px'}}>
                  <path
                    d="M19.35 10.04C18.67 6.59 15.64 4 12 4 9.11 4 6.6 5.64 5.35 8.04 2.34 8.36 0 10.91 0 14c0 3.31 2.69 6 6 6h13c2.76 0 5-2.24 5-5 0-2.64-2.05-4.78-4.65-4.96zM17 13l-5 5-5-5h3V9h4v4h3z"/>
                </SvgIcon>
              </button>
                <button
                  style={{alignSelf:'center', backgroundColor:'#545454', display:'flex',padding:'5px', marginRight:'15px', border:'none', borderRadius:'2px'}}>
                  <SvgIcon style={{fill:'#fff', width:'20px', height:'20px'}}>
                    <path
                      d="M7 14H5v5h5v-2H7v-3zm-2-4h2V7h3V5H5v5zm12 7h-3v2h5v-5h-2v3zM14 5v2h3v3h2V5h-5z"/>
                  </SvgIcon>
                </button></div>
            </div>
            {self.state.submissions.length > 0 && <CanvasPlayer showModal={self.onShowModal.bind(self)}
                                                                next = {self.showRight.bind(self)}
                                                                back = {self.showLeft.bind(self)}
                                                                prevUrl={self.state.currentSubmissionIndex != 0 ? self.state.submissions[self.state.currentSubmissionIndex-1].imageUrl.url : null}
                                                                nextUrl={self.state.currentSubmissionIndex == self.state.submissions.length-1 ? null : self.state.submissions[self.state.currentSubmissionIndex+1].imageUrl.url}
                                                                imageUrl={self.state.submissions[self.state.currentSubmissionIndex].imageUrl.url}
                                                                audioUrl={self.state.submissions[self.state.currentSubmissionIndex].audioUrl}
                                                                annUrl={self.state.submissions[self.state.currentSubmissionIndex].annUrl}
                                                                fullscreen={true}/> }
            <ThumbNails onClick={self.showSubmission.bind(self)} data={self.state.submissions}
                        current={self.state.currentSubmissionIndex}/>
          </div>
        </div>
      </div>
    )
  }

  getDesignResponseComponent() {
    let self = this;
    return (
      <DesignResponse onSwitch={self.switchMenu.bind(self)} currentMenuIndex={self.state.currentMenuIndex}
                      currentDRID= {self.state.currentDRID} project={self.state.project}
                      newDesignResponse={self.state.new_drp}/>
    )
  }

  getOverviewComponent() {
    let that = this;
    return (
      <div style={{display:'flex', flexFlow:'row',justifyContent:'space-between', backgroundColor:'#e6e6e6'}}>
        <div style={{flex:'2.4'}}>
          <div>
            <UserActionComponent pid={that.state.project.id}/>
            <NextAction data={that.state.project} switchMenu={that.switchMenu.bind(that)}/>

            <div style={{minHeight: '64vh'}}>
              <TimeLine project = {that.state.project} switchMenu={that.switchMenu.bind(that)}/>
            </div>
          </div>
        </div>
        <div style={{flex:'0.9'}}>
          <ProjectDetails data={that.state.project}/>
        </div>
        <div style={{flex:'0.9'}}></div>
      </div>
    )
  }

  getCurrentComponent(index) {
    let that = this;
    switch (index) {
    case -3:
      return that.getOverviewComponent();
      break;
    case -2:
      return <WorkSpace pid={that.props.pid}/>;
      break;
    case 0:
      return that.state.submissions.length > 0 ? that.getSubmissionView() : '';
      break;
    default:
      return that.getDesignResponseComponent();
    }
  }
  goBack(){
    let that = this;
    that.setState({
      currentMenuIndex: -3, new_drp: false
    });
  }
  getPage() {
    let self = this;
    let Modal = <CanvasPlayerModal initialIndex={self.state.currentSubmissionIndex} show={self.onShowModal.bind(self)}
                                   data={self.state.currentMenuIndex == 0 && self.state.submissions}/>;
    return (
      <div style={{display:'flex',flexFlow:'column'}}>
        { self.state.new_drp ? <div style={{display: 'flex', zIndex:'1', backgroundColor:'white', position: 'fixed', width: '100%', height:'45px', alignItems:'center'}}>
          <button style={{backgroundColor:'#f2f2f2', height: '100%', padding:'0 15px'}} onClick={self.goBack.bind(self)}>
            <SvgIcon style={{fill: '#5ecece'}}>
              <path d="M21 11H6.83l3.58-3.59L9 6l-6 6 6 6 1.41-1.41L6.83 13H21z"/>
            </SvgIcon>
          </button>
          <div style={{color: '#545454', fontSize:'0.9rem', fontWeight:'400', marginLeft:'20px'}}> {_.result(self.state.project,'attributes.title','')} &nbsp;/&nbsp; Create Project </div>
        </div> : <ProjectActionsMenu project={self.state.project} onClose={self.props.onClose}
                                     meta={_.result(self.state.project,'attributes.meta',{})}
                                     pid={self.state.project.id}
                                     onSwitch={self.switchMenu.bind(self)}/>
        }
        {!self.state.new_drp && <ProjectItemsLists  project={self.state.project} fd_data={{}}
                                                    onClick={self.switchMenu.bind(self)} current={self.state.currentMenuIndex}
                                                    drp_data={self.state.designResponseList} drafts={self.state.draftList}
                                                    unread_status={{}}
                                                    unread_comments={{}}/>}
        {self.getCurrentComponent(self.state.currentMenuIndex)}
        {self.state.showModal && <div style={{position:'absolute'}}>{Modal}</div>}
      </div>
    )
  }

  render() {
    let self = this;
    let CurrentPage = self.state.showNotFound ? <NotFoundPage /> : self.getPage();

    return (
      <div>
        {self.state.isLoading && <CircularProgress color='green' style={{zIndex:'9999',position:'absolute',left:'50%',top:'30%'}} mode="indeterminate" />}
        {CurrentPage}
      </div>
    )
  }
}
export default ProjectViewerComponent;
