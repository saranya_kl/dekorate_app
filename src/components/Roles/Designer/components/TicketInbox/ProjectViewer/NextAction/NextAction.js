/**
 * Created by vikramaditya on 9/7/15.
 */
import React from 'react';
import _ from 'lodash';
import {SvgIcon} from 'material-ui';
import MoreImages from '../../../RightComponent/ActionsPanel/Project_Actions/MoreImages.js';
import DesignerOptionModal from '../../../RightComponent/ActionsPanel/Project_Actions/DesignerOptionModal.js';
import AssignProject from '../../../RightComponent/AssignProject';
import Helper from '../../../../../../helper.js';
import styles from './NextAction.scss';
var {AppConstants: Constants, ProjectsActions, ProjectsStore, withStyles, LoginStore, CustomUtility} = Helper;
const ROLE = LoginStore.getCurrentDashboard();

@withStyles(styles)
class NextAction extends React.Component {
  static contextTypes = {
    setProgressVisibility: React.PropTypes.func
  };
  constructor() {
    super();
    this.state={
      feedback: undefined,
      status: '',
      showDesignerOption: false,
      showUserAssign: false,
      user: {}
    }
  }
  componentWillMount(){
    let that = this;
    that.unsubscribe = ProjectsStore.listen(that.handleProjectStore.bind(that));
    let project = that.props.data;
    let status = project.id && CustomUtility.getProjectStatus(project);
    let userInfo = LoginStore.getUserInfo();
    that.setState({status: status,  user: userInfo, feedback: _.result(project,'attributes.feedback', undefined)});
  }
  componentWillUnmount(){
    this.unsubscribe();
  }
  componentWillReceiveProps(nextProps){
    if(nextProps.data !== this.props.data){
      let project = nextProps.data;
      if(project.attributes){
        let status = CustomUtility.getProjectStatus(nextProps.data);
        this.setState({status: status, feedback: _.result(project,'attributes.feedback', undefined)});
      }
    }
  }
  handleProjectStore(trigger){
    let that = this;
    if(trigger.project){
      that.context.setProgressVisibility(false);
    }
    if(trigger.error){

    }
  }
  //onStatusChange(props){
  //  if(props == TriggerTypes.PROJECT_STARTED){
  //    this.context.setProgressVisibility(false);
  //    console.log('project started');
  //  }
  //}
  startProject(value){
    let r = confirm(`Start Project ?`), that = this;
    if (r == true) {
      ProjectsActions.startProject(that.props.data.id, value);
      that.context.setProgressVisibility(that);
      that.setState({status: value ? Constants.PROJECT_STATUS.WORKING : Constants.PROJECT_STATUS.ASSIGNED});
    }
  }
  sendDesignerOption(value){
    this.setState({showDesignerOption: value});
  }
  showUserAssign(value){
    this.setState({showUserAssign: value});
  }
  switchMenu(type){
    if(type == 'WORKSPACE'){
      this.props.switchMenu(-2);
    }
    if(type == 'NEW'){
      this.props.switchMenu(-1);
    }
  }
  getCurrentComponent(status){
    let that = this;
    let project = that.props.data;
    let hasPaid = _.result(project,'attributes.meta.haspaid',false);
    if(status == Constants.PROJECT_STATUS.ASSIGNED){
      return (
        <div className="Nextaction-wrapper" style={{fontSize:'0.85rem'}}>
          <p>Ready to <b className="Nextaction-bold"> start </b>working on the project ?</p>
          <button onClick={this.startProject.bind(this,true)} className="Nextaction-start-button">Start</button>
        </div>
      )
    }
    if(status == Constants.PROJECT_STATUS.UNASSIGNED){
      if(hasPaid){
        return (
          <div style={{width:'100%'}}>
            <div className="Nextaction-wrapper">
              Please either <b className="Nextaction-bold">Send Designer choices</b> to the client or <b className="Nextaction-bold">Assign</b> the project manually.
              <br/><br/>
              <button onClick={this.sendDesignerOption.bind(this, true)} className="Nextaction-button"> Send Designer Choices</button>
              <br/>OR <br/>
              <button onClick={this.showUserAssign.bind(this,true)} className="Nextaction-button"> Assign manually</button>
            </div>
            {this.state.showDesignerOption && <DesignerOptionModal onDismiss={this.sendDesignerOption.bind(this, false)} />}
            {this.state.showUserAssign && <AssignProject pid={that.props.data.id} onDismiss={this.showUserAssign.bind(this, false)} />}
          </div>
        )
      }else {
        return (
          <div className="Nextaction-wrapper">
            Payment Pending
          </div>
        )
      }
    }
    if(status == Constants.PROJECT_STATUS.WORKING){
      return (<div className="Nextaction-wrapper">

        <p>Use the <b className="Nextaction-bold"><span className='Nextaction-link' onClick={this.switchMenu.bind(this,'WORKSPACE')}>Workspace</span></b> to collaborate with Visualiser.</p>
        <br/>
        <p>Use the <b className="Nextaction-bold"><span className='Nextaction-link' onClick={this.switchMenu.bind(this,'NEW')}>New Design </span></b> button to send designs to the Client.</p>

        {/*<button onClick={this.startProject.bind(this,false)}>Un-Assign</button>*/}
      </div>)
    }
    if(status == Constants.PROJECT_STATUS.DESIGN_SENT){
      return (<div className="Nextaction-wrapper">
        Design Sent to Client
      </div>)
    }

    if(status == Constants.PROJECT_STATUS.CLOSED){
      let stars  = parseInt(this.state.feedback ? this.state.feedback.ratings : undefined);
      let stars_arr = [];
      let MAX_STAR = 5;
      let comment = this.state.feedback ? this.state.feedback.comment : '';
      for(let i=0;i< MAX_STAR;i++){
        stars_arr.push(
          <SvgIcon key={i}>
            {i < stars ? <path fill='#000000' d="M12,17.27L18.18,21L16.54,13.97L22,9.24L14.81,8.62L12,2L9.19,8.62L2,9.24L7.45,13.97L5.82,21L12,17.27Z"/>
              :
              <path fill = "#000000" d="M12,15.39L8.24,17.66L9.23,13.38L5.91,10.5L10.29,10.13L12,6.09L13.71,10.13L18.09,10.5L14.77,13.38L15.76,17.66M22,9.24L14.81,8.63L12,2L9.19,8.63L2,9.24L7.45,13.97L5.82,21L12,17.27L18.18,21L16.54,13.97L22,9.24Z" />
            }
          </SvgIcon>
        )
      }
      return (
        <div className="Nextaction-wrapper">
          <h4>Closed</h4>
          <br />
          {comment && comment.trim() != '' ?  <div>{comment}<br /></div> : ''}
          {stars ? stars_arr : 'Not Rated yet'}
        </div>
      )
    }
  }
  render() {
    return (
      <div className="Nextaction">
        <img src={this.state.user.imageUrl} className="Nextaction-image" />
        {this.getCurrentComponent(this.state.status)}
      </div>
    )
  }
}
export default NextAction;
