/**
 * Created by vikramaditya on 7/21/15.
 */
import React from 'react';
import mui from 'material-ui';
import DropZone from 'react-dropzone';
import _ from 'lodash';
import styles from './DesignResponseStory.scss';
import Helper from '../../../../../../../helper.js';
var {Services, TriggerTypes, AppConstants: Constants, withStyles} = Helper;
var {FlatButton,CardHeader,SvgIcon,Avatar} = mui;

@withStyles(styles)
class DesignResponseStory extends React.Component {
  constructor() {
    super();
    this.DUMMY_STORY = {title: '', subtitle: '', audio: '', text: ''};
    this.state = {
      inProgress: false,
      story:{
        title: '',
        subtitle: '',
        audio: '',
        text: ''
      }
    };
  }
  componentWillMount(){
    if(this.props.newDesignResponse){this.setStory(this.props);}
  }
  setStory(props){
    let x = {title: props.title, subtitle: props.subtitle, audio: props.audio, text: props.text};
    this.setState({story : x});
  }
  componentWillReceiveProps(nextProps) {
    if(nextProps.newDesignResponse && nextProps.drid != this.props.drid){this.setStory(nextProps);}
    if(nextProps.reset){
      let x = {audio: '', text: '', title: '', subtitle: ''};
      this.setState({story : x});
    }
  }
  textBoxChange(tag, e){
    var x = _.cloneDeep(this.state.story);
    x[tag] = e.target.value;
    this.setState({story : x});
  }
  uploadAudio(files){
    let self = this;
    if(files.length !=1){
      alert('Select only one audio file');
    }else{
      let pid = self.props.pid;
      self.audioUploader(files[0], pid);
      self.setState({inProgress: true});
    }
  }
  audioUploader(file, pid){
    let self = this;
    Services.uploadAPI(Constants.SERVER_URL + 'upload', file, `?type=DesignResponse&projectid=${pid}`)
      .then(response => {
        let x = _.cloneDeep(self.state.story);
        x.audio = response.body.url;
        self.setState({story:x, inProgress: false});
      });
  }
  getStory(){
    let x = this.DUMMY_STORY;
    x.title = this.state.story.title ? this.state.story.title : '';
    x.subtitle = this.state.story.subtitle ? this.state.story.subtitle : '';
    x.text = this.state.story.text ? this.state.story.text : '';
    x.audio = this.state.story.audio ? this.state.story.audio : '';
    return x;
  }
  render(){
    var self = this;
    let inProgress = self.state.inProgress;
    return (
      <div className="DesignResponseStory">
        <span className="DesignResponseStory-title"> Story </span>
        {self.props.newDesignResponse ?
          <div className="DesignResponseStory-inputWrapper">
            <span className="DesignResponseStory-label"> Story Title </span>
            <input className="DesignResponseStory-input" type = 'text' value={self.state.story.title} placeholder='Story Title' onChange={self.textBoxChange.bind(self,'title')}/>
            <span className="DesignResponseStory-label"> Story Subtitle </span>
            <input className="DesignResponseStory-input" type = 'text' value={self.state.story.subtitle} placeholder='Subtitle' onChange={self.textBoxChange.bind(self,'subtitle')}/>
            <span className="DesignResponseStory-label"> Story Text </span>
            <textarea rows='8' className="DesignResponseStory-input" value= {self.state.story.text} onChange={self.textBoxChange.bind(self,'text')} placeholder='Text' />
            <div>
              <span className="DesignResponseStory-label">{inProgress ? 'Uploading...': 'Upload audio '} </span>
              {inProgress &&
                <span><img style={{width:'50px',height:'50px', zoom: '0.4', paddingLeft:'20px'}} src={Constants.LOADER_GIF}/></span>
              }
            </div>
            <small style={{margin:'2px 15px'}}>{self.state.story.audio}</small>
            <DropZone className="DesignResponseStory-audio-wrapper" onDrop={self.uploadAudio.bind(self)}>
              <SvgIcon className="DesignResponseStory-audio-icon">
                <path d="M19 13h-6v6h-2v-6H5v-2h6V5h2v6h6v2z"/>
              </SvgIcon>
              <span className="DesignResponseStory-audio-label"> Upload</span>
            </DropZone>
            <div style={{margin:'0px 25px'}}>
              {self.state.story.audio != '' ? <audio ref = "sound" controls = 'controls' type = {"audio/wav"} src = {self.state.story.audio}></audio> : ''}
            </div>
          </div>
          :
          <div className="DesignResponseStory-designresponse flex-col">
            {self.props.title &&
            <div className="DesignResponseStory-designresponse-wrapper flex-col">
              <span className="DesignResponseStory-designresponse-label"> Story Title </span>
              <p className="DesignResponseStory-designresponse-value">{self.props.title}</p>
            </div> }
            {self.props.subtitle && <div className="DesignResponseStory-designresponse-wrapper flex-col">
              <span className="DesignResponseStory-designresponse-label"> Story Subtitle </span>
              <p className="DesignResponseStory-designresponse-value">{self.props.subtitle}</p>
            </div>
            }
            <div className="DesignResponseStory-designresponse-wrapper flex-col">
              <span className="DesignResponseStory-designresponse-label"> Story Text </span>
              <p className="DesignResponseStory-designresponse-value">{self.props.text}</p>
            </div>
            <span className="DesignResponseStory-designresponse-label"> Story Audio </span>
            <audio ref="sound" style={{alignSelf: 'flex-start', margin:'28px'}} controls='controls' type={"audio/wav"} src={self.props.audio}></audio>
          </div>
        }
      </div>
    );
  }
}
export default DesignResponseStory;
