/**
 * Created by vikramaditya on 7/21/15.
 */
import React from 'react';
import mui from 'material-ui';
import _ from 'lodash';
import Helper from '../../../../../../../helper.js';
import Components from '../../../../../../../component.js';
import PasswordModal from '../../../PasswordModal';
import CustomModal from '../../../../../../../../lib/Modal';
import styles from './DesignResponseName.scss';

var {DRPActions, AppConstants: Constants, TriggerTypes, withStyles} = Helper;
let {SvgIcon,Toggle, RadioButtonGroup, RadioButton} = mui;
let ADMIN_PASSWORD = Constants.ADMIN_PASS;

@withStyles(styles) class DesignResponseName extends React.Component {
  static contextTypes = {
    setProgressVisibility: React.PropTypes.func
  };

  constructor() {
    super();
    this.deleteDraft = false;
    this.state = {
      showGlobalModal: false,
      name: '',
      toggleLabel: 'DesignResponse'
    };
  }

  componentDidMount() {
    this.setName(this.props);
  }

  setName(props) {
    let name = props.name;
    this.setState({name: name, toggleLabel: 'DesignResponse'});
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.data !== this.props.data) {
      this.setName(nextProps);
    }
    if (nextProps.reset) {
      this.setState({name: ''});
    }
  }

  textBoxChange(e) {
    this.setState({name: e.target.value});
  }

  deleteDesignResponse() {
    this.deleteDraft = false;
    this.refs.passmodal.refs.withStyles.show();
    //console.log('deleting',this.props.data.id);
  }

  deleteDesignResponseDraft() {
    this.deleteDraft = true;
    this.refs.passmodal.refs.withStyles.show();
  }

  showFeedback() {
    let showFeedBack = (this.state.toggleLabel === 'DesignResponse');
    this.props.onShowFeedback(showFeedBack);
    this.setState({toggleLabel: showFeedBack ? 'Feedback' : 'DesignResponse'});
  }
  makeGlobal(){
    let that = this;
    let drid = that.props.data.id;
    let location = that.refs.location.getSelectedValue();
    DRPActions.makeFeedGlobal(drid, location);
    that.setState({showGlobalModal: false});
  }
  modalAction(type) {
    let self = this;
    let pid = self.props.pid;
    if (self.validateUser(type)) {
      self.context.setProgressVisibility(true);
      if (self.deleteDraft) {
        DRPActions.deleteDraftDesignResponse(pid, self.props.data.id);
        self.props.onSwitch(0);
      } else {
        DRPActions.deleteDesignResponse(pid, self.props.data.id);
        self.props.onSwitch(0);
      }
    }
  }

  validateUser(type) {
    if (type == 'SUBMIT') {
      if (this.refs.passmodal.refs.withStyles.getValue() == ADMIN_PASSWORD) {
        this.refs.passmodal.refs.withStyles.dismiss();
        //ProjectActions.LOCK_PROJECT(this.props.pid,this.toggleValue);
        return true;
      } else {
        alert('Wrong Password');
        //this.refs.locktoggle.setToggled(this.toggleValue);
        this.refs.passmodal.refs.withStyles.dismiss();
        return false;
      }
    } else {
      //this.refs.locktoggle.setToggled(this.toggleValue);
      this.refs.passmodal.refs.withStyles.dismiss();
      return false;
    }
  }

  getValue() {
    return this.state.name;
  }
  getGlobalModal(){
    let that = this;
    return (
      <div className="GlobalFeed">
        <CustomModal isOpen={true} onClose={() => that.setState({showGlobalModal: false})} style={{top:'12%', borderRadius: '3px', height:'70%',width:'50%', left:'25%',padding:'0'}}>
          <div>
            <SvgIcon className='cross-icon' style={{fill:'#808080', zoom:'0.75', position: 'absolute', top: '20px', right: '20px',cursor:'pointer'}}
                     onClick={() => that.setState({showGlobalModal: false})}>
              <path d={Constants.ICONS.close_icon}/>
            </SvgIcon>
          </div>
          <div className="GlobalFeed-content">
            <h6 className="GlobalFeed-content-title">Make Feed Global/Local</h6>
            <div className="GlobalFeed-content-wrapper">
              <div>
                <RadioButtonGroup ref='location' name="location" defaultSelected="local">
                  <RadioButton
                    value="local"
                    label="India"
                    style={{marginBottom:16}} />
                  <RadioButton
                    value="global"
                    label="US"
                    style={{marginBottom:16}}/>
                </RadioButtonGroup>
              </div>
            </div>
          </div>
          <div className="GlobalFeed-footer">
            <div className="input-divider"></div>
            <div className="GlobalFeed-footer-buttonWrapper">
              <button onClick={() => that.setState({showGlobalModal: false})} className="custom-secondary-button">Cancel</button>
              <button onClick={that.makeGlobal.bind(that)} className="custom-primary-button">DONE</button>
            </div>
          </div>
        </CustomModal>
      </div>
    )
  }
  render() {
    let self = this;
    let button_hint_text = self.state.toggleLabel === 'DesignResponse' ? 'View FeedBack' : 'View Design Response';
    let globalModal = self.state.showGlobalModal && self.getGlobalModal();
    return (
      <div className="DesignResponseName">
        <PasswordModal ref='passmodal' onAction={self.modalAction.bind(self)}/>

        <div className='flex-row' style={{alignItems: 'center',justifyContent: 'space-between', paddingRight: '15px'}}>
          { self.props.newDesignResponse &&
          <span className="DesignResponseName-inputLabel"> Design Response Name </span>}
          {self.props.isDraft &&
          <button className="delete-button" onClick={self.deleteDesignResponseDraft.bind(self)}>
            <SvgIcon className='delete-icon'>
              <path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z"/>
            </SvgIcon>
          </button>}
        </div>
        {self.props.newDesignResponse ?
          <input onChange={self.textBoxChange.bind(self)} value={self.state.name} className="DesignResponseName-input"
                 type='text' placeholder='Design Response Name'/>
          :
          <div style={{display:'flex',flexFlow:'row',justifyContent:'space-between',borderBottom: '1px solid #e6e6e6'}}>

            <h4 className="DesignResponseName-name">{self.props.name}</h4>

            <div style={{display:'flex',flexFlow:'row',alignSelf:'center'}}>
              <button style={{margin:'0', marginRight:'5px'}} className='custom-secondary-button' onClick={() => {self.setState({showGlobalModal: true})}}>Make Feed</button>
              {_.result(self.props.data, 'attributes.userfeedback.length', 0) != 0 &&
              <button onClick={self.showFeedback.bind(self)} className="toggle-button">{button_hint_text}</button>}
              <button onClick={self.deleteDesignResponse.bind(self)} className="delete-button">
                <SvgIcon className="delete-icon" style={{fill:'#666'}}>
                  <path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z"/>
                </SvgIcon>
              </button>
            </div>
          </div>}
        {globalModal}
      </div>
    )
  }
}
export default DesignResponseName;
