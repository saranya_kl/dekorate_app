/**
 * Created by vikramaditya on 9/15/15.
 */
import React from 'react';
import _ from 'lodash';
import Helper from '../../../../../../../helper.js';
var {ProductConstants, withStyles} = Helper;
// import ProductItemView from '../../../../ProductScreen/ProductRepository/ProductItemView.js';
import ProductViewer from './ProductViewer.js';
import styles from './ProductDetails.scss';
@withStyles(styles)
class ProductDetails extends React.Component {
  constructor() {
    super();
    this.state = {
      data: _.cloneDeep(ProductConstants.DUMMY_PRODUCT)
    }
  }

  componentWillMount() {
    this.setProductDetails(this.props);
  }

  componentWillUnmount() {

  }
  setProductDetails(props){
    if(props.data && Object.keys(props.data).length > 0){
      this.setState({data: props.data});
    }
  }
  componentWillReceiveProps(nextProps) {
    if(this.props.data !== nextProps.data){
      this.setProductDetails(nextProps);
    }
    if(nextProps.reset){
      this.setState({data: _.cloneDeep(ProductConstants.DUMMY_PRODUCT)});
    }
  }
  handleQuantity(obj){
    this.props.handleQuantity(obj);
  }
  handlePrice(obj){
    this.props.handlePrice(obj);
  }
  render() {
    return (
      <div className='ProductDetails'>
        <ProductViewer data={this.state.data} qty={this.props.qty} handleQuantity={this.handleQuantity.bind(this)} price={this.props.price} handlePrice={this.handlePrice.bind(this)} addProduct={this.props.addProduct.bind(this)}/>
      </div>
    )
  }
}
export default ProductDetails;
