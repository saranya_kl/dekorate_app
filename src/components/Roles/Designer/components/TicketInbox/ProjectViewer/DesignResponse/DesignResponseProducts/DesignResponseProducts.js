/**
 * Created by vikramaditya on 9/15/15.
 */
import React from 'react';
import _ from 'lodash';
import {List, ListItem, ListDivider, Avatar, SvgIcon} from 'material-ui';
import Helper from '../../../../../../../helper.js';
import styles from './DesignResponseProducts.scss';
import CustomModal from '../../../../../../../../lib/Modal';
import SearchPanel from './SearchPanel.js';
import ProductDetails from './ProductDetails.js';
var {withStyles, WorkspaceActions, WorkspaceStore, ProductsStore, AppConstants: Constants} = Helper;

@withStyles(styles) class DesignResponseProducts extends React.Component {
  constructor() {
    super();
    this.DUMMY_ITEM = {
      id: '',
      attributes: {
        qty: '',
        price: '',
        currency: 'INR', // 'USD'
        unitPrice: '',
        reseller: '',
        name: '',
        displayimg: '',
        url: ''
      },
      type: 'product'
    };

    this.state = {
      data: {}, // prop data
      showAddProduct: false,
      showCartModal: false,
      inProgress: false,
      currentPreviewProduct: {},
      currentDRPProduct: _.cloneDeep(this.DUMMY_ITEM),
      showCart: false,
      cartProducts: [],
      resetPreview: false,
      editable_product_index: -1,
      editable_product: {},
      showEditModal: false,
      workspaceData: {},
      searchList: [], // No use just for rendering last state inside search
      cartindex: null
    }
  }

  resetPreview() {
    this.setState({currentDRPProduct: _.cloneDeep(this.DUMMY_ITEM), currentPreviewProduct: {}});
  }

  componentWillUnmount() {
    this.unsubscribe();
    this.unsubscribe_product_store();
  }

  componentWillMount() {
    this.unsubscribe = WorkspaceStore.listen(this.handleStoreChange.bind(this));
    this.unsubscribe_product_store = ProductsStore.listen(this.handleProductStoreChange.bind(this));
    this.setState({data: this.props.data, workspaceData: WorkspaceStore.getWorkSpace(this.props.pid)});
  }

  handleStoreChange() {
    let that = this;
    let data = WorkspaceStore.getWorkSpace(that.props.pid);
    that.setState({workspaceData: data, inProgress: false});
  }

  handleProductStoreChange(trigger) {
    let that = this;
    if(trigger.products){
      that.updateStoreProducts(trigger.products);
    }
  }

  updateStoreProducts(productIdObject) {
    let that = this;
    let x = that.state.data;
    let currency = x.currency || 'INR';
    let products = _.cloneDeep(x.list);
    products = products.filter((e) => {
      return typeof (e) === 'object'
    });

    let all_product_ids = that.getAllProductIds();
    all_product_ids.map((productid, index) => {

      if (productIdObject[productid]) {
        let qty = '1';
        products.map((product, ind) => {
          if (product.id == productid) {
            //Product to update
            qty = product.attributes.qty;
            products.splice(ind, 1);
          }
        });
        let cart_product = ProductsStore.getProduct(productid);
        let parsed_product = that.parseData(cart_product);
        parsed_product.attributes.qty = qty;
        parsed_product.attributes.price = `${(parseInt(parsed_product.attributes.unitPrice[currency]) * parseInt(parsed_product.attributes.qty))}`;
        parsed_product.attributes.unitPrice = parsed_product.attributes.unitPrice[currency];

        products.push(parsed_product);
      }
    });
    x.list = products;
    x.totalcost = that.getTotalCost(products);//`${parseInt(x.totalcost) + parseInt(totalcost)}`;
    that.props.onSubmit(_.cloneDeep(x));
    that.setState({data: x});
  }

  getTotalCost(products) {
    let x = _.cloneDeep(products);
    let totalcost = 0;
    x.map((product) => {

      let uprice = parseInt(product.attributes.unitPrice);
      let qty = parseInt(product.attributes.qty);
      totalcost += (uprice * qty);
    });
    return `${totalcost}`;
  }

  getAllProductIds() {
    let that = this;
    let arr = [];
    let productArray = that.state.data.list;
    productArray.map((product) => {
      if (typeof (product) === 'object') {
        arr.push(product.id)
      }
      else {
        arr.push(product)
      }
    });
    return arr;
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.data !== this.props.data) {
      this.setState({data: nextProps.data});
    }

    if (nextProps.reset) {
      this.setState({data: {list: [], totalcost: '0'}});
    }
  }

  saveSearchResult(data) {
    this.setState({searchList: data});
  }

  removeItem(index, item) {
    let x = this.state.data;
    x.list.splice(index, 1);
    let total_cost = parseInt(x.totalcost) - (parseInt(item.attributes.qty) * parseInt(item.attributes.unitPrice));
    x.totalcost = `${total_cost}`;
    this.props.onSubmit(_.cloneDeep(x));
    this.setState({data: x});
  }

  showAddProductModal() {
    this.setState({showAddProduct: true});
  }

  showCartModal(value) {
    this.setState({showCartModal: value});
  }

  modalDismiss() {
    this.props.onSubmit(_.cloneDeep(this.state.data));
    this.setState({showAddProduct: false});
  }

  onShowProduct(product) {
    let x = this.parseData(product);
    this.setState({currentPreviewProduct: product, currentDRPProduct: x, resetPreview: false});
  }

  parseData(product) {
    let drp_product = _.cloneDeep(this.DUMMY_ITEM);
    drp_product.id = product.productid;
    drp_product.attributes.name = product.name;
    drp_product.attributes.unitPrice = product.amount;
    drp_product.attributes.qty = '1'; // default
    drp_product.attributes.price = product.amount;
    drp_product.attributes.reseller = product.brand;
    drp_product.attributes.displayimg = _.result(product,'displayImage.url','');
    drp_product.attributes.url = `${Constants.SERVER_URL}product/redirect/${product.productid}?pid=${this.props.pid}`;//product.url;
    return drp_product;
  }

  addProduct1() {
    let drp_product = this.state.currentDRPProduct;//this.parseData(this.state.currentPreviewProduct);
    if (Object.keys(this.state.currentPreviewProduct).length != 0) {
      if (!isNaN(parseInt(drp_product.attributes.price)) && typeof(drp_product.attributes.price) == 'string' || ((drp_product.attributes.price).length == 1 && !isNaN(parseInt(drp_product.attributes.price[0])))) {
        let unit_price = parseInt(drp_product.attributes.price);
        let net_price = parseInt(drp_product.attributes.qty) * parseInt(drp_product.attributes.price.toString());
        drp_product.attributes.price = `${net_price}`;
        drp_product.attributes.unitPrice = `${unit_price}`;

        let x = this.state.data;
        let price = parseInt(x.totalcost) + parseInt(net_price);
        x.list.push(drp_product);
        x.totalcost = `${price}`; // converting to string;
        this.setState({
          data: x,
          currentDRPProduct: _.cloneDeep(this.DUMMY_ITEM),
          currentPreviewProduct: {},
          resetPreview: true
        }); // reset preview
      } else {
        alert('Please choose single price');
      }
    } else {
      alert('Choose a product to add');
    }
  }
  addProduct(currency){
    let that = this;
    let drp_product = that.state.currentDRPProduct;
    function isValid(price){
      return !isNaN(parseInt(price));
    }
    if(isValid(drp_product.attributes.price[currency])){
      let unit_price = parseInt(drp_product.attributes.unitPrice[currency]);
      let net_price = parseInt(drp_product.attributes.qty) * parseInt(drp_product.attributes.price[currency]);
      drp_product.attributes.unitPrice = `${unit_price}`;
      drp_product.attributes.price = `${net_price}`;
      drp_product.attributes.currency = currency;

      let x = that.state.data;
      let price = parseInt(x.totalcost) + parseInt(net_price);
      x.list.push(drp_product);
      x.totalcost = `${price}`;
      that.setState({data: x, currentDRPProduct: _.cloneDeep(that.DUMMY_ITEM), currentPreviewProduct: {}, resetPreview: true});
    }else{
      alert('Please enter a valid price for the product');
    }
  }
  onSubmit() {
    //this.props.onSubmit(_.cloneDeep(this.state.data));
    this.modalDismiss();
  }

  handleQuantity(obj) {
    let x = this.state.currentDRPProduct;
    x.attributes.qty = obj;
    this.setState({currentDRPProduct: x});
  }

  handlePrice(obj) {
    let x = this.state.currentDRPProduct;
    x.attributes.price = obj;
    this.setState({currentDRPProduct: x});
  }

  editProduct(value, index) {
    this.setState({
      showEditModal: value,
      editable_product_index: index,
      editable_product: _.cloneDeep(this.state.data.list[index])
    });
  }

  saveEdit() {
    let index = this.state.editable_product_index;
    let product = this.state.editable_product;
    if (isNaN(product.attributes.price)) {
      alert('Enter a valid price and quantity');
    } else {
      let x = this.state.data;
      let total_cost = parseInt(x.totalcost);
      let prev_price = parseInt(x.list[index].attributes.price);
      x.list[index] = product;
      x.totalcost = `${(total_cost - prev_price) + parseInt(product.attributes.price)}`;
      this.props.onSubmit(_.cloneDeep(x));
      this.setState({data: x, showEditModal: false});
    }
  }

  editAttribute(tag, e) {
    let x = this.state.editable_product;
    if (tag == 'PRICE') {
      let uPrice = e.target.value;
      x.attributes.unitPrice = uPrice;
      x.attributes.price = `${parseInt(uPrice) * parseInt(x.attributes.qty)}`;
    }
    if (tag == 'QUANTITY') {
      let q = e.target.value;
      x.attributes.qty = q;
      x.attributes.price = `${parseInt(q) * parseInt(x.attributes.unitPrice)}`;
    }
    this.setState({editable_product: x});
  }

  selectCartProducts(index) {
    this.setState({cartindex: index});
  }

  addCartProducts(index) {
    let that = this;
    //let index = this.state.cartindex;
    let arr = [];
    let totalcost = 0;
    let data = that.state.workspaceData['assets']['productcarts'][index];
    if (data) {
      data.list.map(product => {
        let cart_product = typeof(ProductsStore.getProduct(product.productid)) === 'object' ? ProductsStore.getProduct(product.productid) : product.productid;
        let parsed_product = typeof(cart_product) === 'object' ? this.parseData(cart_product) : cart_product;
        parsed_product.attributes.qty = product.qty || '1';
        let unitPrice = parsed_product.attributes ? isNaN(parseInt(parsed_product.attributes.price.INR)) ? parseInt(parsed_product.attributes.price.USD) : parseInt(parsed_product.attributes.price.INR):0;
        let currency = parsed_product.attributes ? isNaN(parseInt(parsed_product.attributes.price.INR)) ? 'USD' : 'INR' : 'INR';
        //let price = parseInt(parsed_product.attributes ? (parseInt(parsed_product.attributes.price.INR) * parseInt(parsed_product.attributes.qty)) : 0);
        parsed_product.attributes.unitPrice = unitPrice;
        parsed_product.attributes.price = unitPrice * parseInt(parsed_product.attributes.qty);
        parsed_product.attributes.currency = currency;
        totalcost = totalcost + unitPrice;
        arr.push(parsed_product);
      });
    }

    let x = this.state.data;
    //let price = parseInt(x.totalcost) + parseInt(totalcost);
    if (x.list.length != 0) {
      let tmp = x.list;
      x.list = tmp.concat(arr);
    } else {
      x.list = arr;
    }
    x.totalcost = that.getTotalCost(x.list);//`${price}`; // converting to string;
    that.props.onSubmit(_.cloneDeep(x));
    that.setState({data: x, showCartModal: false, cartindex: null});
  }
  getProductCart() {
    let data = this.state.data.list;
    let total_price = this.state.data.totalcost;
    let products_list = this.getList(data);

    return (
      <div className='DesignResponseProducts-products'>
        {products_list}
        {total_price != 0 && <p className='DesignResponseProducts-totalcost'>Total Price : &nbsp;{total_price}</p>}
      </div>
    )
  }
  getAddProductDialog() {
    let data = this.state.data.list;
    return (
      <CustomModal style={{left:'6%', width:'88%', top:'4%', height:'88%', padding:'0'}}
                   onClose={this.modalDismiss.bind(this)}
                   isOpen={true}>
        <div className='add-product'>
          <div className='finish-button'>
            <h6 className="add-product-title"> Add Product(s)</h6>

            <div>{
              this.state.showCart ?
                <button onClick={() => this.setState({showCart: false})} className='cart-button'>
                  <SvgIcon className="cart-button-icon">
                    <path d="M15.41 16.09l-4.58-4.59 4.58-4.59L14 5.5l-6 6 6 6z"/>
                  </SvgIcon>
                  <span className="cart-button-label"> Back </span>

                </button> :
                <button onClick={() => this.setState({showCart: true})} className='cart-button'>
                  <SvgIcon className="cart-button-icon">
                    <path
                      d="M7 18c-1.1 0-1.99.9-1.99 2S5.9 22 7 22s2-.9 2-2-.9-2-2-2zM1 2v2h2l3.6 7.59-1.35 2.45c-.16.28-.25.61-.25.96 0 1.1.9 2 2 2h12v-2H7.42c-.14 0-.25-.11-.25-.25l.03-.12.9-1.63h7.45c.75 0 1.41-.41 1.75-1.03l3.58-6.49c.08-.14.12-.31.12-.48 0-.55-.45-1-1-1H5.21l-.94-2H1zm16 16c-1.1 0-1.99.9-1.99 2s.89 2 1.99 2 2-.9 2-2-.9-2-2-2z"/>
                  </SvgIcon><span className="cart-button-label"> Cart </span>{data.length > 0 &&
                <div className="cart-button-length">{data.length}</div>}
                </button>
            }</div>
          </div>
          <div className="input-divider"></div>
          <div className='flex-col'>
            {this.state.showCart ? this.getProductCart() :
              <div className='container'>
                <SearchPanel setSearchResult={this.saveSearchResult.bind(this)} data={this.state.searchList}
                             showProduct={this.onShowProduct.bind(this)}/>
                <ProductDetails reset={this.state.resetPreview} data={this.state.currentPreviewProduct}
                                qty={this.state.currentDRPProduct.attributes.qty}
                                handleQuantity={this.handleQuantity.bind(this)}
                                price={this.state.currentDRPProduct.attributes.price}
                                handlePrice={this.handlePrice.bind(this)} addProduct={this.addProduct.bind(this)}/>
              </div>
            }
          </div>
          {this.state.showCart ? '' : <div className="input-divider"></div>}
          {this.state.showCart ? '' : <div className="finalbutton-wrapper">
            <button className="done" onClick={this.onSubmit.bind(this)}>Done</button>
          </div>}
        </div>
      </CustomModal>
    )
  }
  getList(data) {
    return (<div>
        {data.map((product, index) => {
          //return (
          let symbol = '';
          return (
            <div key={index} style={{position:'relative'}}>
              { typeof (product) === 'object' ? <div className='product-list'>
                <span style={{fontWeight: 'bold'}}>{index + 1}.</span>
                <div className="product-list-item">
                  <img src={product.attributes.displayimg}
                       className="product-list-item-img"/></div>
                <span className="product-list-name">{product.attributes.name}</span>
                <span className="product-list-item">{product.attributes.reseller}</span>
                <span className="product-list-item">
                  {(product.attributes.currency == 'USD')?'$'+product.attributes.unitPrice : '₹'+product.attributes.unitPrice}
                  <span
                  className="product-list-item-x">x</span><b>{product.attributes.qty}</b></span>
                <span className="product-list-item">
                  {(product.attributes.currency == 'USD')?'$'+product.attributes.price : '₹'+product.attributes.price}
                </span>
                {!this.state.showAddProduct &&
                <span title='Edit' className="product-list-item" onClick={this.editProduct.bind(this, true, index)}>
                  <SvgIcon hoverColor={'green'} style={{fill:'black', zoom:'0.75',cursor:'pointer'}}>
                    <path d={Constants.ICONS.pencil_icon}/>
                  </SvgIcon>
                </span>}
                <span title='Remove' className="product-list-item" onClick={this.removeItem.bind(this,index, product)}>
                  <SvgIcon className='cross-icon' hoverColor={'red'}
                           style={{fill:'black', zoom:'0.75',cursor:'pointer'}}>
                    <path d={Constants.ICONS.close_icon} />
                  </SvgIcon>
                </span>
              </div> :
                <div
                  style={{alignItems:'center',justifyContent:'center',left:'0',top:'0',display:'flex',position:'absolute',background:'rgba(0,0,0,0.42)',width:'100%',height:'100%'}}>
                  <img alt='loading...' style={{width:'50px',height:'50px'}}
                       src='http://press.solarimpulse.com/img/ajax_loader.gif'/>
                </div>
              }
            </div>
          )
        })
        }
      </div>
    );
  }
  getProductCartModal() {
    let that = this;
    var data = [];
    if (typeof(that.state.workspaceData) === 'object') {
      data = that.state.workspaceData['assets'];
    } else {
      WorkspaceActions.fetchWorkSpace(that.props.pid);
      data = [];
    }
    return (

      <CustomModal isOpen={true} onClose={this.showCartModal.bind(this,false)}
                   style={{top:'12%', height:'70%',left:'25%', width:'50%', borderRadius: '3px', padding:'0', overflow: 'hidden'}}>
        <div className='ProductCart'>
          <h6 className='ProductCart-header'>Add product(s) from one of the carts</h6>

          <div className='ProductCart-content'>
            {
              data.productcarts ? data.productcarts.map((obj, index) => {
                let cart_name = obj.name;
                let cancelled = obj.cancelledlist ? obj.cancelledlist.length : 0;
                let cart_length = obj.list ? obj.list.length : 0;
                return (
                  <div key={index} className="cartBox" onMouseEnter={that.selectCartProducts.bind(that, index)} onClick={that.addCartProducts.bind(that, index)}
                       style={{display:'flex', border: this.state.cartindex==index ? '2px solid #f35d54' : 'none'}}>
                    <SvgIcon className='cartIcon' viewBox="0 0 520 520">
                      <path
                        d='M492,0h-80c-7.719,0-14.344,5.531-15.75,13.125L391,64H16c-5.344,0-10.344,2.688-13.313,7.125s-3.516,10.094-1.453,15.031   l80,192C83.719,284.125,89.547,288,96,288h254.281l-11.625,64H128c-8.844,0-16,7.156-16,16s7.156,16,16,16h224   c7.719,0,14.344-5.531,15.75-13.125L429.344,32H496c8.844,0,16-7.156,16-16S504.844,0,496,0z M385.188,96l-11.625,64H288V96   H385.188z M256,96v64h-64V96H256z M160,96v64H66.672L40,96H160z M106.672,256L80,192h80v64H106.672z M192,256v-64h64v64H192z    M288,256v-64h79.75l-11.656,64H288zM176,416c-26.5,0-48,21.469-48,48s21.5,48,48,48s48-21.469,48-48S202.5,416,176,416zM304,416c-26.531,0-48,21.469-48,48s21.469,48,48,48s48-21.469,48-48S330.531,416,304,416z'/>
                    </SvgIcon>

                    <div className="cartName">{cart_name} <br />
                      {cart_length} <span>{cart_length == '1' ? 'item' : 'items'}</span>
                      &nbsp;|&nbsp;{cancelled}&nbsp;<span>cancelled</span>
                    </div>
                  </div>
                )
              })
                :
                <div
                  style={{alignItems:'center',justifyContent:'center',left:'0',top:'0',display:'flex',position:'absolute',background:'rgba(0,0,0,0.42)',width:'100%',height:'100%'}}>
                  <img style={{width:'50px',height:'50px'}} src={Constants.LOADER_GIF}/>
                </div>
            }
            {data.productcarts && data.productcarts.length == 0 &&
            <p style={{fontSize:'0.8rem'}}>No Carts found</p>
            }
          </div>
          {/*<div className="input-divider"></div>
          <div className="ProductCart-buttonWrapper">
            <button onClick={that.addCartProducts.bind(that)} className="send">Select this cart</button>
          </div>*/}
        </div>
      </CustomModal>
    )
  }

  getEditableProductComponent() {
    let product = this.state.editable_product;
    return (
      <CustomModal isOpen={true} onClose={this.editProduct.bind(this,false)}
                   style={{left: '25%', width:'50%', top:'20%', height:'60%', padding:'0'}}>
        <div className='DesignResponseProducts-edit-products'>
          <h6 className="DesignResponseProducts-edit-products-title"> Edit quantity of the product </h6>

          <div className='product-list'>
            <div>
              <img src={product.attributes.displayimg} className="product-list-item-img"/>
            </div>
            <div>
              <span className="product-list-name">{product.attributes.name}</span><br/>
              <span className="product-list-item">{product.attributes.reseller}</span>
            </div>
            <div>
              <input className="price" type='number' min='0' onChange={this.editAttribute.bind(this, 'PRICE')}
                     defaultValue={product.attributes.unitPrice}/>
              &nbsp;X&nbsp;<input className="quan" type='number' min='1'
                                  onChange={this.editAttribute.bind(this,'QUANTITY')}
                                  defaultValue={product.attributes.qty}/>
              <br/><br/><span className="totalPrice">= {product.attributes.price}</span>
            </div>
          </div>
          <div className="input-divider"></div>
          <div className="DesignResponseProducts-edit-products-buttonWrapper">
            <button className='done' onClick={this.saveEdit.bind(this)}>DONE</button>
          </div>
        </div>
      </CustomModal>
    )
  }

  render() {
    let data = this.state.data.list;
    let total_price = this.state.data.totalcost;
    let products_list = this.getList(data);
    let add_product_dialog = this.state.showAddProduct && this.getAddProductDialog();
    let edit_product_dialog = this.state.showEditModal && this.getEditableProductComponent();
    let product_cart = this.state.showCartModal && this.getProductCartModal();
    return (
      <div className='DesignResponseProducts'>
        <h6 className='DesignResponseProducts-title'> Products List </h6>

        <div className='DesignResponseProducts-products'>
          {products_list}
          {total_price != 0 && <p className='DesignResponseProducts-totalcost'>Total Price : &nbsp;{total_price}</p>}
        </div>
        <div className="flex-row">
          <div onClick={this.showAddProductModal.bind(this)} className="DesignResponseProducts-addProduct">
            <SvgIcon style={{fill:'#999'}}>
              <path d="M19 13h-6v6h-2v-6H5v-2h6V5h2v6h6v2z"/>
            </SvgIcon>
            <span style={{fontSize: '0.75rem'}}>Add Product(s)</span>
          </div>
          <div onClick={this.showCartModal.bind(this, true)} className="DesignResponseProducts-addProduct">
            <SvgIcon style={{fill:'#999'}}>
              <path d="M19 13h-6v6h-2v-6H5v-2h6V5h2v6h6v2z"/>
            </SvgIcon>
            <span style={{fontSize: '0.75rem'}}>Add Product(s) from cart</span>
          </div>
        </div>
        {add_product_dialog}
        {edit_product_dialog}
        {product_cart}
      </div>
    )
  }
}
export default DesignResponseProducts;
