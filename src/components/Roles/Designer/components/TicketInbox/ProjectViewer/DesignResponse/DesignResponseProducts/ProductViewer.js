/**
 * Created by vikramaditya on 10/13/15.
 */
import React from 'react';
import Helper from '../../../../../../../helper.js';
import styles from './ProductViewer.scss';

import ProductImageViewer from '../../../../ProductScreen/ProductRepository/ProductImageViewer.js'
var {withStyles} = Helper;

@withStyles(styles)
class ProductViewer extends React.Component {
  constructor() {
    super();
  }
  state = {
    editMode: true,
    currency: 'INR'
  };
  componentWillMount(){
    this.setState({
      data: this.props.data
    });
  }
  componentWillReceiveProps(newprops){
    this.setState({
      data: newprops.data
    });
  }
  getTopMenuBar(items){
    return (
      <div className='ProductItemView-menuBar'>
        {items.map((item, index) => {
          return (
            <button className='menuButton' key={index}>{item}</button>
          )
        })}
      </div>
    )
  }
  getCategoryBar(data){
    return (
      <div className="categoricalData">
        {data.type||'TYPE'} > {data.space ? `${data.space} >` : ''}  {data.category ? `${data.category} >` : ''} {data.itemtype || 'ITEM'}
      </div>
    )
  }
  handleQuantity(e){
    this.props.handleQuantity(e.target.value);
  }
  handlePrice(e){
    this.props.handlePrice(e.target.value);
  }
  changePriceUnit(type){
    this.setState({currency: type});
  }
  onAddProduct(){
    let price = this.props.price[this.state.currency == 'INR' ? 'INR' : 'USD'];
    if(price){
      this.props.addProduct(this.state.currency);
    }else{
      alert('Enter price for the product in '+ this.state.currency);
    }
  }
  render() {
    let data = this.state.data;
    let editMode = this.state.editMode;
    let price = this.props.price[this.state.currency == 'INR' ? 'INR' : 'USD'];
    //console.log(editMode);
    return (
      <div className="ProductItemView">
        <div className="ProductItemView-basicProductInfo">
          <div className="imageViewerPanel">
            <ProductImageViewer data={data.assets.image}/>
          </div>
          <div className="infoPanel">
            <div className="productName">{data.name || 'Product Name'}</div>
            <div className="brandName">{data.brand}</div>
            {this.getCategoryBar(data)}
            <div className="priceContainer">
              <span>{data.amount.INR && `₹${data.amount.INR}`}</span>
              {data.amount.USD && <span className="separator">|</span>}
              <span>{data.amount.USD && `$${data.amount.USD}`}</span>
            </div>
            <div className="input-wrapper">
              <input className="qty" type='number' min='1' value={this.props.qty || 1}
                     onChange={this.handleQuantity.bind(this)}/>
              <div>
                <input checked={this.state.currency == 'INR'} type="radio" name='price' onChange={this.changePriceUnit.bind(this, 'INR')} className="radio-INR"/><span className="price-label">&nbsp;INR</span><br />
                <input checked={this.state.currency == 'USD'} type="radio" name='price' onChange={this.changePriceUnit.bind(this, 'USD')} className="radio-USD"/><span className="price-label">&nbsp;USD</span>
              </div>
              <input className="price" type='text' placeholder='Price' value={price}
                     onChange={this.handlePrice.bind(this)}/>
              <button onClick={this.onAddProduct.bind(this)} className='add-button'>ADD</button>
            </div>
            <div className="detailsTable product">
              <div className="header">PRODUCT DETAILS</div>
              <div className="tr"><div className="td-left">Material</div> <div className="td-right">{data.material}</div> </div>
              <div className="tr"><div className="td-left">Color</div> <div className="td-right">{data.colour}</div> </div>
              <div className="tr"><div className="td-left">Size</div> <div className="td-right">{data.size.w}W x {data.size.h}H x {data.size.d}D</div> </div>
              <div className="tr"><div className="td-left">Origin SKU</div> <div className="td-right">{data.originproductid}</div> </div>
            </div>

            <div className="detailsTable">
              <div className="header">TECH DETAILS</div>
              <div className="tr"><div className="td-left">Product ID</div> <div className="td-right">{data.productid}</div> </div>
              <div className="tr"><div className="td-left">Product Code</div> <div className="td-right">{data.productcode}</div> </div>
            </div>

          </div>
        </div>
      </div>
    )

  }

}


export default ProductViewer;
