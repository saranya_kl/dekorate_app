/**
 * Created by vikramaditya on 7/21/15.
 */
import React from 'react';
import mui from 'material-ui';
import _ from 'lodash';

import Helper from '../../../../../../helper.js';
import Components from '../../../../../../component.js';
import ImageComponent from './ImageComponent';
import DesignResponseName from './DesignResponseName';
import Comments from '../../../RightComponent/Comments';
import DesignResponseStory from './DesignResponseStory';
import CanvasPlayer from '../CanvasPlayer';
import ProductsList from './ProductsList';
import ThumbNails from '../ThumbNails';
import DesignResponseProducts from './DesignResponseProducts';
import styles from './DesignResponse.scss';
import CanvasPlayerModal from '../../../CanvasPlayerModal';
var {ProjectsStore, DRPActions, DRPStore, AppConstants: Constants, withStyles} = Helper;
var {FlatButton,ListDivider, SvgIcon} = mui;

@withStyles(styles)
class DesignResponse extends React.Component {
  static contextTypes = {
    setProgressVisibility: React.PropTypes.func
  };

  constructor(props) {
    super(props);
    let that = this;
    that.showDRP = false;
    that.DUMMY_PRODUCT = {'list': [], 'totalcost': '0', currency: 'INR'};
    that.new_drp_object = _.cloneDeep(Constants.DESIGN_RESPONSE_OBJECT).data.attributes;
    that.state = {
      drp: that.getDesignResponse(props),
      showModal: false,
      showFeedback: false,
      disableAction: false,
      reset_drp: false,
      currentFeedbackIndex: 0,
      currentFeedbackListIndex: 0
    };
    that.unsubscribe_project_store = ProjectsStore.listen(that.handleProjectStore.bind(that));
    that.unsubscribe_drp_store = DRPStore.listen(that.handleDesignResponse.bind(that));
  }

  componentDidMount() {
    let that = this;
    that.showDRP = true;
    //window.addEventListener('keydown', this.onKeyPress.bind(this));
  }

  componentWillUnmount() {
    this.showDRP = false;
    this.unsubscribe_project_store();
    this.unsubscribe_drp_store();
  }

  getDesignResponse(props){
    let drp = _.cloneDeep(Constants.DESIGN_RESPONSE_OBJECT).data;
    if(props.newDesignResponse){
      drp = props.currentMenuIndex != -1 ? DRPStore.getDraftDesignResponse(props.currentDRID) : drp;
    }else{
      drp = DRPStore.getDesignResponse(props.currentDRID);
    }
    return drp;
  }
  componentWillReceiveProps(nextProps) {
    let that = this;
    if(nextProps.currentDRID !== that.props.currentDRID){
      that.setState({pid: nextProps.project.id, showFeedback: false, currentDRID: nextProps.currentDRID, drp: that.getDesignResponse(nextProps)});
    }
  }
  handleProjectStore(trigger){
    if(trigger.project){
      this.context.setProgressVisibility(false);
      this.props.onSwitch(-3); // show submission
    }
  }
  //onStatusChange(props) {
  //  if (props == TriggerTypes.FETCHED_PROJECT) {
  //    this.context.setProgressVisibility(false);
  //    this.props.onSwitch(0); // show submission
  //  }
  //}
  handleDesignResponse(trigger){
    let that = this;
    if(trigger.drp){
      let drp = DRPStore.getDesignResponse(that.props.currentDRID);
      that.setState({drp: drp});
    }
    if(trigger.draft){
      let drp = DRPStore.getDraftDesignResponse(that.props.currentDRID);
      that.setState({drp: drp});
      this.props.onSwitch(-3);
    }
    if(trigger.error){
      console.log('some error in design response');
    }
    this.context.setProgressVisibility(false);
  }
  verifyDesignResponse() {
    let self = this;
    let name = self.refs.name.refs.withStyles.getValue().trim();
    if (name == '') {
      alert('Design Response Name Empty');
    }
    else if (self.new_drp_object['imgs'].length == 0) {
      alert('Submit at least one image');
    }
    else if (this.new_drp_object['spr'].length == 1) {
      let img = new Image();
      img.src = self.new_drp_object['spr'];
      self.context.setProgressVisibility(true);
      self.setState({disableAction: true});
      img.onload = function () {
        if ((img.width == 1024 && img.height == 512) || (img.width == 2048 && img.height == 1024)) {
          self.postDesignResponse();
        }
        else {
          self.setState({disableAction: false});
          self.context.setProgressVisibility(false);
          alert('Only 1024 X 512 or 2048 X 1024 size spherical image allowed.');
        }
      };
    }
    else if (this.new_drp_object['spr'].length > 1) {
      alert('Only single spherical image allowed');
    }
    else {
      self.context.setProgressVisibility(true);
      self.setState({disableAction: true});
      self.postDesignResponse();
    }
  }
  getCurrency(){
    let that = this, count = 0;
    if(that.DesignResponseProducts){
      let products = that.DesignResponseProducts.list;
      products.map((product) => {
        if(product.attributes.currency == 'INR'){
          count++;
        }
      });
      return {unit: count == 0 ? 'USD' : count == products.length ? 'INR' : -1};
    }else if(that.state.drp.attributes.products){
      let products = that.state.drp.attributes.products.list;
      products.map((product) => {
        if(product.attributes.currency == 'INR'){
          count++;
        }
      });
      return {unit: count == 0 ? 'USD' : count == products.length ? 'INR' : -1};
    }
    else{
      return {unit: 'INR'};
    }
  }
  postDesignResponse() {
    let that = this;
    let ref_name = that.refs.name.refs.withStyles.getValue();
    let ref_story = that.refs.story.refs.withStyles.getStory();
    let name = ref_name.trim();
    let story = ref_story;
    let userid = that.props.project.relationship.owner.id;
    let pid = that.props.project.id;
    let attributes = that.new_drp_object;
    attributes.name = name;
    attributes.story = story;

    if (that.DesignResponseProducts) {
      attributes.products = that.DesignResponseProducts;
    }
    else if (that.state.drp.attributes.products) {
      attributes.products = that.state.drp.attributes.products;
    }
    else {
      attributes.products = that.DUMMY_PRODUCT;
    }
    let currency = that.getCurrency();
    if(currency.unit == 'INR' || currency.unit == 'USD'){
      attributes.products.currency = currency.unit;
      DRPActions.sendDesignResponse(attributes, userid, pid, that.state.drp.id);
      that.context.setProgressVisibility(true);
      that.setState({disableAction: true});
    }else{
      that.context.setProgressVisibility(false);
      that.setState({disableAction: false});
      alert('All the products are not in same currency');
    }
  }

  resetImage() {
    this.setState({reset_drp: true});
  }

  setImages(images, tag) {
    this.new_drp_object[tag] = images;
  }

  saveDesignResponse() {
    let ref_name = this.refs.name.refs.withStyles.getValue();
    let ref_story = this.refs.story.refs.withStyles.getStory();
    let name = ref_name.trim();
    let story = ref_story;
    let userid = this.props.project.relationship.owner.id;
    let pid = this.props.project.id;

    let attributes = this.new_drp_object;
    attributes.name = name;
    attributes.story = story;

    if (this.DesignResponseProducts) {
      attributes.products = this.DesignResponseProducts;
    }
    else if (this.state.drp.attributes.products) {
      attributes.products = this.state.drp.attributes.products;
    }
    else {
      attributes.products = this.DUMMY_PRODUCT;
    }
    let currency = this.getCurrency();
    if(currency.unit == 'INR' || currency.unit == 'USD'){
      attributes.products.currency = currency.unit;
      DRPActions.saveDesignResponse(attributes, userid, pid, this.state.drp.id);
      this.context.setProgressVisibility(true);
      this.setState({disableAction: true});
    }else{
      this.context.setProgressVisibility(false);
      this.setState({disableAction: false});
      alert('All the products are not in same currency');
    }
  }

  setDesignResponseProducts(data) {
    this.DesignResponseProducts = data;
  }

  onShowFeedback(value) {
    //console.log(this.props.data);
    this.setState({showFeedback: value});
  }

  onShowModal(value) {
    this.setState({showModal: value})
  }

  showCurrentFeedback(index) {
    if (this.showDRP) {
      this.setState({currentFeedbackIndex: index});
    }
  }

  onChangeFeedBackListIndex(inc) {
    let self = this;
    let oldFeedbackListIndex = self.state.currentFeedbackListIndex;
    let userFeedbackListLength = self.state.drp.attributes.userfeedback.length;

    let feedbackListIndex = oldFeedbackListIndex + inc;
    feedbackListIndex = (feedbackListIndex < 0 ) ? 0 : feedbackListIndex;
    feedbackListIndex = (feedbackListIndex >= userFeedbackListLength ) ? userFeedbackListLength - 1 : feedbackListIndex;
    if (oldFeedbackListIndex != feedbackListIndex) self.setState({currentFeedbackListIndex: feedbackListIndex});
  }

  render() {
    let self = this;
    //console.log(self.props.data ? self.props.data.attributes.products:'');
    let Modal = <CanvasPlayerModal show={self.onShowModal.bind(self)}
                                   data={self.state.showFeedback ? _.result(self.state.drp,'attributes.userfeedback',[]) : []}/>;

    var feedBackBox = function () {
      let userFeedbackList = self.state.drp.attributes.userfeedback;
      let feedbackListIndex = self.state.currentFeedbackListIndex;

      let feedbackArr = userFeedbackList[feedbackListIndex].data;
      let feedbackIndex = self.state.currentFeedbackIndex;
      return (
        <div>
          <div style={{textAlign:'center'}}>
            <button onClick={self.onChangeFeedBackListIndex.bind(self,-1)}>&#x2190;</button>
            {feedbackListIndex + 1} / {userFeedbackList.length}
            <button onClick={self.onChangeFeedBackListIndex.bind(self,1)}>&#x2192;</button>
          </div>

          <CanvasPlayer
            showModal={self.onShowModal.bind(self)}
            imageUrl={feedbackArr[feedbackIndex] ? feedbackArr[feedbackIndex].imageUrl.url:'http://autogation.net/wp-content/uploads/2014/07/no-feedback-150x150.jpg'}
            audioUrl={feedbackArr[feedbackIndex]? feedbackArr[feedbackIndex].audioUrl:'nil'}
            annUrl={feedbackArr[feedbackIndex]? feedbackArr[feedbackIndex].annUrl:'nil'}
            fullscreen={true}/>
          <ThumbNails onClick={self.showCurrentFeedback.bind(self)} data={feedbackArr} current={feedbackIndex}/>
        </div>
      );
    };

    var designResponseBox = function () {
      return (
        <div>
          <ImageComponent pid={self.props.project.id} ref='imgs' reset={self.state.reset_drp} setImage={self.setImages.bind(self)}
                          data={self.state.drp.attributes.imgs} newDesignResponse={self.props.newDesignResponse}
                          title={'Images'} tag={'imgs'}/>
          <ImageComponent pid={self.props.project.id} ref='2dplan' reset={self.state.reset_drp} setImage={self.setImages.bind(self)}
                          data={self.state.drp.attributes['2dplan']}
                          newDesignResponse={self.props.newDesignResponse} title={'2D Plan'} tag={'2dplan'}/>
          <ImageComponent pid={self.props.project.id} ref='spr' reset={self.state.reset_drp} setImage={self.setImages.bind(self)}
                          data={self.state.drp.attributes.spr} newDesignResponse={self.props.newDesignResponse}
                          title={'Spherical Render'} tag={'spr'}/>
          <DesignResponseStory ref='story' reset={self.state.reset_drp}
                               drid = {self.state.drp.id} pid={self.props.project.id}
                               newDesignResponse={self.props.newDesignResponse}
                               audio={self.state.drp.attributes.story ? self.state.drp.attributes.story.audio:''}
                               text={self.state.drp.attributes.story ? self.state.drp.attributes.story.text:''}
                               title={self.state.drp.attributes.story ? self.state.drp.attributes.story.title:''}
                               subtitle={self.state.drp.attributes.story ? self.state.drp.attributes.story.subtitle:''}/>
          {!self.props.newDesignResponse ?
            <ProductsList ref='products' reset={self.state.reset_drp} data={self.state.drp.attributes.products}
                          newDesignResponse={self.props.newDesignResponse}/>
            : <DesignResponseProducts pid = {self.props.project.id} onSubmit={self.setDesignResponseProducts.bind(self)} reset={self.state.reset_drp}
                                      data={self.state.drp.attributes.products}/>}

          {/*self.props.newDesignResponse &&
          <div className="flex-col">
            <div className="input-divider"></div>
            <div className="DesignResponse-buttonWrapper">
              <button disabled={self.state.disableAction} className="DesignResponse-reset" onClick={self.resetImage.bind(self)}>
                <SvgIcon style={{fill:'#999'}}>
                  <path d="M12 5V1L7 6l5 5V7c3.31 0 6 2.69 6 6s-2.69 6-6 6-6-2.69-6-6H4c0 4.42 3.58 8 8 8s8-3.58 8-8-3.58-8-8-8z"/>
                </SvgIcon>
              </button>
              <div className="DesignResponse-mainButtons">
                <button disabled={self.state.disableAction} className="save" onClick={self.saveDesignResponse.bind(self)}> SAVE </button>
                <button disabled={self.state.disableAction} className="send" onClick={self.verifyDesignResponse.bind(self)}> SEND </button>
              </div>
            </div>
          </div>*/}
        </div>
      )
    };
    return (
      <div className="DesignResponse" style={{backgroundColor:'#e6e6e6', overflowY:'scroll', margin: self.props.newDesignResponse?'45px 0 20px 0':'0'}}>
        { self.state.drp != Constants.ITEM_LOADING ?
          <div style={{display:'flex', flexFlow:'row',justifyContent:'space-between'}}>
            <div style={self.props.newDesignResponse ? {display:'flex',flexFlow:'column',width:'100%'} : {display:'flex',flexFlow:'column', flex:'2'}}>
              <div style={self.props.newDesignResponse ? {width: '60%',backgroundColor:'white', margin:'20px auto', borderRadius:'3px'} : {flex:'2', height: '84vh', overflowY: 'scroll', backgroundColor:'white', margin:'20px'}}>

                <div>
                  <DesignResponseName isDraft = {self.props.newDesignResponse && self.props.currentMenuIndex != -1} onSwitch={self.props.onSwitch} ref='name' reset={self.state.reset_drp}
                                      data={self.state.drp} pid={self.props.project.id}
                                      newDesignResponse={self.props.newDesignResponse}
                                      onShowFeedback={self.onShowFeedback.bind(self)}
                                      name={self.state.drp.attributes.name}/>

                  {self.state.showFeedback ? feedBackBox() : designResponseBox()}

                  {self.state.showModal && <div style={{position:'absolute'}}>{Modal}</div>}
                </div>
              </div>
              { self.props.newDesignResponse &&
              <div className="flex-col" style={{backgroundColor: 'white', position: 'fixed', bottom: '0', width: '96%'}}>
                <div className="input-divider"></div>
                <div className="DesignResponse-buttonWrapper">

                  <div className="DesignResponse-mainButtons">
                    <button disabled={self.state.disableAction} className="save" onClick={self.saveDesignResponse.bind(self)}> Save as draft </button>
                    <button disabled={self.state.disableAction} className="send" onClick={self.verifyDesignResponse.bind(self)}> Send </button>
                  </div>
                </div>
              </div>}
            </div>
            {!self.props.newDesignResponse && <div style={{flex:'1', height: '84vh', borderLeft: '1px solid #bdc3c7'}}>
              <Comments drp={self.state.drp} project={self.props.project}/>
            </div> }
          </div>
          : ''}
      </div>
    );
  }
}
export default DesignResponse;
