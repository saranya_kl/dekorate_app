/**
 * Created by vikramaditya on 7/21/15.
 */
import React from 'react';
import mui from 'material-ui';
import DropZone from 'react-dropzone';
import _ from 'lodash';
import styles from './ImageComponent.scss';
import Helper from '../../../../../../../helper.js';
import Components from '../../../../../../../component.js';
import CanvasPlayerModal from '../../../../CanvasPlayerModal';
import CanvasPlayer from '../../CanvasPlayer/CanvasPlayer';
import CustomModal from '../../../../../../../../lib/Modal';
import SphericalViewer from '../../../../../../../Shared/SphericalViewer/SphericalViewer';
var {withStyles, Services, WorkspaceActions, WorkspaceStore, AppConstants: Constants, TriggerTypes, RefreshIndicator} = Helper;

var {Avatar, CardHeader, CardText, SvgIcon} = mui;

@withStyles(styles) class ImageComponent extends React.Component {

  constructor() {
    super();
    this.state = {
      currentIndex: 0,
      img_data: {},
      showModal: false,
      showSphericalModal: false,
      isDragActive: false,
      showURL: '',
      showAssets: false,
      workspaceData: {},
      selectedAssets: {},
      image: {
        'imgs': [],
        '2dplan': [],
        'spr': []
      }
    };
  }

  componentWillMount() {
    let self = this;
    let tag = this.props.tag;
    if (self.props.newDesignResponse) {
      self.setInitialData(this.props.data, tag);
    }
    self.unsubscribe_workspace = WorkspaceStore.listen(self.handleStoreChange.bind(self));
  }

  setInitialData(arr, tag) {
    let data = {}, that = this;
    arr.map((image, index) => {
      data[index] = {isUploading: false, url: image, preview: image};
    });
    let x = that.state.image;
    x[tag] = arr;
    let pid = that.props.pid;
    let workspaceData = WorkspaceStore.getWorkSpace(pid);
    that.props.setImage(x[tag], tag);
    that.setState({image: x, img_data: data, workspaceData: workspaceData});
  }

  componentWillUnmount() {
    if (this.props.newDesignResponse) {
      this.unsubscribe_workspace();
    }
  }

  handleStoreChange() {
    let that = this;
    let pid = that.props.pid;
    let data = WorkspaceStore.getWorkSpace(pid);
    if (that.props.newDesignResponse) {
      that.setState({workspaceData: data});
    }
  }

  componentDidUpdate() {
    this.props.setImage(this.state.image[this.props.tag], this.props.tag);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.reset) {
      let x = {'imgs': [], '2dplan': [], 'spr': []};
      this.setState({image: x, img_data: {}});
    }
    if (nextProps.data !== this.props.data) {
      if (nextProps.newDesignResponse) {
        this.setInitialData(nextProps.data, nextProps.tag);
      }
    }
  }

  onDrop(files) {
    let self = this;
    var x = _.cloneDeep(this.state.img_data);
    for (let i = 0; i < files.length; i++) {
      x[files[i].preview] = {isUploading: true, preview: files[i].preview, url: ''};
    }
    let pid = self.props.pid;
    self.imageUploader(files, this.props.tag, pid);
    //ProjectActions.UPLOAD_IMAGES(files, this.props.tag, `?type=DesignResponse&projectid=${pid}`);
    this.setState({isDragActive: false, img_data: x});
  }

  setImage(tag, preview, url){
    let that = this;
    let img = _.cloneDeep(that.state.image);
    img[tag].push(url);
    let x = _.cloneDeep(that.state.img_data);
    x[preview] = {
      isUploading: false,
      url: url,
      preview: that.state.img_data[preview].preview
    };
    that.setState({image: img, img_data: x});
  }
  imageUploader(files, tag, pid){
    let self = this;
    files.forEach(file => {
      Services.uploadAPI(Constants.SERVER_URL + 'upload', file, `?type=DesignResponse&projectid=${pid}`)
        .then(response => {
          let url = response.body.url;
          self.setImage(tag, file.preview, url);
        });
    });
  }
  removeImage(name) {
    let x = this.state.img_data;
    delete x[name];

    let updatedImage = [];
    for (let key in x) {
      updatedImage.push(x[key].url);
    }
    let img = _.cloneDeep(this.state.image);
    img[this.props.tag] = updatedImage;
    this.setState({img_data: x, image: img});
  }

  showModal(url, index) {
    if(this.props.tag == 'spr'){
      this.setState({showSphericalModal: true, showURL: url, currentIndex: index});
    }else{
      this.setState({showModal: true, showURL: url, currentIndex: index});
    }
  }

  toggleModal(value) {
    this.setState({showModal: value});
  }

  onDragOver() {
    this.setState({isDragActive: true});
  }

  onDragLeave() {
    this.setState({isDragActive: false});
  }

  showWorkspaceAssets(value) {
    let that = this;
    that.setState({showAssets: value});
  }
  uploadAssets(){
    let that = this;
    let x = _.cloneDeep(this.state.img_data);
    let assets = that.state.selectedAssets;
    let arr = Object.keys(assets);
    let updatedImage = [];
    for(let i=0;i<arr.length;i++){
      x[arr[i]] = {isUploading: false, url: arr[i], preview: assets[arr[i]]};
    }
    for (let key in x) {
      updatedImage.push(x[key].url);
    }
    let img = _.cloneDeep(that.state.image);
    img[that.props.tag] = updatedImage;
    that.setState({img_data: x, image: img, selectedAssets : {}, showAssets: false});
  }
  addAsset(asset){
    let that = this;
    let selectedAssets = that.state.selectedAssets;
    let url = asset.url, thumb = asset.thumb;
    if(selectedAssets[url]){
      delete selectedAssets[url];
    } else{
      selectedAssets[url] = thumb;
    }
    that.setState({selectedAssets: selectedAssets});
  }
  showSphericalModal(value = false){
    this.setState({showSphericalModal: value});
  }
  getSphericalModal(){
    let that = this, url = that.state.showURL;
    return (
      <CustomModal id={'spherical_dialog'} style={{backgroundColor: '#fff',left:'2%', width:'93%', height:'85%', top:'4%', position:'fixed'}} isOpen={true}
                   onClose={that.showSphericalModal.bind(that,false)}>
        <SphericalViewer imgurl={url}/>
        <SvgIcon style={{top: '0',right: '0', position: 'absolute', cursor: 'pointer'}} onClick={that.showSphericalModal.bind(that, false)}>
          <path fill='#666' d={Constants.CLOSE_ICON}/>
        </SvgIcon>
      </CustomModal>
    )
  }
  getWorkspaceAssetsComponent() {
    let data = [], that = this;
    let pid = that.props.pid;
    if (typeof(that.state.workspaceData) === 'object') {
      data = that.state.workspaceData['assets'];
    } else {
      WorkspaceActions.fetchWorkSpace(pid);
      data = {};
    }
    return (
      <CustomModal style={{left:'20%', width:'60%', height:'85%', top:'4%', position:'fixed'}} isOpen={true}
                   onClose={that.showWorkspaceAssets.bind(that,false)}>
        <div style={{display:'flex', flexFlow:'row wrap'}}>
          {
            data.shared ? data.shared.map((asset, index) => {
              return (
                <div key={index} style={{position: 'relative', cursor:'pointer'}} onClick={that.addAsset.bind(that, asset)} >
                  <img src={asset.thumb} style={{margin:'10px 10px 0 0', width:'150px',height:'150px' }}/>
                  {asset.note && <div className='note-overlay'></div>}
                  {asset.note && <div className='note-text'>{asset.note}</div>}
                  <div className='label'>{asset.name}</div>
                  {that.state.selectedAssets[asset.url] &&
                    [<div key={1} style={{position:'absolute',left:'0', top:'10px', width:'150px', height:'150px',backgroundColor:'#000', opacity:'0.3'}}>
                    </div>,
                    <SvgIcon key={2} color={'white'} style={{position:'absolute',left:'62px', top:'72px'}}>
                    <path d="M21,7L9,19L3.5,13.5L4.91,12.09L9,16.17L19.59,5.59L21,7Z" />
                    </SvgIcon>
                    ]
                  }
                </div>
              )
            })
              :
              <div
                style={{alignItems:'center',justifyContent:'center',left:'0',top:'0',display:'flex',position:'absolute',background:'rgba(0,0,0,0.42)',width:'100%',height:'100%'}}>
                <img style={{width:'50px',height:'50px'}} src={Constants.LOADER_GIF}/>
              </div>
          }
        </div>
        <button className='custom-primary-button' onClick={that.uploadAssets.bind(that)}>Done</button>
      </CustomModal>
    )
  }

  render() {
    var self = this;
    let workspaceAssets = self.state.showAssets ? self.getWorkspaceAssetsComponent() : '';
    let sphericalModal = self.getSphericalModal();
    if (!self.props.newDesignResponse) {
      return (
        <div style={{backgroundColor:'white', marginTop:'10px'}}>
          <span className="ImageComponent-title"> {self.props.title} </span>
          <CardText>
            <div className='designresponse-image-container'>
              {self.props.data.length > 0 ? self.props.data.map(function (urlObject, index) {
                return (
                  <div className='designresponse-image' key={Math.random()}>
                    <img alt={`image-${index}`} src={urlObject.url} className='designresponse-image'
                         onTouchTap={self.showModal.bind(self,urlObject.url, index)}/>
                  </div>)
              }) : <span style={{fontSize:'0.7rem', marginLeft:'20px'}}>No images uploaded</span>
              }
            </div>
          </CardText>
          {self.state.showModal ?
            <CanvasPlayerModal initialIndex={this.state.currentIndex} show={this.toggleModal.bind(this)}
                               data={self.props.data}/> : ''}
          {self.state.showSphericalModal && sphericalModal}
        </div>
      );
    }

    else {
      var img_length = Object.getOwnPropertyNames(self.state.img_data).length;
      let drop_style = {
        textAlign: 'center',
        height: '118px',
        minWidth: img_length > 0 ? '140px' : '99%',
        margin: img_length > 0 ? '0' : '0 auto',
        border: this.state.isDragActive ? '1px solid green' : '1px dashed #e6e6e6',
        borderRadius: '4px',
        backgroundColor: 'white',
        marginRight: '8px'
      };
      return (
        <div className="ImageComponent">
          <span className="ImageComponent-title"> {self.props.title} </span>
          <button onClick={self.showWorkspaceAssets.bind(self, true)} className='ImageComponent-add-workspace-Btn'>Add from
            Workspace
          </button>
          {self.state.showAssets && workspaceAssets}
          <div className='newdesignresponse-image-container'>
            <DropZone style={drop_style}
                      onDrop={self.onDrop.bind(self)} onDragOver={self.onDragOver.bind(self)}
                      onDragLeave={self.onDragLeave.bind(self)}>
              {!self.state.isDragActive &&
              <div className="ImageComponent-addWrapper" style={{width: img_length>0 ? '140px' : '100%'}}>
                <SvgIcon style={{width: '30px',display: 'block',fill: '#999'}}>
                  <path d="M19 13h-6v6h-2v-6H5v-2h6V5h2v6h6v2z"/>
                </SvgIcon>
                <span style={{fontSize:'0.7rem', color:'#888', padding:'0 5px'}}>Drop your files here, or click to select files to upload.</span>
              </div>
              }
            </DropZone>

            <div className="newdesignresponse-image-wrapper flex-row">
              {
                function () {
                  var imgs = [], index = 0;
                  for (let im in self.state.img_data) {
                    index++;
                    if (self.state.img_data[im].isUploading) {
                      imgs.push(
                        <div className='designresponse-image-holder' key={im}>
                          <img className='designresponse-image-blur' src={self.state.img_data[im].preview}/>

                          <div
                            style={{alignItems:'center',justifyContent:'center',left:'0',top:'0',display:'flex', position:'absolute',width:'100%',height:'100%'}}>
                            <img style={{width:'30px',height:'30px'}} src={Constants.LOADER_GIF}/>
                          </div>
                        </div>
                      )
                    } else {
                      imgs.push(
                        <div className='designresponse-image-holder' key={im}>
                          <img className='designresponse-image' onClick={self.showModal.bind(self, self.state.img_data[im].url, index-1)} src={self.state.img_data[im].preview}/>
                          <SvgIcon className='cross-icon' onClick={self.removeImage.bind(self, im)}>
                            <path fill='#666' d={Constants.CLOSE_ICON}/>
                          </SvgIcon>
                        </div>
                      )
                    }
                  }
                  return imgs;
                }()
              }
            </div>
            {self.state.showModal ?
              <CustomModal style={{width:'97%', height:'90%', left:'0', top:'5%'}} isOpen={true} onClose={self.toggleModal.bind(self)}>
                <CanvasPlayer imageUrl={self.state.showURL} audioUrl={'nil'} annUrl={'nil'} fullscreen={false} height={600}/>
                <SvgIcon style={{top: '0',right: '0', position: 'absolute', cursor: 'pointer'}} onClick={self.toggleModal.bind(self, false)}>
                  <path fill='#666' d={Constants.CLOSE_ICON}/>
                </SvgIcon>
              </CustomModal> : ''}
            {self.state.showSphericalModal && sphericalModal}
          </div>
        </div>
      );
    }
  }
}
export default ImageComponent;
