/**
 * Created by vikramaditya on 7/27/15.
 */
/**
 * Created by vikramaditya on 7/8/15.
 */
import React from 'react';
import mui from 'material-ui';
import styles from './ProductsList.scss';

import Helper from '../../../../../../../helper.js';
var {withStyles} = Helper;
var {List, ListItem, ListDivider, Avatar, SvgIcon, CardHeader} = mui;

@withStyles(styles)
class ProductsList extends React.Component {

  constructor(){
    super();
  }
  showProduct(url){
    window.open(url, '_blank');
  }
  render(){
    var self = this;
    return (
      <div style={{backgroundColor:'white', margin:'10px'}}>
        <h6 className="ProductsList-title"> Products</h6>
        <List className='products-list'>
          {
            (self.props.data.list.length > 0) && self.props.data.list.map(function(product, index){
              return ([
                <ListItem onClick = {self.showProduct.bind(self, product.attributes.url)} key={index}>
                  <div className='product-row'>
                    <span style={{alignSelf:'center',fontWeight:'bold', paddingRight:'10px'}}>{index+1}.</span>
                    <div className='product-left'>
                      <Avatar style={{borderRadius: '0%', border: 'none'}} src={product.attributes.displayimg} />
                      <div style={{display: 'flex', flexFlow: 'column', marginLeft: '12px'}}>
                        <div>
                          {product.attributes.name}
                        </div>
                        <div>
                          {product.attributes.reseller}
                        </div>
                      </div>
                    </div>
                    <div className="product-right">
                      {`${product.attributes.currency == 'USD' ? '$' : '₹'} ${product.attributes.unitPrice}`}  x {product.attributes.qty}
                    </div>
                    <div className="product-right">
                      {`${product.attributes.currency == 'USD' ? '$' : '₹'} ${product.attributes.price}`}
                    </div>
                    {/*<div>
                     {Moment(product.created_at).format('LL')}
                     </div>*/}
                  </div>
                </ListItem>,
                <ListDivider />
              ]);
            })
          }
          <div style={{display: 'flex', flexFlow: 'row', justifyContent: self.props.data.totalcost != 0 ? 'flex-end' : 'center', margin: '0px 10px', fontSize:'0.8rem'}}>
            {self.props.data.totalcost != 0 ? <p style={{marginBottom: '10px'}}>Total Cost : {`${self.props.data.currency == 'USD' ? '$' : '₹'} ${self.props.data.totalcost}`}</p> : <p style={{marginBottom: '10px'}}> No products </p>}
          </div>
        </List>
      </div>
    );
  }
}
export default ProductsList;
