/**
 * Created by vikramaditya on 9/15/15.
 */
import React from 'react';
import {ListDivider, SvgIcon, ListItem, List} from 'material-ui';
import Helper from '../../../../../../../helper.js';
import styles from './SearchPanel.scss';
import _ from 'lodash';
var {withStyles, ProductCatalogActions, ProductCatalogStore, ProductsAction, ProductsStore} = Helper;

@withStyles(styles) class SearchPanel extends React.Component {
  static contextTypes = {
    setProgressVisibility: React.PropTypes.func
  };
  constructor() {
    super();
    this.state = {
      list: []
    }
  }

  componentWillMount() {
    this.setState({list: this.props.data ? this.props.data : []});
    this.unsubscribe = ProductsStore.listen(this.handleProductStore.bind(this));
    this.unsubscribe_catalog = ProductCatalogStore.listen(this.handleProductCatalog.bind(this));
  }

  componentWillUnmount() {
    this.unsubscribe();
    this.unsubscribe_catalog();
  }

  //onStatusChange(props) {
  //  if (props == TriggerTypes.PRODUCT_SEARCH_COMPLETE) {
  //    let result = ProductStore.getSearchResults();
  //    this.props.setSearchResult(result);
  //    this.setState({list: result});
  //    this.context.setProgressVisibility(false);
  //  }
  //}
  handleProductStore(trigger){
    let that = this;
    if(trigger.products){
      if(trigger.searchResults){
        let results = ProductsStore.getSearchResults();
        that.props.setSearchResult(results);
        that.setState({list: results});
      }
    }
    that.context.setProgressVisibility(false);
  }
  handleProductCatalog(trigger){
    let that = this;
    if(trigger.error){
      that.context.setProgressVisibility(false);
    }
    if(trigger.search){
      let list = ProductCatalogStore.getList();
      ProductsAction.fetchProducts(list);
    }
  }
  searchProduct(e) {
    let value = React.findDOMNode(this.refs.searchBox).value.trim();
    if(e.keyCode == undefined || e.keyCode == 13){
      if(value){
        this.searchTerm = value;
        let data = {query: value};
        ProductCatalogActions.getSearchResults(data);
        this.context.setProgressVisibility(true);
        //ProductActions.SEARCH_PRODUCTS(value);
      }
    }
    //if (e.keyCode == 13) {
    //  let value = React.findDOMNode(this.refs.searchBox).value;
    //  ProductActions.SEARCH_PRODUCTS(value);
    //  this.context.setProgressVisibility(true);
    //}
  }
  showProduct(product){
    this.props.showProduct(product);
  }
  getProductsList() {
    let list = this.state.list;
    let self = this;
    let ItemList = (product, index) => {
      if(!product){return;}
      else {
        return (
          <div className='itemList'
               style={{backgroundColor: this.state.currentIndex == index ? 'gray' : 'white'}}>
            <div onClick={self.showProduct.bind(self,product)} className='item-info'>
              <div className='item-left'>
                <img className='product-image' src={_.result(product,'displayImage.url','')}/>

                <div className='product-name'>
                  {product.name}
                </div>
              </div>
              <div className='product-id'>
                {product.productid}
              </div>
            </div>
          </div>
        )
      }
    };
    return (
      <List>
        {
          list.map((product, index) => {
            return (
              <div key={index}>
                {ItemList(product, index)}
              </div>
            )
          })
        }
      </List>
    )
  }

  getSearchBox() {
    return (
      <div className='searchbox'>
        <span className="searchbox-label"> Search for products: </span>
        <input ref='searchBox' type='text' placeholder='Search' className = 'searchbox-input' onKeyDown={this.searchProduct.bind(this)}/>
      </div>
    )
  }

  empty_placeholder(){
    return(
      <div className = "EmptyHolder">
        <div className="EmptyHolder-content">
          <div className="EmptyHolder-content-title">Search for products by</div>
          <ul>
            <li>Type like sofa, bed, etc</li>
            <li> Full or parts of the product name</li>
            <li>Itemtype like furniture, living room, etc</li>
            <li>Brand like Urbanladder, Pepperfry, etc</li>
            <li> Product URL </li>
            <li>Product ID</li>
            <li> Origin Product ID </li>
          </ul>
        </div>
      </div>
    )
  }

  render() {
    let searchbox = this.getSearchBox();
    let products_list = this.getProductsList();
    let empty_placeholder = this.empty_placeholder();
    return (
      <div className ='SearchPanel'>
        <div className='flex-col' style={{height: '100%'}}>
          {searchbox}
          {this.state.list.length == 0 ? empty_placeholder : products_list }
        </div>
      </div>
    )
  }
}
export default SearchPanel;
