/**
 * Created by vikramaditya on 9/23/15.
 */
import React from 'react';
import {CircularProgress} from 'material-ui';
import FilesHolder from '../FilesHolder';
import ProductCartHolder from './ProductCartHolder/ProductCartHolder.js';
import Helper from '../../../../../../helper.js';
var {withStyles, WorkspaceActions, WorkspaceStore} = Helper;

class WorkSpace extends React.Component {
  constructor() {
    super();
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  componentWillMount() {

    let that = this;
    let pid = that.props.pid;
    WorkspaceActions.fetchWorkSpace(pid);

    that.unsubscribe = WorkspaceStore.listen(that.handleChange.bind(that));

    that.setState({
      workspaceData: {},
      productCart: {},
      showProductCartHolder: false,
      currentProductCartIndex: null,
      inProgress: false
    });
  }

  onShowProgress(value) {
    let that = this;
    that.setState({inProgress: value});
  }

  componentDidMount() {
    let that = this;
    that.getStateFromStore(that.props.pid);
  }

  componentWillReceiveProps(nextProps) {
    let that = this;
    if (that.props.pid == nextProps.pid) return;

    let pid = nextProps.pid;
    WorkspaceActions.fetchWorkSpace(pid);
    that.getStateFromStore(pid);
  }

  getStateFromStore(pid) {
    let that = this;
    let data = WorkspaceStore.getWorkSpace(pid);
    let cart_data = (that.state.currentProductCartIndex != null) ? WorkspaceStore.getWorkspaceProductCart(that.props.pid,that.state.currentProductCartIndex) : {};
    that.setState({workspaceData: data, inProgress: false, productCart: cart_data});
  }
  handleChange() {
    let that = this;
    that.getStateFromStore(that.props.pid);
  }

  onShowProductCartHolder(value, index = null) { // index == -1 && value == true then new cart
    let that = this;
    let cart_data = WorkspaceStore.getWorkspaceProductCart(that.props.pid,index);
    that.setState({showProductCartHolder: value, productCart:cart_data, currentProductCartIndex: index});
  }

  render() {
    let that = this;
    let pid = that.props.pid;
    let showProductCartHolder = that.state.showProductCartHolder;
    let currentProductCartIndex = that.state.currentProductCartIndex;
    let workspaceData = that.state.workspaceData;
    let inProgress = that.state.inProgress;
    let productCart = that.state.productCart;
    return (
      <div style={{backgroundColor:'#e6e6e6'}}>
        {showProductCartHolder ?
          <ProductCartHolder pid={pid} onShowProgress={that.onShowProgress.bind(that)}
                             productCartIndex={currentProductCartIndex}
                             productCart={productCart}
                             onShow={this.onShowProductCartHolder.bind(this)}/>
          :
          <FilesHolder pid={pid} data={workspaceData} onShow={this.onShowProductCartHolder.bind(this)}/>
        }
        {inProgress && <CircularProgress color='green' style={{zIndex:'9999',position:'absolute',left:'50%',top:'30%'}} mode="indeterminate" />}
      </div>
    )
  }
}
export default WorkSpace;
