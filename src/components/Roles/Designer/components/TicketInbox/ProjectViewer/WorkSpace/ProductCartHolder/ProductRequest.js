/**
 * Created by vikramaditya on 10/1/15.
 */
import React from 'react';
import styles from './ProductRequest.scss';
import _ from 'lodash';
import Helper from '../../../../../../../helper.js';
var {withStyles} = Helper;
@withStyles(styles) class ProductRequest extends React.Component {
  constructor() {
    super();
    this.DUMMY_OBJECT = {url: '', color: '', note: '', itemtype: '', done: false, cancelled: false};
    this.state = {
      requests: [this.DUMMY_OBJECT]
    }
  }

  addRequest() {
    let that = this;
    let x = that.state.requests;
    let dummy = _.cloneDeep(that.DUMMY_OBJECT);
    x.push(dummy);
    that.setState({requests: x});
  }

  removeRequest(index) {
    let that = this;
    let x = that.state.requests;
    x.splice(index, 1);
    that.setState({requests: x});
  }

  saveRequest() {
    let that = this;
    if(that.checkFields()){
      that.props.setRequest(that.state.requests);
    }else{
      alert('Fill all Required Fields');
    }
  }
  checkFields(){
    let that = this;
    let x = that.state.requests;
    for(let i=0;i<x.length;i++){
      if(x[i].url === '' || x[i].color === '' || x[i].note === ''){
        return false;
      }
    }
    return true;
  }
  handleChange(index, tag, e) {
    let that = this;
    let value = e.target.value;
    let x = that.state.requests;
    x[index][tag] = value;
    that.setState({requests: x});
  }

  getRequestComponent(data, index) {
    let that = this;
    return (
      <div key={index} className='ProductRequest-list'>
        <div className='ProductRequest-list-input'>
          <span className='custom-input-label'> Enter URL<sup>*</sup> </span>
          <input type='url' className='custom-input' onChange={that.handleChange.bind(that, index, 'url')} value={data.url} placeholder='URL'/>

          <span className='custom-input-label'> Enter Item Type<sup>*</sup> </span>
          <input type='text' className='custom-input' onChange={that.handleChange.bind(that, index, 'itemtype')} value={data.itemtype} placeholder='Item type'/>

          <span className='custom-input-label'> Enter color<sup>*</sup> </span>
          <input type='text' className='custom-input' onChange={that.handleChange.bind(that, index, 'color')} value={data.color}
                 placeholder='Color'/>

          <span className='custom-input-label'> Enter size<sup>*</sup> </span>
          <input type='text' className='custom-input' onChange={that.handleChange.bind(that, index, 'note')} value={data.note}
                 placeholder='Size'/>
        </div>
        {/*<div>
          <button onClick={that.removeRequest.bind(that,index)}>Remove</button>
        </div>*/}
      </div>
    )
  }

  render() {
    let that = this;
    let requestComponent = that.state.requests.map((data, index) => that.getRequestComponent(data, index));
    return (
      <div className='ProductRequest'>
        {requestComponent}
        {/*<button onClick={that.addRequest.bind(that)}>Add</button>*/}
        <div className='input-divider'></div>
        <div className='ProductRequest-doneBtn'>
          <button className='custom-primary-button' onClick={that.saveRequest.bind(that)}>Add</button>
        </div>
      </div>
    )
  }
}
export default ProductRequest;
