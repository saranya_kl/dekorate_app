/**
 * Created by vikramaditya on 9/23/15.
 */
import React from 'react';
import {SvgIcon} from 'material-ui';
import ProductCatalog from '../../../../../../../Shared/ProductCatalog/ProductCatalog.js';
import Helper from '../../../../../../../helper.js';
import styles from './ProductCartHolder.scss';
import ProductCart from './ProductCart/ProductCart.js';
import ProductRequest from './ProductRequest.js';
import _ from 'lodash';
var {withStyles, WorkspaceActions, WorkspaceStore} = Helper;
@withStyles(styles) class ProductCartHolder extends React.Component {

  constructor() {
    super();
    this.state = {
      showCart: true,
      store_cart_data: {
        cancelledlist: [],
        list: [],
        name: '',
        id: undefined,
        requests: []
      },
      cartChange: false,
      menuIndex: 0
    }
  }

  componentWillMount() {
    this.getStateFromStore(this.props);
  }

  componentWillReceiveProps(nextProps) {
    let that = this;
    if (nextProps.pid != that.props.pid) {
      nextProps.onShow(false);
    } else {
      that.getStateFromStore(nextProps)
    }
  }

  getStateFromStore(props) {
    let that = this;
    let data = _.cloneDeep(props.productCart);//_.cloneDeep(WorkspaceStore.getWorkspaceProductCart(props.pid, props.productCartIndex));
    let x = that.state.store_cart_data;
    data = _.transform(data, (res, v,k) => {if(v) res[k] = v});
    x = _.assign(x, data);
    that.setState({store_cart_data: x});
  }

  onRemoveFromCart(index) {
    let that = this;
    let x = that.state.store_cart_data;
    let r = confirm(`Remove ?`);
    if (r) {
      x.list.splice(index, 1);
      that.setState({store_cart_data: x, cartChange: true});
    }
  }

  onQuantityChange(qty, index) {
    let that = this;
    let x = that.state.store_cart_data;
    x.list[index].qty = qty;
    that.setState({store_cart_data: x, cartChange: true});
  }
  onNameSet(e){
    let that = this;
    let name = e.target.value;
    let x = that.state.store_cart_data;
    x.name = name;
    that.setState({store_cart_data: x, cartChange: true});
  }
  onCancelItem(index, cancel=true) {
    let that = this;
    let x = that.state.store_cart_data;
    if(cancel){
      let data = x.list[index];
      x.cancelledlist.push(data);
      x.list.splice(index, 1);
    }else{
      let data = x.cancelledlist[index];
      x.list.push(data);
      x.cancelledlist.splice(index, 1);
    }
    that.setState({store_cart_data: x, cartChange: true});
  }

  onAddToCart(data) {
    let that = this;
    let x = that.state.store_cart_data;
    let product = {productid: data.productid, qty: '1'};
    x.list.push(product);
    that.setState({store_cart_data: x, cartChange: true});
  }

  onUploadProduct() {
    let that = this;
    let cartIndex = that.props.productCartIndex;
    let pid = that.props.pid;
    let errorFlag = false;
    if (that.state.store_cart_data.name.trim() != '') {
      if (errorFlag == false) {
        WorkspaceActions.addWorkspaceProductCart(pid, cartIndex, _.cloneDeep(that.state.store_cart_data));
        that.props.onShowProgress(true);
        that.props.onShow(false);
      } else {
        alert(errorFlag);
      }
    }
    else {
      alert('Enter Cart Name');
    }
  }

  deleteProductCart() {
    let that = this;
    let pid = that.props.pid;
    let cartIndex = that.props.productCartIndex;
    let cart = that.state.store_cart_data;
    let r = confirm(`Delete Cart ${cart.name}?`);
    if (r) {
      WorkspaceActions.deleteWorkspaceCart(pid, cartIndex, cart.id);
      that.props.onShowProgress(true);
      that.props.onShow(false);
    }
  }

  setProductRequests(requestArray) {
    let that = this;
    let x = that.state.store_cart_data;
    x.requests = x.requests ? x.requests : [];
    x.requests = x.requests.concat(requestArray);
    that.setState({store_cart_data: x, menuIndex: 0, cartChange: true});
  }

  onRequestRemove(index) {
    let that = this;
    let x = that.state.store_cart_data;
    x.requests.splice(index, 1);
    that.setState({store_cart_data: x, cartChange: true});
  }

  onRequestDone(index) {
    let that = this;
    let x = that.state.store_cart_data;
    x.requests[index]['done'] = !x.requests[index]['done'];
    that.setState({store_cart_data: x, cartChange: true});
  }

  onRequestCancel(index) {
    let that = this;
    let x = that.state.store_cart_data;
    x.requests[index]['cancelled'] = !x.requests[index]['cancelled'];
    that.setState({store_cart_data: x, cartChange: true});
  }

  switchMenu(index) {
    let that = this;
    that.setState({menuIndex: index});
  }

  goBack() {
    let that = this;
    if (that.state.menuIndex == 0) {
      if (that.state.cartChange) {
        let r = confirm('Unsaved Changes in Cart\n Continue without saving?');
        if (r) {
          that.props.onShow(false);
        }
      } else {
        that.props.onShow(false);
      }
    }
    else {
      if (that.state.cartChange) {
        that.setState({menuIndex: 0});
      } else {
        that.props.onShow(false);
      }
    }
  }

  getCurrentComponent() {
    let that = this;
    let cart_data = that.state.store_cart_data;
    let cartChange = that.state.cartChange;
    switch (that.state.menuIndex) {
    case 0:
      return (
        <ProductCart data={cart_data} onQuantityChange={that.onQuantityChange.bind(that)} cartChange={cartChange}
                     setName={that.onNameSet.bind(that)}
                     switchMenu={that.switchMenu.bind(that)}
                     removeItem={that.onRemoveFromCart.bind(that)}
                     cancelItem={that.onCancelItem.bind(that)}
                     addProduct={that.onUploadProduct.bind(that)}
                     removeRequest={that.onRequestRemove.bind(that)}
                     onDoneRequest={that.onRequestDone.bind(that)}
                     onCancelRequest={that.onRequestCancel.bind(that)}
          />);
      break;
    case 1:
      return (<ProductCatalog addProduct={that.onAddToCart.bind(that)}/>);
      break;
    case 2:
      return (<ProductRequest setRequest={that.setProductRequests.bind(that)}/>);
      break;
    default :
      return (<div>Some Error</div>);
    }
  }

  getTabs() {
    let that = this;
    return (
      [
        //Menu in [0]th element and controls on [1]st.
        <div key={1} style={{display:'flex'}}>
          <button onClick={that.goBack.bind(that)} className='back-btn'>&larr;</button>
          <button onClick={that.switchMenu.bind(that, 1)} className='cart-btn'>Add product</button>
          <button onClick={that.switchMenu.bind(that, 2)} className='cart-btn'>Request</button>
        </div>,
        <div key={2}>
          {(that.props.productCartIndex != null) &&
          <button onClick={that.deleteProductCart.bind(that)} className='cart-btn'>
            <SvgIcon style={{fill:'#808080'}}>
              <path d='M19,4H15.5L14.5,3H9.5L8.5,4H5V6H19M6,19A2,2 0 0,0 8,21H16A2,2 0 0,0 18,19V7H6V19Z'/>
            </SvgIcon>
          </button>}
          <button onClick={that.switchMenu.bind(that, 0)} className='cart-btn'>
            <SvgIcon style={{fill:'#808080'}}>
              <path
                d='M17,18C15.89,18 15,18.89 15,20A2,2 0 0,0 17,22A2,2 0 0,0 19,20C19,18.89 18.1,18 17,18M1,2V4H3L6.6,11.59L5.24,14.04C5.09,14.32 5,14.65 5,15A2,2 0 0,0 7,17H19V15H7.42A0.25,0.25 0 0,1 7.17,14.75C7.17,14.7 7.18,14.66 7.2,14.63L8.1,13H15.55C16.3,13 16.96,12.58 17.3,11.97L20.88,5.5C20.95,5.34 21,5.17 21,5A1,1 0 0,0 20,4H5.21L4.27,2M7,18C5.89,18 5,18.89 5,20A2,2 0 0,0 7,22A2,2 0 0,0 9,20C9,18.89 8.1,18 7,18Z'/>
            </SvgIcon>
            {that.state.store_cart_data.list &&
            that.state.store_cart_data.list.length > 0 &&
            <span className='cart-counter-label'>{that.state.store_cart_data.list.length}</span>}
          </button>
        </div>
      ]
    )
  }

  render() {
    let that = this;
    let tabs = that.getTabs();
    let currentComponent = that.getCurrentComponent();
    let cartChange  = that.state.cartChange;
    return (
      <div className='ProductCartHolder'>
        {cartChange &&
          <h6 className='ProductCartHolder-label'>Unsaved Changes in cart. Please save changes!
              <span className='saveBtn'>
                <button className='custom-primary-button' onClick={that.onUploadProduct.bind(that)}>SAVE</button>
              </span>
          </h6>
        }
        <div className='ProductCartHolder-Tab'>
          {tabs}
        </div>
        <div className='ProductCartHolder-body'>
          {currentComponent}
        </div>

      </div>
    )
  }
}
export default ProductCartHolder;
