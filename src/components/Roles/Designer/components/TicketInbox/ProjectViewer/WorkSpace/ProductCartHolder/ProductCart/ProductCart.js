/**
 * Created by vikramaditya on 9/24/15.
 */
import React from 'react';
import _ from 'lodash';
import {SvgIcon} from 'material-ui';
import Helper from '../../../../../../../../helper.js';
import styles from './ProductCart.scss';
import Moment from 'moment';
import CustomModal from '../../../../../../../../../lib/Modal';
import ProductItemView from '../../../../../ProductScreen/ProductRepository/ProductItemView.js';

var {withStyles, WorkspaceStore, ProductsStore, AppConstants: Constants} = Helper;
@withStyles(styles) class ProductCart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: props.data.list,
      cancelled_data: props.data.cancelledlist,
      loading: true,
      totalCost: '0',
      requests: props.data.requests,
      isModalOpen: false,
      previewProduct: {}
    };
  }

  componentWillMount() {
    let that = this;
    that.unsubscribe_product_store = ProductsStore.listen(that.handleProductStoreChange.bind(that));
    that.setProducts(that.props);
  }
  componentWillUnmount() {
    this.unsubscribe_product_store();
  }

  handleProductStoreChange(trigger) {
    let that = this;
    let data = that.state.data, updated_data = [];
    let cancelled_data = that.state.cancelled_data, updated_cancelled = [];
    if (trigger.products) {
      let arr = trigger.products;
      //typeof (data[0]) === 'object'
      data.map((product) => {
        if (arr[product.productid]) {
          let p = ProductsStore.getProduct(product.productid);
          p.qty = product.qty;
          let price = isNaN(parseInt(p.amount.INR)) ? parseInt(p.amount.USD) : parseInt(p.amount.INR);
          p.totalCost = (price * parseInt(p.qty));
          //p.totalCost = (parseInt(p.price) * parseInt(product.qty));
          updated_data.push(p);
        } else {
          updated_data.push(product);
        }
      });
      cancelled_data.map((product) => {
        if (arr[product.productid]) {
          let p = ProductsStore.getProduct(product.productid);
          p.qty = product.qty;
          let price = isNaN(parseInt(p.amount.INR)) ? parseInt(p.amount.USD) : parseInt(p.amount.INR);
          p.totalCost = (price * parseInt(p.qty));
          //p.totalCost = (parseInt(p.price) * parseInt(product.qty));
          updated_cancelled.push(p);
        } else {
          updated_cancelled.push(product);
        }
      });
      let totalCost = that.getTotalCost(updated_data);
      that.setState({data: updated_data, cancelled_data: updated_cancelled, totalCost: totalCost, loading: false});
    }
  }

  setProducts(newProps) {
    let that = this;
    let x = [], totalCost = 0, cancelled = [], found = false;
    let props = newProps.data;
    if (props.list) {
      props.list.map(product => {
        if (typeof(ProductsStore.getProduct(product.productid)) === 'object') {
          let p = ProductsStore.getProduct(product.productid);
          p.qty = product.qty;
          let price = isNaN(parseInt(p.amount.INR)) ? parseInt(p.amount.USD) : parseInt(p.amount.INR);
          p.totalCost = (price * parseInt(p.qty));
          totalCost += (price * parseInt(p.qty));
          found = true;
          x.push(p);
        }else{
          let p = {};
          p.qty = product.qty;
          p.productid = product.productid;
          x.push(p);
        }
      });
      props.cancelledlist ? props.cancelledlist.map((product) => {
        if (typeof(ProductsStore.getProduct(product.productid)) === 'object') {
          let p = ProductsStore.getProduct(product.productid);
          p.qty = product.qty;
          let price = isNaN(parseInt(p.amount.INR)) ? parseInt(p.amount.USD) : parseInt(p.amount.INR);
          p.totalCost = (price * parseInt(p.qty));
          //p.totalCost = (parseInt(p.amount.INR) * parseInt(p.qty));
          //totalCost += (parseInt(p.price) * parseInt(p.qty));
          found = true;
          cancelled.push(p);
        } else{
          let p = {};
          p.qty = product.qty;
          p.productid = product.productid;
          cancelled.push(p);
        }
      }) : null;
    }
    if(found){
      that.setState({
        data: x,
        cancelled_data: cancelled,
        totalCost: `${totalCost}`,
        requests: props.requests ? props.requests : [],
        loading: false
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    let that = this;
    that.setProducts(nextProps);
  }

  getTotalCost(data) {
    let x = data;
    let totalCost = 0;
    x.map((product) => {
      let price = product.amount ? isNaN(parseInt(product.amount.INR)) ? parseInt(product.amount.USD) : parseInt(product.amount.INR) : 0;
      totalCost += (price * parseInt(product.qty || 1))
    });
    return `${totalCost}`;
  }

  saveProductCart() {
    let that = this;
    //let value = React.findDOMNode(that.refs.cartName).value;
    that.props.addProduct();
  }

  changeQuantity(index, e) {
    let that = this;
    let x = that.state.data;
    x[index]['qty'] = `${_.cloneDeep(e.target.value)}`;
    x[index]['totalCost'] = `${parseInt(x[index]['qty']) * parseInt(x[index]['price'])}`;
    that.setState({data: x, totalCost: that.getTotalCost(x)});
    that.props.onQuantityChange(x[index]['qty'], index);
  }

  showModal(value, data = {}) {
    this.setState({isModalOpen: value, previewProduct: data});
  }

  getProductItem(data, index, cancelled = false) {
    let that = this;
    let loading = that.state.loading;
    let pImageThumbUrl = Constants.LOADER_GIF;
    let pName = "Loading", pPrice = {}, pQty = "1", ptotalCost = "0", pFeatures = [], pDetails = false;

    if (!loading) {
      pImageThumbUrl = _.result(data,'displayImage.url', Constants.LOADER_GIF);
      pName = data.name;
      pPrice = _.result(data, 'amount',{INR: '0', USD: '0'});
      pQty = data.qty;
      ptotalCost = _.result(data,'totalCost','0');
      pDetails = _.result(data,'assets.models.length', false);
      pFeatures = [data.brand, data.material, data.colour];
    }
    return (
      <div key={index} className='productListItem'>
        <span>{index + 1}</span>

        <div className="productImgHolder">

          <div className="productImg" style={{backgroundImage:`URL(${pImageThumbUrl})`}}
               onClick={() => !loading && that.showModal(true, data)}>
          </div>
          <div className="productFeatures" onClick={() => !loading && that.showModal(true, data)}>
            <span className='productName'>{pName}</span>
            {pFeatures.map((x, index) => {
              if (x != "") return <li key={index}>{x}</li>; else return null;
            })}
            {pDetails && <div title='Modelled' className='productModelled'>
              <SvgIcon color={'green'}>
                <path
                  d='M21,16.5C21,16.88 20.79,17.21 20.47,17.38L12.57,21.82C12.41,21.94 12.21,22 12,22C11.79,22 11.59,21.94 11.43,21.82L3.53,17.38C3.21,17.21 3,16.88 3,16.5V7.5C3,7.12 3.21,6.79 3.53,6.62L11.43,2.18C11.59,2.06 11.79,2 12,2C12.21,2 12.41,2.06 12.57,2.18L20.47,6.62C20.79,6.79 21,7.12 21,7.5V16.5M12,4.15L6.04,7.5L12,10.85L17.96,7.5L12,4.15M5,15.91L11,19.29V12.58L5,9.21V15.91M19,15.91V9.21L13,12.58V19.29L19,15.91Z'/>
              </SvgIcon>
            </div>}
          </div>
          {/*<button title='Remove Item' onClick={that.props.removeItem.bind(that, index)} className='productRemove'>
           REMOVE
           </button>*/}
          <button title={cancelled ? 'Cancelled Item' : 'Cancel Item'}
                  onClick={() => that.props.cancelItem(index, !cancelled)} className='productCancel'>
            {cancelled ? <span style={{color:'#FF4500'}}>CANCELLED</span> :
              <span style={{color:'#2271b2'}}>CANCEL</span>}
          </button>
        </div>


        <div className="productQuantity">
          <input type='number' min='1' disabled={cancelled} onChange={that.changeQuantity.bind(that,index)} value={pQty}
                 className='quantityInput'/>
        </div>

        <div className="productPrice">
          <span>{pPrice.INR && `₹${pPrice.INR}`}</span>
          {pPrice.USD && <span className="separator">&nbsp;&nbsp;|&nbsp;&nbsp;</span>}
          <span>{pPrice.USD && `$${pPrice.USD}`}</span>
        </div>

        <div className="productTotalPrice">
          {pPrice.INR ? `₹ ${ptotalCost}` : `$ ${ptotalCost}`}
        </div>
        {/*<SvgIcon className='deleteBtn' hoverColor={'red'}>
         <path fill='#000000'
         d='M19,6.41L17.59,5L12,10.59L6.41,5L5,6.41L10.59,12L5,17.59L6.41,19L12,13.41L17.59,19L19,17.59L13.41,12L19,6.41Z'/>
         </SvgIcon>*/}
      </div>
    )
  }

  getRequestList() {
    let that = this;
    let request_list = that.state.requests;
    return (
      request_list.map((request, index) => {

        return (
          <div key={index} className='ProductCart-requests-item'>
            <span>{index + 1}.</span>

            <div className='status'>
              {request.done && <SvgIcon className='status-done'>
                <path d="M9 16.2L4.8 12l-1.4 1.4L9 19 21 7l-1.4-1.4L9 16.2z"/>
              </SvgIcon>}
              {request.cancelled && <button disabled="true" className="status-cancel">
                CANCELLED
              </button>}
            </div>
            <div className='request-list'>
              <div className='link'>
                <a href={request.url} target='_blank'>{request.url}</a>
              </div>
              <div className='itemtype'>{request.itemtype}</div>
              <div className='color'>{request.color}</div>
              <div className='note'>{request.note}</div>
              <div className='button-container'>
                <span className='time'>{Moment(request.created_at).format('h:mm a, L')}</span>
                {/*<button title='Remove Item' onClick={that.props.removeRequest.bind(that, index)} className='remove'>
                 REMOVE
                 </button>*/}
                <button title='Mark as Done' onClick={that.props.onDoneRequest.bind(that, index)} className='done'>
                  {request.done ? 'UNDONE' : 'DONE'}
                </button>
                <button title='Cancel' onClick={that.props.onCancelRequest.bind(that, index)} className='cancel'>
                  {request.cancelled ? 'UN-CANCEL' : 'CANCEL'}
                </button>
              </div>
            </div>
          </div>
        )
      })
    )
  }

  render() {
    let that = this;
    let data = that.state.data;
    let cancelled_data = that.state.cancelled_data;
    let CartProducts = data.map((product, index) => that.getProductItem(product, index));
    let CancelledProducts = cancelled_data.map((product, index) => that.getProductItem(product, index, true));
    let Requests = that.getRequestList();
    let totalCost = that.state.totalCost;
    let showCartChange = that.props.cartChange;
    let cartName = that.props.data.name;
    return (
      <div className='ProductCart'>
        <div className="ProductCart-body">
          <div className='ProductCart-name'>
            <div className='ProductCart-name-label'>Cart Name</div>
            <input ref='cartName' type='text' onChange={that.props.setName.bind(that)} placeholder='Enter Cart Name'
                   className='ProductCart-name-input'
                   defaultValue={cartName}/>
          </div>
          {/*showCartChange &&
          <h6 className='ProductCart-label'>Unsaved Changes in cart. Please save changes!
            <span className='saveBtn'>
              <button className='custom-primary-button' onClick={that.saveProductCart.bind(that)}>SAVE</button>
            </span>
          </h6>*/}

          <div className="ProductCart-products">
            <div className='ProductCart-products-header'>Products
              <div style={{ display:'inline-block',float:'right'}}>
                <button onClick={that.props.switchMenu.bind(that, 1)} className='ProductCart-requests-header-addBtn'>Add
                </button>
              </div>
            </div>
            <div className='ProductCart-list'>
              {CartProducts}
            </div>
            <div className='ProductCart-bottom-totalCost'>Total: <span
              style={{fontWeight:'bold'}}>{totalCost}</span>
            </div>
            <div className='ProductCart-products-header'>Cancelled Products
            </div>
            <div className='ProductCart-list'>
              {CancelledProducts}
            </div>
          </div>

          <div className="ProductCart-requests">
            <div className="ProductCart-requests-header">
              <span>Requests</span>
              <button onClick={that.props.switchMenu.bind(that, 2)} className='ProductCart-requests-header-addBtn'>Add
              </button>
            </div>
            <div className='ProductCart-requests-body'>
              {Requests}
            </div>
          </div>
        </div>
        <div className='ProductCart-saveBtn'>
          <button className='custom-primary-button' onClick={that.saveProductCart.bind(that)}>SAVE</button>
        </div>
        <CustomModal style={{top:'5%',left:'15%',width:'70%',height:'75%'}} isOpen={that.state.isModalOpen}
                     onClose={that.showModal.bind(that, false)}>
          <ProductItemView data={that.state.previewProduct} closeModal={that.showModal.bind(that,false)}/>
        </CustomModal>
      </div>
    )
  }
}
export default ProductCart;
