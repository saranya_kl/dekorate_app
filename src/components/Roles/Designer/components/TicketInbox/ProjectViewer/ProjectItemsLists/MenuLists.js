/**
 * Created by vikramaditya on 11/26/15.
 */
/**
 * Created by vikramaditya on 7/21/15.
 */
import React from 'react';
import mui from 'material-ui';
import _ from 'lodash';
import Helper from '../../../../../../helper.js';
import Svg from '../../../../../../Shared/Svg/Svg';
import styles from './MenuLists.scss';
var {AppConstants: Constants, withStyles, DRPStore, CustomUtility} = Helper;

let UNREAD_STATUS = '01';
let READ_STATUS = '11';
let NONE_STATUS = '00';

@withStyles(styles)
class MenuLists extends React.Component {
  constructor(props) {
    super(props);
    let that = this, feedbackMap = {};
    props.drp_data.map((id) => {
      let feedback = _.result(DRPStore.getDesignResponse(id),'attributes.userfeedback',[]);
      feedbackMap[id] = feedback.length;
    });
    that.state = {
      drp_feedback_map: feedbackMap
    };
    that.unsubscribe_drp_store = DRPStore.listen(that.handleDesignResponse.bind(that));
  }
  componentWillUnmount(){
    this.unsubscribe_drp_store();
  }
  handleDesignResponse(trigger){
    let that = this, feedbackMap = {};
    if(trigger.drp){
      that.props.drp_data.map((id) => {
        let feedback = _.result(DRPStore.getDesignResponse(id),'attributes.userfeedback',[]);
        feedbackMap[id] = feedback.length;
      });
      that.setState({drp_feedback_map: feedbackMap});
    }
    if(trigger.error){
      console.log('Error in Menu List');
    }
  }
  changeMenu(index, id) {
    this.props.onClick(index, id);
  }
  render() {
    let self = this;
    let selectedStyle = {backgroundColor: '#e6e6e6', borderBottom:'2px solid', color: '#808080', fontWeight: '400'},
      defaultStyle = {backgroundColor: '#f2f2f2', color: '#b3b3b3'},
      drp_data = _.cloneDeep(self.props.drp_data), drafts = _.cloneDeep(self.props.drafts),
      currentIndex = self.props.current, projectStatus = CustomUtility.getProjectStatus(self.props.project),
      showWorkspace = projectStatus == Constants.PROJECT_STATUS.WORKING || projectStatus == Constants.PROJECT_STATUS.DESIGN_SENT || projectStatus == Constants.PROJECT_STATUS.CLOSED || projectStatus == Constants.PROJECT_STATUS.ASSIGNED;
    return (
      <div className = 'ProjectMenuLists'>
        <div className='ProjectMenuLists-menuRow'>
          <button className='ProjectMenuLists-menuItemStyle' style={currentIndex == -3 ? selectedStyle: defaultStyle} onClick={self.changeMenu.bind(self, -3)}>Overview</button>
          <button className='ProjectMenuLists-menuItemStyle' style={currentIndex == 0 ? selectedStyle : defaultStyle} onClick={self.changeMenu.bind(self, 0)}>Submission</button>
          <div className='ProjectMenuLists-menuRow-designResponseList'>
            {drp_data.map((item, index) => {
              let feedback_exists = self.state.drp_feedback_map[item];
              return (
                <button className='ProjectMenuLists-menuItemStyle' onClick={self.changeMenu.bind(self,index+1,item)} style={index + 1 == currentIndex ? selectedStyle : defaultStyle} key={index}>
                  <span>Design -{index + 1}</span>
                  {/*<span className='ProjectMenuLists-menuItemStyle-bottomNotification'>3 new comments</span>*/}
                  {feedback_exists ? <Svg iconType={'cam-icon'} style={{position:'absolute',width:'15px',height:'15px',top:'33%',left:'90%',fill: '#808080', zoom:'0.8', margin:'2px 7px 2px -9px'}}/> : ''}
                </button>
              )})
            }
            {drafts.map((id, index) => {
              return (
                <button className='ProjectMenuLists-menuItemStyle' onClick={self.changeMenu.bind(self,drp_data.length+index+1, id)} style={currentIndex == (drp_data.length+index+1) ? selectedStyle : defaultStyle} key={index}>Draft -{index + 1}</button>
              )})
            }
          </div>
        </div>
        { showWorkspace && <div className="ProjectMenuLists-rightLists">
          <button className='ProjectMenuLists-menuItemStyle' style={(currentIndex == -2) ? selectedStyle: defaultStyle}
                  onClick={self.changeMenu.bind(self,-2)}>Workspace</button>
        </div>}
      </div>
    );
  }
}
export default MenuLists;
