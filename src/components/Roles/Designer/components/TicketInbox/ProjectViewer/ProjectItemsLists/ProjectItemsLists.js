/**
 * Created by vikramaditya on 7/21/15.
 */
import React from 'react';
import mui from 'material-ui';
import _ from 'lodash';
import Helper from '../../../../../../helper.js';
import styles from './ProjectItemsLists.scss';
var {AppConstants: Constants, withStyles, DRPStore} = Helper;
var {SvgIcon} = mui;

let UNREAD_STATUS = '01';
let READ_STATUS = '11';
let NONE_STATUS = '00';

@withStyles(styles)
class ProjectItemsLists extends React.Component {
  constructor() {
    super();
    let that = this;
    this.state = {
      all_drp_ids: {},
      unread_comments: {},
      drp_feedback_map: {}
    };
    that.unsubscribe_drp_store = DRPStore.listen(that.handleDesignResponse.bind(that));
  }
  componentWillUnmount(){
    this.unsubscribe_drp_store();
  }
  shouldComponentUpdate(nextProps, nextState) {
    let x = (nextProps.drp_data !== this.props.drp_data);
    let y = (nextProps.current !== this.props.current);
    let z = (nextState.drp_feedback_map !== this.state.drp_feedback_map);
    //console.log(x|y|z);
    return (x | y | z == 1);
  }
  componentWillReceiveProps(nextProps){
    if(nextProps.unread_status !== this.props.unread_status || ! _.isEqual(nextProps.unread_comments, this.state.unread_comments)){
      this.setState({all_drp_ids : nextProps.unread_status, unread_comments : _.cloneDeep(nextProps.unread_comments)});
      this.forceUpdate();
    }
  }
  handleDesignResponse(trigger){
    let that = this, x = {};
    if(trigger.drp){
      that.props.drp_data.map((id) => {
        let feedback = _.result(DRPStore.getDesignResponse(id),'attributes.userfeedback',[]);
        x[id] = feedback.length;
      });
      that.setState({drp_feedback_map: x});
    }
    if(trigger.error){
      console.log('some error in project item list');
    }
  }
  changeMenu(index, id) {

    //if(fd_id && this.state.all_drp_ids[fd_id] && this.state.all_drp_ids[fd_id] != READ_STATUS){
    //  var x = this.state.all_drp_ids;
    //  x[fd_id] = READ_STATUS;
    //  this.setState({all_drp_ids : x});
    //  ProjectActions.READ_DOCUMENT(fd_id);
    //}
    this.props.onClick(index, id);
  }

  render() {
    var self = this;
    var buttonStyle = {
      backgroundColor: '#f2f2f2',
      color: '#b3b3b3',
      fontSize: '0.7rem',
      outline: 'none',
      width: '80px',
      fontFamily: 'Lato',
      border: 'none',
      borderBottom:'none'
    };
    var selectedButtonStyle = {
      backgroundColor: '#f2f2f2',
      border: 'none',
      color: '#808080',
      fontSize: '0.7rem',
      fontWeight: '400',
      fontFamily: 'Lato',
      outline: 'none',
      width: '80px',
      borderBottom:'2px solid'
    };
    let drp_data = _.cloneDeep(self.props.drp_data);
    let draft_data = _.cloneDeep(self.props.drafts);
    //that.renderCss( `.msgActionIcon {margin-left: ${iconSpacing}rem;}`, true );
    return (
      <div className = 'project-items-lists'>
        <div style={{display:'flex',flexFlow:'row nowrap',justifyContent:'flex-start'}}>
          {<button style={(self.props.current == -3) ? selectedButtonStyle: buttonStyle}
                   onClick={self.changeMenu.bind(self,-3,undefined)}>Overview
          </button>}
          {self.props.current == 0 ? <button style={selectedButtonStyle}>Submission</button> :
            <button style={buttonStyle} onClick={self.changeMenu.bind(self,0,undefined)}>Submission</button>}
          <div style={{display:'flex',flexFlow:'row',overflowX:'auto',maxWidth:'560px'}}>
            {drp_data.length > 0 ? drp_data.map(function (item, index) {
              let feedback_exists = self.state.drp_feedback_map[item];
              var buttonStyle = {};
              if (index + 1 == self.props.current) {
                buttonStyle = {
                  backgroundColor: '#e6e6e6',
                  border: 'none',
                  color: '#808080',
                  fontSize: '0.7rem',
                  fontFamily: 'Lato',
                  fontWeight: '400',
                  position: 'relative',
                  outline: 'none',
                  minWidth: '80px',
                  borderBottom:'2px solid'
                };
              } else {
                buttonStyle = {
                  backgroundColor: '#f2f2f2',
                  border:'none',
                  color: '#b3b3b3',
                  fontSize: '0.7rem',
                  fontFamily: 'Lato',
                  position: 'relative',
                  outline: 'none',
                  minWidth: '80px'
                };
              }
              let drp_read_status = self.state.all_drp_ids[item] ? self.state.all_drp_ids[item] : READ_STATUS; // no use currently !!
              let fd_id = (self.props.fd_data[item] && self.props.fd_data[item].length != 0) ? self.props.fd_data[item][0].id : undefined;
              let fd_read_status = fd_id ? self.state.all_drp_ids[fd_id] ? self.state.all_drp_ids[fd_id] : READ_STATUS : NONE_STATUS;
              let fd_color = fd_read_status == NONE_STATUS ? 'transparent' : (fd_read_status == READ_STATUS) ? '#808080' : 'green';
              let current_color = self.props.current == index+1 ? '#808080' : 'rgb(179, 179, 179);';
              return (
                <button onClick={self.changeMenu.bind(self,index+1,item)} style={buttonStyle} key={index}>
                  <span>Design -{index + 1}</span>
                  {feedback_exists ? <SvgIcon
                    style={{position:'absolute',width:'15px',height:'15px',top:'33%',left:'90%',fill: '#808080', zoom:'0.8', margin:'2px 7px 2px -9px'}}>
                    <path
                      d="M17,10.5V7A1,1 0 0,0 16,6H4A1,1 0 0,0 3,7V17A1,1 0 0,0 4,18H16A1,1 0 0,0 17,17V13.5L21,17.5V6.5L17,10.5Z" />
                  </SvgIcon> : ''}
                  {
                    function () {
                      var unread = 0;
                      self.state.unread_comments[item] && Object.keys(self.state.unread_comments[item]).map((key) => {
                        if (self.state.unread_comments[item][key] == UNREAD_STATUS) {
                          unread++;
                        }
                      });
                      return unread > 0 && <span className='comment-notification'> {unread} new comment</span>
                    }()
                  }
                </button>
              );
            }) : ''
            }
          </div>
          {
            this.props.drafts.map((id, index)=>{
              return(
                <button onClick={self.changeMenu.bind(self,drp_data.length+index+1, id)} style={self.props.current == (drp_data.length+index+1) ? selectedButtonStyle : buttonStyle} key={index}>Draft -{index + 1}</button>
              )
            })
          }
        </div>
        { self.props.project.attributes && (self.props.project.attributes.meta.isworking || !self.props.project.attributes.meta.isopen) &&
        <div style={{display:'flex',flexFlow:'row nowrap', justifyContent:'center'}}>
          <button style={(self.props.current == -2) ? selectedButtonStyle: buttonStyle}
                  onClick={self.changeMenu.bind(self,-2,undefined)}>Workspace
          </button>
          {/*<button style={(self.props.current == -1) ? selectedButtonStyle: buttonStyle}
                  onClick={self.changeMenu.bind(self,-1,undefined)}>
            <SvgIcon style={(self.props.current == -1) ? {fill: '#808080', zoom:'0.8', margin:'2px 7px 2px -9px'}: {fill: '#b3b3b3', zoom:'0.8', margin:'2px 7px 2px -9px'}}>
              <path className='icon' d={Constants.ICONS.plus_icon}></path>
            </SvgIcon>Upload
          </button>*/}
        </div>}
      </div>
    );
  }
}
export default ProjectItemsLists;
