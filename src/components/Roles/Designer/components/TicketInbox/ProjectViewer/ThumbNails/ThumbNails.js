/**
 * Created by vikramaditya on 7/21/15.
 */
import React from 'react';
import style from './ThumbNails.scss';
import {SvgIcon} from 'material-ui';
import Helper from '../../../../../../helper.js';
var {withStyles} = Helper;

@withStyles(style)
class ThumbNails extends React.Component {
  constructor() {
    super();
    this.state = {
      index:0
    };
  }
  handleClick(index){
    this.props.onClick(index);
  }
  render(){
    var self = this;
    return (
      <div style={{backgroundColor:'#e6e6e6'}}>
        <div className='thumbnail-container'>
          { self.props.data.length > 0 ? self.props.data.map(function (submission, index) {
            if(index == self.props.current){
              self.currentThumbNailStyle = {border: '1px solid #4c98d8',marginLeft: '10px'};
            }else{
              self.currentThumbNailStyle = {marginLeft: '10px'};
            }
            return (
              <div key={index} onClick={self.handleClick.bind(self,index)} style={{position: 'relative'}}>
                <img src={submission.url ? submission.url : submission.imageUrl.url ? submission.imageUrl.url : submission.imageUrl} height={64} width={64} style={self.currentThumbNailStyle}/>
                {(index == self.props.current) && <div className="selector"></div>}
                { submission.audioUrl && submission.audioUrl != 'nil' ?
                  <div style={{position:'absolute', top:'20px', left:'32px', backgroundColor:'rgba(0,0,0,0.7)', borderRadius:'2px'}}>
                    <SvgIcon style={{fill:'white'}}>
                      <path d="M8 5v14l11-7z"/>
                    </SvgIcon>
                  </div>:''
                }
              </div>
            );
          }, this)
            :''}
        </div>
      </div>
    );
  }
}
export default ThumbNails;
