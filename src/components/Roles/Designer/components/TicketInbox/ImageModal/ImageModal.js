import React from 'react';
import mui from 'material-ui';
import Helper from '../../../../../helper.js';

var {Dialog,SvgIcon} = mui;
var {AppConstants: Constants} = Helper;

//@withStyles(styles)
class ImageModal extends React.Component {

  static contextTypes = {
    router: React.PropTypes.func,
    muiTheme: React.PropTypes.object
  };

  constructor() {

    super();
    this.state = {
      modal: false,
      index1: 0,
      index2: 0,
      left: false,
      right: false,
      image: ''
    };
  }

  componentWillMount() {
    window.addEventListener('keydown', this.KeyPress.bind(this), false);
    if (this.props.url.constructor === String) {
      var i = this.props.url;
      this.setState({image: i});
    }
    else if (this.props.url.constructor === Array) {
      if (this.props.url[0].constructor === Array) {
        if (this.props.url[this.state.index2].length === 1) {
          this.setState({image: this.props.url[this.state.index2][0]});
        }
        else {
          this.setState({image: this.props.url[this.state.index2][this.state.index1], right: true});
        }

      }
      else if (this.props.url[0].constructor === String) {
        if (this.props.url.length === 1) {
          this.setState({image: this.props.url[0]});
        }
        else {
          this.setState({image: this.props.url[this.state.index1], right: true});
        }
      }
    }
  }

  onDismiss() {
    this.props.onDismiss();
  }

  clickLeft() {

    if (this.props.url.constructor === Array) {
      if (this.props.url[0].constructor === Array) {
        if ((this.state.index1 >= 1) && (this.state.index1 < this.props.url[this.state.index2].length)) {
          var ind = this.state.index1 - 1,
            l = (ind >= 1) && (ind < this.props.url[this.state.index2].length),
            r = (ind >= 0) && (ind < this.props.url[this.state.index2].length - 1);
          this.setState({index1: ind, left: l, right: r, image: this.props.url[this.state.index2][ind]});
        }
      }
      else if (this.props.url[0].constructor === String) {

        if ((this.state.index1 >= 1) && (this.state.index1 < this.props.url.length)) {
          var ind = this.state.index1 - 1,
            l = (ind >= 1) && (ind < this.props.url.length),
            r = (ind >= 0) && (ind < this.props.url.length - 1);
          this.setState({index1: ind, left: l, right: r, image: this.props.url[ind]});
        }
      }
    }
  }

  clickRight() {
    if (this.props.url.constructor === Array) {
      if (this.props.url[0].constructor === String) {
        if ((this.state.index1 >= 0) && (this.state.index1 < this.props.url.length - 1)) {
          var ind = this.state.index1 + 1,
            l = (ind >= 1) && (ind < this.props.url.length),
            r = (ind >= 0) && (ind < this.props.url.length - 1);
          this.setState({index1: ind, left: l, right: r, image: this.props.url[ind]});
        }
      }
      else if (this.props.url[0].constructor === Array) {
        if ((this.state.index1 >= 0) && (this.state.index1 < this.props.url[this.state.index2].length - 1)) {
          var ind = this.state.index1 + 1,
            l = (ind >= 1) && (ind < this.props.url[this.state.index2].length),
            r = (ind >= 0) && (ind < this.props.url[this.state.index2].length - 1);
          this.setState({index1: ind, left: l, right: r, image: this.props.url[this.state.index2][ind]});
        }
      }
    }
  }

  KeyPress(e) {
    e.preventDefault();

    if (e.keyCode === 37) {
      if (this.props.url.constructor === Array) {
        if (this.props.url[0].constructor === Array) {
          if ((this.state.index1 >= 1) && (this.state.index1 < this.props.url[this.state.index2].length)) {
            var ind = this.state.index1 - 1,
              l = (ind >= 1) && (ind < this.props.url[this.state.index2].length),
              r = (ind >= 0) && (ind < this.props.url[this.state.index2].length - 1);
            this.setState({index1: ind, left: l, right: r, image: this.props.url[ind]});
          }
        }
        else if (this.props.url[0].constructor === String) {
          if ((this.state.index1 >= 1) && (this.state.index1 < this.props.url.length)) {
            var ind = this.state.index1 - 1,
              l = (ind >= 1) && (ind < this.props.url.length),
              r = (ind >= 0) && (ind < this.props.url.length - 1);
            this.setState({index1: ind, left: l, right: r, image: this.props.url[ind]});
          }
        }
      }

    }
    else if (e.keyCode === 39) {
      if (this.props.url.constructor === Array) {
        if (this.props.url[0].constructor === String) {
          if ((this.state.index1 >= 0) && (this.state.index1 < this.props.url.length - 1)) {
            var ind = this.state.index1 + 1,
              l = (ind >= 1) && (ind < this.props.url.length),
              r = (ind >= 0) && (ind < this.props.url.length - 1);
            this.setState({index1: ind, left: l, right: r, image: this.props.url[ind]});
          }
        }
        else if (this.props.url[0].constructor === Array) {
          if ((this.state.index1 >= 0) && (this.state.index1 < this.props.url[this.state.index2].length - 1)) {
            var ind = this.state.index1 + 1,
              l = (ind >= 1) && (ind < this.props.url[this.state.index2].length),
              r = (ind >= 0) && (ind < this.props.url[this.state.index2].length - 1);
            this.setState({index1: ind, left: l, right: r, image: this.props.url[this.state.index2][ind]});
          }
        }
      }

    }
    else if (e.keyCode === 38) {
      if (this.props.url[0].constructor === Array) {
        if ((this.state.index2 > 0) && (this.state.index2 <= this.props.url.length - 1)) {
          var i = this.state.index2 - 1,
            l = (ind >= 1) && (ind < this.props.url[this.state.index2].length),
            r = (ind >= 0) && (ind < this.props.url[this.state.index2].length - 1);
          this.setState({index2: i, index1: 0, left: l, right: r, image: this.props.url[i][0]});
        }
      }


    }
    else if (e.keyCode === 40) {
      if (this.props.url[0].constructor === Array) {
        if (( (this.state.index2 >= 0) && (this.state.index2 < this.props.url.length - 1)) && (this.props.url[this.state.index2 + 1][0])) {
          var i = this.state.index2 + 1,
            l = (ind >= 1) && (ind < this.props.url[this.state.index2].length),
            r = (ind >= 0) && (ind < this.props.url[this.state.index2].length - 1);
          this.setState({index2: i, index1: 0, left: l, right: r, image: this.props.url[i][0]});
        }
      }
    }
  }

  render() {
    let self = this;
    var w = window.innerWidth / 2,
      h = window.innerHeight / 2;
    return (
      <div style={{position:'relative'}}>

        <Dialog
          ref="imgmodal"
          openImmediately={true}
          autoScrollBodyContent={true}
          bodyStyle={{padding: '0',width:{w},height:{h}}}
          onDismiss={self.onDismiss.bind(self)}>
          <div>
            <img src={self.state.image} onKeyDown={self.KeyPress.bind(self)}
                 style={{maxWidth:'90vw', height: '100%', width:'100%',maxHeight:'95vh',transform: 'scale(1)',backgroundSize:'cover'}}/>
            <SvgIcon className='cross-icon'
                     style={{fill:'black', zoom:'0.75', position: 'absolute', top: '10px', right: '10px',cursor:'pointer'}}
                     onClick={self.onDismiss.bind(self)}>
              <path d={Constants.ICONS.close_icon}></path>
            </SvgIcon>
            {
              self.state.left ? <SvgIcon
                style={{fill:'black', zoom:'0.8', position: 'absolute', top: '50%', left: '10px',cursor:'pointer'}}
                onClick={self.clickLeft.bind(self)}>
                <path d="M15.41 16.09l-4.58-4.59 4.58-4.59L14 5.5l-6 6 6 6z"/>
              </SvgIcon> : ''
            }
            {
              self.state.right ? <SvgIcon
                style={{fill:'black', zoom:'0.8', position: 'absolute', top: '50%', right: '10px',cursor:'pointer'}}
                onClick={self.clickRight.bind(self)}>
                <path d="M8.59 16.34l4.58-4.59-4.58-4.59L10 5.75l6 6-6 6z"/>
              </SvgIcon> : ''
            }
          </div>
        </Dialog>
      </div>
    );
  }
}
export default ImageModal;
