/**
 * Created by vikramaditya on 7/15/15.
 */
//TODO Make and use this component wherever required
import React from 'react';
import mui from 'material-ui';
import Helper from '../../../../../helper.js';
import styles from './PasswordModal.scss';
import CustomModal from '../../../../../../lib/Modal';
var {withStyles} = Helper;
var {Dialog,TextField} = mui;

let ModalConstants = {
  CANCEL: 'CANCEL',
  SUBMIT: 'SUBMIT'
};
@withStyles(styles) class PasswordModal extends React.Component {

  constructor() {
    super();
    this.state = {
      showModal: false
    }
  }

  DialogAction(action) {
    if (action == ModalConstants.SUBMIT) {
      //this.refs.dialog.dismiss();
      this.props.onAction(ModalConstants.SUBMIT);
    }
    if (action == ModalConstants.CANCEL) {
      //this.refs.dialog.dismiss();
      this.props.onAction(ModalConstants.CANCEL);
    }
  }

  getValue() {
    return React.findDOMNode(this.refs.password).value;
  }
  dismiss(){
    this.setState({showModal: false});
  }
  show(){
    this.setState({showModal: true});
  }
  render() {
    let isOpen = this.state.showModal;
    return (
      <div className="PasswordModal">
        <CustomModal isOpen={isOpen} onClose={this.DialogAction.bind(this,ModalConstants.CANCEL)}
                     style={{top:'35%', height:'30%',left:'20%', width:'60%', borderRadius: '3px',padding:'0'}}>
          <div className="flex-col">
            <h6 className="input-form-title"> Enter Password </h6>

            <div className="PasswordModal-content">
              <span className="custom-input-label"> Password </span>
              <input ref='password' type='password' placeholder='Password' className="PasswordModal-input"/>
            </div>
            <div className="input-divider"></div>
            <div className="button-wrapper">
              <button className="submit" onClick={this.DialogAction.bind(this,ModalConstants.SUBMIT)}> Submit</button>
              <button className="cancel" onClick={this.DialogAction.bind(this,ModalConstants.CANCEL)}>Cancel</button>
            </div>
          </div>
        </CustomModal>
      </div>
    )
  }
}
export default PasswordModal;
