/**
 * Created by vikramaditya on 7/6/15.
 */
import React from 'react';
import Router from 'react-router';
import mui from 'material-ui';
import Moment from 'moment';

import Helper from '../../../../helper.js';
import styles from './TicketInbox.scss';
import NewProjectsList from '../../../../Shared/NewProjectsList/NewProjectsList.js';

var {TicketsStore, TicketsActions, withStyles, LoginStore,  NotificationTypes, NotificationStore, ResourceConstants, AppConstants: Constants, CustomUtility} = Helper;
const ProjectTabs = {
  'My': 'My',
  'All' : 'All',
  'Closed': 'Closed',
  'Closed(A)': 'Closed(A)'
};

@withStyles(styles) class TicketInbox extends React.Component {

  static contextTypes = {
    router: React.PropTypes.func,
    muiTheme: React.PropTypes.object
  };

  constructor(props) {
    super(props);
    this.state = {
      allUsers: {},
      newProjects: new Map(),
      myProjects: new Map(),
      myClosedProjects: new Map(),
      allClosedProjects: new Map(),
      showQuiz: false,
      expanded: false,
      pid: '',
      currentTab: ProjectTabs.My,
      searchTerm: '',
      isLoading: !this.checkStore()
    };
  }

  componentWillMount() {
    let that = this;
    that.unsubscribe_ticketstore = TicketsStore.listen(that.handleTicketStore.bind(that));
    //that.unsubscribe_notification = NotificationStore.listen(that.onNotification.bind(that));
    if(!that.checkStore()){
      that.fetchAllProjects();
    }else{
      that.setProjects();
    }
  }
  setProjects(){
    let that = this;
    let newProjects = TicketsStore.getAllProjects();
    let myProjects = TicketsStore.getMyProjects();
    let myClosedProjects = TicketsStore.getMyClosedProjects();
    let allClosedProjects = TicketsStore.getAllClosedProjects();
    that.setState({newProjects: newProjects, myProjects: myProjects, myClosedProjects: myClosedProjects, allClosedProjects: allClosedProjects});
  }
  checkStore() {
    let w = TicketsStore.getAllProjects().size,
      x = TicketsStore.getMyProjects().size,
      y = TicketsStore.getMyClosedProjects().size,
      z = TicketsStore.getAllClosedProjects().size;
    return (w || x || y || z);
  }

  fetchAllProjects(){
    let showAllProjects = LoginStore.checkPermission(ResourceConstants.Projects_ShowAllProjects);
    let ROLE = LoginStore.getCurrentDashboard();
    if(showAllProjects){
      TicketsActions.fetchAllProjects();
      TicketsActions.fetchMyProjects(LoginStore.getUserID(), ROLE);
      TicketsActions.fetchMyClosedProjects(LoginStore.getUserID(), ROLE);
      TicketsActions.fetchAllClosedProjects(LoginStore.getUserID(), ROLE);// for all closed projects
    }else{
      TicketsActions.fetchMyProjects(LoginStore.getUserID(), ROLE);
      TicketsActions.fetchMyClosedProjects(LoginStore.getUserID(), ROLE);
    }
  }
  componentWillUnmount() {
    this.unsubscribe_ticketstore();
    //this.unsubscribe_notification();
  }
  onNotification(props){
    let ROLE = LoginStore.getCurrentDashboard();
    if (props.type == NotificationTypes.PROJECT_ASSIGNED) {
      TicketsActions.fetchMyProjects(LoginStore.getUserID(), ROLE);
    }
    if (props.type == NotificationTypes.PROJECT_CLOSE) {
      this.fetchAllProjects();
    }
    if (props.type == NotificationTypes.PROJECT_NEW) {
      TicketsActions.fetchAllProjects();
    }
  }
  handleTicketStore(trigger){
    let that = this;
    if(trigger.all_projects){
      let newProjects = TicketsStore.getAllProjects();
      that.setState({newProjects: newProjects, isLoading: false});
    }
    if(trigger.my_projects){
      let myProjects = TicketsStore.getMyProjects();
      that.setState({myProjects: myProjects, isLoading: false});
    }
    if(trigger.my_closed_projects){
      let myClosedProjects = TicketsStore.getMyClosedProjects();
      that.setState({myClosedProjects: myClosedProjects, isLoading: false});
    }
    if(trigger.all_closed_projects){
      let myClosedProjects = TicketsStore.getAllClosedProjects();
      that.setState({allClosedProjects: myClosedProjects, isLoading: false});
    }
  }

  onProjectClick(pid, type) {
    this.context.router.transitionTo(`/${LoginStore.getCurrentDashboard()}/projects/${pid}`);
    this.props.showProject(true, pid, type);
  }

  changeTab(tab) {
    this.setState({currentTab: tab, searchTerm: ''});
  }
  onSearchProject(currentTab, e) {
    let value = (e.target.value).toLowerCase();

    function isEqual(obj) {
      let project = obj[1];
      let name = (`${project.relationship.owner.name}`).toLowerCase();
      let title = (`${project.attributes.title}`).toLowerCase();
      let id = (`${project.id}`).toLowerCase();
      let project_status = (`${CustomUtility.getProjectStatus(project)}`).toLowerCase();
      if (name.indexOf(value) >= 0 || title.indexOf(value) >= 0 || id.indexOf(value) >= 0 || project_status.indexOf(value) >= 0) {
        return true;
      }
      return false;
    }
    let x = [];
    x = (currentTab == ProjectTabs.My) ? (this.mapToArray(TicketsStore.getMyProjects())) : x;
    x = (currentTab == ProjectTabs.All) ? this.mapToArray(TicketsStore.getAllProjects()) : x;
    x = (currentTab == ProjectTabs.Closed) ? this.mapToArray(TicketsStore.getMyClosedProjects()) : x;
    x = (currentTab == ProjectTabs['Closed(A)']) ? this.mapToArray(TicketsStore.getAllClosedProjects()) : x;
    let filtered = new Map(x.filter(isEqual));
    this.setState({
      myProjects: (currentTab == ProjectTabs.My) ? filtered :  TicketsStore.getMyProjects(),
      newProjects: (currentTab == ProjectTabs.All) ? filtered :  TicketsStore.getAllProjects(),
      myClosedProjects: (currentTab == ProjectTabs.Closed) ? filtered :  TicketsStore.getMyClosedProjects(),
      allClosedProjects: (currentTab == ProjectTabs['Closed(A)']) ? filtered :  TicketsStore.getAllClosedProjects(),
      searchTerm: e.target.value
    });
  }
  mapToArray(map){
    let arr = [];
    for(let x of map){
      arr.push(x);
    }
    return arr;
  }
  getProjectCard(p, index) {
    let project_status = (`${CustomUtility.getProjectStatus(p)}`);
    let status_color = '#000000', that = this;
    switch (project_status) {
    case Constants.PROJECT_STATUS.UNASSIGNED:
      status_color = '#f35d54';
      break;
    case Constants.PROJECT_STATUS.ASSIGNED:
      status_color = '#feb054';
      break;
    case Constants.PROJECT_STATUS.WORKING:
      status_color = '#6e92cc';
      break;
    case Constants.PROJECT_STATUS.CLOSED:
      status_color = '#82dcd4';
      break;
    case Constants.PROJECT_STATUS.DESIGN_SENT:
      status_color = '#512398';
      break;
    default:
      status_color = '#000000';
    }
    let time = Moment(p.attributes.meta.created_at).format('MMM Do');
    return (
      <div className="ProjectCard" key={index} onClick={that.onProjectClick.bind(that, index)}>
        <div className="ProjectCard-maincard">
          <div className="ProjectCard-header">
            <span className="ProjectCard-header-title">{p.attributes.title}</span>
            <span className="ProjectCard-header-time">{time.substring(0,time.length-2)}</span>
          </div>
          <img src={p.attributes.meta.displayThumb} className="ProjectCard-image" />
          <div title={project_status} className="ProjectCard-user">
            <div className="ProjectCard-user-wrapper">
              <img src={p.relationship.owner.imageUrl} className="ProjectCard-user-image"/>
              <div className="ProjectCard-user-name">{p.relationship.owner.name}</div>
            </div>
            <div className="ProjectCard-user-status" style={{backgroundColor:status_color}}></div>
          </div>
        </div>
      </div>
    )
  }
  getTabList(items, currentTab) {
    let flex_row = {
      display: 'flex',
      flexFlow: 'row',
      fontSize: '0.7rem',
      minHeight: '44px',
      fontFamily:'Lato',
      alignItems: 'center',
      borderRight: 'none',
      padding: '0'
    };
    let currentStyle = {
      color: 'black',
      backgroundColor: '#f2f2f2',
      fontWeight:'400'
    };
    let buttonStyle = {
      backgroundColor: '#fff',
      color: '#999'
    };
    let spanStyle={
      color:'white'
    };
    return (
      <div style={flex_row} className="TabList">
        {items.map((item, index) => {
          let length = 0;
          switch (item){
          case ProjectTabs.My:
            length = this.state.myProjects.size;
            break;
          case ProjectTabs.All:
            length = this.state.newProjects.size;
            break;
          case ProjectTabs.Closed:
            length = this.state.myClosedProjects.size;
            break;
          default:
            length = this.state.allClosedProjects.size;
            break;
          }
          return (
            <button key={index} onClick={this.changeTab.bind(this, item)} className="TabList-tab"
                    style={item == currentTab ? currentStyle : buttonStyle}>{item}
              { length > 0 && <span className="TabList-length">{length}</span>}
            </button>
          )
        })}
      </div>
    )
  }

  render() {
    let self = this;
    let showAllTabs = LoginStore.checkPermission(ResourceConstants.Projects_ShowAllProjects);
    let Tabs = showAllTabs ?
      this.getTabList([ProjectTabs.My, ProjectTabs.All, ProjectTabs.Closed, ProjectTabs['Closed(A)']], this.state.currentTab) :
      this.getTabList([ProjectTabs.My, ProjectTabs.Closed], this.state.currentTab);
    let MyProjects=[], AllProjects = [], MyClosedProjects=[], AllClosedProjects = [];
    self.state.myProjects.forEach((project, key) => MyProjects.push(self.getProjectCard(project, key)));
    self.state.newProjects.forEach((project, key) => AllProjects.push(self.getProjectCard(project, key)));
    self.state.myClosedProjects.forEach((project, key) => MyClosedProjects.push(self.getProjectCard(project, key)));
    self.state.allClosedProjects.forEach((project, key) => AllClosedProjects.push(self.getProjectCard(project, key)));
    let Loading = self.state.isLoading;
    return (
      <div className='TicketInbox'>
        <div className='TicketInbox-mainContent'>
          {Tabs}
          <div className='TicketInbox-searchBox'>
            <input placeholder='Search project by id, title, owner name or status' type='text' value={self.state.searchTerm} onChange={self.onSearchProject.bind(self, self.state.currentTab)}/>
          </div>
          {Loading ? <h1 style={{textAlign:'center'}}>Loading Projects..</h1> :
          <div className="TicketInbox-list">
            {self.state.currentTab == ProjectTabs.My && <NewProjectsList type={ProjectTabs.My} pid={self.props.pid} expanded={self.props.expanded} data={self.state.myProjects}
                                                                         onClick={self.onProjectClick.bind(self)}/>}
            {self.state.currentTab == ProjectTabs.All && <NewProjectsList type={ProjectTabs.All} pid={self.props.pid} expanded={self.props.expanded} data={self.state.newProjects}
                                                                          onClick={self.onProjectClick.bind(self)}/>}
            {self.state.currentTab == ProjectTabs.Closed && <NewProjectsList type={ProjectTabs.Closed} pid={self.props.pid} expanded={self.props.expanded} data={self.state.myClosedProjects}
                                                                             onClick={self.onProjectClick.bind(self)}/>}
            {self.state.currentTab == ProjectTabs['Closed(A)'] && <NewProjectsList type={ProjectTabs['Closed(A)']} pid={self.props.pid} expanded={self.props.expanded} data={self.state.allClosedProjects}
                                                                                   onClick={self.onProjectClick.bind(self)}/>}
          </div>}
        </div>
      </div>
    );
  }
}
export default TicketInbox;
/*
 {this.state.currentTab == ProjectTabs.My &&
 <NewProjectsList pid={self.props.pid} expanded={self.props.expanded} data={self.state.myProjects}
 onClick={self.onProjectClick.bind(self)}/>}
 {showAllTabs && this.state.currentTab == ProjectTabs.All &&
 <NewProjectsList pid={self.props.pid} expanded={self.props.expanded} data={self.state.newProjects}
 onClick={self.onProjectClick.bind(self)}/>}
 {this.state.currentTab == ProjectTabs.Closed &&
 <NewProjectsList pid={self.props.pid} expanded={self.props.expanded} data={self.state.myClosedProjects}
 onClick={self.onProjectClick.bind(self)}/>}
 {showAllTabs && this.state.currentTab == ProjectTabs['Closed(A)'] &&
 <NewProjectsList pid={self.props.pid} expanded={self.props.expanded} data={self.state.allClosedProjects}
 onClick={self.onProjectClick.bind(self)}/>}
* */
