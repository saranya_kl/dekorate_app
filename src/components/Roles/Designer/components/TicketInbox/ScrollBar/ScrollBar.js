/**
 * Created by vikramaditya on 7/14/15.
 */
import React from 'react';
import mui from 'material-ui';
import styles from './ScrollBar.scss';
import Helper from '../../../../../helper.js';

var {withStyles, AppConstants: Constants} = Helper;
var {SvgIcon} = mui;

let ScrollConstants = {
  LEFT: 'LEFT',
  RIGHT: 'RIGHT'
};
@withStyles(styles)
class ScrollBar extends React.Component {

  constructor() {
    super();
    this.leftButtonStyle={border:'1px solid black',width:'32px', padding :'0px'};
    this.rightButtonStyle={border:'1px solid black',width:'32px', padding :'0px'};
    this.buttonStyle={border:'1px solid black',minWidth:'110px',padding: '0px'};
  }
  onScroll(value){

    var node = React.findDOMNode(this.refs.scrollBar);
    if(value == ScrollConstants.LEFT){
      var left = node.style.left.replace('px','');
      node.style.left = (parseInt(left) + 110).toString()+'px';
    }else{
      var left = node.style.left.replace('px','');
      node.style.left = (parseInt(left) - 110).toString()+'px';
    }

  }
  onButtonClick(index){
    this.props.onClick(index);
  }

  render(){
    var self = this;
    let num = 8;
    return(
      <div style={{minWidth:'500px',display: 'flex', flexFlow:'row nowrap',justifyContent:'space-between'}}>
        <button style={self.leftButtonStyle} onClick ={self.onScroll.bind(self,ScrollConstants.LEFT)}>
          <SvgIcon>
            <path d={Constants.ICONS.arrow_left}></path>
          </SvgIcon>
        </button>
        <div ref='scrollBar' style={{position:'relative',maxWidth:'400px', zIndex:'0',left:'-20px',display:'flex',transition:'left 0.5s linear', flexFlow:'row nowrap',justifyContent:'flex-start'}}>
          {
            (function(){
              var arr = [];
              for(var i=0;i<num;i++){
                arr.push(<button key = {i} onClick={self.onButtonClick.bind(self,i)} style= {self.buttonStyle}>{i}</button>);
              }
              return arr;
            })()
          }
        </div>
        <button style={self.rightButtonStyle} onClick={self.onScroll.bind(self,ScrollConstants.RIGHT)}>
          <SvgIcon>
            <path d={Constants.ICONS.arrow_right}></path>
          </SvgIcon>
        </button>
      </div>
    );
  }
}
export default ScrollBar;
