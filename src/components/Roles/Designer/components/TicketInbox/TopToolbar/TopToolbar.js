import React from 'react';
import mui from 'material-ui';
import Helper from '../../../../../helper.js';
var {withStyles, LoginStore, NotificationStore, TicketActions, AppConstants: Constants, TriggerTypes, NotificationActions} = Helper;
import styles from './TopToolbar.scss';

var {Checkbox, FlatButton, SvgIcon, TextField, Avatar, Snackbar, ListItem, ListDivider} = mui;
var {Colors,Spacing} = mui.Styles;
var ThemeManager = new mui.Styles.ThemeManager();

@withStyles(styles) class TopToolbar extends React.Component {

  static contextTypes = {
    router: React.PropTypes.func
  };

  constructor() {
    super();
    this.style1 = {cursor: 'not-allowed', backgroundColor: '#fbfbfb'};
    this.style2 = {cursor: 'pointer', backgroundColor: 'green'};
    this.state = {
      avatar: '',
      showNotification: false,
      showLogout: false,
      unreadProjects: false,
      badge_count: 0,
      all_notifications: [],
      username: ''
    }
  }
  componentWillMount(){
    NotificationActions.FETCH_ALL_NOTIFICATIONS(LoginStore.getUserID());
  }
  onMouseClick(e){

  }
  componentWillUnmount(){this.unsubscribe(); window.removeEventListener('click', this.onMouseClick.bind(this), false)}
  componentDidMount() {
    window.addEventListener('click', this.onMouseClick.bind(this), false);
    this.setState({username: LoginStore.getUserName()});
    this.unsubscribe = NotificationStore.listen(this.onStatusChange.bind(this));
    //this.unsubscribe_project = ProjectStore.listen(this.onStatusChange.bind(this));
  }

  logout() {
    LoginStore.logoutUser();
  }
  showProfile(){
    this.context.router.transitionTo('profile', {id: LoginStore.getUserID()});
  }
  refresh() {
    TicketActions.FETCH_PROJECTS();
  }
  onStatusChange(props) {
    //TODO change refresh button color on notification !!
    if(props == TriggerTypes.FETCHED_NOTIFICATION){
      this.setState({badge_count: NotificationStore.getBadgeCount(), all_notifications: NotificationStore.getAllNotifications()});
    }
    if(props == TriggerTypes.FETCHED_ALL_BACKEND_USERS){
      let url = '';
      this.setState({avatar: url});
    }
  }
  showLogout(){
    this.setState({showLogout: !this.state.showLogout})
  }
  showNotification() {
    //if(this.state.showNotification == false){
    //  let self = this;
    //  setTimeout( function (){self.setState({showNotification: false})}, 3000);
    //}
    NotificationActions.RESET_BADGE_COUNT(LoginStore.getUserID());
    this.setState({badge_count: 0, showNotification: !this.state.showNotification});
  }
  markRead(id, index){
    if(this.state.all_notifications[index].read == false){
      var x = this.state.all_notifications;
      x[index].read = true;
      NotificationActions.READ_NOTIFICATION(id);
      this.setState({all_notifications: x});
    }
  }
  getLogoutComponent(){
    return (
      <div style={{position:'absolute', display:'flex', width:'100%',flexFlow:'column', zIndex:'2', top: '35px', right: '10px', backgroundColor:'rgba(122, 120, 120, 0.9)'}}>
        <div style={{margin:'10px'}}>
          <img src='http://lorempixel.com/g/100/100/' width='50px' height='50px'/>
          <span style={{paddingLeft: '30px',fontSize: '0.8em'}}>{this.state.username}</span>
        </div>
        <div style={{display:'flex', flexFlow:'row nowrap', justifyContent:'space-between', margin:'10px'}}>
          <button onClick={this.showProfile.bind(this)}>Profile</button>
          <button onClick={this.logout.bind(this)}>Logout</button>
        </div>

      </div>
    )
  }
  getNotificationComponent() {
    let data = this.state.all_notifications;
    return ( data.length != 0 && <div style={{position:'absolute', display:'flex', flexFlow:'column', zIndex:'2', top: '35px', right: '65px', boxShadow:'0px 0px 18px #bbb',backgroundColor:'rgba(242, 242, 242,1)',border:'1px solid rgba(0,0,0,0.1)'}}>
        <SvgIcon style={{fill: 'rgba(242,242,242,1)', position: 'absolute', top: '-10px',right: '-8px'}}>
        <path d='M16 0 L8 10 L16 10 Z'></path>
      </SvgIcon>
      <div style={{maxHeight:'500px', overflowY:'scroll'}}>
      { data.map((notification, index) => {
        return ([
          <ListItem onClick={this.markRead.bind(this,notification.id, index)} style={{fontSize: '0.8em', width: '300px',backgroundColor: notification.read ? 'transparent': 'rgb(230,230,230)'}}>
            <div style={{display: 'flex', flexFlow: 'column nowrap', justifyContent:'center'}}>
              <div style={{color: Colors.black, display: 'flex', flexFlow: 'row nowrap', justifyContent:'space-between'}}>
                <Avatar style={{zoom:'0.6'}} src={notification.img}/>
                <div style={{order: '2',alignSelf:'center', flex: '3',padding:'0px 10px', fontSize: '0.7em'}}>
                  <span dangerouslySetInnerHTML={{__html: notification.text}}></span>
                </div>
                <div style={{order: '4', alignSelf:'center', flex: '1', fontSize: '0.7em', textAlign: 'end',color:'gray'}}>
                  {notification.time}
                </div>
              </div>
            </div>
          </ListItem>,
          <ListDivider />
        ])
      })
      }
      </div>
    </div>)
  }

  render() {
    let self = this;
    return (
      <div className="topToolBar" style={{ backgroundColor: '#f2f2f2', height: '61px'}}>
        <div className="toolBarButtons">
          <div className="checkBoxWrapper">
            <Checkbox style={{position: 'absolute', right: '3px', top:'4px'}}
                      iconStyle={{width:'16px', height: '16px', fill: '#ABABAB'}}/>
          </div>

          <button type='button'
                  onClick={self.refresh.bind(self)} className="checkBoxWrapper">
            <SvgIcon className = 'refresh'>
              <path
                d="M12 5V1L7 6l5 5V7c3.31 0 6 2.69 6 6s-2.69 6-6 6-6-2.69-6-6H4c0 4.42 3.58 8 8 8s8-3.58 8-8-3.58-8-8-8z"></path>
            </SvgIcon>
          </button>

          <div className="searchbar">
            <div>
              <SvgIcon style={{fill: '#7f8c8d',paddingLeft: '5px', paddingTop: '2px', zoom: '0.8'}}>
                <path
                  d='M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z'></path>
              </SvgIcon>
              <input type="text" className="searchBox" placeholder="Search"/>
            </div>
          </div>
        </div>
        <div
          style={{display:'flex', flexFlow:'row nowrap', justifyContent:'flex-end', alignSelf:'center', position:'relative', lineHeight:'32px'}}>
          {this.state.showNotification && this.getNotificationComponent()}
          <span style={{color:'#717171', fontSize: '0.8em', margin:'0 10px'}}>{this.state.username}</span>
          {this.state.showLogout && this.getLogoutComponent()}
          <div style={{position:'relative'}}>
            <SvgIcon onClick={this.showNotification.bind(this)}
                     style={{fill: '#7f8c8d', cursor:'pointer', margin:'0 10px'}}>
              <path d={Constants.ICONS.bell_icon}></path>
            </SvgIcon>
            {this.state.badge_count != 0 && <span
              style={{position:'absolute', color:'white',backgroundColor:'red',width:'16px', height:'16px', borderRadius:'50%', textAlign:'center', top: '-7px' ,right: '3px',fontSize: '10px',lineHeight: '16px'}}>
              {this.state.badge_count}</span>}
          </div>
          <div ref ='logout'>
            <Avatar src = {this.state.avatar.imageUrl} onClick={this.showLogout.bind(this)} style={{zoom:'0.8', marginRight:'10px'}} />
          </div>
        </div>
      </div>
    );
  }
}
export default TopToolbar;
/*
* <Snackbar
 style={{position: 'fixed', left: '50%', top:'10px'}}
 ref='notification'
 message={self.state.notification}
 action="dismiss"
 autoHideDuration={3000}
 />
* */
