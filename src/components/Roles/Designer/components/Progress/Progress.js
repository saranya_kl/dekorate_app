/**
 * Created by vikramaditya on 7/15/15.
 */

import React from 'react';
import mui from 'material-ui';

var {CircularProgress} = mui;
class Progress extends React.Component {

  constructor(){
    super();
    this.state = {
      inProgress: false
    }
  }
  componentWillMount(){

  }
  componentWillReceiveProps(nextProps){
    this.setState({inProgress: nextProps.visibility});
  }
  render(){
    let self = this;
    return (
      <div style= {{position: 'absolute', left: '40%', top: '40%'}}>
        { self.state.inProgress ?
          <CircularProgress mode = "indeterminate" value = {60} size = {0.5} /> : ''
        }
      </div>
    );
  }
}
export default Progress;
