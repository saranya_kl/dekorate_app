import React from 'react';
import Select from 'react-select';
import _ from 'lodash';
import styles from './Postman.scss';
import Helper from '../../../helper.js';
var {AppConstants: Constants, withStyles} = Helper;
import Services from '../../../../services/Services.js';

let options = {
  PROJECT: 'Project',
  DESIGN_RESPONSE: 'DesignResponse',
  PRODUCT: 'Product'

};
@withStyles(styles)
class Postman extends React.Component {
  static contextTypes = {
    setProgressVisibility: React.PropTypes.func
  };
  constructor() {
    super();
    this.currentIndex = null;
    this.state = {
      success:'',
      json_text: undefined
    };
    this.cascadeOptions = [];
    for(let i=0;i<Object.keys(options).length;i++){
      this.cascadeOptions[i] = {value: Object.keys(options)[i], label: options[Object.keys(options)[i]]};
    }
  }
  prettyPrint(text) {
    return JSON.stringify(text, undefined, 4);
  }
  FetchData(){
    switch (this.currentIndex){
    case 'PROJECT':
      var val = React.findDOMNode(this.refs.inputfield).value;
      this.fetchProject(val);
      break;
    case 'DESIGN_RESPONSE':
      var val = React.findDOMNode(this.refs.inputfield).value;
      this.fetchDesignResponse(val);
      break;
    case 'PRODUCT':
      var val = React.findDOMNode(this.refs.inputfield).value;
      this.fetchProduct(val);
      break;
    default:
      console.log('Nothing');
    }
  }
  fetchProject(pid){
    Services.getAPI(Constants.SERVER_URL + 'project/' + pid)
      .then(response => {
        var data = response.body.data;
        this.setState({json_text:data});
      })
      .catch((response) => {
        console.log(response);
      });
  }
  updateProject(json){
    Services.postAPI(Constants.SERVER_URL + 'project/update', {data: json})
      .then((response)=> {
        this.setState({json_text:undefined, success: 'SUCCESSFULLY UPDATED'});
      })
      .catch((response) => {
        console.log(response);
      });
  }
  fetchDesignResponse(drid){
    Services.postAPI(Constants.SERVER_URL + 'project/drs', {ids: [drid]})
      .then(response => {
        var data = response.body.data;
        this.setState({json_text:data});
      })
      .catch((response) => {
        console.log(response);
      });
  }
  updateDesignReponse(json){
    Services.postAPI(Constants.SERVER_URL + 'drs/update', {data: json})
      .then((response)=> {
        this.setState({json_text: undefined, success: 'SUCCESSFULLY UPDATED'});
      })
      .catch((response) => {
        console.log(response);
      });
  }
  fetchProduct(prid){
    Services.postAPI(Constants.SERVER_URL + 'getproductsbyids', {data: {ids: [prid]}})
      .then(response => {
        var data = response.body.data;
        this.setState({json_text:data});
      })
      .catch(response => {
        console.log(response);
      })
  }
  updateProduct(json){
    Services.postAPI(Constants.SERVER_URL + 'product/update', {data: json})
      .then((response)=> {
        this.setState({json_text:undefined, success: 'SUCCESSFULLY UPDATED'});
      })
      .catch((response) => {
        console.log(response);
      });
  }
  onUpdate() {
    let that = this, val = React.findDOMNode(that.refs.json).innerHTML;
    val = JSON.parse(val);
    switch (that.currentIndex) {
    case 'PROJECT':
      that.updateProject(val);
      break;
    case 'DESIGN_RESPONSE':
      that.updateDesignReponse(val[0]);
      break;
    case 'PRODUCT':
      that.updateProduct(val[0]);
      break;
    default:
    }
    that.setState({success: 'Please wait...'});
  }
  onSelection(selectedIndex){
    this.currentIndex = selectedIndex;
  }
  getTextArea(){
    let data = this.prettyPrint(this.state.json_text);
    return <div className='json-editor' ref = 'json' contentEditable='true' dangerouslySetInnerHTML={{__html: data}}/>
  }
  render() {
    //console.log(this.state);
    let successStyle = (this.state.success == 'SUCCESSFULLY UPDATED') ? {fontWeight: '400', color: '#ffb549'} : {fontWeight:'300', color:'#f05562'};
    return (
      <div className='Postman flex-col'>
        <div className='Postman-success' style={successStyle}>{this.state.success}</div>
        <div className='Postman-inputs'>
          <Select className='Postman-inputs-dropdown' clearable={false} placeholder='TYPE' options={this.cascadeOptions} searchable={false}
                  onChange={this.onSelection.bind(this)}/>
          <input type='text' ref ='inputfield' className='Postman-inputs-text'/>
          <button onClick={this.FetchData.bind(this)} className="Postman-inputs-fetchbutton">Fetch</button>
        </div>
        <div className= 'Postman-container flex-row'>
          <div className='line-number-wrapper'>
            {function(){var arr=[]; for(let i=0;i<30;i++){arr.push(<div className='line-number' key={i}>{i+1}</div>)} return arr;}()}
          </div>
          <div>{this.getTextArea()}</div>
        </div>
        <div className="Postman-update-wrapper">
          <button className="Postman-update-button" onClick={this.onUpdate.bind(this)}>UPDATE</button>
        </div>
      </div>
    )
  }
}
export default Postman;
