/**
 * Created by vikramaditya on 8/3/15.
 */
import React from 'react';
import Helper from '../../helper.js';
var {LoginStore} = Helper;
export default class Modeller extends React.Component{
  constructor(){
    super();
  }
  logout(){
    LoginStore.logoutUser();
  }
  render(){
    let self = this;
    return(<div>
      Modeller
        <button onClick={self.logout.bind(self)}>Logout</button>
      </div>
    )
  }
}
