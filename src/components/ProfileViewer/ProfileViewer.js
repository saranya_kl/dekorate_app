import React from 'react';
import {SvgIcon} from 'material-ui';
import _ from 'lodash';
import DropZone from 'react-dropzone';
import Menu from '../Roles/Designer/components/SideMenu';
import TopToolBar from '../Roles/Designer/components/TicketInbox/TopToolbar';
import Helper from '../helper.js';
import styles from './ProfileViewer.scss';
import Svg from '../Shared/Svg/Svg';
import CustomModal from '../Shared/Modal/Modal';
import LoaderImage from '../../public/loader-2.gif';
var {withStyles, ProfileActions, ProfileStore, LoginStore, AppConstants: Constants, Services} = Helper;
let ITEM_TYPE = {
  PROFILE: 'img',
  COVER: 'coverimg',
  PROJECTS: 'projects'
};
@withStyles(styles)
class ProfileViewer extends React.Component {

  constructor() {
    super();
    this.userID = LoginStore.getUserID();
    this.createNewProfile = false;
    this.state = {
      uploadStatus: {},
      uploading_projects: [],
      projectDisplayImage: null,
      desprofile: {},
      isLoading: false,
      isEditable: false,
      isProjectListShown: true,
      descriptionHeight: '70',
      showProjectUploadModal: false,
      selectedProjectIndex: 0
    }
  }
  componentWillUnmount(){
    this.unsubscribe();
  }
  componentWillMount() {
    let that = this, profile = ProfileStore.getProfile(that.userID);
    that.unsubscribe = ProfileStore.listen(that.handleChange.bind(that));
    if(profile  == Constants.ITEM_LOADING){
      ProfileActions.fetchProfile(that.userID);
      that.setState({isLoading: true});
    }
    else{
      let dummy_profile = {
        coverimg: profile.coverimg || 'https://placeholdit.imgix.net/~text?txtsize=40&txt=Cover%20Image&w=1400&h=150',
        desc: profile.desc || '',
        id: that.userID,
        img: profile.img || 'https://placeholdit.imgix.net/~text?txtsize=40&txt=Profile%20Image&w=170&h=170',
        name: profile.name,
        projects: profile.projects || []
      };
      that.createNewProfile = profile.text != undefined;
      that.setState({desprofile: dummy_profile, isLoading: false});
    }
  }

  handleChange() {
    let that = this;
    let profile = ProfileStore.getProfile(that.userID);
    let dummy_profile = {
      coverimg: profile.coverimg || 'https://placeholdit.imgix.net/~text?txtsize=40&txt=Cover%20Image&w=1400&h=150',
      desc: profile.desc || '',
      id: that.userID,
      img: profile.img || 'https://placeholdit.imgix.net/~text?txtsize=40&txt=Profile%20Image&w=170&h=170',
      name: profile.name,
      projects: profile.projects || []
    };
    that.createNewProfile = profile.text != undefined;
    that.setState({desprofile: dummy_profile, isLoading: false});
  }

  showProjectView(index) {
    this.setState({
      isProjectListShown: false,
      selectedProjectIndex: index
    });
  }

  showProjectListView() {
    this.setState({
      isProjectListShown: true
    });
  }
  editDescription(){
    let that = this;
    let tArea = React.findDOMNode(that.refs.description);
    tArea.focus(); tArea.value += ' '; // to show blinking cursor at the end.
    this.setState({isEditable: true});
  }
  handleDescriptionInput(e){
    let that = this;
    that.setState({descriptionHeight: e.target.scrollHeight});
  }
  saveDescription(){
    let that = this;
    let tArea = React.findDOMNode(that.refs.description);
    let value = tArea.value;
    let profile = that.state.desprofile;
    profile.desc = value;
    that.updateProfile(profile);
    that.setState({isEditable: false, desprofile: profile});
  }
  onCoverImageUpload(files){
    let that = this;
    if(files.length>1){
      alert('Choose single image file to upload');
    }
    else{
      let file = files[0];
      let profile = that.state.desprofile;
      profile[ITEM_TYPE.COVER] = file.preview;
      that.s3Upload(file, profile.id, ITEM_TYPE.COVER)
      .then((response) => {
        let uploadStatus = that.state.uploadStatus;uploadStatus[ITEM_TYPE.COVER] = 100;
        let profile = that.state.desprofile;
        profile[ITEM_TYPE.COVER] = response;//url
        that.updateProfile(profile);
        that.setState({uploadStatus: uploadStatus, desprofile: profile})})
      .catch((err) => {console.log('Error')});

      let uploadStatus = that.state.uploadStatus;uploadStatus[ITEM_TYPE.COVER] = 0;
      that.setState({uploadStatus: uploadStatus, desprofile: profile});
    }
  }
  onProfileImageUpload(files) {
    let that = this;
    if (files.length > 1) {
      alert('Choose single image file to upload');
    }
    else if(files[0].type == 'image/jpeg' || files[0].type == 'image/jpg'){
      let file = files[0];
      let profile = that.state.desprofile;
      profile[ITEM_TYPE.PROFILE] = file.preview;
      that.s3Upload(file, profile.id, ITEM_TYPE.PROFILE)
        .then((response) => {
          let uploadStatus = that.state.uploadStatus;uploadStatus[ITEM_TYPE.PROFILE] = 100;
          let profile = that.state.desprofile;
          profile[ITEM_TYPE.PROFILE] = response;//url
          that.updateProfile(profile);
          that.setState({uploadStatus: uploadStatus, desprofile: profile})})
        .catch((err) => console.log('Error', err));

      let uploadStatus = that.state.uploadStatus;uploadStatus[ITEM_TYPE.PROFILE] = 0;
      that.setState({uploadStatus: uploadStatus, desprofile: profile});
    }
    else{
      alert('Only JPG/JPEG file formats are supported');
    }
  }
  onProgress(p){
    //coverimg, img
    //let that = this;
    //let [type, percent] = p;
    //let uploadStatus = that.state.uploadStatus;
    //uploadStatus[type] = Math.floor(percent);
    //that.setState({uploadStatus: uploadStatus});
    //console.log(type, percent);
  }
  s3Upload(file, drid, type = ITEM_TYPE.PROFILE){
    let that = this;
    return new Promise((resolve, reject) => {
      Services.getAPI(Constants.SERVER_URL + 'gets3signedurl', `?designerid=${drid}&filename=${file.name}&filetype=${file.type}`)
        .then(response => {
          let signed_request = response.body.data.signed_request;
          let url = response.body.data.url;
          //let profile = that.state.desprofile;
          //profile[type] = url;
          Services.s3UploadAPI(file, signed_request, type, that.onProgress.bind(that))
            .then(response => {
              resolve(url);
              //resolve(profile);
            })
            .catch((err) => {
              reject('failed s3');
            });
        })
        .catch((err2) => {
          reject('failed backend');
        })
    });
  }
  uploadProjectImages(files){
    let that = this, pid = that.userID;
    let promise_arr = [], progress = that.state.uploading_projects;
    let index = [];
    for (let i = 0; i < files.length; i++) {
      progress.push({url: files[i].preview, percent: 0});
      index.push(progress.length-1);
      promise_arr.push(that.s3Upload(files[i], pid, progress.length-1));
    }
    Promise.all(promise_arr).then((resp) => {
      let uploading_projects = that.state.uploading_projects;
      for(let i = 0; i <index.length;i++){
        uploading_projects[index[i]] = {url : resp[i]};
      }
      let uploadStatus = that.state.uploadStatus;
      uploadStatus[ITEM_TYPE.PROJECTS] = false;
      that.setState({uploadStatus: uploadStatus, uploading_projects: uploading_projects, projectDisplayImage: uploading_projects[0].url});
    })
    .catch((err) => {
      console.log('error S3 upload', err);
    });
    let uploadStatus = that.state.uploadStatus;uploadStatus[ITEM_TYPE.PROJECTS] = true;
    that.setState({uploading_projects: progress, uploadStatus: uploadStatus});
  }
  submitProjects(){
    let that = this, title = React.findDOMNode(that.refs['project-title']).value.trim();
    if(title){
      let prev_projects = that.state.desprofile.projects;
      let new_project_imgs = that.state.uploading_projects;
      let new_project = new_project_imgs.length > 0 ? [{id: Math.random().toString(), name: title, imgs: new_project_imgs, disImg: that.state.projectDisplayImage || new_project_imgs[0].url}] : [];
      let all_projects = new_project.concat(prev_projects);

      let profile = that.state.desprofile;
      profile.projects = all_projects;
      that.updateProfile(profile);
      that.setState({showProjectUploadModal: false});
    }else{
      alert('Empty title');
    }
  }
  changeDisplayImage(index, isUploading){
    let that = this;
    if(!isUploading){
      that.setState({projectDisplayImage: that.state.uploading_projects[index].url});
    }
  }
  updateProfile(profile){
    if(this.createNewProfile){
      ProfileActions.insertProfile(profile);
    }else{
      ProfileActions.updateProfile(profile);
    }
  }
  getProjectUploadModal(){
    let self = this;
    return (
      <CustomModal isOpen={self.state.showProjectUploadModal} onClose={() => {self.setState({showProjectUploadModal: false})}} style={{left:'15%', top:'3%', width:'70%', height:'90%'}}>
        <h3 className="input-form-title">Upload Your Projects</h3>
        <div className="ProfileViewer-uploadModal-inputTitle">
          <span className="custom-input-label">Enter project Title</span>
          <input ref='project-title' type="text" className="custom-input"/>
        </div>
        <div className='ProfileViewer-uploadModal-projectUpload'>
          <span className="custom-input-label title">Upload Project Images</span>
          <div style={{display: 'flex',flexFlow: 'row', justifyContent: 'center'}}>
            {self.state.uploading_projects.map((item, index) => {
              let imgurl = (item.percent < 100) ? LoaderImage : null;
              return (
                <div key={index} onClick={self.changeDisplayImage.bind(self, index, imgurl)} style={{display: 'flex', flexFlow:'row', alignItems:'center', height: '200px',width: '200px', position:'relative',border:'1px solid #ccc',marginTop:'20px', backgroundSize:'cover',backgroundImage:'url('+item.url+')'}}>
                  {imgurl && <div style={{position:'absolute', left: '0', top:'0', width:'100%', height:'100%',backgroundRepeat: 'no-repeat', backgroundPosition: '50% 50%', backgroundImage:'url('+imgurl+')'}}></div>}
                </div>
              )
            })}
            <DropZone onDrop={self.uploadProjectImages.bind(self)} style={{display: 'flex', flexFlow:'row', alignItems:'center', height: '200px',width: '200px', position:'relative',border:'1px solid #ccc',marginTop:'20px', cursor:'pointer'}}>
                  <span style={{margin:'0 44%'}}>
                    <Svg iconType={'plus-icon'} style={{fill:'#000', padding: '3px 3px 1px 5px'}}/>
                  </span>
            </DropZone>
          </div>
        </div>
        <div style={{textAlign: 'center', marginTop: '25px'}}>
          <p className="custom-input-label" style={{marginLeft:'0', marginBottom:'10px'}}>Display Image</p>
          {self.state.uploading_projects.length > 0 && <img width='200px' height='200px' src={self.state.projectDisplayImage}/>}
        </div>
        <div style={{textAlign: 'right'}}>
          <button className="custom-secondary-button" onClick={() => {self.setState({showProjectUploadModal: false})}}>Cancel</button>
          <button className="custom-primary-button" onClick={self.submitProjects.bind(self)}>Done</button>
        </div>
      </CustomModal>
    )
  }
  render() {
    let self = this;
    let profile = this.state.desprofile;
    let selectedProject = profile.projects ? profile.projects[this.state.selectedProjectIndex] : {};
    let profile_available = Object.keys(profile).length != 0;
    let error_title = this.state.isLoading ? 'Please wait ...': 'Profile Not available';
    let projectUploadModal = self.state.showProjectUploadModal && self.getProjectUploadModal();
    let projectItem = (proj, _index) => {
      let index = _index;
      return (
        <div onClick={self.showProjectView.bind(self,index)} key={index}
             style={{display: 'inline-block',height: '200px',width: '200px',position:'relative',border:'1px solid #ccc',margin:'0 10px 10px 0',backgroundSize:'cover',backgroundImage:'url('+proj.disImg+')'}}>
            <span
              style={{bottom:'0',left: '0',position: 'absolute',padding:'5px 0px 5px 13px', width: '100%',color:'white',boxSizing: 'border-box',backgroundColor:'rgba(74, 74 , 74, 0.61)'}}>
              {proj.name}<br/>
              <span style={{fontSize:'0.7em'}}>{_.result(proj,'imgs.length',0)} Photos</span>
            </span>
        </div>
      )
    };

    let projectImageItem = (item, index) => {
      return (
        <div key={index}
             style={{display: 'inline-block',height: '200px',width: '200px',position:'relative',border:'1px solid #ccc',margin:'0 10px 10px 0',backgroundSize:'cover',backgroundImage:'url('+item.url+')'}}>
        </div>
      );
    };
    let currentProjectView = (proj) => {
      return (
        <div>
          <div style={{marginBottom:'20px'}}>
            <span onClick={self.showProjectListView.bind(self)} style={{cursor:'pointer'}}>&#8592;</span>
            <span style={{fontSize:'1.2em'}}> {proj.name}</span>
          </div>
          {proj.imgs.map(projectImageItem)}
        </div>
      )
    };
    let projectListView = (projects) => {
      return (
        <div>
          <div style={{marginBottom:'20px'}}>
            <span style={{fontSize:'1.2em'}}> {projects.length} Projects</span>
          </div>
          <div style={{display: 'flex',flexFlow:'row wrap'}}>
            {projects.map(projectItem)}
            <div onClick={() => {self.setState({showProjectUploadModal: true})}} style={{cursor: 'pointer', display: 'flex', flexFlow:'row', alignItems:'center', height: '200px',width: '200px', position:'relative',border:'1px solid #ccc',margin:'0 10px 10px 0'}}>
              <div style={{border: 'none', width:'100%'}}>
                <span style={{margin:'0 44%'}}><SvgIcon><path fill="#000000" d="M19,13H13V19H11V13H5V11H11V5H13V11H19V13Z" /></SvgIcon></span>
              </div>
            </div>
          </div>
        </div>
      );
    };
    return (
      <div className='ProfileViewer'>
        {
          profile_available ?
            <div className="ProfileViewer-container">
              <div className='coverImage' style={{backgroundImage: `url(${profile.coverimg})`}}>
                {(self.state.uploadStatus[ITEM_TYPE.COVER] < 100) && <div className="onUpload"><img src={LoaderImage} style={{position:'relative', top:'38%'}}/></div>}
                <DropZone onDrop={self.onCoverImageUpload.bind(self)} className='coverEdit'>
                  <span className="hint"> Change Cover Image</span>
                  <Svg iconType={'camera'} style={{fill:'#fff', padding: '3px 3px 1px 5px', stroke: '#000'}}/>
                </DropZone>
                <div className='profileImageContainer'>
                  {/*<img src={profile.img} className='profileImage' />*/}
                  <div className='profileImage' style={{backgroundImage: `url(${profile.img})`}}>
                    <DropZone onDrop={self.onProfileImageUpload.bind(self)} style={{}} className='profileImageEdit'>
                      <Svg iconType={'camera'} style={{fill:'#fff', padding: '3px 3px 1px 5px'}}/>
                      <span className="hint"> Change Profile Image</span>
                    </DropZone>
                    {(self.state.uploadStatus[ITEM_TYPE.PROFILE] < 100) && <div className="onUpload"><img src={LoaderImage} style={{position:'relative', top:'38%'}}/></div>}
                  </div>
                </div>
                <h2 className='profileName'>{profile.name}</h2>
              </div>

              <div className="profileDescriptionContainer">
                <textarea ref='description' onKeyDown={self.handleDescriptionInput.bind(self)} className='description' style={{fontSize: '14px', overflow:'hidden', height: `${self.state.descriptionHeight}px`}} defaultValue={profile.desc || 'Your Description here...'} />
                  {!self.state.isEditable ?
                    <div onClick={self.editDescription.bind(self)} className='descriptionEdit'>
                      <span className="hint">Edit Description</span>
                    </div>
                  : <div className="updateBtn">
                      <button className='custom-primary-button' onClick={self.saveDescription.bind(self)}> Update </button>
                    </div>
                  }
              </div>

              <div className="profileProjects">
                {
                  !this.state.isLoading && (self.state.isProjectListShown ?
                    projectListView(profile.projects) : currentProjectView(selectedProject))
                }
              </div>
            </div>
            :
            <div style={{width:'100%'}}>
              <h1 className='ProfileViewer-notfound'> {error_title} </h1>
            </div>
        }
        {projectUploadModal}
      </div>
    )

  }
}
export default ProfileViewer;
