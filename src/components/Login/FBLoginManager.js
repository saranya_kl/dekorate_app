/**
 * Created by vikramaditya on 10/23/15.
 */
import Helper from '../helper.js';
var {AppConstants : Constants, LoginActions, LoginActionTypes} = Helper;
export default{
  initializeFB(onLoginFn){
    window.fbAsyncInit = () => {
      FB.init({
        appId      : Constants.FB_APP_ID,
        xfbml      : true,
        version    : 'v2.4'
      });
      FB.getLoginStatus((response) => {
        this.statusChangeCallback(response, onLoginFn);
      })
    };
  },
  loadSDK(){
    (function(d, s, id){
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {return;}
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
  },
  statusChangeCallback(response, onLoginFn) {
    if (response.status === 'connected') {
      let access_token = response.authResponse.accessToken;
      let userID = response.authResponse.userID;
      LoginActions.LOGIN_USER(LoginActionTypes.facebook, access_token, userID);
      onLoginFn();
    } else if (response.status === 'not_authorized') {
      console.log('not not_authorized');
    } else {
      console.log('not logged into facebook');
    }
  },
  checkLoginState() {
    FB.getLoginStatus((response) => {
      this.statusChangeCallback(response);
    });
  },
  handleClickFb() {
    FB.login((response) => {
      this.statusChangeCallback(response);
    });
  }
}
