/**
 * Created by vikramaditya on 7/24/15.
 */
import React from 'react';
import Helper from '../helper.js';
import styles from './Login.scss';
import FBManager from './FBLoginManager.js';
import GLoginManager from './GoogleLoginManager.js';
var {withStyles} = Helper;
import LoadingImageUrl from '../../public/loader.gif';

@withStyles(styles) class Login extends React.Component {
  static contextTypes = {
    router: React.PropTypes.func
  };

  constructor() {
    super();
    this.update = true;
    this.state = {
      loggingIn: false
    }
  }
  componentWillMount(){
    //FBManager.loadSDK();
  }
  componentDidMount() {
    let that = this;
    //FBManager.initializeFB(that.onLogin.bind(that));
    GLoginManager.initializeGAPI(that.onLogin.bind(that));
  }
  onLogin(){
    let that = this;
    that.setState({loggingIn: true});
  }
  render() {
    let self = this;
    let loadingStyle = {
      width: '50px',
      height: '50px',
      margin: '16px auto'
    };
    return (
      <div className='Login'>
        <div className='Login-button' id='g-signin2'></div>
        {/*<div className='Login-button fb-login-button'></div>*/}
        {/*<div className="fb-login-button" scope="public_profile,email" onlogin={FBManager.handleClickFb}
             data-max-rows="1" data-size="xlarge" data-show-faces="false"
             data-auto-logout-link="false"></div>*/}
        {self.state.loggingIn && <img alt='Loading..' src={LoadingImageUrl} style={loadingStyle}/>}
      </div>
    )
  }
}
export default Login;
/*
 //LoginStore.setLoggedInUser(profile.getEmail());
 //sessionStorage.setItem('authToken', profile.getId());
 //sessionStorage.setItem('name', profile.getName());
 //sessionStorage.setItem('imageUrl', profile.getImageUrl());
 //sessionStorage.setItem('email', profile.getEmail());
 //
 //account.refine('authToken').set(sessionStorage.getItem('authToken'));
 //account.refine('name').set(sessionStorage.getItem('name'));
 //account.refine('imageUrl').set(sessionStorage.getItem('imageUrl'));
 //account.refine('email').set(sessionStorage.getItem('email'));
 //var id_token = googleUser.getAuthResponse().id_token;
 //console.log(profile.getId());
 //console.log(profile.getName());
 //console.log(profile.getImageUrl());
 //console.log(profile.getEmail());
 //console.log("ID Token: " + id_token);
 */
