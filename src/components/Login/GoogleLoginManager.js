/**
 * Created by vikramaditya on 10/23/15.
 */
import Helper from '../helper.js';
var {AppConstants : Constants, LoginActions, LoginActionTypes} = Helper;
export default{

  initializeGAPI(onLoginFn){
    window.gapi ? window.gapi.signin2.render('g-signin2', {
      'scope': 'https://www.googleapis.com/auth/userinfo.email',
      'width': 250,
      'height': 50,
      'longtitle': true,
      'theme': 'dark',
      'onsuccess': this.onSignIn.bind(this, onLoginFn)
    }) : console.log('sign in undefined');
  },
  onSignIn(onLoginFn, googleUser) {
    let profile = googleUser.getBasicProfile();
    LoginActions.LOGIN_USER(LoginActionTypes.google, googleUser.getAuthResponse().access_token);
    onLoginFn();
    //LoginActions.REGISTER_USER(googleUser.getAuthResponse().access_token);
  }
}
