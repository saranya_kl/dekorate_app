import React from 'react';
import mui from 'material-ui';
import Helper from '../helper.js';
import styles from './NotFound.scss';
import Router from 'react-router';
var Link = Router.Link;

var {AppConstants: Constants, withStyles} = Helper;

@withStyles(styles)
class NotFound extends React.Component {

  constructor() {

    super();
    this.state = {
    }
  }
  goToHome(){

  }
  render(){

    return(
      <div className="NotFound">
        <div className="fouroh">
          <span>4</span>
          <img src='https://s3.amazonaws.com/deko-repo-dev/icon/Icon-40%402x.png' className="oh-logo"/>
          <span>4</span>
        </div>
        <div className="Sorry"> Oops! The page you are looking for is not found! </div>
        <div className="alter"> Hopefully, this would help </div>
        <Link to="/"><button className="home-button">Home</button></Link>
      </div>
    );
  }
}
export default NotFound;
