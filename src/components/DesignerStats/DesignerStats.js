/**
 * Created by vikramaditya on 9/19/15.
 */
import React from 'react';
import Helper from '../helper.js';
import styles from './DesignerStats.scss';
var {AppConstants: Constants, StatsActions, StatsStore, UsersStore, UsersActions, withStyles} = Helper;
@withStyles(styles) class DesignerStats extends React.Component {
  constructor() {
    super();
    this.state = {
      designer_stats: {},
      isLoading: false
    }
  }

  componentWillMount() {
    let data = StatsStore.getDesignerStats(), that = this;
    UsersActions.fetchDesigners();
    StatsActions.fetchDesignerStats();
    if(data && Object.keys(data).length > 0){
      that.setState({designer_stats: data,isLoading: true});
    }else{
      that.setState({isLoading: true});
    }
    that.unsubscribe = StatsStore.listen(that.onStatusChange.bind(that));
    that.unsubscribe_userStore = UsersStore.listen(that.handleUsersStore.bind(that));
  }

  componentWillUnmount() {
    this.unsubscribe();
    this.unsubscribe_userStore();
  }

  getUserInfo(userid) {
    return UsersStore.getUser(userid);
  }

  onStatusChange(trigger) {
    let that = this;
    if(trigger.designerStats){
      let data = StatsStore.getDesignerStats(), ids = [];
      data.designer.map((id) => {ids.push(id)});
      that.setState({designer_stats: data,isLoading: false});
    }
    //if (props == TriggerTypes.RECEIVED_DESIGNER_STATS) {
    //  let data = ProjectStore.getDesignerStats();
    //  this.setState({designer_stats: data,isLoading: false});
    //}
    //if (props == TriggerTypes.FETCHED_ALL_BACKEND_USERS) {
    //  this.forceUpdate();
    //}
  }
  handleUsersStore(trigger){
    if(trigger.designers){
      this.forceUpdate();
    }
  }
  sampleDiv(id, stats, index){
    let info = this.getUserInfo(id);
    let name = `${info.firstname} ${info.lastname}`;
    let img = `${info.imageUrl}`;
    return(
      <div key={index} className="Designer">
        <div className="Designer-profile">
          <img src={img} className="Designer-profile-img"/>
          <div className="Designer-profile-name">{name}
          </div>
        </div>
        <div className="Designer-statistics">
          <div className="Designer-statistics-pending wrapper">
            <div className="colorStrip">
            </div>
            <div className="title">
              Pending
            </div>
            <div className="value">
              {stats.pending}
            </div>
          </div>
          <div className="Designer-statistics-working wrapper">
            <div className="colorStrip">
            </div>
            <div className="title">
              Working
            </div>
            <div className="value">
              {stats.working}
            </div>
          </div>
          <div className="Designer-statistics-designsent wrapper">
            <div className="colorStrip">
            </div>
            <div className="title">
              Designs Sent
            </div>
            <div className="value">
              {stats.designsent}
            </div>
          </div>
          <div className="Designer-statistics-closed wrapper">
            <div className="colorStrip">
            </div>
            <div className="title">
              Closed
            </div>
            <div className="value">
              {stats.close}
            </div>
          </div>
          <div className="Designer-statistics-total wrapper">
            <div className="colorStrip">
            </div>
            <div className="title">
              TOTAL
            </div>
            <div className="value">
              {stats.total}
            </div>
          </div>
        </div>
      </div>
    )
  }
  /*mapDesignerStats(id, stats, index) {
    let info = this.getUserInfo(id);

    let name = `${info.firstname} ${info.lastname}`;
    let img = `${info.imageUrl}`;
    return (
      <div key={index} className="DesignerStats-time">
        <div className="DesignerStats-time-avatar">
          <img src={img} className="DesignerStats-time-avatar-img"/>
        </div>
        <div className="DesignerStats-time-details">
          <h5 className="DesignerStats-time-title black"> {name} </h5>

          <div className="DesignerStats-time-stats">
            <div className="number-wrapper">
              <div className="number coral"> {stats.pending}</div>
              <span className="label"> Pending </span>
            </div>
            <div className="number-wrapper">
              <div className="number green"> {stats.working}</div>
              <span className="label"> Working </span>
            </div>
            <div className="number-wrapper">
              <div className="number black"> {stats.designsent}</div>
              <span className="label"> Design Sent </span>
            </div>
            <div className="number-wrapper">
              <div className="number blue"> {stats.close}</div>
              <span className="label"> Closed </span>
            </div>
            <div className="number-wrapper">
              <div className="number purple"> {stats.total}</div>
              <span className="label"> Total </span>
            </div>
          </div>
        </div>
      </div>
    )
  }*/
  //UsersActions.fetchUsers(ids);
  render() {
    let Stats = this.state.designer_stats.designer && this.state.designer_stats.designer.map((id, index) => {return this.sampleDiv(id, this.state.designer_stats.stats[index], index)});
    let showProgress = this.state.isLoading;
    return (
      <div className='DesignerStats'>
        {showProgress && <div className='DesignerStats-progress'>Refreshing...</div>}
        {Stats}
      </div>
    )
  }
}
export default DesignerStats;
