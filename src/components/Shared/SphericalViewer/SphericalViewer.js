/**
 * Created by vikramaditya on 12/13/15.
 */
import React from 'react';
import _ from 'lodash';
import THREE from 'three.js';
import {RefreshIndicator} from 'material-ui';
var manualControl = false;
var longitude = 0;
var latitude = 0;
var savedX;
var savedY;
var savedLongitude;
var savedLatitude;

// panoramas background
var panoramasArray = [];
var panoramaNumber = Math.floor(Math.random() * panoramasArray.length);
var scene = new THREE.Scene();
var renderer = {};
var camera = {};
var THIS = {};
class SphericalViewer extends React.Component {

  // setting up the renderer
  constructor(props) {
    super(props);
    THIS = this;
    THIS.state = {
      loading: true
    };
    THIS.isVisible = undefined;
  }
  componentWillUnmount(){
    THIS.isVisible = false;
    document.removeEventListener("mousedown", this.onDocumentMouseDown);
    document.removeEventListener("mousemove", this.onDocumentMouseMove);
    document.removeEventListener("mouseup", this.onDocumentMouseUp);
  }
  componentDidMount(){
    let path = THIS.props.imgurl;
    THIS.loadTexture(`${path}?x=1`);
    THIS.setState({loading: true});
  }
  loadTexture(file){
    THIS.isVisible = true;
    let width = document.getElementById('spherical_dialog').offsetWidth - 40, height = '600';
    renderer = new THREE.WebGLRenderer();
    renderer.setSize(width, height);
    document.getElementById('spherical').appendChild(renderer.domElement);
    camera = new THREE.PerspectiveCamera(75, width/ height, 1, 1000);
    camera.target = new THREE.Vector3(0, 0, 0);

    // creation of a big sphere geometry
    var sphere = new THREE.SphereGeometry(100, 100, 40);
    sphere.applyMatrix(new THREE.Matrix4().makeScale(-1,1,1));

    // creation of the sphere material
    var sphereMaterial = new THREE.MeshBasicMaterial();
    //THREE.ImageUtils.crossOrigin = 'Anonymous';
    //sphereMaterial.map = THREE.ImageUtils.loadTexture(file, undefined, ()=>{THIS.setState({loading: false})},() =>{THIS.setState({loading: false});console.log('Error loading spr image')});

    var loader = new THREE.TextureLoader();
    loader.crossOrigin = 'Anonymous';
    loader.load(file, (x) => {
      sphereMaterial.map = x;
      var sphereMesh = new THREE.Mesh(sphere, sphereMaterial);
      scene.add(sphereMesh);
      THIS.renderScene();
      THIS.setState({loading: false});
    },
    ()=>{},
    () => {THIS.setState({loading: false});console.log('Error loading spr image')}
    );

    // geometry + material = mesh (actual object)
    //var sphereMesh = new THREE.Mesh(sphere, sphereMaterial);
    //scene.add(sphereMesh);

    // listeners
    document.addEventListener("mousedown",this.onDocumentMouseDown,false);
    document.addEventListener("mousemove",this.onDocumentMouseMove,false);
    document.addEventListener("mouseup",this.onDocumentMouseUp,false);
    //this.renderScene();

    // pressing a key (actually releasing it) changes the texture map
    //document.onkeyup = function (event) {
    //  panoramaNumber = (panoramaNumber + 1) % panoramasArray.length;
    //  sphereMaterial.map = THREE.ImageUtils.loadTexture(panoramasArray[panoramaNumber])
    //
    //}
  }
  renderScene() {
    if(THIS.isVisible){
      requestAnimationFrame(THIS.renderScene);
    }
    if (!manualControl) {
      longitude += 0.1;
    }

    // limiting latitude from -85 to 85 (cannot point to the sky or under your feet)
    latitude = Math.max(-85, Math.min(85, latitude));

    // moving the camera according to current latitude (vertical movement) and longitude (horizontal movement)
    camera.target.x = 500 * Math.sin(THREE.Math.degToRad(90 - latitude)) * Math.cos(THREE.Math.degToRad(longitude));
    camera.target.y = 500 * Math.cos(THREE.Math.degToRad(90 - latitude));
    camera.target.z = 500 * Math.sin(THREE.Math.degToRad(90 - latitude)) * Math.sin(THREE.Math.degToRad(longitude));
    camera.lookAt(camera.target);
    // calling again render function
    renderer.render(scene, camera);

  }
  // when the mouse is pressed, we switch to manual control and save current coordinates
  onDocumentMouseDown(event) {
    event.preventDefault();

    manualControl = true;

    savedX = event.clientX;
    savedY = event.clientY;

    savedLongitude = longitude;
    savedLatitude = latitude;

  }
  // when the mouse moves, if in manual contro we adjust coordinates
  onDocumentMouseMove(event) {
    if (manualControl) {
      longitude = (savedX - event.clientX) * 0.1 + savedLongitude;
      latitude = (event.clientY - savedY) * 0.1 + savedLatitude;
    }

  }
  // when the mouse is released, we turn manual control off
  onDocumentMouseUp(event) {
    manualControl = false;
  }
  render(){
    let that = this;
    let loading = that.state.loading;
    let width = _.result(document.getElementById('spherical_dialog'),'offsetWidth','1000');
    let height = 600;
    return (
      <div style={{position: 'relative'}}>
        <div id="spherical"></div>
        {loading && <RefreshIndicator size={40} left={(width-40)/2} top={(height-40)/2} status='loading' />}
      </div>
    )
  }
}
export default SphericalViewer;
