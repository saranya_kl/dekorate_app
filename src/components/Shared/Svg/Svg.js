/**
 * Created by vikramaditya on 11/23/15.
 */
import React from 'react';
import {SvgIcon} from 'material-ui';
import Helper from '../../helper';
var {AppIcons} = Helper;
class Svg extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    let that = this;
    let path = AppIcons[that.props.iconType], hoverColor = that.props.hoverColor, color = that.props.fillColor, iconStyle = that.props.style;
    let className = that.props.className;
    return (
      <SvgIcon className={className} style={iconStyle}>
        <path d={path}/>
      </SvgIcon>
    )
  }
}
export default Svg;
