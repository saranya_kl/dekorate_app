/**
 * Created by vikramaditya on 10/21/15.
 */
import React from 'react';
import Router from 'react-router';
import mui from 'material-ui';
import _ from 'lodash';
import styles from './SideMenu.scss';
import Helper from '../../helper.js';
import Notification from '../Notification';
import UserAccount from '../UserAccount/UserAccount.js';
//import Constants from './MenuConstants.js';
var {withStyles, LoginStore} = Helper;
var {SvgIcon,ListDivider} = mui;
var {Colors,Spacing} = mui.Styles;
var ThemeManager = new mui.Styles.ThemeManager();
@withStyles(styles) class SideMenu extends React.Component {

  static contextTypes = {
    router: React.PropTypes.func,
    muiTheme: React.PropTypes.object
  };
  static childContextTypes = {
    muiTheme: React.PropTypes.object
  };
  propTypes:{
    Constants: React.propTypes.array.isRequired
  };
  getChildContext() {
    return {
      muiTheme: ThemeManager.getCurrentTheme()
    }
  }

  constructor(props) {
    super(props);
    let that = this;
    let Constants = that.props.Constants;
    for(let i=0;i<Constants.MENU_ICONS.length;i++){
      let dashboard = LoginStore.getCurrentDashboard();
      let payload = Constants.MENU_ICONS[i].payload;
      Constants.MENU_ICONS[i].regex = `^\/${dashboard}\/${payload}`;
    }
    that.state = {
      selectedMenu: Constants.MENU_ICONS[0].payload
    };
  }

  componentWillMount() {
    ThemeManager.setPalette({textColor: 'rgba(255,255,255,0.5)'});

    ThemeManager.setComponentThemes({
      menu: {
        backgroundColor: Colors.white
      },
      menuItem: {
        dataHeight: 32,
        height: 48,
        hoverColor: 'rgba(0, 0, 0, .35)',
        padding: Spacing.desktopGutter,
        selectedTextColor: Colors.white,
        color: Colors.white
      }
    });
    this.setMenuSelection();
  }

  componentWillReceiveProps(nextProps) {
    this.setMenuSelection();
  }

  setMenuSelection() {
    let that = this;
    let currentPath = that.context.router.getCurrentPath();
    let Constants = that.props.Constants;
    for (let i = 0; i < Constants.MENU_ICONS.length; i++) {
      let reg = new RegExp(Constants.MENU_ICONS[i].regex, 'gi');
      let payload = Constants.MENU_ICONS[i].payload;
      if (reg.test(currentPath)) {
        that.setState({selectedMenu: payload});
        break;
      }
      if (i + 1 == Constants.MENU_ICONS.length) { //default menu selection
        that.setState({selectedMenu: Constants.MENU_ICONS[0].payload});
      }
    }
  }

  menuClickHandler(index, menuItem) {
    if (menuItem.payload == 'profile') {
      this.context.router.transitionTo(`/${LoginStore.getCurrentDashboard()}/${menuItem.payload}/${LoginStore.getUserID()}`);
    }
    else if (menuItem.payload == '/') {
      this.context.router.transitionTo(`/${LoginStore.getCurrentDashboard()}`);
    }
    else {
      this.context.router.transitionTo(`/${LoginStore.getCurrentDashboard()}/${menuItem.payload}`, this.context.router.getCurrentParams());
    }
  }

  getTheme() {
    return this.context.muiTheme.getCurrentTheme();
  }

  navigateToHome() {
    this.context.router.transitionTo(LoginStore.getCurrentDashboard());
  }

  render() {
    let self = this;
    let theme = self.getTheme();
    return (
      <div className="SideMenu">
        <div>
          <div onClick={self.navigateToHome.bind(self)}
               style={{backgroundSize: '40px 40px', backgroundPosition: '5px', backgroundRepeat:'no-repeat', cursor:'pointer',color: theme.palette.canvasColor,textAlign: 'center',width:'50px', height: '50px', backgroundImage: 'url(https://s3.amazonaws.com/deko-repo-dev/icon/Icon-40%402x.png)'}}>
          </div>
          <ListDivider style={{backgroundColor:'#5A5A5A'}}/>

          <div className="menu-bar">
            <div style={{backgroundColor :'#323031'}}>
              {
                (self.props.Constants.MENU_ICONS.map((iconpath, i) => {
                  let showIcon = iconpath.roles ? LoginStore.checkRole(iconpath.roles) : true;
                  return (
                  showIcon && <div key={i} title={iconpath.title} onClick={self.menuClickHandler.bind(self,i,iconpath)}>
                      <div
                        style={(iconpath.payload == self.state.selectedMenu) ? {display: 'flex', cursor:'pointer',position:'relative', textAlign:'center', flexFlow:'row nowrap',justifyContent:'center', alignItems:'center',height:'55px', backgroundColor: 'rgba(255,255,255,0.1)'} : {display: 'flex', cursor:'pointer',position:'relative', textAlign:'center', flexFlow:'row nowrap',height:'55px', alignItems:'center', justifyContent:'center'}}>
                        <SvgIcon color={Colors.white} style={{display:'block',zoom:'0.8',padding: '8px 0'}}>
                          <path d={iconpath.path}/>
                        </SvgIcon>
                      </div>
                    </div>
                  )
                }))
              }
            </div>
          </div>
        </div>

        <div className='lower-section'>
          <div className='avatar'>
            <UserAccount />
          </div>
          <div className='notification'>
            <Notification />
          </div>
        </div>
      </div>
    );
  }
}
export default SideMenu;
