/**
 * Created by vikramaditya on 9/2/15.
 */
import React from 'react';
let MenuItem = require('material-ui/lib/menus/menu-item');
let IconMenu = require('material-ui/lib/menus/icon-menu');
import {SvgIcon, Avatar, IconButton} from 'material-ui';

import Helper from '../../helper.js';
var {LoginStore} = Helper;
let MenuText = ['Profile', 'Dashboard', 'Logout'];
class UserAccount extends React.Component {
  static contextTypes = {
    router: React.PropTypes.func
  };
  constructor() {
    super();
    this.state = {
      avatar: '',
      showMenu: false,
      user: {}
    }
  }
  componentWillMount(){
    this.setState({user: LoginStore.getUserInfo()})
  }
  showPopupMenu(){
    this.setState({showMenu: !this.state.showMenu})
  }
  logout() {
    LoginStore.logoutUser();
    this.context.router.replaceWith('/login');
  }
  showProfile(){
    this.context.router.transitionTo(`/${LoginStore.getCurrentDashboard()}/profile/${LoginStore.getUserID()}`);
    //this.context.router.transitionTo('/profile', {id: LoginStore.getUserID()});
  }
  showDashboard(){
    this.context.router.transitionTo('/dashboard');
  }
  /*getMenuComponent(){
    let imageUrl = this.state.user.imageUrl;
    let userName = this.state.user.name;
    return (
      <div style={{position:'absolute', display:'flex', flexFlow:'column', bottom: '0px', left: '43px', backgroundColor:'#323031'}}>
        <div style={{padding:'10px 10px 0px 10px'}}>
          <img src={imageUrl} width='50px' height='50px'/>
          <p style={{paddingTop:'10px', fontSize: '0.8em', margin:'0'}}>{userName}</p>
        </div>
        <div style={{width:'120px', display:'flex', flexFlow:'row nowrap', justifyContent:'space-between', padding:'10px'}}>
          <button style={{padding:'5px', margin:'0'}} onClick={this.showProfile.bind(this)}>Profile</button>
          <button style={{padding:'5px', margin:'0'}} onClick={this.logout.bind(this)}>Logout</button>
        </div>
      </div>
    )
  }*/
  handleMenu(e){
    let text = e.target.innerHTML, that = this;
    if(text == MenuText[0]){ // Profile
      that.showProfile();
    }
    if(text == MenuText[1]){ // Dashboard
      that.showDashboard();
    }
    if(text == MenuText[2]){ // logout
      that.logout();
    }
  }
  render() {
    let that = this;
    let imageUrl = that.state.user.imageUrl;
    let userName = `${that.state.user.name}`;
    let iconButtonElement = <button
            style={{width:'32px', height:'32px', borderRadius:'50%', border:'none', backgroundSize:'contain', backgroundImage:`url(${imageUrl})`}}>
    </button>;
    let menuElementStyle={fontSize:'0.8rem', color: '#000'};
    return (
      <div style={{position: 'relative'}}>
        <IconMenu onItemTouchTap={that.handleMenu.bind(that)} iconButtonElement={iconButtonElement} openDirection={'top-right'}>
          <MenuItem disabled innerDivStyle={menuElementStyle} leftIcon={<Avatar style={{width:'40px', margin:'4px 0 0 4px', padding: '0', height:'40px'}} src={imageUrl} />} primaryText={userName} />
          <MenuItem innerDivStyle={menuElementStyle} primaryText={MenuText[0]}/>
          <MenuItem innerDivStyle={menuElementStyle} primaryText={MenuText[1]}/>
          <MenuItem innerDivStyle={menuElementStyle} primaryText={MenuText[2]} />
        </IconMenu>
      </div>
    )
  }
}
export default UserAccount;
