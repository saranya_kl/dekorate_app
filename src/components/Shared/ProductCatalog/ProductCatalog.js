import React from 'react';
import {SvgIcon, Checkbox} from 'material-ui';
import styles from './ProductCatalog.scss';
import Helper from '../../helper.js';
import Modal from '../../../lib/Modal'
import ProductItemView from './ProductItemView.js';
import _ from 'lodash';
var {withStyles,ProductsStore,ProductCatalogStore,ProductsAction,ProductCatalogActions, AppConstants: Constants} = Helper;

@withStyles(styles) class ProductCatalog extends React.Component {

  state = {
    list: [],
    query: ""
  };

  componentWillMount() {
    let that = this;
    that.unsubscribe = ProductCatalogStore.listen(that.handleChange);
    //ProductsStore.ensureProducts(this.state.list);

  }

  componentWillUnmount() {
    let that = this;
    that.unsubscribe();

  }

  componentDidMount() {
    let that = this;
    that.getStateFromStore();
  }

  handleChange = () => {
    let that = this;
    that.getStateFromStore();
  };

  getStateFromStore() {
    let that = this;
    let list = ProductCatalogStore.getList();
    let query = ProductCatalogStore.getQuery();
    that.setState({
      list: list,
      query: query
    });
    ProductsAction.fetchProducts(list);
  }

  getProductListItemComponent = (pid, index) => {
    let that = this;
    return (<ProductListItemComponent key={index} pid={pid} addProduct={that.props.addProduct}/>);
  };

  onSearchBoxKeyDown(e) {
    let that = this;
    if (e.keyCode == 13) {
      //Enter Pressed
      that.onSearch();
    }
  }

  onSearch() {
    let that = this;
    let query = React.findDOMNode(that.refs.searchtext).value;
    let onlyModelled = React.findDOMNode(that.refs.modelledCheckBox).checked;
    let data = {query: query, modelled: onlyModelled};
    ProductCatalogActions.getSearchResults(data);
  }

  render() {
    let that = this;
    let productList = that.state.list;
    let query = that.state.query;
    return (
      <div className="ProductCatalog">

        <div className="topPanel">
          <div className="searchPanel">
            <input ref="searchtext" type="text" placeholder="Search products .." className="searchInput"
                   onKeyDown={that.onSearchBoxKeyDown.bind(that)}/>
            <input ref='modelledCheckBox' type='checkbox' className='searchFilter'/>
            <span className="searchFilterLabel">Only Modelled</span>
            <input type="submit" className="searchSubmitBtn" value="Search" onClick={that.onSearch.bind(that)}/>
          </div>
        </div>

        <div className="productListHolder">
          <div className="productList">
            {productList.map(that.getProductListItemComponent)}
          </div>
        </div>

      </div>

    )
  }


}


class ProductListItemComponent extends React.Component {
  state = {
    loading: true,
    data: null,
    isModalOpen: false
  };

  getStateFromStore(pid) {

    let loading = false;
    let data = ProductsStore.getProduct(pid);
    if (typeof(data) !== "object") {
      loading = true;
      data = null;
    }
    this.setState({
      loading: loading,
      data: data
    });
  }

  componentWillMount() {
    let that = this;
    that.unsubscribe = ProductsStore.listen(that.handleChange);
  }

  componentWillUnmount() {
    let that = this;
    that.unsubscribe();
  }

  componentWillReceiveProps(nextProps) {
    let that = this;
    if (nextProps.pid != that.props.pid) {
      that.getStateFromStore(nextProps.pid);
    }
  }

  componentDidMount() {
    let that = this;
    that.getStateFromStore(that.props.pid);
  }

  handleChange = () => {
    let that = this;
    that.getStateFromStore(that.props.pid);
  };

  showModal() {
    this.setState({isModalOpen: true});
  }

  closeModal() {
    this.setState({isModalOpen: false});

  }

  render() {
    let that = this;

    let data = that.state.data;
    let loading = that.state.loading;
    let isModalOpen = that.state.isModalOpen;
    let pImageThumbUrl = Constants.LOADER_GIF;
    let pName = "Loading";
    let pPrice = "0";
    let pFeatures = [];
    let pDetails = false;
    if (!loading) {
      pImageThumbUrl = _.result(data,'displayImage.url', Constants.LOADER_GIF);
      pName = data.name;
      pPrice = data.amount;
      pDetails = data.assets.models.length > 0;
      pFeatures = [data.brand, data.material, data.colour];
    }

    return (
      <div>
        <div className="ProductListItem" onClick={() => { !loading && that.showModal()}}>
          <div className="productImgHolder">
            <div className="productImg" style={{backgroundImage:`URL('${pImageThumbUrl}')`}}>
            </div>
          </div>

          <div className="productName">
            {pName}
          </div>

          <div className="productPrice">
            <div className="priceContainer">
              <span>{pPrice.INR && `₹${pPrice.INR}`}</span>
              <span>{pPrice.USD && `$${pPrice.USD}`}</span>
            </div>
          </div>

          <div className="productFeatures">
            <div>
              {pFeatures.map((x, index) => {
                if (x != "") return <li key={index}>{x}</li>; else return null;
              })}
            </div>
            {pDetails && <div title='Modelled'>
              <SvgIcon color={'green'}>
                <path
                  d='M21,16.5C21,16.88 20.79,17.21 20.47,17.38L12.57,21.82C12.41,21.94 12.21,22 12,22C11.79,22 11.59,21.94 11.43,21.82L3.53,17.38C3.21,17.21 3,16.88 3,16.5V7.5C3,7.12 3.21,6.79 3.53,6.62L11.43,2.18C11.59,2.06 11.79,2 12,2C12.21,2 12.41,2.06 12.57,2.18L20.47,6.62C20.79,6.79 21,7.12 21,7.5V16.5M12,4.15L6.04,7.5L12,10.85L17.96,7.5L12,4.15M5,15.91L11,19.29V12.58L5,9.21V15.91M19,15.91V9.21L13,12.58V19.29L19,15.91Z'/>
              </SvgIcon>
            </div>}
          </div>


        </div>
        <Modal style={{top:'5%',left:'15%',width:'70%',height:'75%'}} isOpen={isModalOpen}
               onClose={that.closeModal.bind(that)}> <ProductItemView data={data}
                                                                      closeModal={that.closeModal.bind(that)}
                                                                      addProduct={this.props.addProduct}/> </Modal>
      </div>

    )
  }
}

export default ProductCatalog;
