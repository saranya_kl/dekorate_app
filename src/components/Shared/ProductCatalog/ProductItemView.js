/**
 * Created by rick on 13/09/15.
 */
import React from 'react';
import {SvgIcon} from 'material-ui';
import Helper from '../../helper.js';
import styles from './ProductItemView.scss';
import Services from '../../../services/Services.js';
import SigningServices from '../../../services/SigningServices.js';
import ProductImageViewer from './ProductImageViewer.js'
var {withStyles, AppConstants: Constants, LoginStore, ResourceConstants} = Helper;
@withStyles(styles)
class ProductItemView extends React.Component {
  constructor() {
    super();
  }
  state = {
    editMode: true
  };
  componentWillMount(){
    this.setState({
      data: this.props.data
    });
  }
  componentWillReceiveProps(newprops){
    this.setState({
      data: newprops.data
    });
  }
  getTopMenuBar(items){
    return (
      <div className='ProductItemView-menuBar'>
        {items.map((item, index) => {
          return (
            <button className='menuButton' key={index}>{item}</button>
          )
        })}
      </div>
    )
  }
  getCategoryBar(data){
    return (
      <div className="categoricalData">
        {data.type||'TYPE'} > {data.space ? `${data.space} >` : ''}  {data.category ? `${data.category} >` : ''} {data.itemtype || 'ITEM'}
      </div>
    )
  }
  downloadProduct(url){
    //TODO input callback as second arg.
    SigningServices.downloadProduct(url);
  }
  render() {
    let that = this;
    let data = this.state.data;
    let editMode = this.state.editMode;

    //console.log(editMode);
    return (
      <div className="ProductItemView">
        <div className="ProductItemView-basicProductInfo">
          <div className="imageViewerPanel">
            <ProductImageViewer data={data.assets.image}/>
          </div>
          <div className="infoPanel">
            <div className="productName">{data.name || 'Product Name'}</div>
            <div className="brandName">{data.brand}</div>
              {this.getCategoryBar(data)}
            <div className="priceField">
              <div className="priceContainer">
                <span>{data.amount.INR && `₹${data.amount.INR}`}</span>
                {data.amount.USD && <span className="separator">|</span>}
                <span>{data.amount.USD && `$${data.amount.USD}`}</span>
              </div>
            </div>
            <div className="urlField"><a target="_blank" href={data.url}>{data.url}</a></div>
            <div className="detailsTable">
              <div className="header">PRODUCT DETAILS</div>
              <div className="tr"><div className="td-left">Material</div> <div className="td-right">{data.material}</div> </div>
              <div className="tr"><div className="td-left">Color</div> <div className="td-right">{data.colour}</div> </div>
              <div className="tr"><div className="td-left">Size</div> <div className="td-right">{data.size.w}W x {data.size.h}H x {data.size.d}D</div> </div>
              <div className="tr"><div className="td-left">Origin SKU</div> <div className="td-right">{data.originproductid}</div> </div>
            </div>


            <div className="detailsTable">
              <div className="header">TECH DETAILS</div>
              <div className="tr"><div className="td-left">Product ID</div> <div className="td-right">{data.productid}</div> </div>
              <div className="tr"><div className="td-left">Product Code</div> <div className="td-right">{data.productcode}</div> </div>
              {that.props.addProduct && <button onClick={() => {that.props.addProduct(data); that.props.closeModal();}}>Add</button> }
            </div>
            <div className='renderedDetails'>
              {LoginStore.checkPermission(ResourceConstants.ProductCatalog_ProductDownload) && data.assets.models.length > 0 &&
                data.assets.models.map((url, index) => {
                  return (
                    <div key={index}>
                      <SvgIcon color={'green'}>
                        <path
                          d='M21,16.5C21,16.88 20.79,17.21 20.47,17.38L12.57,21.82C12.41,21.94 12.21,22 12,22C11.79,22 11.59,21.94 11.43,21.82L3.53,17.38C3.21,17.21 3,16.88 3,16.5V7.5C3,7.12 3.21,6.79 3.53,6.62L11.43,2.18C11.59,2.06 11.79,2 12,2C12.21,2 12.41,2.06 12.57,2.18L20.47,6.62C20.79,6.79 21,7.12 21,7.5V16.5M12,4.15L6.04,7.5L12,10.85L17.96,7.5L12,4.15M5,15.91L11,19.29V12.58L5,9.21V15.91M19,15.91V9.21L13,12.58V19.29L19,15.91Z'/>
                      </SvgIcon>
                      <button className='downloadBtn' onClick={that.downloadProduct.bind(that,url)}>Download</button>
                      {/*<a href={url} target='_blank'>Download</a>*/}
                    </div>
                  )
                })
              }
            </div>

          </div>
        </div>
      </div>
    )

  }

}


export default ProductItemView;
