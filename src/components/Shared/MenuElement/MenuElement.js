/**
 * Created by vikramaditya on 9/2/15.
 */
import React from 'react';
import {SvgIcon} from 'material-ui';
import Helper from '../../helper.js';
var {AppConstants: Constants, LoginStore} = Helper;

class MenuElement extends React.Component {
  static contextTypes = {
    router: React.PropTypes.func
  };
  constructor() {
    super();
    this.state = {
      avatar: '',
      showLogout: false,
      user: {}
    }
  }
  componentWillMount(){
    this.setState({user: LoginStore.getUserInfo()});
  }
  showLogout(){
    this.setState({showLogout: !this.state.showLogout})
  }
  logout() {
    this.context.router.replaceWith('/login');
    LoginStore.logoutUser();
  }
  showProfile(){
    this.context.router.transitionTo(`/${LoginStore.getCurrentDashboard()}/profile/${LoginStore.getUserID()}`);
    //this.context.router.transitionTo('/profile', {id: LoginStore.getUserID()});
  }
  getLogoutComponent(){
    let imageUrl = this.state.user.imageUrl;
    let userName = this.state.user.name;
    return (
      <div style={{position:'absolute', display:'flex', flexFlow:'column', bottom: '0px', left: '40px', backgroundColor:'rgba(122, 120, 120, 0.9)'}}>
        <div style={{margin:'10px 10px 0px 10px'}}>
          <img src={imageUrl} width='50px' height='50px'/>
          <p style={{padding: '0px 10px',margin:'10px 0px 0px 0px', fontSize: '0.8em'}}>{userName}</p>
        </div>
        <div style={{display:'flex', flexFlow:'row nowrap', justifyContent:'space-between', margin:'10px'}}>
          <button onClick={this.showProfile.bind(this)}>Profile</button>
          <button onClick={this.logout.bind(this)}>Logout</button>
        </div>
      </div>
    )
  }
  render() {
    return (
      <div style={{position: 'relative'}}>
        {this.state.showLogout && this.getLogoutComponent()}
        <SvgIcon onClick={this.showLogout.bind(this)}>
          <path d={Constants.ICONS.menu_horizontal}/>
        </SvgIcon>
      </div>
    )
  }
}
export default MenuElement;
