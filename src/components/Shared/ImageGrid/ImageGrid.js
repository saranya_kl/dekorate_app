/**
 * Created by vikramaditya on 10/16/15.
 */
import React from 'react';
class ImageGrid extends React.Component {
  constructor() {
    super();
    let that = this;
    that.state = {
      data: [
        'http://lorempixel.com/g/100/100/',
        'http://lorempixel.com/g/100/100/',
        'http://lorempixel.com/g/100/100/',
        'http://lorempixel.com/g/100/100/',
        'http://lorempixel.com/g/100/100/',
        'http://lorempixel.com/g/100/100/',
        'http://lorempixel.com/g/100/100/'
      ]
    }
  }
  componentWillMount() {
    let that = this;
  }
  getImageGrid(data){
    return (
    data.map((url, index) => {
      return (
        <div key={index} style={{boxSizing:'border-box'}}>
          <img src={url} width='100px' height='100px'/>
        </div>
      )
    })
    )
  }
  render() {
    let that = this;
    let Grid = that.getImageGrid(that.state.data);
    let gridStyle = {
      display: 'flex',
      flexFlow:'row nowrap',
      overflowX: 'scroll',
      width:'500px'
    };
    return (
      <div style={gridStyle}>
        {Grid}
      </div>
    )
  }
}
export default ImageGrid;
