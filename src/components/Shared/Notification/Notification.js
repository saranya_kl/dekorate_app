/**
 * Created by vikramaditya on 9/2/15.
 */
import React from 'react';
import Helper from '../../helper.js';
import Modal from '../../../lib/Modal';
import Router from 'react-router';

import {SvgIcon, Avatar, ListItem, ListDivider} from 'material-ui';
var {AppConstants: Constants, NotificationActions, NotificationStore, LoginStore, TriggerTypes} = Helper;

class Notification extends React.Component {
  static contextTypes = {
    router: React.PropTypes.func,
    muiTheme: React.PropTypes.object
  };
  constructor() {
    super();
    this.state = {
      showNotification: false,
      unreadProjects: false,
      badge_count: 0,
      all_notifications: []
    }
  }
  componentWillMount(){NotificationActions.FETCH_ALL_NOTIFICATIONS(LoginStore.getUserID());}
  componentWillUnmount(){this.unsubscribe();}
  componentDidMount() {
    this.unsubscribe = NotificationStore.listen(this.onStatusChange.bind(this));
  }
  showNotification() {
    if(!this.state.showNotification && this.state.badge_count != 0){
      NotificationActions.RESET_BADGE_COUNT(LoginStore.getUserID());
    }
    this.setState({badge_count: 0, showNotification: !this.state.showNotification});
  }
  onStatusChange(props) {
    if(props == TriggerTypes.FETCHED_NOTIFICATION){
      this.state.all_notifications.length != 0 && React.findDOMNode(this.refs.notification_audio).play();
      this.setState({badge_count: NotificationStore.getBadgeCount(), all_notifications: NotificationStore.getAllNotifications()});
    }
  }
  markRead(id, index){
    if(this.state.all_notifications[index].read == false){
      var x = this.state.all_notifications;
      x[index].read = true;
      NotificationActions.READ_NOTIFICATION(id);
      this.setState({all_notifications: x});
    }
  }
  onNotificationClicked(notification,index){
    this.markRead(notification.id,index);
    this.context.router.transitionTo(`/${LoginStore.getCurrentDashboard()}/projects/${notification.projectid}`);
  }
  getNotificationComponent() {
    let data = this.state.all_notifications;
    return ( data.length != 0 && <Modal className="Notification" isOpen={true} overlayStyle={{backgroundColor: 'rgba(0,0,0,0.3)'}} onClose={this.showNotification.bind(this)} style={{display:'flex', flexFlow:'column', bottom:'0', height:'100%', left: '55px',backgroundColor:'white',border:'1px solid rgba(0,0,0,0.1)',maxHeight:'100vh',padding:'0',width:'300px'}}>
      <div className="Notification-title">
        <div style={{display:'flex',flexFlow:'column'}} className="">
          <div className="Notification-title-titleWrapper" style={{fontSize:'0.8rem',padding:'20px',display:'flex',flexFlow:'row'}}>
            <span style={{color:'black'}}>Notifications</span>
            <div style={{backgroundColor: '#27ae60',color: 'white',padding: '0 5px',borderRadius: '12px',marginLeft: '5px',fontSize:'0.7rem'}}>{data.length}</div>
          </div>
          <ListDivider />
        </div>
        { data.map((notification, index) => {
          return (
            <div key={index} style={{width: '300px'}}>
              <ListItem onClick={this.onNotificationClicked.bind(this,notification, index)} style={{fontSize: '0.8em', backgroundColor: notification.read ? 'transparent' : 'rgb(230,230,230)'}}>
                <div style={{display: 'flex', flexFlow: 'column nowrap', justifyContent:'center'}}>
                  <div style={{color: '#000000', display: 'flex', flexFlow: 'row nowrap', justifyContent:'space-between'}}>
                    <Avatar style={{zoom:'0.6'}} src={notification.img}/>
                    <div style={{order: '2',alignSelf:'center', flex: '3',padding:'0px 10px', fontSize: '0.7em'}}>
                      <span dangerouslySetInnerHTML={{__html: notification.text}}></span>
                    </div>
                    <div style={{order: '4', alignSelf:'center', flex: '1', fontSize: '0.7em', textAlign: 'end',color:'gray'}}>
                      {notification.time}
                    </div>
                  </div>
                </div>
              </ListItem>
              <ListDivider />
            </div>
          )
        })
        }
      </div>
    </Modal>)
  }

  render() {
    return (
      <div style={{position:'relative'}}>
        {this.state.showNotification && this.getNotificationComponent()}
        <div onClick={this.showNotification.bind(this)}>
          <SvgIcon style={{ cursor:'pointer'}}>
            <path d={Constants.ICONS.bell_icon}/>
          </SvgIcon>
        </div>
        {this.state.badge_count != 0 && <span
          style={{position:'absolute', color:'white',backgroundColor:'red',width:'16px', height:'16px', borderRadius:'50%', textAlign:'center', top: '-7px' ,right: '3px',fontSize: '10px',lineHeight: '16px'}}>
              {this.state.badge_count}</span>}

        <audio ref='notification_audio'>
          <source src={Constants.NOTIFICATION_SOUND} type="audio/mpeg"/>
        </audio>

      </div>
    )
  }
}
export default Notification;

/*<SvgIcon style={{fill: 'rgba(242,242,242,1)', position: 'absolute', bottom: '-12px',left: '-12px', zoom:'2'}}>
 <path d='M16 0 L8 10 L16 10 Z'></path>
 </SvgIcon>
 */
