/**
 * Created by vikramaditya on 12/4/15.
 */
/**
 * Created by vikramaditya on 12/3/15.
 */
import React from 'react';
import invariant from '../../../../node_modules/react/lib/invariant';
import { canUseDOM } from '../../../../node_modules/react/lib/ExecutionEnvironment';
import DropZone from 'react-dropzone';
import styles from './Moodboard.scss';
import _ from 'lodash';
import Helper from '../../helper';
import Line from '../../../public/line.png';
export default React.createClass({

  componentWillUnmount(){
    styles.unuse();
  },
  getInitialState: function(){
    let that = this;
    that.itemGroup = {};
    that.itemKeys = [];
    that.keySpriteMap = {};
    that.itemBeingDragged = {scale:{}, currentIndex: 0};
    that.selectedItem = null;
    that.selectedItemInfo = { tween : null};
    that.cropRectPoint = {x1: 0, x2: 0, y1: 0, y2: 0};
    that.startCrop = false;
    that.stopCrop = false;
    return { x: '1'};
  },
  preload: function () {},
  create: function () {
    let that = this;
    that.game.stage.backgroundColor = '#ccc';
    that.itemGroup = that.game.add.group();
  },
  componentDidMount: function(){
    let that = this;
    if (canUseDOM) {
      invariant(styles.use, `The style-loader must be configured with reference-counted API.`);
      styles.use();
    }
    let canvas = React.findDOMNode(that.refs['canvas-holder']);
    that.game = new Phaser.Game(1200, 675, Phaser.CANVAS, canvas, { preload: that.preload, create: that.create, update: that.update});
  },
  lowerAlphaForAllExcept: function(item) {
    let that = this, game = that.game;
    that.itemGroup.forEach(function (_item) {
      if (item != _item) game.add.tween(_item).to({alpha: 0.5}, 50, "Linear", true);
    });
  },
  resetAlphaToNormal: function(){
    let that = this, game = that.game;
    that.itemGroup.forEach(function(_item){
      game.add.tween(_item).to( { alpha: 1 }, 50, "Linear", true);
    });
  },
  onGameInputDown: function(){
    this.startCrop = true;
  },
  onGameInputUp: function(e){
    this.stopCrop = true;
  },
  loadInputImage: function(){
    let that = this;
    let path = React.findDOMNode(that.refs.inputUrl).value;
    that.loadExternalImage(path+'?x=1');
  },
  loadExternalImage: function(path){
    let that = this;
    let itemKey = 'item'+ new Date().getTime()+Math.random() * 10;
    let loader = new Phaser.Loader(that.game);
    loader.crossOrigin = "Anonymous";
    loader.image(itemKey, path);
    loader.onFileComplete.addOnce(that.handleLoadComplete);
    loader.start();
  },
  loadCropTools(path){
    let that = this;
    let itemKey = 'item'+ new Date().getTime()+Math.random() * 10;
    let loader = new Phaser.Loader(that.game);
    loader.crossOrigin = "Anonymous";
    loader.image(itemKey, path);
    loader.onFileComplete.addOnce((p, key, success) => {
      that.addSlider(key);
    });
    loader.start();
  },
  handleLoadComplete: function(progress,key,success){
    let that = this;
    if(success){
      that.addItemToGame(key);
    }else {
      alert("Can't load this image");
    }
  },
  addItemToGame: function(itemKey){
    let that = this, game = that.game;
    let x = game.world.centerX + (Math.random() * 100);
    let y = game.world.centerY + (Math.random() * 100);
    let item = game.add.sprite(x, y, itemKey);
    item.anchor.setTo(0, 0);
    item.inputEnabled = true;
    item.input.useHandCursor = true;
    item.input.enableDrag(false,false,true);
    item.events.onDragStart.add(that.itemDragStart);
    item.events.onDragStop.add(that.itemDragStop);
    item.events.onInputDown.add(that.itemOnClick);
    that.itemGroup.add(item);
    //that.addSlider(itemKey);
  },
  itemDragStart: function(sprite){
    let that = this, game = that.game;
    that.itemBeingDragged.scale = {x: sprite.scale.x,y: sprite.scale.y};
    game.add.tween(sprite.scale).to( { x: that.itemBeingDragged.scale.x * 1.05, y: that.itemBeingDragged.scale.y * 1.05 }, 50, Phaser.Easing.Linear.None, true);

    //Alpha
    that.lowerAlphaForAllExcept(sprite);

    // Position
    that.itemBeingDragged.currentIndex = that.itemGroup.getChildIndex(sprite);
    sprite.bringToTop();
  },
  itemDragStop(sprite){
    let that = this, game = that.game, itemBeingDragged = that.itemBeingDragged;
    game.add.tween(sprite.scale).to( { x: itemBeingDragged.scale.x , y: itemBeingDragged.scale.y }, 50, Phaser.Easing.Linear.None, true);

    that.resetAlphaToNormal();
    that.itemGroup.setChildIndex(sprite,itemBeingDragged.currentIndex);
  },
  itemOnClick(sprite){
    let that = this;
    that.selectedItem = sprite;
  },
  update(){
    let that = this, game = that.game, selectedItem = that.selectedItem;
    if(this.currentCropSprite){
      this.updatePosition(this.currentCropSprite);
    }
    if(selectedItem && false){
      //this.item.setText('x: ' + selectedItem.position.x + ' y: ' + selectedItem.position.y, 32, 32);
      if(!that.selectedItem.input.draggable && that.startCrop){
        that.cropRectPoint.x1 = _.clone(game.input.mousePointer.x);
        that.cropRectPoint.y1 = _.clone(game.input.mousePointer.y);
        that.startCrop = false;
        //let t = game.add.text(that.cropRectPoint.x1, that.cropRectPoint.y1, `${that.cropRectPoint.x1} ${that.cropRectPoint.y1}`);
        //console.log('P1: ',that.cropRectPoint.x1, that.cropRectPoint.y1, that.selectedItem.position);
      }
      if(!that.selectedItem.input.draggable && that.stopCrop){
        that.cropRectPoint.x2 = game.input.mousePointer.x;
        that.cropRectPoint.y2 = game.input.mousePointer.y;
        let x1 = that.cropRectPoint.x1, y1 = that.cropRectPoint.y1;
        let x2 = that.cropRectPoint.x2, y2 = that.cropRectPoint.y2;
        let w = x2-x1, h = y2-y1;
        let cropRect = new Phaser.Rectangle(x1 - that.selectedItem.position.x, y1 - that.selectedItem.position.y, w, h);
        //let t = game.add.text(game.input.mousePointer.x, game.input.mousePointer.y, `${game.input.mousePointer.x} ${game.input.mousePointer.y}`);
        //console.log(x1, y1, x2, y2);
        //that.selectedItem.position.setTo(x1, y1);
        //that.selectedItem.crop(cropRect, false);
        //that.selectedItem.updateCrop();
        this.graphics = game.add.graphics(that.cropRectPoint.x1, that.cropRectPoint.y1);
        this.graphics.lineStyle(2, 0x0000FF, 1);
        this.graphics.drawRect(0, 0, w, h);

        that.stopCrop = false; that.startCrop = false;
        that.selectedItem.input.draggable = true;
        that.game.input.onUp.add(()=>{});
        that.game.input.onDown.add(()=>{});
      }
    }
    //console.log(that.startCrop, that.stopCrop);
  },
  handleControls: function(index, asd){
    let that = this, selectedItemInfo = that.selectedItemInfo, selectedItem = that.selectedItem, game = that.game;
    if (selectedItemInfo.tween) { if(selectedItemInfo.tween.isRunning) return;}
    if(selectedItem) {
      switch (index) {
      case 0:
        selectedItem.sendToBack();
        selectedItemInfo.tween = game.add.tween(selectedItem.scale).to({x: selectedItem.scale.x * 0.8, y: selectedItem.scale.y * 0.8}, 100, Phaser.Easing.Linear.None, true, 0, 0, true);
        break;
      case 1:
        selectedItem.bringToTop();
        selectedItemInfo.tween = game.add.tween(selectedItem.scale).to({x: selectedItem.scale.x * 1.2, y: selectedItem.scale.y * 1.2}, 100, Phaser.Easing.Linear.None, true, 0, 0, true);
        break;
      case 2:
        selectedItem.moveUp();
        selectedItemInfo.tween = game.add.tween(selectedItem.scale).to( { x: selectedItem.scale.x * 1.2, y: selectedItem.scale.y  * 1.2}, 100, Phaser.Easing.Linear.None, true,0,0,true);
        break;
      case 3:
        selectedItem.moveDown();
        selectedItemInfo.tween = game.add.tween(selectedItem.scale).to( { x: selectedItem.scale.x * 0.8, y: selectedItem.scale.y  * 0.8}, 100, Phaser.Easing.Linear.None, true,0,0,true);
        break;
      case 4:
        selectedItemInfo.tween = game.add.tween(selectedItem.scale).to( { x: selectedItem.scale.x * -1, y: selectedItem.scale.y }, 200, Phaser.Easing.Linear.None, true);
        break;
      case 5:
        selectedItemInfo.tween = game.add.tween(selectedItem.scale).to( { x: selectedItem.scale.x, y: selectedItem.scale.y  * -1}, 200, Phaser.Easing.Linear.None, true);
        break;
      case 6:
        selectedItem.scale.setTo(selectedItem.scale.x*1.1, selectedItem.scale.y*1.1);
        break;
      case 7:
        selectedItem.scale.setTo(selectedItem.scale.x*0.9, selectedItem.scale.y*0.9);
        break;
      case 8:
        selectedItem.angle -= 5;
        break;
      case 9:
        selectedItem.angle += 5;
        break;
      case 10:
        if(selectedItem.input.draggable) {
          that.loadCropTools(Line);
          selectedItem.input.draggable = false;
        }
        break;
      case 11:
        selectedItem.input.draggable = true;
        that.handleDoneCropping();
        break;
      case 12:
        selectedItem.destroy();selectedItem = null;
        break;
      default:

      }
    }
  },
  addSlider: function(sprite){
    let that = this, game = that.game;
    this.originPoint = {x: 100, y: 100};
    let point = that.originPoint;
    that.sliderL = game.add.sprite(point.x, point.y, sprite);
    let w = that.sliderL.width, h = that.sliderL.height;
    that.sliderL.angle += 90;
    that.sliderR = game.add.sprite(point.x + w, point.y, sprite);
    that.sliderR.angle += 90;
    that.sliderT = game.add.sprite(point.x, point.y, sprite);
    that.sliderB = game.add.sprite(point.x, point.y + w, sprite);

    let sliderL = that.sliderL, sliderR = that.sliderR, sliderT = that.sliderT, sliderB = that.sliderB;
    sliderL.tagName='L';sliderL.inputEnabled = true;sliderL.input.enableDrag();sliderL.input.allowVerticalDrag = false;
    sliderR.tagName='R';sliderR.inputEnabled = true;sliderR.input.enableDrag();sliderR.input.allowVerticalDrag = false;
    sliderT.tagName='T';sliderT.inputEnabled = true;sliderT.input.enableDrag();sliderT.input.allowHorizontalDrag = false;
    sliderB.tagName='B';sliderB.inputEnabled = true;sliderB.input.enableDrag();sliderB.input.allowHorizontalDrag = false;

    sliderL.events.onDragStart.add(that.sliderStartDrag, that);sliderL.events.onDragStop.add(that.sliderStopDrag, that);
    sliderR.events.onDragStart.add(that.sliderStartDrag, that);sliderR.events.onDragStop.add(that.sliderStopDrag, that);
    sliderT.events.onDragStart.add(that.sliderStartDrag, that);sliderT.events.onDragStop.add(that.sliderStopDrag, that);
    sliderB.events.onDragStart.add(that.sliderStartDrag, that);sliderB.events.onDragStop.add(that.sliderStopDrag, that);
  },
  sliderStartDrag(sprite, pointer){
    this.currentCropSprite = sprite;
    if(sprite.tagName == 'L'){
      sprite.position.x = pointer.x;
    }
    if(sprite.tagName == 'R'){
      sprite.position.x = pointer.x;
    }
    if(sprite.tagName == 'T'){
      sprite.position.y = pointer.y;
    }
    if(sprite.tagName == 'B'){
      sprite.position.y = pointer.y;
    }
  },
  sliderStopDrag(sprite, pointer){
    this.currentCropSprite = null;
    this.updatePosition(sprite);
  },
  updatePosition(sprite){
    let that = this;
    let sliderL = that.sliderL, sliderR = that.sliderR, sliderT = that.sliderT, sliderB = that.sliderB;
    if(sprite.tagName == 'L'){
      sliderT.width = sliderT.width + (sliderT.position.x - sprite.position.x);
      sliderB.width = sliderB.width + (sliderB.position.x - sprite.position.x);
      sliderT.position.x = sprite.position.x;
      sliderB.position.x = sprite.position.x;
    }
    if(sprite.tagName == 'R'){
      sliderT.width = sprite.position.x - sliderT.position.x;
      sliderB.width = sprite.position.x - sliderB.position.x;
    }
    if(sprite.tagName == 'T'){
      sliderL.width = sliderL.width + (sliderL.position.y - sprite.position.y);
      sliderR.width = sliderR.width + (sliderR.position.y - sprite.position.y);
      sliderL.position.y = sprite.position.y;
      sliderR.position.y = sprite.position.y;
    }
    if(sprite.tagName == 'B'){
      sliderL.width = sprite.position.y - sliderL.position.y;
      sliderR.width = sprite.position.y - sliderR.position.y;
    }
  },
  handleDoneCropping(){
    let that = this;
    let sliderL = that.sliderL, sliderR = that.sliderR, sliderT = that.sliderT, sliderB = that.sliderB;
    let rect_x = sliderL.position.x <  sliderR.position.x ? sliderL.position.x - that.selectedItem.position.x : sliderR.position.x - that.selectedItem.position.x;
    let rect_y = sliderT.position.y <  sliderB.position.y ? sliderT.position.y - that.selectedItem.position.y : sliderB.position.y - that.selectedItem.position.y;
    let cropRect = new Phaser.Rectangle(rect_x, rect_y, Math.abs(sliderT.width), Math.abs(sliderL.width));
    console.log(cropRect);
    //console.log(x1, y1, x2, y2);
    that.selectedItem.crop(cropRect, false);
    that.selectedItem.updateCrop();
    that.selectedItem.position.setTo(sliderL.position.x <  sliderR.position.x ? sliderL.position.x :  sliderR.position.x,
      sliderT.position.y <  sliderB.position.y ? sliderT.position.y : sliderB.position.y);
    sliderL.destroy();sliderR.destroy();sliderT.destroy();sliderB.destroy();
  },
  saveImage(){
    let canvas = document.querySelector('#canvas-holder canvas');
    let url = canvas.toDataURL();
    window.open(url, '_blank');
  },
  onDrop(files){
    let that = this;
    files.forEach((file) => {
      that.loadExternalImage(file.preview);
    });
  },
  getTopMenu(){
    let that = this;
    let controls=[
      {text: 'Move To Back'}, {text: 'Move To Front'}, {text: 'Move Up'}, {text: 'Move Down'}, {text: 'Flip Horizontal'}, {text: 'Flip Vertical'},
      {text: 'Scale Up'}, {text: 'Scale Down'}, {text: 'Rotate Left'}, {text: 'Rotate Right'}, {text: 'Crop'}, {text: 'Done Cropping'},{text: 'Remove'}
    ];
    return (
      <div className="Moodboard-topMenu">
        {controls.map((data, index) => {
          return(
            <button onClick={that.handleControls.bind(that,index)} key={index} className="controls">{data.text}</button>
          )
        })
        }
      </div>
    );
  },
  changeBackgroundColor(e){this.game.stage.backgroundColor = e.target.value},
  render(){
    let that = this;
    let topMenu = that.getTopMenu();
    return (
      <div className="Moodboard">
        {topMenu}
        <div style={{backgroundColor: 'transparent', display:'inline-block'}} ref="canvas-holder" id="canvas-holder">
        </div>
          <div className="Moodboard-bottomMenu">
            <div className="container">
              <input defaultValue="#cccccc" className='inputColor' type="color" onChange={that.changeBackgroundColor}/>
              <div className="inputForm">
                <input className="custom-input" ref='inputUrl' id='inputUrl' type="text" placeholder="url"/>
                <button className='custom-secondary-button' id="loadBtn" onClick={that.loadInputImage}>Load</button>
              </div>
              <DropZone style={{}} onDrop={that.onDrop}>
                <button className='custom-secondary-button'>Browse Files</button>
              </DropZone>
              <button className='custom-primary-button' onClick={that.saveImage}>Save Image</button>
            </div>
            <br/>
          </div>
        </div>
    )
  }
});
