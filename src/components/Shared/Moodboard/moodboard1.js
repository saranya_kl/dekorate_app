window.onload = function() {

  var game = new Phaser.Game(1200, 675, Phaser.CANVAS, 'deko-moodboard-canvas-holder', { preload: preload, create: create });

  var itemGroup;
  var itemKeys = [];
  var keySpriteMap = {};
  var itemBeingDragged = {
    scale:{},
    currentIndex: 0
  };
  var selectedItem = null;
  var selectedItemInfo = { tween : null};

  document.querySelector('#loadBtn').addEventListener("click", LoadFromUrl);
  document.querySelector('#DekoMoodboard .btnMoveToBack').addEventListener("click",handleMoveToBack);
  document.querySelector('#DekoMoodboard .btnMoveToFront').addEventListener("click",handleMoveToFront);
  document.querySelector('#DekoMoodboard .btnMoveUp').addEventListener("click",handleMoveUp);
  document.querySelector('#DekoMoodboard .btnMoveDown').addEventListener("click",handleMoveDown);
  document.querySelector('#DekoMoodboard .btnRemove').addEventListener("click",handleRemove);
  document.querySelector('#DekoMoodboard .btnFlipH').addEventListener("click",handleFlipH);
  document.querySelector('#DekoMoodboard .btnFlipV').addEventListener("click",handleFlipV);
  document.querySelector('#DekoMoodboard .btnScaleUp').addEventListener("click",handleScaleUp);
  document.querySelector('#DekoMoodboard .btnScaleDown').addEventListener("click",handleScaleDown);
  //document.querySelector('#DekoMoodboard .btnRotateRight').addEventListener("click",handleRotateRight);
  document.querySelector('#DekoMoodboard .btnRotateRight').addEventListener("mousedown",StartRotateRight);
  document.querySelector('#DekoMoodboard .btnRotateRight').addEventListener("mouseup",StopRotateRight);
  //document.querySelector('#DekoMoodboard .btnRotateLeft').addEventListener("click",handleRotateLeft);
  document.querySelector('#DekoMoodboard .btnRotateLeft').addEventListener("mousedown",StartRotateLeft);
  document.querySelector('#DekoMoodboard .btnRotateLeft').addEventListener("mouseup",StopRotateLeft);
  var RotateRightInterval = null, RotateLeftInterval= null;
  function StartRotateRight(){
    RotateRightInterval = setInterval(function(){
      if (selectedItemInfo.tween) { if(selectedItemInfo.tween.isRunning) return;}
      if(selectedItem){
        selectedItem.angle += 1;
      }
    }, 40);
  }
  function StopRotateRight(){
    clearInterval(RotateRightInterval);
  }
  function StartRotateLeft(){
    RotateLeftInterval = setInterval(function(){
      if (selectedItemInfo.tween) { if(selectedItemInfo.tween.isRunning) return;}
      if(selectedItem){
        selectedItem.angle -= 1;
      }
    }, 40);
  }
  function StopRotateLeft(){
    clearInterval(RotateLeftInterval);
  }
  function handleScaleUp(){
    if (selectedItemInfo.tween) { if(selectedItemInfo.tween.isRunning) return;}
    if(selectedItem){
      selectedItem.scale.setTo(selectedItem.scale.x*1.1, selectedItem.scale.y*1.1);
    }
  }
  function handleScaleDown(){
    if (selectedItemInfo.tween) { if(selectedItemInfo.tween.isRunning) return;}
    if(selectedItem){
      selectedItem.scale.setTo(selectedItem.scale.x*0.9, selectedItem.scale.y*0.9);
    }
  }
  function handleRotateRight(){
    if (selectedItemInfo.tween) { if(selectedItemInfo.tween.isRunning) return;}
    if(selectedItem){
      selectedItem.angle += 5;
    }
  }
  function handleRotateLeft(){
    if (selectedItemInfo.tween) { if(selectedItemInfo.tween.isRunning) return;}
    if(selectedItem){
      selectedItem.angle -= 5;
    }
  }
  function handleFlipH(){
    if (selectedItemInfo.tween) { if(selectedItemInfo.tween.isRunning) return;}

    if(selectedItem) selectedItemInfo.tween = game.add.tween(selectedItem.scale).to( { x: selectedItem.scale.x * -1, y: selectedItem.scale.y }, 200, Phaser.Easing.Linear.None, true);
  }
  function handleFlipV(){
    if (selectedItemInfo.tween) { if(selectedItemInfo.tween.isRunning) return;}

    if(selectedItem) selectedItemInfo.tween = game.add.tween(selectedItem.scale).to( { x: selectedItem.scale.x, y: selectedItem.scale.y  * -1}, 200, Phaser.Easing.Linear.None, true);
  }
  function handleMoveToBack(){
    if (selectedItemInfo.tween) { if(selectedItemInfo.tween.isRunning) return;}
    if(selectedItem) {
      selectedItem.sendToBack();
      selectedItemInfo.tween = game.add.tween(selectedItem.scale).to( { x: selectedItem.scale.x * 0.8, y: selectedItem.scale.y  * 0.8}, 100, Phaser.Easing.Linear.None, true,0,0,true);
    }
  }
  function handleMoveToFront(){
    if (selectedItemInfo.tween) { if(selectedItemInfo.tween.isRunning) return;}
    if(selectedItem){
      selectedItem.bringToTop();
      selectedItemInfo.tween = game.add.tween(selectedItem.scale).to( { x: selectedItem.scale.x * 1.2, y: selectedItem.scale.y  * 1.2}, 100, Phaser.Easing.Linear.None, true,0,0,true);
    }
  }
  function handleMoveUp(){
    if (selectedItemInfo.tween) { if(selectedItemInfo.tween.isRunning) return;}

    if(selectedItem) {
      selectedItem.moveUp();
      selectedItemInfo.tween = game.add.tween(selectedItem.scale).to( { x: selectedItem.scale.x * 1.2, y: selectedItem.scale.y  * 1.2}, 100, Phaser.Easing.Linear.None, true,0,0,true);
    }
  }
  function handleMoveDown(){
    if (selectedItemInfo.tween) { if(selectedItemInfo.tween.isRunning) return;}
    if(selectedItem) {
      selectedItem.moveDown();
      selectedItemInfo.tween = game.add.tween(selectedItem.scale).to( { x: selectedItem.scale.x * 0.8, y: selectedItem.scale.y  * 0.8}, 100   , Phaser.Easing.Linear.None, true,0,0,true);
    }
  }
  function handleRemove(){
    if(selectedItem) {
      selectedItem.destroy();
      selectedItem = null;
    }
  }


  function LoadFromUrl(){
    var url = document.querySelector('#inputUrl').value;
    loadExternalImage(url);
  }

  function loadExternalImage(path){
    var itemKey = 'item'+ new Date().getTime()+Math.random() * 10;
    var loader = new Phaser.Loader(game);
    loader.crossOrigin = "Anonymous";
    loader.image(itemKey, path);
    loader.onFileComplete.addOnce(handleLoadComplete);
    loader.start()
  }
  function handleLoadComplete(progress,key,success){
    if(success){
      // itemKeys.push(key);
      addItemToGame(key);
    }else {
      console.log('Error: Invalid file');
    }

  }
  function loadImage(path){
    var itemKey = 'item'+ new Date().getTime()+Math.random() * 10;
    game.load.image(itemKey, path);
    // itemKeys.push(itemKey);
  }

  function preload () {
    // loadImage('phaser.png');
    // loadImage('cat.jpg');
  }

  function addItemToGame(itemKey){
    var x = game.world.centerX + (Math.random() * 100);
    var y = game.world.centerY + (Math.random() * 100);
    var item = game.add.sprite(x, y, itemKey);
    item.anchor.setTo(0.5 , 0.5);
    item.inputEnabled = true;
    item.input.useHandCursor = true;
    item.input.enableDrag(false,false,true);
    item.events.onDragStart.add(itemDragStart, this);
    item.events.onDragStop.add(itemDragStop, this);
    item.events.onInputDown.add(itemOnClick, this);

    itemGroup.add(item);
  }

  function itemOnClick(sprite){
    // console.log('show context menu');
    selectedItem = sprite;
    showContextMenu();
  }

  function showContextMenu(){
    // console.log(selectedItem,selectedItem.x,selectedItem.y);
  }


  function lowerAlphaForAllExcept(item){
    itemGroup.forEach(function(_item){
      if(item != _item) game.add.tween(_item).to( { alpha: 0.5 }, 50, "Linear", true);
    });
  }
  function resetAlphaToNormal(){
    itemGroup.forEach(function(_item){
      game.add.tween(_item).to( { alpha: 1 }, 50, "Linear", true);
    });
  }

  function itemDragStart(sprite){
    //Scale
    itemBeingDragged.scale = {x: sprite.scale.x,y: sprite.scale.y};
    game.add.tween(sprite.scale).to( { x: itemBeingDragged.scale.x * 1.05, y: itemBeingDragged.scale.y * 1.05 }, 50, Phaser.Easing.Linear.None, true);

    //Alpha
    lowerAlphaForAllExcept(sprite);

    // Position
    itemBeingDragged.currentIndex = itemGroup.getChildIndex(sprite);
    sprite.bringToTop();
  }

  function itemDragStop(sprite){
    //Scale
    game.add.tween(sprite.scale).to( { x: itemBeingDragged.scale.x , y: itemBeingDragged.scale.y }, 50, Phaser.Easing.Linear.None, true);

    //Alpha
    resetAlphaToNormal();

    //Position
    itemGroup.setChildIndex(sprite,itemBeingDragged.currentIndex);
  }

  function create () {
    game.stage.backgroundColor = '#ccc';
    itemGroup = game.add.group();
    //Load 2 samples
    loadExternalImage('sofa1.png');
    loadExternalImage('chair1.png');
    loadExternalImage('phaser.png');
  }

};
