/**
 * Created by vikramaditya on 10/9/15.
 */
export default {
  PIE: {
    title: {
      text: 'Product Repository',
      margin: 0
    },
    chart: {
      plotBackgroundColor: '#fff',
      height: 220,
      width: 350,
      plotShadow: false,
      type: 'pie'
    },
    legend: {
      margin: 0
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer'
      }
    },
    series: [{
      name: 'Quantity',
      margin: 0,
      colors: ['#3498db', '#323031'],
      data: [
        ['Model Count', 20],
        ['Product Count', 80]
      ]
    }]
  }
}
