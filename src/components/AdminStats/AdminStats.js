import React from 'react';
import mui from 'material-ui';
import Helper from '../helper.js';
import HighCharts from 'react-highcharts/more';
import styles from './AdminStats.scss';
import Config from './ChartConfig.js';
var {AppConstants: Constants, withStyles, StatsActions, StatsStore} = Helper;
var {SvgIcon} = mui;

@withStyles(styles) class AdminStats extends React.Component {

  constructor() {

    super();
    this.state = {
      config: Config.PIE,
      data: {
        modelledcount: 0,
        productcount: 0,
        today: {
          open: 0,
          close: 0,
          averageTimeFromOpen:0,
          designSentFromOpen: 0,
          averageTimeFromWorking: 0
        },
        overall: {
          open: 0,
          close: 0,
          averageTimeFromOpen:0,
          designSentFromOpen: 0,
          averageTimeFromWorking: 0
        },
        last30days: {
          open: 0,
          close: 0,
          averageTimeFromOpen:0,
          designSentFromOpen: 0,
          averageTimeFromWorking: 0
        },
        last7days: {
          open: 0,
          close: 0,
          averageTimeFromOpen:0,
          designSentFromOpen: 0,
          averageTimeFromWorking: 0
        }
      }
    };
  }

  componentWillMount() {
    this.unsubscribe = StatsStore.listen(this.onStatusChange.bind(this));
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  componentDidMount() {
    let obj = StatsStore.getProductStats();
    if(obj && Object.keys(obj).length > 0){
      let x = this.state.config;
      x.series[0].data = [
        ['Modelled',   obj.modelledcount],
        ['Un-Modelled', obj.productcount-obj.modelledcount]
      ];
      this.setState({data: obj, config: x});
    }else{
      StatsActions.fetchProductStats();
    }
  }
  onStatusChange(trigger) {
    let that = this;
    if(trigger.productStats){
      let obj = StatsStore.getProductStats();
      let x = that.state.config;
      x.series[0].data = [
        ['Modelled',   obj.modelledcount],
        ['Un-Modelled', obj.productcount-obj.modelledcount]
      ];
      that.setState({data: obj, config: x});
    }
    if(trigger.error){

    }
    //if (props == TriggerTypes.RECEIVED_STATS) {
    //  let obj = ProjectStore.getStatsData();
    //  let x = this.state.config;
    //  x.series[0].data = [
    //    ['Modelled',   obj.modelledcount],
    //    ['Un-Modelled', obj.productcount-obj.modelledcount]
    //  ];
    //  this.setState({data: obj, config: x});
    //}
  }
  getStatsComponent(data, title, color){
    return (
      <div className="Stats-time">
        <h5 className={`Stats-time-title ${color}`}> {title} </h5>
        <div className="Stats-time-stats">
          <div className="number-wrapper">
            <div className={`number ${color}`}> {data.open}</div>
            <span className="label"> Logged in </span>
          </div>
          <div className="number-wrapper">
            <div className={`number ${color}`}> {data.designSentFromOpen}</div>
            <span className="label"> Responded with Design </span>
          </div>
          <div className="number-wrapper">
            <div className={`number ${color}`}> {data.close}</div>
            <span className="label"> Closed </span>
          </div>
          <div className="number-wrapper">
            <div className={`number ${color}`}> {data.averageTimeFromOpen}</div>
            <span className="label">Turnaround time<br/>Avg&nbsp;(days)</span>
          </div>

          <div className="number-wrapper">
            <div className={`number ${color}`}> {data.averageTimeFromWorking}</div>
            <span className="label">Design time<br/>Avg&nbsp;(days)</span>
          </div>
        </div>
      </div>
    )
  }
  getProductStatsComponent(Products, Modelled, percentage){
    let config = this.state.config;
    return (
      <div className="Stats-products">
        <HighCharts config={config}/>
        <div className="Stats-products-wrapper">
          <div className="detail-wrapper">
            <div className="detail-label"> Total Products </div>
            <div className="detail"> {Products}</div>
          </div>
          <div className="detail-wrapper">
            <div className="detail-label"> Modelled Products </div>
            <div className="detail"> {Modelled} </div>
          </div>
          <div className="detail-wrapper">
            <div className="detail-label"> Percentage</div>
            <div className="detail"> {Math.round(percentage * 100) / 100} % </div>
          </div>
        </div>
      </div>
    )
  }
  render() {
    let self = this;
    let Modelled = self.state.data.modelledcount, Products = self.state.data.productcount;
    let per = isNaN((Modelled*100)/Products) ? 0 : (Modelled*100)/Products;
    let todayStats = self.getStatsComponent(self.state.data.today,'Today','green');
    let last7days = self.getStatsComponent(self.state.data.last7days,'Last 7 days','blue');
    let previousWeek = self.getStatsComponent(self.state.data.last30days,'Last 30 days','purple');
    let thisMonth = self.getStatsComponent(self.state.data.overall,'Overall','coral');
    let ProductStat = self.getProductStatsComponent(Products, Modelled, per);
    return (
      <div className="Stats">
        {ProductStat}
        {todayStats}
        {last7days}
        {previousWeek}
        {thisMonth}
      </div>
    );
  }
}
export default AdminStats;
