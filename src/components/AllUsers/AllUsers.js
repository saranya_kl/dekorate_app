/**
 * Created by vikramaditya on 9/16/15.
 */
import React from 'react';
import styles from './AllUsers.scss';
import Moment from 'moment';
import Helper from '../helper.js';
var {withStyles, AppConstants} = Helper;
import Services from '../../services/Services.js';
@withStyles(styles) class AllUsers extends React.Component {
  constructor() {
    super();
    this.non_state_data = [];
    this.state = {
      loading: false,
      count: 0,
      data: [],
      users_length: 0
    }
  }

  componentDidMount() {
    this.fetchAllUsers();
  }

  fetchAllUsers() {
    this.setState({loading: true});
    Services.getAPI(AppConstants.SERVER_URL + 'getallusers')
      .then((response) => {
        this.non_state_data = response.body.data;
        this.setState({
          data: response.body.data,
          count: response.body.count,
          loading: false,
          users_length: response.body.data.length
        });
      });
  }

  filterUsers(e) {
    let value = (e.target.value).toLowerCase();

    function isEqual(str) {
      let name = (`${str.first_name} ${str.last_name}`).toLowerCase();
      let email = (`${str.email}`).toLowerCase();
      let id = (`${str.id}`).toLowerCase();
      let date = (`${str.created_at}`).toLowerCase();
      if (name.indexOf(value) >= 0 || id.indexOf(value) >= 0 || email.indexOf(value) >= 0) {
        return true;
      }
      return false;
    }
    let x = this.non_state_data;
    let filtered = x.filter(isEqual);
    this.setState({data: filtered});
  }

  getUserItem(item, index) {
    return (
      <div className="userItem" key={index}>
        <div className="userItem-item">
          <a href={`https://www.facebook.com/${item.clientid}`} target="_blank">
            <img src={`http://graph.facebook.com/${item.clientid}/picture`} className="userItem-image"/>
          </a>
        </div>
        <div className="userItem-item">{item.first_name} {item.last_name}</div>
        <div className="userItem-item">{item.email}</div>
        <div className="userItem-item">{item.id}</div>
        <div className="userItem-item">{Moment(item.created_at).format('MMMM Do YYYY, h:mm:ss a')}</div>
      </div>

    )
  }

  getTopBar() {
    let that = this;
    let isLoading = that.state.loading;
    let total_count = this.state.users_length;
    return (
      <div className="topBar">
        <input className='searchBox' onChange={that.filterUsers.bind(that)} placeholder="Search user"/>
        <button className='refresh' onClick={that.fetchAllUsers.bind(that)}>Refresh</button>
        <span className='total-count'>Total Count - {total_count} </span>
        {isLoading && <img style={{position:'absolute', right:'10px', top:'10px',width:'50px',height:'50px'}}
                           src='http://press.solarimpulse.com/img/ajax_loader.gif'/> }

      </div>
    )

  }

  render() {
    let that = this;
    let data = this.state.data;

    return (
      <div className="EntityBrowser">
        {that.getTopBar()}
        <div className="userList">
          {data.map(that.getUserItem)}
        </div>
      </div>
    )
  }
}
export default AllUsers;
