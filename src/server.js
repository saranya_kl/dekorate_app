/*! React Starter Kit | MIT License | http://www.reactstarterkit.com/ */

import 'babel/polyfill';
import _ from 'lodash';
import fs from 'fs';
import path from 'path';
import express from 'express';
import React from 'react';
import compress from 'compression';
import './core/Dispatcher';
//import './stores/AppStore';
import db from './core/Database';
//import App from './components/App';


const server = express();
server.set('port', (process.env.PORT || 5000));
server.use(compress());
server.use(express.static(path.join(__dirname, 'public')));
//
// Register API middleware
// -----------------------------------------------------------------------------
server.use('/api/query', require('./api/query'));

//
// Register server-side rendering middleware
// -----------------------------------------------------------------------------

// The top-level React component + HTML template for it
const templateFile = path.join(__dirname, 'templates/index.html');
const template = _.template(fs.readFileSync(templateFile, 'utf8'));

server.get('*', async (req, res, next) => {
  try {
    let uri = req.path;
    let notFound = false;
    let css = [];
    let data = {description: ''};

    //let app = <App
    //  path={req.path}
    //  context={{
    //    onInsertCss: value => css.push(value),
    //    onSetTitle: value => data.title = value,
    //    onSetMeta: (key, value) => data[key] = value,
    //    onPageNotFound: () => notFound = true
    //  }} />;
    //await db.getPage(uri);
    data.title = 'Dekorate';
    data.body = ''; //React.renderToString(app)

    console.log('Environment: ',process.env.ENV);
    if (process.env.ENV == 'Production'){
      data.serverurl = 'https://webapi.dekorate.in/';
      data.pubnub_sub_key = 'sub-c-707f5a7c-1ef2-11e5-9205-0619f8945a4f';
    }
    else if(process.env.ENV == 'Staging'){
      data.serverurl = 'https://deko-backend-staging-server.herokuapp.com/';
      data.pubnub_sub_key = 'sub-c-9035f108-5aa9-11e5-8a9f-0619f8945a4f';
    }
    else{ // Local
      data.serverurl = 'https://deko-backend-staging-server.herokuapp.com/';
      data.pubnub_sub_key = 'sub-c-9035f108-5aa9-11e5-8a9f-0619f8945a4f';
    }
    console.log('ServerURL: ',data.serverurl);

    data.css = css.join('');
    let html = template(data);
    if (notFound) {
      res.status(404);
    }
    res.send(html);
  } catch (err) {
    next(err);
  }
});

//
// Launch the server
// -----------------------------------------------------------------------------

server.listen(server.get('port'), () => {
  if (process.send) {
    process.send('online');
  } else {
    console.log('The server is running at http://localhost:' + server.get('port'));
  }
});
