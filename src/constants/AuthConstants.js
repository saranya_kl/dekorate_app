/**
 * Created by vikramaditya on 9/8/15.
 */


class AuthConstants {
  static Dashboards = {
    DESIGNER: 'designer',
    VISUALIZER: 'visualizer',
    MODELLER: 'modeller',
    ADMIN: 'admin',
    OPERATIONS: 'operations'
  };

  static Roles = {
    DESIGNER: 'designer',
    LEAD_DESIGNER: 'lead-designer',
    VISUALIZER: 'visualizer',
    MODELLER: 'modeller',
    ADMIN: 'admin',
    OPERATIONS_USER: 'operations-user',
    OPERATIONS_LEAD: 'operations-lead'
  };

}

AuthConstants.Role_Dashboard = {
  'designer': [AuthConstants.Dashboards.DESIGNER],
  'lead-designer': [AuthConstants.Dashboards.DESIGNER],
  'visualizer': [AuthConstants.Dashboards.VISUALIZER],
  'modeller': [AuthConstants.Dashboards.MODELLER],
  'admin': [AuthConstants.Dashboards.ADMIN,AuthConstants.Dashboards.DESIGNER,AuthConstants.Dashboards.VISUALIZER,AuthConstants.Dashboards.MODELLER,AuthConstants.Dashboards.OPERATIONS],
  'operations-user': [AuthConstants.Dashboards.OPERATIONS],
  'operations-lead': [AuthConstants.Dashboards.OPERATIONS]
};

export default AuthConstants;
