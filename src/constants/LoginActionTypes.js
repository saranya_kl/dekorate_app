/**
 * Created by vikramaditya on 8/3/15.
 */

import keyMirror from 'react/lib/keyMirror';
export default keyMirror({
  LOGIN_USER: null,
  REGISTER_USER: null,

  //
  local: null,
  google: null,
  facebook: null
});
