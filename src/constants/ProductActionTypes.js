/**
 * Created by vikramaditya on 7/20/15.
 */

import keyMirror from 'react/lib/keyMirror';
export default keyMirror({
  FETCH_PRODUCTS: null,
  FETCH_URL_PRODUCTS: null,
  ADD_PRODUCT: null,
  UPDATE_PRODUCT: null,
  DELETE_PRODUCT: null,
  SEARCH_PRODUCTS: null
});
