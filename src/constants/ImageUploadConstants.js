/**
 * Created by vikramaditya on 7/10/15.
 */
let ImageUploadConstants = {

  IMAGE: 'IMAGE',
  RENDERED: 'RENDERED',
  _3D: '3D'
};
export default ImageUploadConstants;
