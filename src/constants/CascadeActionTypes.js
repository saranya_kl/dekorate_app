/**
 * Created by vikramaditya on 7/20/15.
 */

import keyMirror from 'react/lib/keyMirror';
export default keyMirror({
  TYPE_LOAD: null,
  SPACE_LOAD: null,
  CATEGORY_LOAD: null,
  ITEM_LOAD: null
});
