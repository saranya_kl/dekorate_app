/**
 * Created by vikramaditya on 7/9/15.
 */

export default {
  DUMMY_PRODUCT: {
    amount: {INR: '', USD: ''},
    assets: {image: [], renderedimage: [], models: []},
    available: true,
    brand: '',
    category: '',
    colour: '',
    created_at: '',
    displayImage: {url: ''},
    isdirty: true,
    itemtype: '',
    marketplace:'',
    material: '',
    name: '',
    notes: '',
    originproductid: '',
    price: [],
    productcode: '',
    productid: '',
    size: {h: '', w: '', d: ''},
    space: '',
    style: '',
    type: '',
    url: ''
  },
  LIST: 'LIST',
  ONLINE: 'ONLINE',
  NEW: 'NEW',
  UPDATE: 'UPDATE',
  CANCEL: 'CANCEL',
  SUBMIT: 'SUBMIT',
  DIRTY: 'DIRTY',
  NON_DIRTY: 'NON_DIRTY',
  SEARCH: 'SEARCH'
};
