import keyMirror from 'react/lib/keyMirror';

export default keyMirror({
  ProductCatalog_ProductDownload: null,
  Projects_ShowAllProjects:null,
  Projects_ProjectDelete:null,
  Projects_ProjectClose:null,
  Products_ProductDelete:null,

  // Modules
  Projects:null,
  ProductCatalog:null,
  ProductRepository:null,
  Postman:null,
  AdminStats:null,
  DesignerStats: null,
  Profile: null,
  AllUsers: null
})
