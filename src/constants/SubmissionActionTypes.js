/**
 * Created by vikramaditya on 7/20/15.
 */

import keyMirror from 'react/lib/keyMirror';

export default keyMirror({
  GET_TEXT_FILE: null
});
