/**
 * Created by vikramaditya on 8/14/15.
 */
import keyMirror from 'react/lib/keyMirror';

export default keyMirror({
  /*Action  types*/
  FETCH_ALL_NOTIFICATIONS: null,
  FETCH_BADGE_COUNT: null,
  READ_NOTIFICATION: null,
  RESET_BADGE_COUNT: null,
  /*Notification types*/
  COMMENT: null,
  PROJECT_ACTION: null,
  PROJECT_ACTION_SUBMITTED: null,
  NEW_DESIGN_RESPONSE: null,
  PROJECT_CLOSE: null,
  LOCK_PROJECT: null,
  PROJECT_ASSIGNED: null,
  USERFEEDBACK_SUBMITTED: null,
  WORKSPACE_COMMENT: null,
  WORKSPACE_SHARED_ASSETS: null,
  WORKSPACE_SHARED_ASSETS_DELETE: null,
  PROJECT_NEW: null, // just for pubnub,
  USER_ACTION_SUBMITTED: null,
  COLLABORATOR_ADDED: null,
  PRODUCT_UPDATED: null,
  FORCE_RELOAD: null
});
