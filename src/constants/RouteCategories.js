/**
 * Created by vikramaditya on 8/3/15.
 */
import keyMirror from 'react/lib/keyMirror';

export default keyMirror({
  LOGIN: null,
  ADMIN: null,
  DESIGNER: null,
  MODELLER: null,
  VISUALIZER: null
})
