/**
 * Created by vikramaditya on 8/6/15.
 */
export default {
  TIMELINE_TYPE: {
    'project-close': {
      key: 'ANY',
      my_msg: ' marked project as closed'

    },
    'comment-created': {
      key: 'ANY',
      my_msg: 'said',
      text: '',
      'custom-data': {commentername: '', commenterid: '', commentid: ''}
    },

    'designresponse-submitted': {
      key: 'SERVER',
      my_msg: ' sent you a design ',
      text: '',
      imageUrl: []
    },
    'project-created': {
      key: 'USER',
      my_msg: ' created a project ',
      text: '',
      imageUrl: []
    },
    'userfeedback-submitted': {
      key: 'USER',
      my_msg: ' submitted feedback.',
      imageUrl: []
    },
    'projectaction-sent': {
      key: 'SERVER',
      my_msg: ' requested an action ',
      subtype: ''

    },
    'projectaction-submit': {
      key: 'USER',
      my_msg: ' submitted an action ',
      subtype: ''
    },
    'project-lock': {
      key: 'SERVER',
      my_msg: ' started working on the project.',
      subtype: ''
    }
  }
}
