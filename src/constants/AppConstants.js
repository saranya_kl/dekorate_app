/**
 * Created by vikramaditya on 6/23/15.
 */
export default {

  ADMIN_PASS: '123',
  ITEM_LOADING: 'item_loading',
  DESIGN_RESPONSE_OBJECT: {
    'data': {
      'projectid': '',
      'userid': '',
      'thumburl': '',
      'attributes': {
        'name': '',
        'imgs': [],
        '2dplan': [],
        'spr': [],
        'story': {'audio': '', 'text': ''},
        'products': {
          'list': [],
          'totalcost': '0',
          'currency': 'INR' //USD
        }
      }
    }
  },

  PUBNUB_SUBSCRIBE_KEY: PUBNUB_SUB_KEY, //for deko-backend
  SERVER_URL: API_SERVER_URL,
  //SERVER_URL: 'http://192.168.1.112:3030/',
  FB_APP_ID:'425322410994117',
  LOADER_GIF: 'http://press.solarimpulse.com/img/ajax_loader.gif',
  PLACEHOLDER: 'https://placeholdit.imgix.net/~text?txtsize=33&txt=Uploading...&w=400&h=250',
  IMAGE_GRID_SIZE: 9,
  TIMELINE_ICONS: {
    'userfeedback-submitted': 'M20 2H4c-1.1 0-1.99.9-1.99 2L2 22l4-4h14c1.1 0 2-.9 2-2V4c0-1.1-.9-2-2-2zm-7 12h-2v-2h2v2zm0-4h-2V6h2v4z',
    'comment-created': 'M21.99 4c0-1.1-.89-2-1.99-2H4c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h14l4 4-.01-18zM18 14H6v-2h12v2zm0-3H6V9h12v2zm0-3H6V6h12v2z',
    'designresponse-submitted': 'M20 4H4c-1.1 0-1.99.9-1.99 2L2 18c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm0 4l-8 5-8-5V6l8 5 8-5v2z',
    'image-submitted': 'M22 16V4c0-1.1-.9-2-2-2H8c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h12c1.1 0 2-.9 2-2zm-11-4l2.03 2.71L16 11l4 5H8l3-4zM2 6v14c0 1.1.9 2 2 2h14v-2H4V6H2z',
    'project-created': 'M14 5c0-1.1-.9-2-2-2h-1V2c0-.55-.45-1-1-1H6c-.55 0-1 .45-1 1v1H4c-1.1 0-2 .9-2 2v15c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2h8V5h-8zm-2 13h-2v-2h2v2zm0-9h-2V7h2v2zm4 9h-2v-2h2v2zm0-9h-2V7h2v2zm4 9h-2v-2h2v2zm0-9h-2V7h2v2z'
  },
  PLUS_ICON: 'M19,13H13V19H11V13H5V11H11V5H13V11H19V13Z',
  CLOSE_ICON: 'M19,6.41L17.59,5L12,10.59L6.41,5L5,6.41L10.59,12L5,17.59L6.41,19L12,13.41L17.59,19L19,17.59L13.41,12L19,6.41Z',
  TIMELINE_DOT: 'M2,12a10,10 0 1,0 20,0a10,10 0 1,0 -20,0',
  SUBMISSION_ICON: {
    Play: 'M8,5.14V19.14L19,12.14L8,5.14Z',
    Pause: 'M14,19.14H18V5.14H14M6,19.14H10V5.14H6V19.14Z',
    Replay: 'M19,12H22.32L17.37,16.95L12.42,12H16.97C17,10.46 16.42,8.93 15.24,7.75C12.9,5.41 9.1,5.41 6.76,7.75C4.42,10.09 4.42,13.9 6.76,16.24C8.6,18.08 11.36,18.47 13.58,17.41L15.05,18.88C12,20.69 8,20.29 5.34,17.65C2.22,14.53 2.23,9.47 5.35,6.35C8.5,3.22 13.53,3.21 16.66,6.34C18.22,7.9 19,9.95 19,12Z',
    fullscreen: 'M5,5H10V7H7V10H5V5M14,5H19V10H17V7H14V5M17,14H19V19H14V17H17V14M10,17V19H5V14H7V17H10Z',
    fullscreenExit: 'M14,14H19V16H16V19H14V14M5,14H10V19H8V16H5V14M8,5H10V10H5V8H8V5M19,8V10H14V5H16V8H19Z'
  },
  MENU_LEFT: 'M14,7L9,12L14,17V7Z',
  ICONS: {
    plus_icon: 'M17,13H13V17H11V13H7V11H11V7H13V11H17M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2Z',
    delete_icon: 'M19,4H15.5L14.5,3H9.5L8.5,4H5V6H19M6,19A2,2 0 0,0 8,21H16A2,2 0 0,0 18,19V7H6V19Z',
    info_icon: 'M13,9H11V7H13M13,17H11V11H13M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2Z',
    close_icon: 'M19,6.41L17.59,5L12,10.59L6.41,5L5,6.41L10.59,12L5,17.59L6.41,19L12,13.41L17.59,19L19,17.59L13.41,12L19,6.41Z',
    play_icon: 'M8,5.14V19.14L19,12.14L8,5.14Z',
    rotate_left: 'M4,2H7A2,2 0 0,1 9,4V20A2,2 0 0,1 7,22H4A2,2 0 0,1 2,20V4A2,2 0 0,1 4,2M20,15A2,2 0 0,1 22,17V20A2,2 0 0,1 20,22H11V15H20M14,4A8,8 0 0,1 22,12L21.94,13H19.92L20,12A6,6 0 0,0 14,6V9L10,5L14,1V4Z',
    rotate_right: 'M10,4V1L14,5L10,9V6A6,6 0 0,0 4,12L4.08,13H2.06L2,12A8,8 0 0,1 10,4M17,2H20A2,2 0 0,1 22,4V20A2,2 0 0,1 20,22H17A2,2 0 0,1 15,20V4A2,2 0 0,1 17,2M4,15H13V22H4A2,2 0 0,1 2,20V17A2,2 0 0,1 4,15Z',
    arrow_left: 'M20,11V13H8L13.5,18.5L12.08,19.92L4.16,12L12.08,4.08L13.5,5.5L8,11H20Z',
    arrow_right: 'M4,11V13H16L10.5,18.5L11.92,19.92L19.84,12L11.92,4.08L10.5,5.5L16,11H4Z',
    bell_icon: 'M14,20A2,2 0 0,1 12,22A2,2 0 0,1 10,20H14M12,2A1,1 0 0,1 13,3V4.08C15.84,4.56 18,7.03 18,10V16L21,19H3L6,16V10C6,7.03 8.16,4.56 11,4.08V3A1,1 0 0,1 12,2Z',
    menu_up: 'M7,15L12,10L17,15H7Z',
    menu_horizontal: 'M10,12a2,2 0 1,0 4,0a2,2 0 1,0 -4,0 M5,12a2,2 0 1,0 4,0a2,2 0 1,0 -4,0 M15,12a2,2 0 1,0 4,0a2,2 0 1,0 -4,0',
    menu_vertical: 'M10,12a2,2 0 1,0 4,0a2,2 0 1,0 -4,0 M10,7a2,2 0 1,0 4,0a2,2 0 1,0 -4,0 M10,17a2,2 0 1,0 4,0a2,2 0 1,0 -4,0',
    pencil_icon: 'M20.71,7.04C21.1,6.65 21.1,6 20.71,5.63L18.37,3.29C18,2.9 17.35,2.9 16.96,3.29L15.12,5.12L18.87,8.87M3,17.25V21H6.75L17.81,9.93L14.06,6.18L3,17.25Z'
  },
  USER_ACTION: {
    data: {
      projectid: '',
      userid: '',
      title: 'Take our Style Quiz',
      subtitle: 'It helps us to know you better in terms of your design inclination',
      positiveButtonText: 'Take Quiz',
      negativeButtonText: 'Later',
      type: 'styleQuiz',
      quizid: '717381fe0d74962cce60'
    }
  },
  PROJECT_ACTION: {
    data: {
      userid: '',
      projectid: '',
      title: '',
      subtitle: '',
      action: 'Submit',
      type: ''
    }
  },
  PROJECT_STATUS: {'CLOSED': 'Closed', 'ASSIGNED': 'Assigned', 'UNASSIGNED': 'Unassigned', 'WORKING': 'Working', DESIGN_SENT: 'Design Sent'},
  NOTIFICATION_SOUND : 'http://freesound.org/data/previews/254/254819_4597795-lq.mp3',
  ADMIN_IMAGE_URL: 'https://s3.amazonaws.com/deko-repo-dev/icon/Icon-40%402x.png'
};
