/**
 * Created by vikramaditya on 7/30/15.
 */

import keyMirror from 'react/lib/keyMirror';

export default keyMirror({

  /*TicketStore*/
  TICKETS_FETCHED: null,
  MY_TICKETS_FETCHED: null,
  MY_CLOSED_TICKETS_FETCHED: null,
  ALL_CLOSED_TICKETS_FETCHED: null,
  FETCHED_ALL_USER_DETAILS: null,

  /* Project Store triggers*/
  FETCHED_PROJECT: null,
  FETCHED_DESIGN_RESPONSES: null,
  FETCHED_DRAFT_DESIGN_RESPONSES: null,
  REFRESH_DESIGN_RESPONSE: null,
  NEW_DESIGN_RESPONSE: null,
  UPDATED_PROJECT: null,
  NEW_COMMENT: null,
  COMMENT_UNREAD: null,
  COMMENT_READ: null,
  CHANGE_COMMENT: null,
  PROJECT_LOCKED: null,
  PROJECT_UNLOCKED: null,
  PROJECT_DELETED: null,
  SENT_STYLE_QUIZ: null,
  USER_ACTION_TOGGLE: null,
  PROJECT_ACTION_SENT: null,
  PROJECT_ACTION_CANCEL: null,
  UPDATED_DESIGN_RESPONSE: null,
  FETCHED_ALL_BACKEND_USERS: null,
  FETCHED_ALL_DESIGNERS: null,
  FETCHED_PROJECT_BACKEND_USERS: null,
  PROJECT_ASSIGNED: null,
  PROJECT_CLOSED: null,
  PROJECT_OPEN: null,
  //RECEIVED_ALL_QUIZ: null,
  //RECEIVED_QUIZ: null,
  FETCHED_QUESTIONS: null,
  FETCHED_WORKSPACE_OBJECT: null,
  WORKSPACE_UPLOADED: null,
  WORKSPACE_CHAT_SENT: null,
  WORKSPACE_ASSETS_DELETED: null,
  PROJECT_STARTED: null,
  RECEIVED_STATS: null,
  RECEIVED_DESIGNER_STATS: null,

  /* Quiz Maker */
  QUIZ_DELETED: null,
  RECEIVED_ALL_QUIZ: null,
  RECEIVED_QUIZ: null,
  UPLOADED_QUIZ: null,
  QUIZ_UPDATED: null,

  /* Design Response Store triggers*/
  AUDIO_UPLOADED: null,
  RESET_NEW_DESIGN_RESPONSE: null,
  IMAGE_REMOVED: null,
  IMAGE_UPLOADED: null,
  FEEDBACK_RECEIVED: null,
  FEEDBACK_UPDATED: null,

  /*Product Store triggers*/
  PREVIEW_UPDATED: null,
  PRODUCTS_FETCHED: null,
  FETCHED_URL_PRODUCT: null,
  NEW_PRODUCT_SET: null,
  UPDATED_PRODUCT_SET: null,
  PRODUCT_ADDED: null,
  PRODUCT_SEARCH_COMPLETE: null,

  /*Login triggers*/
  LOGIN_SUCCESS: null,
  LOGIN_FAILED: null,
  LOGIN_ERROR: null,
  UNAUTHORIZED: null,
  SESSION_EXPIRE: null,
  SHOW_HOME: null,
  LOGOUT: null,
  CHANGE_ROUTE: null,

  /*Cascade triggers*/
  PROPS_SET: null,
  TYPE: null,
  SPACE: null,
  CATEGORY: null,
  ITEM: null,

  /*User Store*/
  FETCHED_USER_DETAILS: null,
  FETCHED_TIME_LINE: null,
  FETCHED_USER_PROJECT: null,
  FETCHED_DESIGNER_PROFILE: null,

  /*ZipStore*/
  ZIP_DOWNLOADED: null,
  /*Notification*/
  NOTIFICATION: null,
  FETCHED_NOTIFICATION: null,
  FETCHED_BADGE_COUNT: null,
  NEW_PUBNUB_NOTIFICATION: null,

  BACKEND_ERROR: null
});
