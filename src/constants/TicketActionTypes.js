/**
 * Created by vikramaditya on 7/20/15.
 */

import keyMirror from 'react/lib/keyMirror';

export default keyMirror({
  FETCH_PROJECTS: null,
  FETCH_MY_PROJECTS: null,
  FETCH_USERS: null,
  FETCH_MY_CLOSED_PROJECTS: null
});
