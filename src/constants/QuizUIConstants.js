/**
 * Created by vikramaditya on 7/16/15.
 */

let QuizUIConstants = {

  QUIZ: {
    'quizid': 'q3',
    'type': 'linear-compulsary',
    'title': 'Help us know you better!',
    'version': 'B',
    'questions': [
      {
        'type': 'single-text-image',
        'question': {
          'text': 'Which door would you like to unlock?'
        },
        'options': [
          {
            'img': 'http://d196kgo02410lt.cloudfront.net/quiz/q3/1/1.png',
            'value': 'Modern'
          },
          {
            'img': 'http://d196kgo02410lt.cloudfront.net/quiz/q3/1/2.png',
            'value': 'Rustic'
          },
          {
            'img': 'http://d196kgo02410lt.cloudfront.net/quiz/q3/1/3.png',
            'value': 'Southwestern'
          },
          {
            'img': 'http://d196kgo02410lt.cloudfront.net/quiz/q3/1/4.png',
            'value': 'Tuscan'
          }
        ]
      },
      {
        'type': 'single-text-image',
        'question': {
          'text': 'Which mirror do you prefer?'
        },
        'options': [
          {
            'img': 'http://d196kgo02410lt.cloudfront.net/quiz/q3/2/1.png',
            'value': 'Modern, Geometric, Edgy'
          },
          {
            'img': 'http://d196kgo02410lt.cloudfront.net/quiz/q3/2/2.png',
            'value': 'Flowy, Curvy'
          },
          {
            'img': 'http://d196kgo02410lt.cloudfront.net/quiz/q3/2/3.png',
            'value': 'Linear, Modern, Simple, Minimalistic'
          },
          {
            'img': 'http://d196kgo02410lt.cloudfront.net/quiz/q3/2/4.png',
            'value': 'Artistic, Ornamental, Floral'
          }
        ]
      },
      {
        'type': 'single-text-image',
        'question': {
          'text': 'Which element describes you the best?'
        },
        'options': [
          {
            'img': 'http://d196kgo02410lt.cloudfront.net/quiz/q3/3/1.png',
            'value': 'Nature'
          },
          {
            'img': 'http://d196kgo02410lt.cloudfront.net/quiz/q3/3/2.png',
            'value': 'Airy'
          },
          {
            'img': 'http://d196kgo02410lt.cloudfront.net/quiz/q3/3/3.png',
            'value': 'Water'
          },
          {
            'img': 'http://d196kgo02410lt.cloudfront.net/quiz/q3/3/4.png',
            'value': 'Fire'
          }
        ]
      },
      {
        'type': 'single-text-image',
        'question': {
          'text': 'Your guiding light would be from which lamp?'
        },
        'options': [
          {
            'img': 'http://d196kgo02410lt.cloudfront.net/quiz/q3/4/1.png',
            'value': 'Abstract'
          },
          {
            'img': 'http://d196kgo02410lt.cloudfront.net/quiz/q3/4/2.png',
            'value': 'Floral'
          },
          {
            'img': 'http://d196kgo02410lt.cloudfront.net/quiz/q3/4/3.png',
            'value': 'Geometric'
          },
          {
            'img': 'http://d196kgo02410lt.cloudfront.net/quiz/q3/4/4.png',
            'value': 'Texture'
          }
        ]
      },
      {
        'type': 'single-text-image',
        'question': {
          'text': 'How would you lay your table ?'
        },
        'options': [
          {
            'img': 'http://d196kgo02410lt.cloudfront.net/quiz/q3/5/1.png',
            'value': 'Everything'
          },
          {
            'img': 'http://d196kgo02410lt.cloudfront.net/quiz/q3/5/2.png',
            'value': 'Elegant'
          },
          {
            'img': 'http://d196kgo02410lt.cloudfront.net/quiz/q3/5/3.png',
            'value': 'Glamourous'
          },
          {
            'img': 'http://d196kgo02410lt.cloudfront.net/quiz/q3/5/4.png',
            'value': 'Bright'
          }
        ]
      },
      {
        'type': 'single-text-image',
        'question': {
          'text': 'You share your space with ?'
        },
        'options': [
          {
            'img': 'http://d196kgo02410lt.cloudfront.net/quiz/q3/6/1.png',
            'value': 'Alone'
          },
          {
            'img': 'http://d196kgo02410lt.cloudfront.net/quiz/q3/6/2.png',
            'value': 'Nuclear'
          },
          {
            'img': 'http://d196kgo02410lt.cloudfront.net/quiz/q3/6/3.png',
            'value': 'Couple'
          },
          {
            'img': 'http://d196kgo02410lt.cloudfront.net/quiz/q3/6/4.png',
            'value': 'Extended'
          }
        ]
      }
    ]
  }

};
export default QuizUIConstants;
