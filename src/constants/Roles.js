/**
 * Created by vikramaditya on 8/17/15.
 */
import keyMirror from 'react/lib/keyMirror';

export default keyMirror({
  DESIGNER: null,
  MODELLER: null,
  VISUALIZER: null
})
