/**
 * Created by vikramaditya on 7/20/15.
 */

import keyMirror from 'react/lib/keyMirror';

export default keyMirror({
  GET_USER_DETAILS: null,
  GET_USER_PROJECTS: null,
  GET_USER_TIME_LINE: null,
  GET_DESIGNER_PROFILE: null,
});
