import keyMirror from 'react/lib/keyMirror';
import AuthConstants from './AuthConstants.js'
import Resource from './ResourceConstants.js'

let Roles = AuthConstants.Roles;
let Exports = {};

//Modules
//Exports[Resource.Projects] = [Roles.ADMIN, Roles.LEAD_DESIGNER, Roles.DESIGNER, Roles.OPERATIONS_LEAD,Roles.VISUALIZER];
//Exports[Resource.ProductCatalog] = [Roles.ADMIN, Roles.LEAD_DESIGNER, Roles.DESIGNER, Roles.OPERATIONS_USER, Roles.OPERATIONS_LEAD, Roles.MODELLER, Roles.VISUALIZER];
//Exports[Resource.ProductRepository] = [Roles.ADMIN, Roles.LEAD_DESIGNER];
//Exports[Resource.Postman] = [Roles.ADMIN];
//Exports[Resource.AdminStats] = [Roles.ADMIN];
//Exports[Resource.DesignerStats] = [Roles.ADMIN];
//Exports[Resource.Profile] = [Roles.ADMIN, Roles.LEAD_DESIGNER, Roles.DESIGNER, Roles.OPERATIONS_USER, Roles.OPERATIONS_LEAD, Roles.MODELLER, Roles.VISUALIZER];
//Exports[Resource.AllUsers] = [Roles.ADMIN];

//Module_Object
Exports[Resource.Projects_ShowAllProjects] = [Roles.ADMIN, Roles.LEAD_DESIGNER, Roles.OPERATIONS_LEAD];
Exports[Resource.ProductCatalog_ProductDownload] = [Roles.ADMIN, Roles.VISUALIZER, Roles.OPERATIONS_LEAD, Roles.OPERATIONS_USER];
Exports[Resource.Projects_ProjectDelete] = [Roles.ADMIN, Roles.LEAD_DESIGNER];
Exports[Resource.Projects_ProjectClose] = [Roles.ADMIN, Roles.LEAD_DESIGNER, Roles.DESIGNER];
Exports[Resource.Products_ProductDelete] = [Roles.ADMIN, Roles.OPERATIONS_LEAD];

export default Exports;
