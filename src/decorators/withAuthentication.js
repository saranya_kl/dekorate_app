/**
 * Created by vikramaditya on 9/8/15.
 */
import React from 'react';
import LoginStore from '../stores/LoginStore.js';
import RouterActions from '../actions/RouterActions.js';

/**
 * Exports a higher-order component that connects the component to the LoginStore.
 * This higher-order component is most easily used as an ES7 decorator.
 * Decorators are just a syntax sugar over wrapping class in a function call.
 */
export default (ComposedComponent) => {
  return class withAuthentication extends React.Component {

    static willTransitionTo(transition) {

      //console.log('&*&*&* willTransitionTo for authenticated page. Next transition path:', transition.path, 'logged in:', LoginStore.isLoggedIn());

      if (!LoginStore.isLoggedIn()) {

        let transitionPath = transition.path;

        //store next path in RouterStore for redirecting after authentication
        //as opposed to storing in the router itself with:
        // transition.redirect('/login', {}, {'nextPath' : transition.path});
        RouterActions.setTransitionPath(transitionPath);

        //go to login page
        transition.redirect('/login');
      }
      else {
        !LoginStore.testPath(transition.path) && transition.redirect('/error');
      }
    }

    constructor() {
      super();
      //this.state = this._getLoginState();
    }

    //_getLoginState() {
    //  return {
    //    userLoggedIn: LoginStore.isLoggedIn(),
    //    user: LoginStore.user,
    //    jwt: LoginStore.jwt
    //  };
    //}

    //componentDidMount() {
    //  this.changeListener = this._onChange.bind(this);
    //  LoginStore.addChangeListener(this.changeListener);
    //}
    //
    //_onChange() {
    //  this.setState(this._getLoginState());
    //}

    //componentWillUnmount() {
    //  LoginStore.removeChangeListener(this.changeListener);
//    //}
//    user={this.state.user}
//  jwt={this.state.jwt}
  //userLoggedIn={this.state.userLoggedIn}

    render() {
      return (
        <ComposedComponent {...this.props} />
      );
    }
  }
};
