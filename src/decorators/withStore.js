/* eslint-disable */
import React, { Component } from 'react'; // eslint-disable-line no-unused-vars
import _ from 'lodash';

function withStore( Store, Actions, getStateFromStore ) {
    return function ( ComposedComponent ) {

        return class WithStore extends Component {

            constructor() {
                super();

                let that = this,
                    store, actions;

                that.store = store = Store.subscriptions ? Store : Store.createStore( (that.actions = actions = Actions.createActions()) );

                that.state = {
                    actions: actions,
                    store:   store
                };
            }

            render() {
                return (<ComposedComponent {...this.props} {...this.state} />);
            }

            componentDidMount() {
                let that = this;
                that.unsubscribe = that.store.listen( that.onStatusChange.bind( that ) );
            }

            componentWillUnmount() {
                this.unsubscribe();
            }

            onStatusChange( props ) {
                let state = _.isFunction( getStateFromStore ) ? getStateFromStore( props ) : props;

                this.setState( state );
            }
        };
    };
}

export default withStore;
