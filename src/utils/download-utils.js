/**
 * Created by vikramaditya on 11/14/15.
 */
import JSZip from 'jszip';
import FileSaver from '../lib/FileSaver/FileSaver';
import _ from 'lodash';
import Constants from '../constants/AppConstants.js';
import Services from '../services/Services';
import CustomUtility from './Custom-Utility';
export default {
  downloadProductZIP(object){

    return new Promise((resolve, reject) => {
      let keys = Object.keys(object), other_keys = [], zip = new JSZip(), data = '';

      keys.forEach(key => {
        if (typeof object[key] != 'object') {
          data = data + '\n' + key + ' : ' + object[key];
        } else if (Array.isArray(object[key])) {
          data = data + '\n' + key + ' : ' + object[key];
        } else if (key == 'size') {
          data = data + '\n' + key + ' : ' + JSON.stringify(object[key]);
        }
        else {
          other_keys.push(key);
        }
      });
      zip.file('details.txt', data);

      let secondary_keys = Object.keys(object[other_keys[0]]);
      let url = object[other_keys[0]][secondary_keys[0]];

      function download_original(index) {
        url[index] = url[index] ? url[index].split('?')[0] : undefined;
        let extension = url[index] ? '.' + url[index].split('.').pop() : undefined;

        Services.zipAPI(url[index], 'assets/original', zip, extension)
          .then(response => {
            if (response == 'SUCCESS') {
              index < (url.length - 1) ? download_original(++index) : download_rendered(0)
            } else {
              download_rendered(0)
            }
          })
      }

      function download_rendered(index) {
        let url = object[other_keys[0]][secondary_keys[1]];

        url[index] = url[index] ? url[index].split('?')[0] : undefined;
        let extension = url[index] ? '.' + url[index].split('.').pop() : undefined;

        Services.zipAPI(url[index], 'assets/rendered', zip, extension)
          .then(response => {
            if (response == 'SUCCESS') {
              index < (url.length - 1) ? download_rendered(++index) : download_3d(0)
            } else {
              download_3d(0)
            }
          })
      }

      function download_3d(index) {
        let url = object[other_keys[0]][secondary_keys[2]];
        url[index] = url[index] ? url[index].split('?')[0] : undefined;
        let extension = url[index] ? '.' + url[index].split('.').pop() : undefined;

        Services.zipAPI(url[index], 'assets/3d', zip, extension)
          .then(response => {
            if (response == 'SUCCESS') {
              index < (url.length - 1) ? download_3d(++index) :
                CustomUtility.saveZip(zip, object.productcode, resolve);
              //location.href = "data:application/zip;base64," + zip.generate({type: "base64"});
            } else {
              CustomUtility.saveZip(zip, object.productcode, resolve);
            }
          })
        .catch((e) => {reject(e)});
      }

      download_original(0);
    });
  },
  downloadImageZIP(array,folder){
    let that = this, zip = new JSZip(), count = 0, fileLength = array.length;
    return new Promise((resolve, reject) => {
      (function startDownload(){
        if(count < fileLength){
          let extension = array[count] ? '.' + array[count].split('.').pop() : undefined;
          Services.zipAPI(array[count], `${folder}`, zip, extension)
            .then(() => {
              count++;
              startDownload();
            });
          //that.addFilesToZip(count++,foldername);
        }else{
          CustomUtility.saveZip(zip, folder, resolve);
        }
      }());
    });
    //startDownload();
  }
}
