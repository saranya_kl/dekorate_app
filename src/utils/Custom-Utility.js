/**
 * Created by vikramaditya on 11/3/15.
 */
import _ from 'lodash';
import Constants from '../constants/AppConstants.js';
import ImageUploadConstants from '../constants/ImageUploadConstants.js';
import Services from '../services/Services';
import Request from 'superagent';
import JSZip from 'jszip';
import FileSaver from '../lib/FileSaver/FileSaver';
import FileSize from '../lib/filesize';
let size_cache = new Map();
export default {
  getProjectStatus(project){
    let meta = _.result(project,'attributes.meta', undefined);
    if(!meta){
      return 0;
    }
    let isOpen = meta.isopen, isAssign = meta.isassign, isWorking = meta.isworking, hasDesign = meta.hasdesign;
    let STATUS = Constants.PROJECT_STATUS;

    if (!isOpen) {
      return STATUS.CLOSED
    }
    if (isWorking) {
      if (hasDesign) {
        return STATUS.DESIGN_SENT
      }
      return STATUS.WORKING;
    }
    if (isAssign) {
      return STATUS.ASSIGNED
    }
    return STATUS.UNASSIGNED;
  },
  fileUpload(file){

  },
  zipDownload(img_url, name){

  },
  uploadProductImage(files, tag, pcode){
    return new Promise((resolve, reject) => {
      if (tag == ImageUploadConstants._3D) {
        files.forEach(file => {
          Services.uploadAPI(Constants.SERVER_URL + 'product/upload' + '?productcode=' + pcode, file)
            .then(response => {
              let url = response.body.url;
              resolve({tag: tag, url: url});
            })
            .catch((e) => {
              reject({error: e})
            });
        });
      }
      else {
        files.forEach(file => {
          Services.uploadAPI(Constants.SERVER_URL + 'product/upload', file)
            .then(response => {
              let url = response.body.url;
              resolve({tag: tag, url: url});
            })
            .catch((e) => {
              reject({error: e})
            })
        });
      }
    })
  },
  saveZip(zip, name, resolve){
    let content = zip.generate({type: 'blob'});
    FileSaver.saveAs(content, name);
    resolve('done');
  },
  saveFile(url, filename = 'File'){
    return new Promise((resolve, reject) => {
      let extension = url.substring(url.lastIndexOf('.'), url.length);
      let xhr = new XMLHttpRequest();
      xhr.open('GET', url+'?x=1', true);
      xhr.responseType = 'blob';
      xhr.onload = function (evt) {
        FileSaver.saveAs(xhr.response, filename+extension);
        resolve('done');
      };
      xhr.onerror = () => {reject('failed')};
      xhr.send();
    });
  },
  getFileSize(url){
    if(size_cache.has(url)){
      return new Promise((resolve, reject) => {
        resolve(size_cache.get(url));
      })
    }else{
      return new Promise((resolve, reject) => {
        Request.head(url).end(function (err, resp) {
          if (err) {reject('err'); throw err;}
          let size = FileSize(resp.xhr.getResponseHeader('Content-Length'));
          size_cache.set(url,size);
          resolve(size);
        });
      })
    }
  },
  parseUrlFromText(text){
    let regEx = /(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/g;
    text = text.replace(regEx, (x) => `<a target='_blank' href=${x}>${x}</a>`);
    return text;
  }
}
