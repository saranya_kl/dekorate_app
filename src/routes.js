/**
 * Created by vikramaditya on 8/3/15.
 */
import React from 'react';
import Router from 'react-router';
import RouteCategories from './constants/RouteCategories.js';
import Components from './components/component.js';

var { DefaultRoute, Route, NotFoundRoute,Redirect } = Router;
var {App, Login, ProfileViewer, HomePage, ProductScreen, ProductsScreen, Postman, UnAuthorized, DashBoards, Admin,
  Modeller, ModellerView, Designer, Visualiser, VisualiserView, ProductManager,
  NotFoundPage, AdminStats, AllUsers, DesignerStats, ProductCatalog, Moodboard} = Components;

class Rout {

  static getDesignerRoutes() {
    return (
      <Route name='designer' path='/designer' handler={Designer}>
        <DefaultRoute handler={HomePage}/>
        <Route path='profile/:id' handler={ProfileViewer}/>
        <Route path='projects/:pid' handler={HomePage}/>
        <Route path='allusers' handler={AllUsers}/>
        <Route path='catalog' handler={ProductCatalog}/>
        <Route path='moodboard' handler={Moodboard}/>
      </Route>
    )
  }
  static getAdminRoutes() {
    return (
      <Route name='admin' path='/admin' handler={Admin}>
        <DefaultRoute handler={HomePage}/>
        <Route path='profile/:id' handler={ProfileViewer}/>
        <Route path='projects/:pid' handler={HomePage}/>
        <Route path='products' handler={ProductScreen}/>
        <Route path='productrepo' handler={ProductsScreen}/>
        <Route path='postman' handler={Postman}/>
        <Route path='stats' handler={AdminStats}/>
        <Route path='allusers' handler={AllUsers}/>
        <Route path='designerstats' handler={DesignerStats}/>
        <Route path='catalog' handler={ProductCatalog}/>
        <Route path='moodboard' handler={Moodboard}/>
      </Route>
    )
  }

  static getModellerRoutes() {
    return (
      <Route name='modeller' path='/modeller' handler={Modeller}>
        <DefaultRoute handler={ModellerView}/>
        <Route path='profile/:id' handler={ProfileViewer}/>
      </Route>
    )
  }

  static getVisualizerRoutes() {
    return (
      <Route name='visualizer' path='/visualizer' handler={Visualiser}>
        <DefaultRoute handler={VisualiserView}/>
        <Route path='projects/:pid' handler={VisualiserView}/>
        <Route path='catalog' handler={ProductCatalog}/>
        <Route path='profile/:id' handler={ProfileViewer}/>
      </Route>
    )
  }
  static getProductManagerRoutes() {
    return (
      <Route name='operations' path='/operations' handler={ProductManager}>
        <DefaultRoute handler={ProductScreen}/>
        <Route path='projects' handler={HomePage}/>
        <Route path='projects/:pid' handler={HomePage}/>
        {/*<Route path='products' handler={ProductScreen}/>*/}
        <Route path='catalog' handler={ProductCatalog}/>
        <Route path='profile/:id' handler={ProfileViewer}/>
        {/*<Redirect from='/operations' to='/operations/products'/>*/}
      </Route>
    )
  }
  static getRoutes() {
    return (
      <Route handler={App} path='/'>
        {Rout.getDesignerRoutes()/* /designer  */}
        {Rout.getModellerRoutes()/* /modeller  */}
        {Rout.getVisualizerRoutes()/* /visualizer  */}
        {Rout.getAdminRoutes()/* /admin  */}
        {Rout.getProductManagerRoutes()/* /product-manager  */}
        <Route path='login' name='login' handler={Login}/>
        <Route path='dashboard' name='dashboard' handler={DashBoards}/>
        <Route path='error' name='error' handler={UnAuthorized}/>
        {/*<Route path='allusers' name='allusers' handler={AllUsers}/>*/}
        <Redirect from='/' to='/login'/>
        <NotFoundRoute handler={NotFoundPage}/>
      </Route>
    );
  }
}

export default Rout.getRoutes;
